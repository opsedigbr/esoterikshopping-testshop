<div class="inventur2">
    Liebe Kunden!<br />
    Vom 27.01.2014 - 31.01.2014 finden bei uns Inventurarbeiten statt.<br />
    Hier erfolgt der Versand nur sehr eingeschr&auml;nkt!<br />
    In dieser Zeit kann weder ein telefonischer, noch ein Support per Email stattfinden.<br />
    Ihre Anfragen werden wir gerne ab dem 03.02.2014 wieder beantworten.<br /><br />
    Wir m&ouml;chten uns schon einmal vorab f&uuml;r Ihr Verst&auml;ndnis bedanken.<br />
</div>