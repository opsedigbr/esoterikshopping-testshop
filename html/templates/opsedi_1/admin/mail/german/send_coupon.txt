{$MESSAGE}

Sie k�nnen den Gutschein bei Ihrer Bestellung einl�sen. Geben Sie daf�r Ihren Gutschein-Nummer in das Feld Gutscheine ein.

Ihr Gutschein-Nummer lautet: {$COUPON_ID}

Heben Sie Ihre Gutschein-Nummer gut auf, nur so k�nnen Sie von diesem Angebot profitieren
wenn Sie uns das n�chste mal unter {$WEBSITE} besuchen.

/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Esoterikshopping / Spart-Bares GbR
H.Klinzmann, U. Dittrich
Humboldtstr. 134
90459 N�rnberg
Tel.   0911 - 891 79 50
Fax. 0911 - 891 79 51

Telefonisch erreichbar:
Montag - Donnerstag: 10:00 - 16:00
Freitag: 10:00 - 14:00 Uhr

email: info@esoterikshopping.de
www.esoterikshopping.de
Steuer-NR. DE 204/155/59604
Ust-ID: DE234465241

Bankverbindung:

Empf�nger: Esoterikshopping
Dresdner Bank N�rnberg
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF
IBAN: DE17 76080040 0154379901

�ber Paypal:
Paypal: paypal@spart-bares.de
 
\-------------------------------------------------/