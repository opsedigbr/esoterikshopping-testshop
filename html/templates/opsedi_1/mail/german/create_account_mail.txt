

Hallo {$VORNAME} {$NACHNAME},

Sie haben soeben Ihr Kundenkonto erfolgreich erstellt.
Um sich zuk�nftig im Shop anzumelden geben Sie bitte folgende Daten ein:
Benutzer: {$EMAIL}
Passwort: wie von Ihnen vergeben


Als registrierter Kunde haben sie folgende Vorteile in unserem Shop.

-Kundenwarenkorb - Jeder Artikel bleibt registriert bis Sie zur Kasse gehen, oder die Produkte aus dem Warenkorb entfernen.
-Adressbuch - Wir k�nnen jetzt die Produkte zu der von Ihnen ausgesuchten Adresse senden. Der perfekte Weg ein Geburtstagsgeschenk zu versenden.
-Vorherige Bestellungen - Sie k�nnen jederzeit Ihre vorherigen Bestellungen �berpr�fen.
-Meinungen �ber Produkte - Teilen Sie Ihre Meinung zu unseren Produkten mit anderen Kunden.      

Falls Sie Fragen zu unserem Kunden-Service haben, wenden Sie sich bitte an: {$content.MAIL_REPLY_ADDRESS}


Achtung: Diese eMail-Adresse wurde uns von einem Kunden bekannt gegeben. Falls Sie sich nicht angemeldet haben, senden Sie bitte eine eMail an: {$content.MAIL_REPLY_ADDRESS}
    
{if $SEND_GIFT==true}
Als kleines Willkommensgeschenk senden wir Ihnen einen Gutschein �ber:	{$GIFT_AMMOUNT}

Ihr pers�nlicher Gutscheincode lautet {$GIFT_CODE}. 
Sie k�nnen diese Gutschrift an der Kasse w�hrend des Bestellvorganges verbuchen.

Um den Gutschein einzul�sen verwenden Sie bitte den folgenden link {$GIFT_LINK}.
{/if}

{if $SEND_COUPON==true}
 Als kleines Willkommensgeschenk senden wir Ihnen einen Kupon.
 Kuponbeschreibung: {$COUPON_DESC}
 
Geben Sie einfach Ihren pers�nlichen Code {$COUPON_CODE} w�hrend des Bezahlvorganges ein

{/if}

/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich 
Humboldtstr. 134
90459 N�rnberg
Umsatzsteuer-Identifikationsnummer: DE 234465241

www.esoterikshopping.de
E-mail: bestellung@esoterikshopping.de
Tel.: 08404 - 939 264
Fax: 08404 - 939 265

Bankverbindung
Empf�nger: Esoterikshopping 
Dresdner Bank N�rnberg 
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF 
IBAN: DE17 76080040 0154379901 
�ber Paypal: 
paypal@spart-bares.de

Telefonisch erreichbar:
Montag - Donnerstag: 09.00 - 12.00 und 15.00 - 18.00
Freitag: 09.00 - 12.00

\-------------------------------------------------/