----------------------------------------------------------------------------------------
  Herzlichen Gl�ckwunsch, Sie haben einen Gutschein �ber {$AMMOUNT} erhalten !
----------------------------------------------------------------------------------------

Dieser Gutschein wurde Ihnen �bermittelt von {$FROM_NAME},
Mit der Nachricht:

{$MESSAGE}

Ihr pers�nlicher Gutscheincode lautet {$GIFT_CODE}. Sie k�nnen diese Gutschrift entweder w�hrend dem Bestellvorgang verbuchen.

Um den Gutschein einzul�sen klicken Sie bitte auf {$GIFT_LINK}

Falls es mit dem obigen Link Probleme beim Einl�sen kommen sollte,
k�nnen Sie den Betrag w�hrend des Bestellvorganges verbuchen.


/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich 
Humboldtstr. 134
90459 N�rnberg
Umsatzsteuer-Identifikationsnummer: DE 234465241

www.esoterikshopping.de
E-mail: bestellung@esoterikshopping.de
Tel.: 08404 - 939 264
Fax: 08404 - 939 265

Bankverbindung
Empf�nger: Esoterikshopping 
Dresdner Bank N�rnberg 
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF 
IBAN: DE17 76080040 0154379901 
�ber Paypal: 
paypal@spart-bares.de

Telefonisch erreichbar:
Montag - Donnerstag: 09.00 - 12.00 und 15.00 - 18.00
Freitag: 09.00 - 12.00

\-------------------------------------------------/