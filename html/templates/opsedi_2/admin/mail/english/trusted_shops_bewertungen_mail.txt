{#gender#} {#firstname#} {#lastname#},

vielen Dank für Ihre Bestellung in unserem Onlineshop.

Wir hoffen, dass Sie mit der Lieferung und den Produkten zufrieden sind.
Da viele Kunden Wert auf Erfahrungsberichte anderer Kunden legen, würden wir uns sehr freuen, wenn auch Sie eine Bewertung für uns abgeben würden.

Für die Bewertung benötigen Sie nur Ihre Bestellnummer - diese lautet: {#oId#}

Wenn Sie eine Bewertung abgeben möchten, müssten Sie bitten diesen Link in Ihren Browser kopieren:
https://www.trustedshops.com/bewertung/bewerten_XECB830E0AE0C3827CC8EC3B083D488FC.html

Als kleines Dankeschön für Ihre Bewertung erhalten Sie einen Gutschein in Höhe von 5,- Euro auf Ihre nächste Bestellung.
Dieser wird Ihnen umgehend nach Erhalt der Bewertung per Email zugeschickt.

Bei Fragen stehe ich Ihnen jederzeit gerne zur Verfügung.

Mit freundlichen Grüßen
Wendi Safr

-------------------------------------------------------------------------------
Auslieferungslager Spart Bares GbR
Esoterikshopping.de
Am Bach 9
93349 Mindelstetten
Deutschland
-------------------------------------------------------------------------------
Tel.: 08404 / 939264
Fax: 08404 / 939265
Shop: http://www.esoterikshopping.de
e-mail: info@esoterikshopping.de
-------------------------------------------------------------------------------