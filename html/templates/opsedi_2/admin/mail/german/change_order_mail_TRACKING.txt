

Bestelldatum: {$ORDER_DATE}
{if $KUNDEN_NR}Kundennummer: {$KUNDEN_NR}{/if}
Bestell-Nr:   {$ORDER_NR}




Hallo {$NAME}, 

Der Status Ihrer Bestellung {if $ORDER_NR}Nr. {$ORDER_NR}{/if} {if $ORDER_DATE}vom {$ORDER_DATE}{/if} wurde ge�ndert.

{if $ORDER_LINK}Link zur Bestellung: hier klicken{/if}

{if $NOTIFY_COMMENTS}
Anmerkungen und Kommentare zu Ihrer Bestellung: {$NOTIFY_COMMENTS} 
{/if} 
Neuer Status: {$ORDER_STATUS}

{if $ORDER_STATUS == 'Versendet'}

{if $DHL_TRACKING_ID != '0'}
Wir haben das Paket mit DHL verschickt. Das Paket hat die Tracking-Nummer {$DHL_TRACKING_ID}.

Sie k�nnen den Sendungsstatus �ber folgende Internetseite verfolgen:
http://nolp.dhl.de/nextt-online-public/set_identcodes.do?idc={$DHL_TRACKING_ID}

Hinweis: Sollte der Link nicht funktionieren, kopieren Sie die komplette Adresse ohne Zeilenumbr�che in die Adresszeile Ihres Browsers und dr�cken Sie anschlie�end "Enter". {/if} {if $GLS_TRACKING_ID != '0'}
Wir haben das Paket mit GLS verschickt. Das Paket hat die Auftragsnummer {$GLS_TRACKING_ID}.

Sie k�nnen den Sendungsstatus �ber folgende Internetseite verfolgen unter Angabe der genanten Auftragsnummer:
http://www.gls-group.eu/276-I-PORTAL-WEB/content/GLS/DE03/DE/5004.htm?txtRefNo={$GLS_TRACKING_ID}&txtAction=71000

Hinweis: Sollte der Link nicht funktionieren, kopieren Sie die komplette Adresse ohne Zeilenumbr�che in die Adresszeile Ihres Browsers und dr�cken Sie anschlie�end "Enter". {/if} 

{$GLS_TRACKING_ID} Bei Fragen zu Ihrer Bestellung antworten Sie bitte auf diese E-Mail.{/if} 

/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich
Humboldtstr. 134
90459 N�rnberg
Tel. 08404 - 939 264
Fax. 08404 - 939 265

Telefonisch erreichbar:
Montag - Donnerstag: 10:00 - 16:00

email: info@esoterikshopping.de
www.esoterikshopping.de
Ust-ID: DE234465241

Bankverbindung:

Empf�nger: Esoterikshopping
Dresdner Bank N�rnberg
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF
IBAN: DE17 76080040 0154379901

�ber Paypal:
Paypal: paypal@spart-bares.de
 
\-------------------------------------------------/