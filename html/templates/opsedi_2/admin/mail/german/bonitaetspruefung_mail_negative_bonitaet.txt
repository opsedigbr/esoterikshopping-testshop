{#gender#} {#firstname#} {#lastname#},

vielen Dank für Ihre Bestellung in unserem Onlineshop.

Leider ist es Ihnen, aufgrund einer negativen Bonitätsprüfung, nicht möglich bei uns "auf Rechnung" zu bestellen.
Bitte überweisen Sie den Betrag per Vorkasse.

Unsere Bankverbindung:
Bankinstitut: Commerzbank Nürnberg
Empfänger: Esoterikshopping GbR
BLZ: 76080040
Kontonummer: 154379901
Verwendungszweck: Bestellnummer {#oId#}
Betrag: {#summe#}

Sie haben natürlich auch die Möglichkeit den Betrag per PayPal zu überweisen.
Empfängerkonto: paypal@spart-bares.de

Sobald wir Ihren Zahlungseingang registriert haben, werden Ihre Bestellung umgehend versenden.
Bei weiteren Fragen stehe ich Ihnen jederzeit gerne zur Verfügung.

Mit freundlichen Grüßen
Wendi Safr

-------------------------------------------------------------------------------
Auslieferungslager Spart Bares GbR
Esoterikshopping.de
Am Bach 9
93349 Mindelstetten
Deutschland
-------------------------------------------------------------------------------
Tel.: 08404 / 939264
Fax: 08404 / 939265
Shop: http://www.esoterikshopping.de
e-mail: info@esoterikshopping.de
-------------------------------------------------------------------------------