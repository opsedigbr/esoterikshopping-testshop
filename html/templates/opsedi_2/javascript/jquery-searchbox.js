//jQuery-Autocomplete by TechWay.de

//MODIFICATIONS:
//2011-04-19: NEW LINK DEFINITION by web28 www.rpa-com.de
//2011-04-19: products_short_description added by TechWay
//2011-04-20: some changes by web28 www.rpa-com.de
//2011-04-21: noimage.gif if no products_image, cleaned short_description (no html-tags), products_price added by TechWay
//2011-04-22: check for empty and undefined entries  by web28 www.rpa-com.de

function html_entity_decode(str) {
  var ta=document.createElement("textarea");
  ta.innerHTML=str.replace(/</g,"&lt;").replace(/>/g,"&gt;");
  return ta.value;
}

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
        var self = this,
            currentCategory = "";
        $.each( items, function( index, item ) {
            if ( item.category != currentCategory ) {
                item.category = html_entity_decode(item.category);
                ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                currentCategory = item.category;
            }
            item.label = html_entity_decode(item.label);
            item.desc = html_entity_decode(item.desc);
            self._renderItem( ul, item );
        });
    }
});

$(function() {
    $("#searchbox").catcomplete({
        source: function( request, response ) {
            request.term = escape(request.term);
            var lastXhr = $.getJSON( "autocomplete.php", request, function( data, status, xhr ) {
            response( data );
        });
        },
        minLength: 2,
        delay: 200,
		//N�heres zu Position siehe: http://docs.jquery.com/UI/Position
		position: {
			//my: "left top",
			my: "center top",
			//at: "left bottom",
			at: "center bottom",
			collision: "flip horizontal"
		},
        highlight: true,
        select: function(event, ui) {
          
          //BOF - web28 - 2011-04-19 - NEW link and session handling
          var sid = $.getUrlVar('XTCsid');
          if (sid != null) {
            var separator = "?";
            if (ui.item.link.indexOf("&") != -1) {
              separator = "&";
            }
            sid = separator + "XTCsid" + "=" + sid ;
          } else {
            sid = "";
          }

          location.href = ui.item.link + sid;
          //EOF - web28 - 2011-04-19 - NEW link and session handling

          return false;
        }
    })
       .data( "catcomplete" )._renderItem = function( ul, item ) {
            //BOF - web28 - 2011-04-22 - NEW check for empty and undefined entries
            var desc = "";
            if (item.desc != null && item.desc != "") {
              desc = "<div class='short_desc'>" + item.desc + "</div>";
            }
            var image = "";
            if (item.image != null && item.image != "") {
              image = "<div class='prod_img'><img src='" + item.image + "' /></div>";
            }
            var price = "";
            if (item.price != null && item.price != "") {
              price = "<div class='prod_preis'>" + item.price + "</div>";
            }
            var label = "";
            if (item.label != null && item.label != "") {
              label = "<div class='prod_name'>" + item.label + "</div>";
            }
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a><div class='prod_komplett'>" + image + label + desc + price + "</div></a>" )
                .appendTo( ul );
            //EOF - web28 - 2011-04-22 - NEW check for empty and undefined entries
        };

});