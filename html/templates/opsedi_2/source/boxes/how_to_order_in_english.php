<?php

/* -----------------------------------------------------------------------------------------
   $Id: information.php 1302 2005-10-12 16:21:29Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(information.php,v 1.6 2003/02/10); www.oscommerce.com
   (c) 2003	 nextcommerce (information.php,v 1.8 2003/08/21); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

$box_smarty->assign('language', $_SESSION['language']);

$content_string .= '
If you don&acute;t speak German<br />
and you like to order,<br/>
please <a href="http://www.esoterikshopping.de/content/Kontakt-zu-uns.html" target="_self" title="Please contact us, if you dont speak German and you like to order"><strong>&gt;&gt; CONTACT US &gt;&gt;</strong></a><br/>
by mail!';
				  
if ($content_string != '')
	$box_smarty->assign('BOX_CONTENT', $content_string);				  

if (!$cache) {
	$box_how_to_order_in_english = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_how_to_order_in_english.html');
} else {
	$box_how_to_order_in_english = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_how_to_order_in_english.html', $cache_id);
}


$smarty->assign('box_HOW_TO_ORDER_IN_ENGLISH', $box_how_to_order_in_english);
?>