<?php

/* -----------------------------------------------------------------------------------------
   $Id: information.php 1302 2005-10-12 16:21:29Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(information.php,v 1.6 2003/02/10); www.oscommerce.com
   (c) 2003	 nextcommerce (information.php,v 1.8 2003/08/21); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

$box_smarty->assign('language', $_SESSION['language']);

$content_string .= '<a href="http://www.house-of-fantasy.de/" target="_blank" title="Fabelwesen und Sagengestalten in der Fantasy Welt">Fabelwesen - Fantasy</a><br>
                    <a href="http://www.engel-orakel.de/" target="_blank" title="Engel Online Orakel">Engel Online Orakel</a><br />
				        <a href="http://www.lenormand-wahrsagekarten.de/" target="_blank" title="Lenormandkarten online">Lenormandkarten online</a><br />
                    <a href="http://www.kipper-karten.de/" target="_blank" title="Kipperkarten">Kipperkarten</a><br />
                    <a href="http://www.zigeuner-karten.com/" target="_blank" title="Zigeunerkarten">Zigeunerkarten</a><br />
                    <a href="http://www.tarot-decks.de/" target="_blank" title="Tarot-Decks">Tarot-Decks</a><br />
                    <a href="http://www.waldfee.net/" target="_blank" title="Kartenkombinationen">Kartenkombinationen</a><br />
                    <a href="http://www.heiledelstein.de/" target="_blank" title="Heilsteinlexikon">Heilsteinlexikon</a><br />
                    <a href="http://www.edelsteinwasser-herstellen.de/" target="_blank" title="Edelsteinwasser">Edelsteinwasser</a><br />
                    <a href="http://www.moqui-marbles.com/" target="_blank" title="Moqui-Marbles">Moqui-Marbles</a><br />
                    <a href="/shop_content.php?coID=13" target="_self" title="Kartenlegen kostenlos">Kartenlegen kostenlos</a><br />
                  ';
				  
if ($content_string != '')
	$box_smarty->assign('BOX_CONTENT', $content_string);				  

if (!$cache) {
	$box_hilfreiche_links = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_hilfreiche_links.html');
} else {
	$box_hilfreiche_links = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_hilfreiche_links.html', $cache_id);
}


$smarty->assign('box_HILFREICHE_LINKS', $box_hilfreiche_links);
?>