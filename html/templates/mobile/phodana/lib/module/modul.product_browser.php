<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the GNU General Public License (GPLv3).
 * It is available through the world-wide-web at this URL: http://www.opensource.org/licenses/gpl-3.0.html.
 *
 *
 * @category    modules
 * @package     fws2
 * @version     1.0
 * @copyright   Copyright (c) 2011 squidio.de (http://www.squidio.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

// create object
$box_smarty = new smarty;
$box_content = '';

// assign useful
$box_smarty->assign('language', $_SESSION['language']);
$box_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

// max show (if not configured else 3items)
if (isset($zeige_maximale_anzahl_neue_produkte)){
	$zeige_maximal = $zeige_maximale_anzahl_neue_produkte;
}else{
	$zeige_maximal = '8';
};

// fsk18 lock
$fsk_lock = '';
if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
	$fsk_lock = ' and p.products_fsk18!=1';
}

// check group permissions
if (GROUP_CHECK == 'true') {
	$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
}

/* ******************************************************************* "TOP PRODUKTE" */
// wieviele top produkte (wenn nicht konfiguriert dann 12)
if (isset($product_browser_qty)){
	$zeige_maximal = $product_browser_qty;
}else{
	$zeige_maximal = '8';
};
// top produkte produkte
$newproducts_listing_query = "SELECT * FROM
				".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd WHERE
				p.products_id=pd.products_id and
				p.products_startpage = '1'
				".$group_check."
				".$fsk_lock."
				and p.products_status = '1' and pd.language_id = '".(int) $_SESSION['languages_id']."'
				order by p.products_startpage_sort 
				asc limit ".$zeige_maximal;	
$newproducts_listing_query = xtDBquery($newproducts_listing_query);

if (xtc_db_num_rows($newproducts_listing_query, true) >= 1) {
	$box_smarty->assign('top_products_available', true);
}

// collect items in array
$top_products = array ();
while ($newproducts_listing = xtc_db_fetch_array($newproducts_listing_query, true)) {
	$top_products[] = $product->buildDataArray($newproducts_listing);
}
// assign contents
$box_smarty->assign('top_products', $top_products);



/* ******************************************************************* "ANGEBOTS PRODUKTE" */
// order items from database
$specials_listing_query = "select distinct
				p.products_id,
				pd.products_name,
				pd.products_short_description,
				p.products_price,
				p.products_tax_class_id,
				p.products_image,
				s.expires_date,
				p.products_vpe,
				p.products_vpe_status,
				p.products_vpe_value,
				s.specials_new_products_price
				from ".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd,
				".TABLE_SPECIALS." s where p.products_status = '1'
				and p.products_id = s.products_id
				and pd.products_id = s.products_id
				and pd.language_id = '".$_SESSION['languages_id']."'
				and s.status = '1'
				".$group_check."
				".$fsk_lock."                                             
				order by s.specials_date_added
				desc limit ".$zeige_maximal;	
$specials_listing_query = xtDBquery($specials_listing_query);

if (xtc_db_num_rows($specials_listing_query, true) >= 1) {
	$box_smarty->assign('special_products_available', true);
}

// collect items in array
$special_products = array ();
while ($specials_listing = xtc_db_fetch_array($specials_listing_query, true)) {
	$special_products[] = $product->buildDataArray($specials_listing);
}
// assign contents
$box_smarty->assign('special_products', $special_products);


/* ******************************************************************* "BESTSELLER PRODUKTE" */
require_once (DIR_FS_INC.'xtc_row_number_format.inc.php');
if (isset ($current_category_id) && ($current_category_id > 0)) {
	$best_sellers_query = "select distinct
				p.products_id,
				p.products_price,
				p.products_tax_class_id,
				p.products_image,
				p.products_vpe,
				p.products_vpe_status,
				p.products_vpe_value,
				pd.products_short_description,
				pd.products_name from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd, ".TABLE_PRODUCTS_TO_CATEGORIES." p2c, ".TABLE_CATEGORIES." c
				where p.products_status = '1'
				and c.categories_status = '1'
				and p.products_ordered > 0
				and p.products_id = pd.products_id
				and pd.language_id = '".(int) $_SESSION['languages_id']."'
				and p.products_id = p2c.products_id
				".$group_check."
				".$fsk_lock."
				and p2c.categories_id = c.categories_id and '".$current_category_id."'
				in (c.categories_id, c.parent_id)
				order by p.products_ordered 
				desc limit ".$zeige_maximal;	
} else {
	$best_sellers_query = "select distinct
				p.products_id,
				p.products_image,
				p.products_price,
				p.products_vpe,
				p.products_vpe_status,
				p.products_vpe_value,
				p.products_tax_class_id,
				pd.products_short_description,
				pd.products_name from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
				where p.products_status = '1'
				".$group_check."
				and p.products_ordered > 0
				and p.products_id = pd.products_id ".$fsk_lock."
				and pd.language_id = '".(int) $_SESSION['languages_id']."'
				order by p.products_ordered 
				desc limit ".$zeige_maximal;	
}
$best_sellers_query = xtDBquery($best_sellers_query);

if (xtc_db_num_rows($best_sellers_query, true) >= 1) {
	$box_smarty->assign('bestseller_products_available', true);
}

if (xtc_db_num_rows($best_sellers_query, true) >= MIN_DISPLAY_BESTSELLERS) {
	$rows = 0;
	$bestseller_products = array ();
	while ($best_sellers = xtc_db_fetch_array($best_sellers_query, true)) {
		$rows ++;
		$image = '';
		
		$best_sellers = array_merge($best_sellers, array ('ID' => xtc_row_number_format($rows)));
		$bestseller_products[] = $product->buildDataArray($best_sellers);
		
	}

	$box_smarty->assign('bestseller_products', $bestseller_products);
}

// set cache ID
if (!$cache) {
	if ($special_products!='') {
		$product_browser = $box_smarty->fetch(CURRENT_TEMPLATE.'/module/product_browser.html');
	}
} else {
	$product_browser = $box_smarty->fetch(CURRENT_TEMPLATE.'/module/product_browser.html', $cache_id);
}

// display

$smarty->assign('PRODUCT_BROWSER', $product_browser);

?>
