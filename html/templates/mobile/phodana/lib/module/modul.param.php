<?php 
/* -----------------------------------------------------------------------------------------
   MobileST Template
   http://www.phodana.de

   Copyright (c) 2013 Phodana media
   ---------------------------------------------------------------------------------------*/
   
  function rm_url_param($param_rm, $query='') {
    empty($query)? $query=$_SERVER['QUERY_STRING'] : '';
    parse_str($query, $params);
    unset($params[$param_rm]);
    $newquery = '';
    foreach($params as $k => $v)
        { $newquery .= '&'.$k.'='.$v; }
    return substr($newquery,1);
  }
?>
