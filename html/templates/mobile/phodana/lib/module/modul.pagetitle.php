 <?php 
/* -----------------------------------------------------------------------------------------
   MobileST Template
   http://www.phodana.de

   Copyright (c) 2013 Phodana media
   ---------------------------------------------------------------------------------------*/
   
  $breadcrumbTitle = 	end($breadcrumb->_trail); // <-- BugFix
	$breadcrumbTitle = 	$breadcrumbTitle['title'];

  if (strstr($PHP_SELF, FILENAME_PRODUCT_INFO)) {

    if ($product->isProduct()) {
      $meta_title =  $product->data['products_meta_title'].' '.$product->data['products_name']; 
    } else {
      $meta_title = TITLE; 
    }

  } else {
    if ($_GET['cPath']) {
      if (strpos($_GET['cPath'], '_') == '1') {
        $arr = explode('_', xtc_input_validation($_GET['cPath'], 'cPath', ''));
        $_cPath = $arr[1];
      } else {
        if (isset ($_GET['cat'])) {
          $site = explode('_', $_GET['cat']);
          $cID = $site[0];
          $_cPath = str_replace('c', '', $cID);
        }
      }
      $categories_meta_query = xtDBquery("SELECT categories_meta_keywords,
                                                  categories_meta_description,
                                                  categories_meta_title,
                                                  categories_name
                                                  FROM " . TABLE_CATEGORIES_DESCRIPTION . "
                                                  WHERE categories_id='" . $_cPath . "' and
                                                  language_id='" . $_SESSION['languages_id'] . "'");
      $categories_meta = xtc_db_fetch_array($categories_meta_query, true);
      if ($categories_meta['categories_meta_keywords'] == '') {
        $categories_meta['categories_meta_keywords'] = META_KEYWORDS;
      }
      if ($categories_meta['categories_meta_description'] == '') {
        $categories_meta['categories_meta_description'] = META_DESCRIPTION;
      }
      if ($categories_meta['categories_meta_title'] == '') {
        $categories_meta['categories_meta_title'] = $categories_meta['categories_name'];
      }
      
      $meta_title = $categories_meta['categories_meta_title']; 

    } else {
      if ($_GET['coID']) {
        $contents_meta_query = xtDBquery("SELECT content_heading
                                                    FROM " . TABLE_CONTENT_MANAGER . "
                                                    WHERE content_group='" . $_GET['coID'] . "' and
                                                    languages_id='" . $_SESSION['languages_id'] . "'");
        $contents_meta = xtc_db_fetch_array($contents_meta_query, true);

        $meta_title = $contents_meta['content_heading']; 

      } else {
        $meta_title = $breadcrumbTitle; 
      }
    }
  }
  
  $xmtitle = str_replace(" - " . ML_TITLE, "", $meta_title);
  $xmumlaute = Array("/&auml;/","/&ouml;/","/&uuml;/","/&Auml;/","/&Ouml;/","/&Uuml;/","/�/", "/ - /", "/ /");
  $xmreplace = Array("ae","oe","ue","Ae","Oe","Ue","ss", "_", "_");
  $xmtitle_id = preg_replace($xmumlaute, $xmreplace, $xmtitle);
  $_SESSION["metatitleid"] = $xmtitle_id;
  $_SESSION["metatitle"] = $xmtitle;

?>