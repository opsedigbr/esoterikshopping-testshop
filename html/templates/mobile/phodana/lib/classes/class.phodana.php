<?php
/* -----------------------------------------------------------------------------------------
   MobileST Template
   http://www.phodana.de

   Copyright (c) 2013 Phodana media
   ---------------------------------------------------------------------------------------*/

class phodana extends Smarty{
	
	/**
	 * class constructor
	 *
	 */
	function phodana($page = '', $category_depth = '', $current_template = ''){
		$this->filename = $this->_getPageTypeFromFilename($page);
		$this->category_depth = $category_depth;
		$this->current_template = $current_template;
	}
	
	/**
	 * Add Template Specific Smarty Functions/Modifier Folder/s
	 *
	 */
	function _addPhodanaPluginFolders($plugin_folders){
		global $smarty; 
		
		if (!is_array($plugin_folders)) $plugin_folders = explode(",", $plugin_folders);
		$path_pattern = 'templates/%s/phodana/lib/%s/';
		
		foreach ($plugin_folders as $template_plugins){
			$folders[] = sprintf($path_pattern, $this->current_template, trim($template_plugins));
		}
		
		$result = array_merge($folders, $smarty->plugins_dir);
		$smarty->plugins_dir = $result;
	}
	
	/**
	 * Set Page Type from Filename
	 *
	 */
	function _getPageTypeFromFilename($filename){
		$filename = basename(preg_replace('/.php/', '', $filename));
		return $filename;
	}
	
	/**
	 * minimum php-version ermitteln
	 *
	 */
	function phpMinV($v){$phpV = PHP_VERSION;if ($phpV[0] >= $v[0]) {if (empty($v[2]) || $v[2] == '*') {return true;} elseif ($phpV[2] >= $v[2]) {if (empty($v[4]) || $v[4] == '*' || $phpV[4] >= $v[4]) {return true;}}}return false;}

	/**
	 * suchwort der produktsuche
	 *
	 */
	function _getSearchKeyword(){
		if (isset($_GET['keywords'])){
			return $_GET['keywords'];
		}else{
			return false;
		}
	}
	
	/**
	 * Set Content-ID
	 *
	 */
	function _setCheckoutArea(){
		if (strstr($this->filename, 'checkout') OR $this->filename == 'shopping_cart'  OR ($this->filename == 'login' && (strstr($_SERVER["HTTP_REFERER"], 'shopping_cart')))){
			return 'true';
		}else{
			return 'false';
		}
	}
	
	/**
	 * Set Page-Type
	 *
	 */
	function _getPageType(){
		$filename = $this->filename;

		$category_depth = $this->category_depth;
		
		if ($filename == 'index' && $category_depth == 'products'){
			$page = 'product_listing';
		}elseif ($filename == 'index' && $category_depth == 'nested'){
			$page = 'categorie_listing';
		}elseif ($filename == 'index' && $category_depth == 'top'){
			if ($_GET['manufacturers_id']) {
				$page = 'manufacturers_product_listing';
			}else{
				$page = 'index';
			}
		}elseif ($filename == 'shop_content' && $_GET['coID'] != ''){
			$page = 'shop_content';
		}elseif (strstr($filename, 'checkout')){
			$page = 'checkout';
		}else{
			$page = $filename;
		}
		
		return $page;
	}
	
	/**
	 * Get SEO-Title
	 *
	 */
	function _getSeoTitle($page){
		$filename = $page;

		if ($filename == 'product_listing' || $filename == 'categorie_listing'){
			// kategorie
			$categories_meta_query = xtDBquery("SELECT categories_name FROM " . TABLE_CATEGORIES_DESCRIPTION . " WHERE categories_id='".$_GET['cPath']."' and  language_id='" . $_SESSION['languages_id'] . "'");
			$categories_meta = xtc_db_fetch_array($categories_meta_query, true);
			$page_title = $categories_meta['categories_name'];
		}elseif ($filename == 'product_info'){
			// produkt detail seite
			$page_title = $product->data['products_name'].' | '.$product->data['products_short_description'];
		}elseif ($filename == 'advanced_search_result'){
			// suchseite
			$page_title =  $product->data['products_name'].' | '.$_GET['keywords'];
		}elseif ($filename == 'shop_content'){
			// cms (contant manager) elemente
			$contents_meta_query = xtDBquery("SELECT content_heading FROM ".TABLE_CONTENT_MANAGER." WHERE content_group='".$_GET['coID']."' and languages_id='".$_SESSION['languages_id']."'");
			$contents_meta = xtc_db_fetch_array($contents_meta_query, true);
			$page_title = $contents_meta['content_heading'];
		}else{
		// alle anderen seiten
			$page_title = META_DESCRIPTION;
		}
		return $page_title;
	}
	
}

?>