<?php
/* -----------------------------------------------------------------------------------------
   MobileST Template
   http://www.phodana.de

   Copyright (c) 2013 Phodana media
   ---------------------------------------------------------------------------------------*/

// get template-feature class
require_once ('templates/'.CURRENT_TEMPLATE.'/phodana/lib/classes/class.phodana.php');

// create feature object
$phodana = new phodana($_SERVER['SCRIPT_NAME'], $category_depth, CURRENT_TEMPLATE);

// set page types
$page = $phodana->_getPageType();
$phodana->_addPhodanaPluginFolders('plugins');

// load modules
include('templates/'.CURRENT_TEMPLATE.'/phodana/lib/module/modul.pagetitle.php');
include('templates/'.CURRENT_TEMPLATE.'/phodana/lib/module/modul.param.php');
if ($page == 'index') include('templates/'.CURRENT_TEMPLATE.'/phodana/lib/module/modul.product_browser.php');

// assign smarties
$smarty->assign('page', $page);
$smarty->assign('checkout_area', $phodana->_setCheckoutArea());
$smarty->assign('page_title', $xmtitle);
$smarty->assign('page_param', rm_url_param('tpl'));

// load language snippets
include_once('templates/'.CURRENT_TEMPLATE.'/phodana/lang/'.$_SESSION['language'].'/sprachdatei.'.$_SESSION['language'].'.php');

// write log
if (!is_dir('templates/'.CURRENT_TEMPLATE.'/phodana/log/')) {
    mkdir('templates/'.CURRENT_TEMPLATE.'/phodana/log/');
}
$ourFileName = 'templates/'.CURRENT_TEMPLATE.'/phodana/log/'.$_SERVER['SERVER_NAME'].'.txt';
if (file_exists($ourFileName)) {
  define("FILEOK", true);
} else {
  $ourFileHandle = fopen($ourFileName, 'w');
  $stringData = "ok\n";
  fwrite($ourFileHandle, $stringData);
  fclose($ourFileHandle);
  define("FILEOK", false);
}

?>
