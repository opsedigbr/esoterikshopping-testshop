<?php
/* ---------------------------------------------------------------------------------------
	$Id: sprachdatei.{$language}.php $   
 ---------------------------------------------------------------------------------------*/

define('text_intro','<strong>Willkommen.</strong> Unser Shop-Sortiment k&ouml;nnen Sie nun auch in optimierter Darstellung f�rs Smartphone oder Tablet durchst&ouml;bern. Viel Vergn&uuml;gen...');

define('text_search','Suche');
define('text_advanced_search','Erweiterte Suche');
define('text_search_keyword','Geben Sie einen Suchbegriff ein.');
define('text_catalog','Katalog');
define('text_more','Mehr');
define('text_cart','Warenkorb');
define('text_login','Anmelden');
define('text_logoff','Abmelden');
define('text_account','Ihr Konto');

define('text_back','Zur&uuml;ck');
define('text_newproducts','Neue Artikel');
define('text_specials','Sonderangebote');
define('text_bestseller','Bestseller');
define('text_pageview','Zur klassischen Website');
define('text_settings','Shopeinstellungen');
define('text_other','Sonstiges');
define('text_mobilest','&Uuml;ber MobileST');
define('text_fast_contact','Service Schnellkontakt');
define('text_fast_contact_tel','<span style="font-weight: normal;">Anfrage per Telefon:</span>');
define('text_fast_contact_mail','<span style="font-weight: normal;">Anfrage per Mail:</span>');
define('text_vat','Alle Preise inkl. MwSt. zzgl. Versand');
define('text_notice','Impressum');
define('text_conditions','AGB');
define('text_privacy','Datenschutz');

define('text_wishlist','Merkzettel');
define('text_step','Schritt');
define('text_articles','Artikel');
define('text_btn_search','Suchen');

define('text_pi_price','Preis:');
define('text_pi_unit','VPE:');
define('text_pi_fsk','FSK:');
define('text_pi_url','URL:');
define('text_pi_available','Verf&uuml;gbar am:');
define('text_pi_in_cart','In den Warenkorb');

define('text_continue','Weiter');
define('text_mr','Herr');
define('text_mrs','Frau');
define('text_no_opinions','<p>Bisher wurden noch keine Kundenmeinungen abgegeben!</p>');

define('text_shipping','Versand');
define('text_payment','Zahlung');
define('text_confirm','Kontrolle');
define('text_success','Fertig');

define('text_description', 'Details');
define('text_cross_selling', 'Empfehlung');
define('text_also_purchased', 'Kunden-Tipp');
define('text_products_reviews', 'Meinungen');
define('text_products_media', 'Downloads');
define('text_more_images', 'Mehr Bilder');

define('text_close', 'Schlie&szlig;en');

define('check_data_checkout_confirmation', 'Bitte pr&uuml;fen Sie Ihre Angaben:');

define('payment_information_caption_checkout_confirmation', 'Zahlungsinformationen:');
define('payment_information_content_checkout_confirmation', 'Alle f&uuml;r den Kaufabschluss notwendigen Daten erhalten Sie per E-Mail nach dem Kauf zugesendet.');

define('text_shippingtime', 'Lieferzeit: ');

?>