<?php
/* ---------------------------------------------------------------------------------------
	$Id: sprachdatei.{$language}.php 0001 2010-02-30 $   
 ---------------------------------------------------------------------------------------*/

define('text_intro','<strong>Welcome.</strong> Our products now can be browsed in optimized view for smartphones and tablet pcs. Happy shopping...');

define('text_search','Search');
define('text_advanced_search','Advanced Search');
define('text_search_keyword','Enter a keyword for your search.');
define('text_catalog','Catalog');
define('text_more','More');
define('text_cart','Shopping Cart');
define('text_login','Login');
define('text_logoff','Logoff');
define('text_account','Account');

define('text_back','Back');
define('text_newproducts','New Products');
define('text_specials','Specials');
define('text_bestseller','Bestseller');
define('text_pageview','Back to classic website');
define('text_settings','Shop-Settings');
define('text_other','Other');
define('text_mobilest','About MobileST');
define('text_fast_contact','Service Quickcontact');
define('text_fast_contact_tel','<span style="font-weight: normal;">Phone contact:</span>');
define('text_fast_contact_mail','<span style="font-weight: normal;">Mail contact:</span>');
define('text_vat','All prices incl. VAT plus shipping');
define('text_notice','Site notice');
define('text_conditions','Conditions of use');
define('text_privacy','Privacy notice');

define('text_wishlist','Wishlist');
define('text_step','Step');
define('text_articles','Products');
define('text_btn_search','Search');

define('text_pi_price','Price:');
define('text_pi_unit','Unit:');
define('text_pi_fsk','FSK:');
define('text_pi_url','URL:');
define('text_pi_available','Available on:');
define('text_pi_in_cart','Add to shopping cart');

define('text_continue','Continue');
define('text_mr','Mr.');
define('text_mrs','Ms./Mrs.');
define('text_no_opinions','<p>There are no customer opinions yet!</p>');

define('text_shipping','Shipping');
define('text_payment','Payment');
define('text_confirm','Confirm');
define('text_success','Done');

define('text_description', 'Details');
define('text_cross_selling', 'Recommendation');
define('text_also_purchased', 'Customer-tip');
define('text_products_reviews', 'Reviews');
define('text_products_media', 'Downloads');
define('text_more_images', 'More images');

define('text_close', 'Close');

define('check_data_checkout_confirmation', 'Please check your data:');

define('payment_information_caption_checkout_confirmation', 'Payment Information:');
define('payment_information_content_checkout_confirmation', 'You will receive all required data via email.');

define('text_shippingtime', 'Shipping time: ');
?>