<?php
/* --------------------------------------------------------------------
	GUNNART "SHOW_CATEGORY ADVANCED"
	
	erweiterte Kategorien-Navigation f�r xt:Commerce 3.04 SP1 / SP2.1

 	Proudly togetherfummeled by Gunnar Tillmann
 	http://www.gunnart.de
 	Version 0.3 Beta / 12. Juli 2007 
 	- Bugfix 1, Danke f�r's Testen, Sandi 
 	- Bugfix 2, Danke f�r den Hinweis, Krischan 
   --------------------------------------------------------------------

   
   --------------------------------------------------------------------
	1) BESONDERHEITEN
	
	Mit dieser neuen Kategorien-Navigation f�r xt:Commerce kann man 
	a) Leere Kategorien verstecken
	b) Bestimmen, wie weit der Kategorien-Baum von Anfang an 
	   "ausgeklappt" sein soll
	c) Festlegen, ob sich der Kategorien-Baum trotz einer festgelegten
	   "Beschr�nkung" unterhalb von aktiven Kategorien weiter
	   ausklappen kann
	
	Die HTML-Ausgabe erfolgt als hierarchische Liste
   --------------------------------------------------------------------

   
   --------------------------------------------------------------------
	2) ANLEITUNG / KONFIGURATION
	
	Sie k�nnen die Navigation nach Ihren W�nschen anpassen. 
	In den Zeilen 96 bis 98 finden Sie 3 Variablen, mit denen das 
	Verhalten der Kategorien-Liste bestimmt werden kann.
   --------------------------------------------------------------------
	a) "TIEFE" DER NAVIGATION BESTIMMEN 

		$MaxLevel 	=	Zahl von 1 bis 5 oder false
	
		false:		=	Alle Kategorie-Ebenen werden angezeigt
		3:			=	Es werden alle Kategorien bis einschlie�lich  
						der "dritten Ebene" gezeigt
   --------------------------------------------------------------------
	b) LEERE KATEGORIEN VERSTECKEN:
	
		$HideEmpty	=	true oder false
	
		true		= 	Leere Kategorien werden nicht angezeigt
		false		= 	Leere Kategorien werden angezeigt
   --------------------------------------------------------------------
	c) "UNTERKATEGORIEN AKTIVER KATEGORIEN" ANZEIGEN:
	
		$ShowAktSub	=	true oder false
	
		true		= 	Wenn man seine Kategorien z.B. bis zur zweiten 
						Ebene "aufklappt" und dann eine Kategorie der 
						zweiten Ebene anw�hlt, werden die enthaltenen
						Unterkategorien der dritten Ebene angezeigt
		false		= 	Es werden NUR die Kategorie-Ebenen angezeigt, 
						die man mit $MaxLevel bestimmt hat
   --------------------------------------------------------------------

   
   --------------------------------------------------------------------
	BITTE BEACHTEN SIE: 

	1) Der Einsatz dieser Kategorien-Navi geschieht auf eigene Gefahr! 
	2) Jegliche Haftung f�r Sch�den an Ihrem System oder Verdienst-
	Ausf�lle wird abgelehnt 
	3) Ich empfehle dringend, diese Erweiterung VOR einem produktiven
	Einsatz zun�chst an einem Test-System auszuprobieren!
   --------------------------------------------------------------------

   
   --------------------------------------------------------------------
	IN EIGENER SACHE:
	
	Diese Funktionen wurden kostenlos und ohne Anspruch auf Gegen-
	leistung zur Verf�gung gestellt. Ich bitte allerdings darum, den 
	Autoren-Hinweis intakt zu lassen bzw. bei Bearbeitungen mit zu 
	�bernehmen. 
	
	Ein Backlink zu meiner Website http://www.gunnart.de w�rde mich 
	nat�rlich auch freuen.
	
	Vielen Dank
	
	Gunnar Tillmann 
   --------------------------------------------------------------------
	Feedback is Welcome: http://www.gunnart.de?p=311
   --------------------------------------------------------------------
*/

// KONFIGURATION
global $MaxLevel, $HideEmpty, $ShowAktSub;

	$MaxLevel = 1;
	$HideEmpty = true;
	$ShowAktSub = false;
	
function fadeCategory($cat_id)
{
	$my_array = array('256', '364'); // id "256" = Angebote, '364' = Gratisproben
	//$my_array = array();
	if(in_array($cat_id, $my_array))
		return true;
	else
		return false;
}	

function xtc_show_category($cid, $level, $foo, $cpath) {

	global $old_level, $categories_string; //, $HTTP_GET_VARS; // Brauchen wir nicht
	global $MaxLevel, $HideEmpty, $ShowAktSub;
	
	$fadeCategory = fadeCategory($cid);

	// 1) �berpr�fen, ob Kategorie Produkte enth�lt
	$Empty = true;
	$pInCat = xtc_count_products_in_category($cid);
	if ($pInCat > 0)
		$Empty = false;
	
	// 2) �berpr�fen, ob Kategorie gezeigt werden soll
	$Show = false;
	if ($HideEmpty) {
		if (!$Empty)
			$Show = true;
	} else {
		$Show = true;
	}

	// 3) �berpr�fen, ob Unterkategorien gezeigt werden sollen
	$ShowSub = false;
	if ($MaxLevel) {
		if ($level < $MaxLevel)
			$ShowSub = true;
	} else {
		$ShowSub = true;
	}
				
	if($Show && !($fadeCategory)) { // Wenn Kategorie gezeigt werden soll ....
	
		if ($cid != 0) {
			
			// 24.06.2007 BugFix
			// Auf "product_info"-Seiten wurde Kategorie nicht erkannt 
			// $category_path = explode('_',$HTTP_GET_VARS['cPath']);
			$category_path = explode('_',$GLOBALS['cPath']); 
			$in_path = in_array($cid, $category_path);
			$this_category = array_pop($category_path);
		
			for ($a = 0; $a < $level; $a++)                           ;
			
			// Produktz�hlung
			$ProductsCount = false;
			// Lange ger�tselt, aber das ist tats�chlich 
			// ein String und kein Boolean.                                                                                
			if (SHOW_COUNTS == 'true') 
				$ProductsCount = ' <em>(' . $pInCat . ')</em>';	
                                                  
			// Aktiv - Nicht Aktiv
			$Aktiv = false;
			if ($this_category == $cid) 
				// Wenn Kategorie aktiv ist
				$Aktiv = ' Current'; 
			elseif ($in_path) 
				// Wenn Oberkategorie aktiv ist
				$Aktiv = ' CurrentParent'; 
	
			// Hat ein SubMenue - hat kein SubMenue
			// CSS-Klasse festlegen
			$SubMenue = false;
			if (xtc_has_category_subcategories($cid)) 
				$SubMenue = " SubMenue";
			
			// Listenpunkt
			// CSS-Klasse festlegen
			$MainStyle = 'CatLevel'.$level;
			
			// Quelltext einr�cken
			$Tabulator = str_repeat("\t",$level-1);
	
			// Navigations-Liste ist jetzt hierarchisch!
			if($old_level) { 
				if ($old_level < $level) {
					$Pre = "\n<ul>";
					$Pre = str_replace("\n","\n".$Tabulator, $Pre)."\n";
				} else {
					$Pre = "</li>\n";
					if ($old_level > $level) {
						// Listenpunkte schlie�en
						// Quelltext einr�cken
						for ($counter = 0; $counter < $old_level - $level; $counter++) {
							$Pre .= str_repeat("\t", $old_level - $counter -1)."</ul>\n".str_repeat("\t", $old_level - $counter- 2)."</li>\n";
						}
					}
				} 
			}
				
			// Listenpunkte zusammensetzen
			$categories_string .=	$Pre.$Tabulator.
									'<li class="'.$MainStyle.$SubMenue.$Aktiv.'">'.
									// Bugfix, 12. Juli 2007
									//'<a href="' . xtc_href_link(FILENAME_DEFAULT, 'cPath=' . $cpath . $cid) . '">'.
									'<a href="' . xtc_href_link(FILENAME_DEFAULT, xtc_category_link($cid, $foo[$cid]['name']) ) . '">'.
									$foo[$cid]['name'].$ProductsCount.
									'</a>';
		}
		
		// f�r den n�chsten Durchgang ...
		$old_level = $level;
	
		// Unterkategorien durchsteppen
		foreach ($foo as $key => $value) {
	
			if ($foo[$key]['parent'] == $cid) {
					
				// Sollen Unterkategorien gezeigt werden?
				if ($ShowAktSub && $Aktiv)
					$ShowSub = true;
				
				if ($ShowSub) 
					xtc_show_category($key, $level+1, $foo, ($level != 0 ? $cpath . $cid . '_' : ''));
			} 
		}
	} // Ende if($Show)
} 		
?>