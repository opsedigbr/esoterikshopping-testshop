<?php
/* -----------------------------------------------------------------------------------------
  $Id: categories.php,v 1.3 2004/03/16 14:59:01 fanta2k Exp $

  XT-Commerce - community made shopping
  http://www.xt-commerce.com

  Copyright (c) 2003 XT-Commerce
  -----------------------------------------------------------------------------------------
  based on:
  (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
  (c) 2002-2003 osCommerce(categories.php,v 1.23 2002/11/12); www.oscommerce.com
  (c) 2003  nextcommerce (categories.php,v 1.10 2003/08/17); www.nextcommerce.org

  Released under the GNU General Public License
  -----------------------------------------------------------------------------------------
  Third Party contributions:
  Enable_Disable_Categories 1.3         Autor: Mikel Williams | mikel@ladykatcostumes.com
  Add Bullet Images for Categories Infobox v1.0 (12/09/2002)  by Toby Adams <toby@opticalfunk.co.uk
  All Categories V1.7 (06/17/2003)   by Christian Lescuyer <cl@goelette.net>
  hw_categories.php_v0.2 (22.06.2003)  FrankW <Daywalker@Hawkweb.de>

  Lots of Additional Programming (done in June 2007)  by Gunnar Tillmann <info@gunnart.de>


  Released under the GNU General Public License
  ---------------------------------------------------------------------------------------
	BITTE BEACHTEN SIE: 

	1) Der Einsatz dieser Kategorien-Navi geschieht auf eigene Gefahr! 
	2) Jegliche Haftung f�r Sch�den an Ihrem System oder Verdienst-
	Ausf�lle wird abgelehnt 
	3) Ich empfehle dringend, diese Erweiterung VOR einem produktiven
	Einsatz zun�chst an einem Test-System auszuprobieren!
  ---------------------------------------------------------------------------------------
	IN EIGENER SACHE:
	
	Diese Funktionen wurden kostenlos und ohne Anspruch auf Gegen-
	leistung zur Verf�gung gestellt. Ich bitte allerdings darum, den 
	Autoren-Hinweis intakt zu lassen bzw. bei Bearbeitungen mit zu 
	�bernehmen. 
	
	Ein Backlink zu meiner Website http://www.gunnart.de w�rde mich 
	nat�rlich auch freuen.
	
	Vielen Dank
	
	Gunnar Tillmann 
  ---------------------------------------------------------------------------------------
	Feedback is Welcome: http://www.gunnart.de?p=311
  ---------------------------------------------------------------------------------------
*/	

// reset var
$box_smarty = new smarty;
$box_content='';
$box_smarty->assign('tpl_path','templates/'.CURRENT_TEMPLATE.'/');

// include needed functions
require_once(DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/inc/xtc_show_category.inc.php');
require_once(DIR_FS_INC . 'xtc_has_category_subcategories.inc.php');
require_once(DIR_FS_INC . 'xtc_count_products_in_category.inc.php');


$categories_string = '';
if (GROUP_CHECK=='true') {
	$group_check = "and c.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
}
$categories_query = xtc_db_query(	"select c.categories_id,
									cd.categories_name,
									c.parent_id from " .
									TABLE_CATEGORIES . " c, " .
									TABLE_CATEGORIES_DESCRIPTION . " cd
									where c.categories_status = '1'
									".$group_check."
									and c.categories_id = cd.categories_id
									and cd.language_id='" . (int)$_SESSION['languages_id'] ."'
									order by sort_order, cd.categories_name");

while ($categories = xtc_db_fetch_array($categories_query))  {
	$foo[$categories['categories_id']] = array(	'name' => $categories['categories_name'],
												'parent' => $categories['parent_id']);
}
 
xtc_show_category(0, 0, $foo, '');

// NaviListe bekommt die ID "CatNavi"
$CatNaviStart = "";

// H�tte man auch einfacher machen k�nnen, aber mit Tabulatoren ist schicker.
// Au�erdem kann man so leichter nachpr�fen, ob auch wirklich alles korrekt l�uft.
for ($counter = 1; $counter < $old_level+1; $counter++) {
	$CatNaviEnd .= "</li>\n".str_repeat("\t", $old_level - $counter)."\n";
	if ($old_level - $counter > 0)
		$CatNaviEnd .= str_repeat("\t", ($old_level - $counter)-1);
}

// Fertige Liste zusammensetzen
$box_smarty->assign('BOX_CONTENT', $CatNaviStart.$categories_string.$CatNaviEnd);
$box_smarty->assign('language', $_SESSION['language']);
// Jibbie - darauf einen Dujardin

// Viele, viele bunte Smarties
if (USE_CACHE=='false') {
	$box_smarty->caching = 0;
	$box_categories= $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_categories.html');
	$box_categories_popup = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_categories_popup.html');
} else {
	$box_smarty->caching = 1;
	$box_smarty->cache_lifetime=CACHE_LIFETIME;
	$box_smarty->cache_modified_check=CACHE_CHECK;
	$cache_id = $_SESSION['language'].$_GET['cPath'];
	$box_categories= $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_categories.html',$cache_id);
	$box_categories_popup = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_categories_popup.html', $cache_id);
}
$smarty->assign('box_CATEGORIES',$box_categories);
$smarty->assign('box_CATEGORIES_POPUP', $box_categories_popup);
?>