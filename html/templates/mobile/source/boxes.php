<?php
/* -----------------------------------------------------------------------------------------
   $Id: boxes.php 1298 2005-10-09 13:14:44Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

  // BOF - Tomcraft - 2009-10-27 - Prevent duplicate content, see: http://www.gunnart.de/tipps-und-tricks/doppelten-content-vermeiden-productredirect-fuer-xtcommerce/
  require_once (DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/source/inc/gunnart_productRedirect.inc.php');

  define('DIR_WS_BOXES',DIR_FS_CATALOG .'templates/'.CURRENT_TEMPLATE. '/source/boxes/');

  include(DIR_WS_BOXES . 'categories.php');
  require_once(DIR_WS_BOXES . 'content.php');
  require_once(DIR_WS_BOXES . 'search.php');
  require_once(DIR_WS_BOXES . 'information.php');
  require_once(DIR_WS_BOXES . 'last_viewed.php');
  include(DIR_WS_BOXES . 'languages.php');
  if ($_SESSION['customers_status']['customers_status_id'] == 0) {
  	include(DIR_WS_BOXES . 'admin.php');
  }
  
  if ($_SESSION['customers_status']['customers_status_show_price'] == 1) {
    require_once(DIR_WS_BOXES . 'shopping_cart.php');
  }
  
  if (!$product->isProduct()) { include(DIR_WS_BOXES . 'best_sellers.php'); }
  if (substr(basename($PHP_SELF), 0, 8) != 'checkout') { include(DIR_WS_BOXES . 'currencies.php'); }

  $smarty->assign('tpl_path','templates/'.CURRENT_TEMPLATE.'/');
?>