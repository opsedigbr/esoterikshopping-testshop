
Bitte best�tigen Sie Ihre Pa�wortanfrage - {$VORNAME} {$NACHNAME}!

Bitte best�tigen Sie, da� Sie selber ein neues Pa�wort angefordert haben. 
Aus diesem Grund haben wir Ihnen diese E-mail mit einem pers�nlichen 
Best�tigungslink geschickt. Wenn Sie den Link best�tigen, indem Sie ihn 
anklicken, wird Ihnen umgehend ein neues Pa�wort in einer weiteren E-mail 
zur Verf�gung gestellt
      
Ihr Best�tigungslink:
{$LINK}



/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich 
Humboldtstr. 134
90459 N�rnberg
Umsatzsteuer-Identifikationsnummer: DE 234465241

www.esoterikshopping.de
E-mail: bestellung@esoterikshopping.de
Tel.: 08404 - 939 264
Fax: 08404 - 939 265

Bankverbindung
Empf�nger: Esoterikshopping 
Dresdner Bank N�rnberg 
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF 
IBAN: DE17 76080040 0154379901 
�ber Paypal: 
paypal@spart-bares.de

Telefonisch erreichbar:
Montag - Donnerstag: 09.00 - 12.00 und 15.00 - 18.00
Freitag: 09.00 - 12.00

\-------------------------------------------------/