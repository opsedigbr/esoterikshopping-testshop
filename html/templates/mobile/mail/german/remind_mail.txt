Hallo {$CUSTOMERS_FIRSTNAME} {$CUSTOMERS_LASTNAME},

Sie wollten eine Produkterinnerung erhalten, wenn der folgende Artikel wieder verf�gbar ist:
{$PRODUCTS_NAME} 

Hiermit m�chten wir Sie informieren, dass der gew�nschte Artikel ab sofort wieder in unserem Shop (www.esoterikshopping.de) bestellt werden kann.

Bei Fragen stehen wir Ihnen jederzeit gerne zur Verf�gung.

Mit freundlichen Gr��en
Ihr Esoterikshopping.de - Team

Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich 
Humboldtstr. 134
90459 N�rnberg
Umsatzsteuer-Identifikationsnummer: DE 234465241
www.esoterikshopping.de
E-mail: bestellung@esoterikshopping.de
Tel.: 08404 - 939 264
Fax: 08404 - 939 265 

Telefonisch erreichbar:
Montag - Donnerstag: 09.00 - 12.00 und 15.00 - 18.00
Freitag: 09.00 - 12.00
