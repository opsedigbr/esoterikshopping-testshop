�ber die Adresse {$content.REMOTE_ADDRESS} haben wir eine Anfrage zur Passworterneuerung
f�r Ihren Zugang zum Partnerprogramm erhalten.

Ihr neues Passwort f�r Ihren Zugang zum Partnerprogramm von {$content.STORE_NAME} lautet ab sofort: {$content.NEWPASS}