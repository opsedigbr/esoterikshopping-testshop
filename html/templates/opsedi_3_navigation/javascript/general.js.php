<?php
/* -----------------------------------------------------------------------------------------
   $Id: general.js.php 1262 2005-09-30 10:00:32Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/


   // this javascriptfile get includes at every template page in shop, you can add your template specific
   // js scripts here
?>
<link href="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery-ui/css/sunny/jquery-ui-1.10.3.custom.css" rel="stylesheet">

<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.jcountdown1.3.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery-ui/js/jquery-ui.1.9.0.min.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery-searchbox.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.easing-1.3.pack.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
<script src="<?php echo '/templates/'.CURRENT_TEMPLATE; ?>/javascript/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script>

<script type="text/javascript">
// Attibute automatisch
function fc_options_recalc(f_nr) {
    sum=form_price_basic[f_nr];

    for (var i = 0; i < form_id[f_nr].length; i++) {
	    obj = document.getElementById(form_id[f_nr][i]);
    
    	if( obj.type == 'radio' ) {
        	if( obj.checked == true ) {
          		sum += form_optval[f_nr][i];
        	}
      	} else {
        	if( obj.selected == true ) {
          		sum += form_optval[f_nr][i];
        	}
      	}
    }                                                   
    fc_setze_preis(f_nr, sum);
}
  
function fc_setze_preis( f_nr, price ) {
	obj=document.getElementById('f'+f_nr+'_price');
    if( obj != null ) {
    	obj.innerHTML = fc_darstellung_wert(price); 
   	}
}
  
function fc_darstellung_wert( number ) {
	laenge = curr_format_decimal_places;
	sep    = curr_format_decimal_point;
	th_sep = curr_format_thousands_point;

	number = Math.round( number * Math.pow(10, laenge) ) / Math.pow(10, laenge);
	str_number = number+'';
	arr_int = str_number.split('.');
	
	if(!arr_int[0])
		arr_int[0] = '0';

	if(!arr_int[1])
		arr_int[1] = '';

	if(arr_int[1].length < laenge) {
    	nachkomma = arr_int[1];
    	
    	for(i=arr_int[1].length+1; i <= laenge; i++) {
    		nachkomma += '0';
    	}

    	arr_int[1] = nachkomma;
  	}
  	
  	if(th_sep != '' && arr_int[0].length > 3) {
    	Begriff = arr_int[0];
    	arr_int[0] = '';
    
    	for(j = 3; j < Begriff.length ; j+=3) {
      		Extrakt = Begriff.slice(Begriff.length - j, Begriff.length - j + 3);
      		arr_int[0] = th_sep + Extrakt +  arr_int[0] + '';
    	}
    
    	str_first = Begriff.substr(0, (Begriff.length % 3 == 0) ? 3 : (Begriff.length % 3));
    	arr_int[0] = str_first + arr_int[0];
  	}
  	
  	ret = arr_int[0]+sep+arr_int[1];
  
  	if( curr_format_symbol_left != '' ) {
    	ret = curr_format_symbol_left+ret;
  	}

  	if( curr_format_symbol_right != '' ) {
    	ret = ret+' '+curr_format_symbol_right;
  	}
  	
  	return ret;
}
// Produkte automatisch: END


function setCookie(Name,Value,Duration){
  now = new Date();
  expiration = new Date(now.getTime()+Duration*86400000);
  document.cookie=Name+"="+Value+";expires="+expiration.toGMTString()+";";
}

function readCookie(Name){
  var searchname = Name + "=";
  var cookies = document.cookie.split(';');
  for(var i=0;i< cookies.length;i++){
   var singelcookie = cookies[i];
   while (singelcookie.charAt(0)==' ') singelcookie = singelcookie.substring(1,singelcookie.length);
     if(singelcookie.indexOf(searchname) == 0)
      return singelcookie.substring(searchname.length,singelcookie.length);
  }
  return null;
}

if(document.cookie){
    if(readCookie("width") == null || readCookie("height") == null){
        setCookie("width",screen.width,1);
        setCookie("height",screen.height,1);
        location.reload();
    }
}else{
    setCookie("width",screen.width,1);
    setCookie("height",screen.height,1);
    location.reload();
}
</script>

<script type="text/javascript">

$(document).ready(function() {
	// Countdown
	$("#time").countdown({
		date: "december 01, 2012", //Counting TO a date
		htmlTemplate: "<span class=\"cd-time-text\">nur noch</span><br /><span class=\"cd-time-digit\">%{d}</span><span class=\"cd-time\">Tag(e)</span>&nbsp;&nbsp;<span class=\"cd-time-digit\">%{h}</span><span class=\"cd-time\">Stunde(n)</span>&nbsp;&nbsp;<span class=\"cd-time-digit\">%{m}</span><span class=\"cd-time\">Minunte(n)</span>&nbsp;&nbsp;<span class=\"cd-time-digit\">%{s}</span><span class=\"cd-time\">Sekunde(n)</span><br /><span class=\"cd-time-text\">dann startet der Adventskalender!</span>",
		//date: "july 1, 2011 19:24", //Counting TO a date
		onChange: function( event, timer ){
		},
		onComplete: function( event ){
		
			$(this).html("");
		},
		leadingZero: false,
		direction: "down"
	});
	
	// show link to accessory only for this website
	$(".esoterikshopping.relevant-accessory").addClass("show");
	
	// Check if "Mobile Device"
	var isMobileDevice = navigator.userAgent.match(/mobile/i);
	if(isMobileDevice != null)
	{
		var clicks = 0;
		var totalClicks = 0;
		var value_1 = 0;
		var value_2 = 0;
		
		// Click Effect for images
		$("#free-samples .samples-stage ul li p.img img").click(function(e) {
			
			clickedDiv = $(this).closest("li").next("div");
			
			clicks++;
			totalClicks++;
			
			// get stored data
			if(clicks==1)
			{
				value_1 = ($("#free-samples .samples-stage ul li p.img img").index(this));
			}
			else
			{
				value_2 = ($("#free-samples .samples-stage ul li p.img img").index(this));
				clicks = 0;
			}
			
			if( (value_2 - value_1) == 0 && totalClicks!=1)
			{
				$(clickedDiv)
				.toggle();
			}
			else
			{
				$("#free-samples .samples-stage ul div").css("display", "none");
				
				$(clickedDiv)
				.toggle()
				.css({"top": (e.pageY+80)+"px", "left": (e.pageX-250)+"px"});
			}
	
		});			
	}
	else
	{
		// Mouseover Effect for images
		$("#free-samples .samples-stage ul li p.img img").bind({
			mouseover: function() {
				$(this)
				.closest("li")
				.next("div")
				.fadeIn(300);
			},
			
			mousemove: function(e) {
				$(this)
				.closest("li")
				.next("div")
				.css({"top": (e.pageY+15)+"px", "left": (e.pageX+15)+"px"});
			},
			
			mouseout: function() {
				$(this)
				.closest("li")
				.next("div")
				.fadeOut(300);
			}
		});		
	}
	

	/* Fanybox:begin */
	/* This is basic - uses default settings */
	$("a.grouped_elements").fancybox();
	
	/* Using custom settings */
	$("a#inline").fancybox({
		'hideOnContentClick': true
	});

	/* Apply fancybox to multiple items */
	$("a.grouped_elements").fancybox({
		'transitionIn'			:	'elastic',
		'transitionOut'			:	'elastic',
		'speedIn'				:	800, 
		'speedOut'				:	400,
		'overlayOpacity'		:	0.8,
		'overlayColor'			:	'#000',
		'hideOnContentClick'	:	true,
	});
	/* Fanybox:end */


	// Produkt: Versandkostenfrei 
	// Show "Add to Cart" Button if Attr is checked/selected
	$("#product-attributes-radioset").buttonset();
	$("#product-attributes-radioset input").click(function() {
		var visibility = $("#add-to-cart").css("display");
		if(typeof(visibility) !== undefined && visibility == 'none') {
			$("#add-to-cart").fadeIn("slow");
		}
	});

	// Button Design
	$("#shopnews button").button({
		icons: {
			primary: "ui-icon-lightbulb"
		}
	});
	
	// create account
	$(".error-msg img")
		.attr("src","images/icons/output_warning.gif")
		.attr("alt", "")
		.attr("title", "")
		.css({
			"height": "20px",
			"width": "24px"
		});

	// checkout process
	var checkoutProcess = $("#checkout-process button.btn");
	var inputObj = $("#checkout-process .shipping input, #checkout-process .payment input, #checkout-process .change-shipping-address input");

	// Einheitlicher Btn in Checkout Prozess
	checkoutProcess.button();

	// Zuklappen der Boxen
	inputObj.each(function() {
		inputObj.next("table").css("display", "none");
	});

	// Vorbelegung der inputs entfernen
	inputObj.removeAttr('checked');

	// check effect
	inputObj.click(function() {
		inputObj.each(function() {
			inputObj.parent().removeClass("checked").find("table").css("display", "none");
		})
		$(this).parent().addClass("checked").find("table").fadeIn(2000);
		checkoutProcess.button("enable");
	});

	// AGB Check Leiste
	var checkedCondition = $("#checkout-process .conditions input, #checkout-process .change-shipping-address input");
	checkedCondition.click(function() {
		if($(this).is(":checked")) {
			$(this).parent().addClass("checked");
		}
		else {
			$(this).parent().removeClass("checked");
		}
	});	

	// Modal: AGB
	$("#checkout-process .agb").click(function() {
		var content = $.get('media/content/agb.php')
          .done(function(data) {
            $("#dialog-agb").dialog({
              title: "Allgemeine Gesch&auml;ftsbedingungen",
              width: 750,
              height: 750,
              buttons: [{ 
                text: "Fenster schliessen!", 
                click: function() { 
                  $(this).dialog("close"); 
                } 
              }],
              hide: { 
                effect: "explode", 
                duration: 1000 
              },
              modal: true,

            }).html(data);  
          })

          .fail(function() { 
            alert( "Die entsprechende Seite ist nicht vorhanden!" );
          });
	});

	// Modal: Widerrufsbelehrung
	$("#checkout-process .widerruf").click(function() {
		var content = $.get('media/content/widerrufbelehrung.php')
          .done(function(data) {
            $("#dialog-widerruf").dialog({
              title: "Widerrufsbelehrung",
              width: 750,
              height: 750,
              buttons: [{ 
                text: "Fenster schliessen!", 
                click: function() { 
                  $(this).dialog("close"); 
                } 
              }],
              hide: { 
                effect: "explode", 
                duration: 1000 
              },
              modal: true,

            }).html(data);  
          })

          .fail(function() { 
            alert( "Die entsprechende Seite ist nicht vorhanden!" );
          });
	});	

	$(".products_versandkosten").click(function() {
		var content = $.get('media/content/versandkosten.php')
		.done(function(data) {
		    $("#dialog-versandkosten").dialog({
		        title: "Versandkosten",
		        width: 750,
		        height: 750,
		        buttons: [{ 
		            text: "Fenster schliessen!", 
		            click: function() { 
		                $(this).dialog("close"); 
		            } 
		        }],
		        hide: { 
		            effect: "explode", 
		            duration: 1000 
		        },
		        modal: true,
		    }).html(data);  
		})

		.fail(function() { 
		    alert( "Die entsprechende Seite ist nicht vorhanden!" );
		});
	});

	// Modal: Zahlungsaufschlag Invoice
	$("#checkout-process .invoice-aufschlag").click(function() {	
		$("#dialog-invoice-aufschlag").dialog({
              width: 600,
              height: 420,
              buttons: [{ 
                text: "Information schliessen!", 
                click: function() { 
                  $(this).dialog("close"); 
                } 
              }],
              hide: { 
                effect: "explode", 
                duration: 1000 
              },
              modal: true,
        })
    });

	// Zur�ck Btn
	$(".btn-back").click(function(event) {
	    event.preventDefault();
	    history.back(1);
	});	

	// jQuery UI Design Pattern
	$("#create-account .create-account-stage button.btn.login").button();

	// Neue Adresse - Layout aus "Neuer Kunde" ziehen
	$("#checkout-process #create-account .create-account-stage .add-textfield-class input").addClass("textfield");

	// Arrows
	var arrowColorDown = $(".arrow.down").css("border-top-color");
	var arrowColorUp = $(".arrow.up").css("border-bottom-color");	

	// Login Bereich
	var loginText = $(".login-text");
	var loginTextPosition = loginText.position();
	var widthLoginBox 	= parseInt($("#login-box.mystage").width());
	var widthLoginText 	= parseInt(loginText.width());

	$(".navtrail .login-text").on('click', function() {
		$(this).mousemove(function(e) {
			x = e.pageX - loginTextPosition.left - parseInt(2);
			y = e.pageY - loginTextPosition.top - parseInt(2);

			if(x < 0 || y < 0 || x > (widthLoginText - parseInt(5)) ) {
				$( "#login-box.mystage" ).slideUp("fast", function() {
						$(".navtrail .login-text .down").css({
							"display": "inline-block",
							"border-top-color": arrowColorDown,
						});
						$(".navtrail .login-text .up").css("display", "none");	
				});
			}			
		});

		// true == offen		
		var status = $( "#login-box.mystage" ).is(":hidden");

		$( "#login-box.mystage" )
		.css({
			"top": loginTextPosition.top + parseInt(51),
			"left": loginTextPosition.left - ( (widthLoginBox-widthLoginText) / 2) - parseInt(5),
		})
		.slideToggle("fast");



		if(status == true) {
			$(".navtrail .login-text .down").css({
				"display": "inline-block",
				"border-top-color": "transparent",
			});

			$(".navtrail .login-text .up").css({
				"display": "inline-block",
				"position": "absolute",
				"top": loginTextPosition.top + parseInt(41),
				"left": loginTextPosition.left + (widthLoginText/2) - parseInt(10),
			});
		}
		else {
			$(".navtrail .login-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});
			$(".navtrail .login-text .up").css("display", "none");
		}
	});	

	$( "#login-box.mystage" ).mouseleave(function() {
		$(this).slideUp("fast", function() {
			$(".navtrail .login-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});
			$(".navtrail .login-text .up").css("display", "none");	
		});
	});

	$( "#login-box.mystage .btn" ).button();

	// Warenkorb Bereich
	var shoppingcartText = $(".shoppingcart-text");
	var shoppingcartTextPosition = shoppingcartText.position();

	var widthShoppingcartBox 	= parseInt($("#shoppingcart-box.mystage").width());
	var widthShoppingcartText 	= parseInt(shoppingcartText.width());

	$(".navtrail .shoppingcart-text").on('click', function() {
		$(this).mousemove(function(e) {
			x = e.pageX - shoppingcartTextPosition.left - parseInt(2);
			y = e.pageY - shoppingcartTextPosition.top - parseInt(2);

			if(x < 0 || y < 0 || x > (widthShoppingcartText - parseInt(5)) ) {
				$( "#shoppingcart-box.mystage" ).slideUp("fast", function() {
						$(".navtrail .shoppingcart-text .down").css({
							"display": "inline-block",
							"border-top-color": arrowColorDown,
						});
						$(".navtrail .shoppingcart-text .up").css("display", "none");	
				});
			}			
		});

		// true == offen		
		var status = $( "#shoppingcart-box.mystage" ).is(":hidden");

		$( "#shoppingcart-box.mystage" )
		.css({
			"top": shoppingcartTextPosition.top + parseInt(51),
			"left": shoppingcartTextPosition.left - ( (widthShoppingcartBox-widthShoppingcartText) / 2) - parseInt(5),
		})
		.slideToggle("fast");

		if(status == true) {
			$(".navtrail .shoppingcart-text .down").css({
				"display": "inline-block",
				"border-top-color": "transparent",
			});
			$(".navtrail .shoppingcart-text .up").css({
				"display": "inline-block",
				"position": "absolute",
				"top": shoppingcartTextPosition.top + parseInt(41),
				"left": shoppingcartTextPosition.left + (widthShoppingcartText/2) - parseInt(10),
			});
		}
		else {
			$(".navtrail .shoppingcart-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});

			$(".navtrail .shoppingcart-text .up").css("display", "none");
		}
	});	

	$( "#shoppingcart-box.mystage" ).mouseleave(function() {
		$(this).slideUp("fast", function() {
			$(".navtrail .shoppingcart-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});
			$(".navtrail .shoppingcart-text .up").css("display", "none");	
		});
	});

	$( "#shoppingcart-box.mystage .btn" ).button();	

	// account Bereich
	var accountText = $(".account-text");
	var accountTextPosition = accountText.position();

	var widthAccountBox 	= parseInt($("#account-box.mystage").width());
	var widthAccountText 	= parseInt(accountText.width());

	$(".navtrail .account-text").on('click', function() {
		$(this).mousemove(function(e) {
			x = e.pageX - accountTextPosition.left - parseInt(2);
			y = e.pageY - accountTextPosition.top - parseInt(2);

			if(x < 0 || y < 0 || x > (widthAccountText - parseInt(5)) ) {
				$( "#account-box.mystage" ).slideUp("fast", function() {
						$(".navtrail .account-text .down").css({
							"display": "inline-block",
							"border-top-color": arrowColorDown,
						});
						$(".navtrail .account-text .up").css("display", "none");	
				});
			}			
		});

		// true == offen		
		var status = $( "#account-box.mystage" ).is(":hidden");

		$( "#account-box.mystage" )
		.css({
			"top": accountTextPosition.top + parseInt(51),
			"left": accountTextPosition.left - ( (widthAccountBox-widthAccountText) / 2) - parseInt(5),
		})
		.slideToggle("fast");

		if(status == true) {
			$(".navtrail .account-text .down").css({
				"display": "inline-block",
				"border-top-color": "transparent",
			});
			$(".navtrail .account-text .up").css({
				"display": "inline-block",
				"position": "absolute",
				"top": accountTextPosition.top + parseInt(41),
				"left": accountTextPosition.left + (widthAccountText/2) - parseInt(10),
			});
		}
		else {
			$(".navtrail .account-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});

			$(".navtrail .account-text .up").css("display", "none");
		}
	});	

	$( "#account-box.mystage" ).mouseleave(function() {
		$(this).slideUp("fast", function() {
			$(".navtrail .account-text .down").css({
				"display": "inline-block",
				"border-top-color": arrowColorDown,
			});
			$(".navtrail .account-text .up").css("display", "none");	
		});
	});

	$( "#account-box.mystage .btn" ).button();		

});

</script>


<script type="text/javascript">

$(window).load(function() {
	$('#slider').nivoSlider();
	$('.produktdetails_content a[href*=".mp"]').attr('target', '_blank');
});
</script>
