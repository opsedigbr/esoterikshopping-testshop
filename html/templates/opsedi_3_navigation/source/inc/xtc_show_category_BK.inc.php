<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_show_category.inc.php 1262 2005-09-30 10:00:32Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.23 2002/11/12); www.oscommerce.com
   (c) 2003	 nextcommerce (xtc_show_category.inc.php,v 1.4 2003/08/13); www.nextcommerce.org 

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

function fadeAngebote($cat_id)
{
	$my_array = array('256', '364'); // id "256" = Angebote, '364' = Gratisproben
	//$my_array = array();
	if(in_array($cat_id, $my_array))
		return true;
	else
		return false;
}

function xtc_show_category($counter) 
{
	global $foo, $categories_string, $id;
	
	// Angebote ausblenden
	$angebot_ausblenden = fadeAngebote($foo[$counter]['path']);

	$img_1='';
	
	$products_in_category = xtc_count_products_in_category($counter);
	$cPath_new=xtc_category_link($counter,$foo[$counter]['name']);
	if ( ($id) && (in_array($counter, $id)) )
	{
		$current = ' aktiv';
	}
	$level = $foo[$counter]['level']+1;
	$nextId = $foo[$counter]['next_id'];
	$levelNextId = $foo[$nextId]['level']+1;
	
	($products_in_category < 1) ? $doNotDisplay = ' none' : $doNotDisplay = '';

	if(!$angebot_ausblenden)
	{
		$categories_string .= '<li class="level-'.$level.$current.$doNotDisplay.'"><a href="';
		$categories_string .= xtc_href_link(FILENAME_DEFAULT, $cPath_new);
		$categories_string .= '">';

		if($level>1)
			$categories_string .= "<span>&#8250;</span>&nbsp;";
			
		if(empty($foo[$counter]['url_name']) || $foo[$counter]['url_name'] == '')
		{
			$categories_string .= $foo[$counter]['name'];
		}
		else
		{
			$categories_string .= $foo[$counter]['url_name'];
		}
		
		if (SHOW_COUNTS == 'true') 
		{
			$categories_string .= '<span>(' . $products_in_category . ')</span>';
			//$categories_string .= "<span>($counter - $levelNextId)</span>";
		}
		
		$categories_string .= '</a>';
		
		if($levelNextId == $level)
		{
			$categories_string .= '</li>';	
		}
		
		if($levelNextId > $level)
		{
			$categories_string .= '<ul>';
		}
		else if($levelNextId < $level)
		{
			$categories_string .= '</ul></li>';
		}			
	}

	
	if ($foo[$counter]['next_id']) 
	{
		xtc_show_category($foo[$counter]['next_id']);
	} 
	else 
	{
		$categories_string .= '';
	}
}


?>