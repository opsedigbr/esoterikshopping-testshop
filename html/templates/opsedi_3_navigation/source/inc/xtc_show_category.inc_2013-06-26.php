<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_show_category.inc.php 1262 2005-09-30 10:00:32Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.23 2002/11/12); www.oscommerce.com
   (c) 2003	 nextcommerce (xtc_show_category.inc.php,v 1.4 2003/08/13); www.nextcommerce.org 

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

function fadeAngebote($cat_id)
{
	$my_array = array('256', '364'); // id "256" = Angebote, '364' = Gratisproben
	//$my_array = array();
	if(in_array($cat_id, $my_array))
		return true;
	else
		return false;
}

function xtc_show_category($counter) 
{
	global $foo, $categories_string, $id;
	
	// Angebote ausblenden
	$angebot_ausblenden = fadeAngebote($foo[$counter]['path']);


	// image for first level
	//$img_1='<img src="templates/'.CURRENT_TEMPLATE.'/img/icon_arrow.jpg" alt="" />&nbsp;';
	$img_1='';
	
	$products_in_category = xtc_count_products_in_category($counter);

	for ($a=0; $a<$foo[$counter]['level']; $a++) 
	{
		if ($foo[$counter]['level']=='1') 
		{
			if($products_in_category > 0)
			{
				if(!$angebot_ausblenden)
				{
					$categories_string .= "&nbsp;-&nbsp;";
				}
			}
		}
	}
	
	if ($foo[$counter]['level']=='' || $foo[$counter]['level']=='0') 
	{
		//$categories_string .= $img_1;
		if($products_in_category > 0)
		{
			if(!$angebot_ausblenden)
			{
				$categories_string .= '<strong><a class="';
				if ( ($id) && (in_array($counter, $id)) ) 
				{
					$categories_string .= 'show_categorie_aktiv" href="';
				}
				else
				{
					$categories_string .= 'show_categorie" href="';
				}
			}
		}
	} 
	else 
	{
		if($products_in_category > 0)
		{
			if(!$angebot_ausblenden)
			{
				$categories_string .= '<a class="';
				if ( ($id) && (in_array($counter, $id)) ) 
				{
					$categories_string .= 'show_categorie_aktiv" href="';
				}
				else
				{
					$categories_string .= 'show_categorie" href="';
				}				
			}
		}
	}

	$cPath_new=xtc_category_link($counter,$foo[$counter]['name']);

	if($products_in_category > 0)
	{
		if(!$angebot_ausblenden)
		{
			$categories_string .= xtc_href_link(FILENAME_DEFAULT, $cPath_new);
			$categories_string .= '">';
		}
	}

	if ( ($id) && (in_array($counter, $id)) ) 
	{
		if($products_in_category > 0)
		{
			if(!$angebot_ausblenden)
			{
				$categories_string .= '<strong>';
			}
		}
	}

	// display category name
	if($products_in_category > 0)
	{   
		if(!$angebot_ausblenden)
		{
			if(empty($foo[$counter]['url_name']) || $foo[$counter]['url_name'] == '')
			{
				$categories_string .= $foo[$counter]['name'];
			}
			else
			{
				$categories_string .= $foo[$counter]['url_name'];
			}
		}
	}

	if ( ($id) && (in_array($counter, $id)) ) 
	{
		if($products_in_category > 0)
		{		
			if(!$angebot_ausblenden)
			{
				$categories_string .= '</strong>';
			}
		}
	}

	if ($foo[$counter]['level']=='' || $foo[$counter]['level']=='0') 
	{
		if($products_in_category > 0)
		{
			if(!$angebot_ausblenden)
			{
				$categories_string .= '</a></strong>';
			}
		}
	} 
	else 
	{
		if($products_in_category > 0)
		{		
			if(!$angebot_ausblenden)
			{
				$categories_string .= '</a>';
			}
		}
	}

	if (SHOW_COUNTS == 'true') 
	{
		if ($products_in_category > 0) 
		{
			if(!$angebot_ausblenden)
			{
				$categories_string .= '&nbsp;<span style="font-size:10px;">(' . $products_in_category . ')<!--'.$angebot_ausblenden.'--></span>';
				$categories_string .= '';
			}
		}
	}

	if($products_in_category > 0)
	{    
		if(!$angebot_ausblenden)
		{
			$categories_string .= '<div style="height:6px"></div>';
			$categories_string .= '<div class="categorie_space"></div>';
			$categories_string .= '<div style="height:6px"></div>';
		}
	}

	if ($foo[$counter]['next_id']) 
	{
		xtc_show_category($foo[$counter]['next_id']);
	} 
	else 
	{
		$categories_string .= '';
	}
}


?>