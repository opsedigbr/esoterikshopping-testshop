<?php

/* -----------------------------------------------------------------------------------------
   $Id: social_network.php 1302 2005-10-12 16:21:29Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(social_network.php,v 1.6 2003/02/10); www.oscommerce.com
   (c) 2003	 nextcommerce (social_network.php,v 1.8 2003/08/21); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

//$box_smarty->assign('language', $_SESSION['language']);
define('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

// Facebook
$content_string .= '<div class="box_top">Immer auf dem Laufenden...</div>
<div class="box_body">
    <a rel="nofollow" href="https://www.facebook.com/Esoterikshopping" target="_blank">
        <img style="float:left;margin:5px 10px;" src="'.tpl_path.'/img/facebook_1.jpg" name="FB" width="35" height="35" border="0" align="top" id="FB" alt="Zu unserem Facebook Konto" title="Zu unserem Facebook Konto" />
        <span style="margin-top:8px;display:block;">Esoterikshopping.de<br />bei <strong>Facebook!</strong></span>
    </a>';

// YouTube
/*
$content_string .='<div style="clear:both;margin-bottom:0;"></div>

    <a rel="nofollow" href="https://www.youtube.com/user/Esoterikshopping" target="_blank">
        <img style="float:left;margin:5px 10px;" src="'.tpl_path.'/img/youtube_2.jpg" name="YT" width="42" height="43" border="0" id="YT" alt="Zu unseren Videos bei YouTube" title="Zu unseren Videos bei YouTube" />
        <span style="margin-top:14px;display:block;">Esoterikshopping.de<br />bei  <strong>Youtube!</strong></span>
    </a>
    <div style="clear:both; height:5px;"></div>
</div>';
*/

$content_string .= '<div style="clear:both; height:5px;"></div></div>';
				  
if ($content_string != '') {
	//$box_smarty->assign('BOX_CONTENT', $content_string);
   $box_social_network = $content_string;

}
else {
   if (!$cache) {
   	$box_social_network = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_social_network.html');
   } else {
   	$box_social_network = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_social_network.html', $cache_id);
   }
}


$smarty->assign('box_SOCIAL_NETWORK', $box_social_network);
?>