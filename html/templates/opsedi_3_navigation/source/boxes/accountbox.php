<?php

/* -----------------------------------------------------------------------------------------
   $Id: accountbox.php 1262 2005-09-30 10:00:32Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommercebased on original files from OSCommerce CVS 2.2 2002/08/28 02:14:35 www.oscommerce.com 
   (c) 2003	 nextcommerce (accountbox.php,v 1.10 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   Loginbox V1.0        	Aubrey Kilian <aubrey@mycon.co.za>

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$box_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');
$box_content = '';

if(xtc_session_is_registered('customer_id')) {
   $box_smarty->assign('BOX_CONTENT', $accountboxcontent);
	$box_smarty->caching = 0;
	$box_smarty->assign('language', $_SESSION['language']);
	$box_accountbox = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_account.html');
	$smarty->assign('box_ACCOUNT', $box_accountbox);
}
?>