<?php

/* -----------------------------------------------------------------------------------------
   $Id: search.php 1262 2005-09-30 10:00:32Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(search.php,v 1.22 2003/02/10); www.oscommerce.com 
   (c) 2003	 nextcommerce (search.php,v 1.9 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$box_smarty->assign('TPL_PATH', 'templates/'.CURRENT_TEMPLATE.'/');
$box_smarty->assign('SHOP_NAME', TITLE);
$box_smarty->assign('SHOP_NAME_URL', encodeForFileName(TITLE));

$box_content = '';

require_once (DIR_FS_INC.'xtc_image_submit.inc.php');
require_once (DIR_FS_INC.'xtc_hide_session_id.inc.php');

$box_smarty->assign('language', $_SESSION['language']);
// set cache ID
 if (!CacheCheck()) {
	$box_smarty->caching = 0;
	$box_trustedshops = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_trustedshops.html');
} else {
	$box_smarty->caching = 1;
	$box_smarty->cache_lifetime = CACHE_LIFETIME;
	$box_smarty->cache_modified_check = CACHE_CHECK;
	$cache_id = $_SESSION['language'];
	$box_trustedshops = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_trustedshops.html', $cache_id);
}

$smarty->assign('SHOP_NAME', "test");

if ("english"==$_SESSION['language']) {
	//$smarty->assign('box_TRUSTEDSHOPS', $box_trustedshops);	
}
if ("french"==$_SESSION['language']) {
	//$smarty->assign('box_TRUSTEDSHOPS', $box_trustedshops);	
}
else if ("german"==$_SESSION['language']) {
	$smarty->assign('box_TRUSTEDSHOPS', $box_trustedshops);	
}
// else $smarty->assign('box_TRUSTEDSHOPS', $box_trustedshops); 


function encodeForFileName($sentence)
{  
	
  $sentence = str_replace("ß", "ss", $sentence);
  $sentence = str_replace("é", "e", $sentence);
  $sentence = str_replace("á", "a", $sentence);
  $sentence = str_replace("ë", "e", $sentence);    
  $sentence = str_replace("ä", "ae", $sentence);
  $sentence = str_replace("ü", "ue", $sentence);
  $sentence = str_replace("ö", "oe", $sentence);
  $sentence = str_replace("Ä", "Ae", $sentence);
  $sentence = str_replace("Ü", "Ue", $sentence);
  $sentence = str_replace("Ö", "Oe", $sentence);    
  
  $sentence = str_replace("&auml;", "ae", $sentence);
  $sentence = str_replace("&uuml;", "ue", $sentence);
  $sentence = str_replace("&ouml;", "oe", $sentence);
  $sentence = str_replace("&Auml;", "Ae", $sentence);
  $sentence = str_replace("&Uuml;", "Ue", $sentence);
  $sentence = str_replace("&Ouml;", "Oe", $sentence);
  $sentence = str_replace("&szlig;", "ss", $sentence);
  $sentence = str_replace("&amp;", "_", $sentence);      
  $sentence = preg_replace("/[*&;\.:\/]+/","-",$sentence);    
  $sentence = preg_replace("/ /","-",$sentence);  
  $sentence = preg_replace("/_/","-",$sentence);         
  $sentence = preg_replace("/[^a-zA-Z0-9_-]/","",$sentence);
  $sentence = preg_replace("/(-)+/","-",$sentence);
  $sentence = preg_replace("/-$/","",$sentence);  
  $sentence = preg_replace("/^-/","",$sentence);
  
  return $sentence;
}
?>