<?php

/* -----------------------------------------------------------------------------------------
   $Id: search.php 1262 2005-09-30 10:00:32Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(search.php,v 1.22 2003/02/10); www.oscommerce.com 
   (c) 2003	 nextcommerce (search.php,v 1.9 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

$box_smarty->assign('language', $_SESSION['language']);
$box_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

// file_put_contents not available for PHP4
if (!function_exists('file_put_contents')) 
{
	function file_put_contents($file, $contents = '', $method = 'w+') 
	{
		$file_handle = fopen($file, $method);
		fwrite($file_handle, $contents);
		fclose($file_handle);
		return true;
	}
}

function cache_check($filename_cache, $timeout = 10800) 
{
	if (file_exists($filename_cache)) 
	{
		$timestamp = filemtime($filename_cache);
		// Seconds
		if (mktime() - $timestamp < $timeout) 
		{
			return true;
		}
		else
		{
			return false;
		}
	} 
	else
	{
		return false;
	}
}

if (!cache_check($filename = "XECB830E0AE0C3827CC8EC3B083D488FC.gif", 10800)) 
{
	// Load fresh widget from trustedshops Website
	// and write in local file
	// Open the file to get existing content
	$current = file_get_contents("https://www.trustedshops.com/bewertung/widget/widgets.nfs/XECB830E0AE0C3827CC8EC3B083D488FC.gif");
	// Write the contents back to the file
	file_put_contents($filename, $current);
	error_log("new widget saved!");
}
else
{
	error_log("old widget loaded!");
}


$content_string .= '
<a rel="nofollow" target="_blank" href="https://www.trustedshops.de/bewertung/info_XECB830E0AE0C3827CC8EC3B083D488FC.html" title="Kundenbewertungen von Esoterikshopping.de einsehen!">
	<img class="bewertung" alt="Kundenbewertungen von Esoterikshopping.de" border="0" src="'.$filename.'"/>
</a>
<br />
<br />
';

if ($content_string != '')
	$box_smarty->assign('BOX_CONTENT', $content_string);


// set cache ID
if (!$cache) 
{
	$box_trustedshops = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_trustedshops.html');
} 
else 
{
	$box_trustedshops = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_trustedshops.html', $cache_id);
}

$smarty->assign('box_TRUSTEDSHOPS', $box_trustedshops);

function encodeForFileName($sentence)
{  
	
  $sentence = str_replace("ß", "ss", $sentence);
  $sentence = str_replace("é", "e", $sentence);
  $sentence = str_replace("á", "a", $sentence);
  $sentence = str_replace("ë", "e", $sentence);    
  $sentence = str_replace("ä", "ae", $sentence);
  $sentence = str_replace("ü", "ue", $sentence);
  $sentence = str_replace("ö", "oe", $sentence);
  $sentence = str_replace("Ä", "Ae", $sentence);
  $sentence = str_replace("Ü", "Ue", $sentence);
  $sentence = str_replace("Ö", "Oe", $sentence);    
  
  $sentence = str_replace("&auml;", "ae", $sentence);
  $sentence = str_replace("&uuml;", "ue", $sentence);
  $sentence = str_replace("&ouml;", "oe", $sentence);
  $sentence = str_replace("&Auml;", "Ae", $sentence);
  $sentence = str_replace("&Uuml;", "Ue", $sentence);
  $sentence = str_replace("&Ouml;", "Oe", $sentence);
  $sentence = str_replace("&szlig;", "ss", $sentence);
  $sentence = str_replace("&amp;", "_", $sentence);      
  $sentence = preg_replace("/[*&;\.:\/]+/","-",$sentence);    
  $sentence = preg_replace("/ /","-",$sentence);  
  $sentence = preg_replace("/_/","-",$sentence);         
  $sentence = preg_replace("/[^a-zA-Z0-9_-]/","",$sentence);
  $sentence = preg_replace("/(-)+/","-",$sentence);
  $sentence = preg_replace("/-$/","",$sentence);  
  $sentence = preg_replace("/^-/","",$sentence);
  
  return $sentence;
}
?>