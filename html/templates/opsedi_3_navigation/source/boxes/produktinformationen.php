<?php

/* -----------------------------------------------------------------------------------------
   $Id: produktinformationen.php 1302 2005-10-12 16:21:29Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(produktinformationen.php,v 1.6 2003/02/10); www.oscommerce.com
   (c) 2003	 nextcommerce (produktinformationen.php,v 1.8 2003/08/21); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

$box_smarty->assign('language', $_SESSION['language']);
// set cache ID
if (!CacheCheck()) {
	$cache=false;
	$box_smarty->caching = 0;
} else {
	$cache=true;
	$box_smarty->caching = 1;
	$box_smarty->cache_lifetime = CACHE_LIFETIME;
	$box_smarty->cache_modified_check = CACHE_CHECK;
	$cache_id = $_SESSION['language'].$_SESSION['customers_status']['customers_status_id'];
}

if (!$box_smarty->is_cached(CURRENT_TEMPLATE.'/boxes/box_produktinformationen.html', $cache_id) || !$cache) {
	$box_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

	if (GROUP_CHECK == 'true') {
		$group_check = "and group_ids LIKE '%c_".$_SESSION['customers_status']['customers_status_id']."_group%'";
	}

	$content_query = "SELECT
	 					content_id,
	 					categories_id,
	 					parent_id,
	 					content_title,
	 					content_group, 
						content_file, 
						content_cpaths 
	 					FROM ".TABLE_CONTENT_MANAGER."
	 					WHERE languages_id='".(int) $_SESSION['languages_id']."'
	 					and file_flag=10 ".$group_check." and content_status=1 order by content_id DESC";

	$content_query = xtDBquery($content_query);
	
	$content_string = '<div id="productinformationen-box">';

	while ($content_data = xtc_db_fetch_array($content_query, true)) {
		
		$content_cPaths = '';
		
		if(!empty($content_data['content_cpaths']) && $content_data['content_cpaths'] != '')
		{
			if(strpos($content_data['content_cpaths'], ',') !== false)
			{
				 $content_cPaths = str_replace(',', '_', $content_data['content_cpaths']);
			}
			else
			{
				$content_cPaths = $content_data['content_cpaths'];
			}
			//$content_cPaths = '&cPath='.$content_cPaths;
			$content_cPaths = $content_cPaths;
		}

		
		$SEF_parameter = '';
		if (SEARCH_ENGINE_FRIENDLY_URLS == 'true')
			//$SEF_parameter = '&product='.xtc_cleanName($content_data['content_title']);
			$SEF_parameter = xtc_cleanName($content_data['content_title']);
			
		if($content_data['content_file'] != '')
		{
			$imgCF		= $content_data['content_file'];
			$imgCF		= explode('.', $imgCF);
			$imgCF		= $imgCF[0];
			//$imgCFLink	= '<a href="'.xtc_href_link('product-informations.php', 'coID='.$content_data['content_group'].$content_cPaths.$SEF_parameter).'"><img class="produktinformationen-thumbnail" src="media/content/'.$imgCF.'.jpg" alt="'.$content_data['content_title'].'" title="'.$content_data['content_title'].'" /></a>';
			$imgCFLink	= '<a class="link" href="l-pg/'.$content_data['content_group'].'/'.$content_cPaths.'/'.$SEF_parameter.'-guenstig-hier-im-shop-kaufen.html"><img class="produktinformationen-thumbnail" src="media/content/'.$imgCF.'.jpg" alt="'.$content_data['content_title'].'" title="'.$content_data['content_title'].'" /></a>';
		}

		$content_string .= '
							<div>'.$imgCFLink.'
								<span class="right"><a class="link" href="l-pg/'.$content_data['content_group'].'/'.$content_cPaths.'/'.$SEF_parameter.'-guenstig-hier-im-shop-kaufen.html">'.$content_data['content_title'].'</a></span>
							</div>';
	}
	
	$content_string .= '</div>';

	if ($content_string != '')
		$box_smarty->assign('BOX_CONTENT', $content_string);

}

if (!$cache) {
	$box_produktinformationen = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_produktinformationen.html');
} else {
	$box_produktinformationen = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_produktinformationen.html', $cache_id);
}

$smarty->assign('box_PRODUKTINFORMATIONEN', $box_produktinformationen);
?>