<?php

/* -----------------------------------------------------------------------------------------
   $Id: information.php 1302 2005-10-12 16:21:29Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(information.php,v 1.6 2003/02/10); www.oscommerce.com
   (c) 2003	 nextcommerce (information.php,v 1.8 2003/08/21); www.nextcommerce.org

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
$box_smarty = new smarty;
$content_string = '';

$box_smarty->assign('language', $_SESSION['language']);

$content_string .= '<script type="text/javascript"><!--
google_ad_client = "pub-3964650550913848";
/* 160x600, Erstellt 28.05.11 */
google_ad_slot = "9353672234";
google_ad_width = 160;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>';
				  
if ($content_string != '')
	$box_smarty->assign('BOX_CONTENT', $content_string);				  

if (!$cache) {
	$box_werbung_links_1 = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_werbung_links_1.html');
} else {
	$box_werbung_links_1 = $box_smarty->fetch(CURRENT_TEMPLATE.'/boxes/box_werbung_links_1.html', $cache_id);
}


$smarty->assign('box_WERBUNG_LINKS_1', $box_werbung_links_1);
?>