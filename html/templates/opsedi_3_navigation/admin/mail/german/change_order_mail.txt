Hallo {$NAME},

vielen Dank f�r Ihre Bestellung.

Ihre Bestellung mit der Nummer {$ORDER_NR} vom {$ORDER_DATE} wird heute unser Versandlager verlassen.
Sie werden in den n�chsten 24 Stunden eine weitere E-Mail mit der Sendungsnummer von DHL erhalten.
Mit dieser Sendungsverfolgungsnummer, die Sie von DHL erhalten, k�nnen Sie jederzeit Ihre Sendung online verfolgen.

Bei Fragen zu Ihrer Bestellung stehen wir Ihnen jederzeit gerne zur Verf�gung.


/-------------------------------------------\
 
       www.Esoterikshopping.de
 
       Ihr esoterisches Kaufhaus
   mit hunderten von Karten-Decks
und tausenden esoterischen Artikeln

Ihr Esoterikshopping Team
Spart-Bares GbR
Gesellschafter: H.Klinzmann und U. Dittrich
Humboldtstr. 134
90459 N�rnberg
Tel. 08404 - 939 264
Fax. 08404 - 939 265

Telefonisch erreichbar:
Montag - Donnerstag: 09.00 - 17.00
Freitag: 09.00 - 15.00

email: info@esoterikshopping.de
www.esoterikshopping.de
Ust-ID: DE234465241

Bankverbindung:

Empf�nger: Esoterikshopping
Dresdner Bank N�rnberg
Kto: 154379901
Blz : 76080040

Aus dem Ausland:
BIC: DRES DE FF
IBAN: DE17 76080040 0154379901

�ber Paypal:
Paypal: paypal@spart-bares.de
 
\-------------------------------------------------/