<?php
/* -----------------------------------------------------------------------------------------
   $Id: products_new.php 1292 2005-10-07 16:10:55Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce 
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(products_new.php,v 1.25 2003/05/27); www.oscommerce.com 
   (c) 2003	 nextcommerce (products_new.php,v 1.16 2003/08/18); www.nextcommerce.org

   Released under the GNU General Public License 
   -----------------------------------------------------------------------------------------
   Third Party contributions:
   Enable_Disable_Categories 1.3        	Autor: Mikel Williams | mikel@ladykatcostumes.com

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

include ('includes/application_top.php');
// include needed function
require_once (DIR_FS_INC.'xtc_date_long.inc.php');
require_once (DIR_FS_INC.'xtc_get_vpe_name.inc.php');

$file = '';
$days = '';

define('MAX_DISPLAY_NEW_PRODUCTS_DAYS_OP', '60'); 

if (MAX_DISPLAY_NEW_PRODUCTS_DAYS_OP != '0') 
{
	$date_new_products = date("Y-m-d 00:00:00", mktime(1, 1, 1, date(m), date(d) - MAX_DISPLAY_NEW_PRODUCTS_DAYS_OP, date(Y)));
	$days = " and p.products_date_added > '".$date_new_products."' ";
}

//$schema = 'produkt_name;versand_infos;steuer_rate;produkt_beschreibung;produkt_preis;produkt_link;produkt_detail_button;grundpreis_name;grundpreis_value;produkt_bild'."\n";
$schema = '';

$products_new_export_query = xtc_db_query("select distinct
                                    p.products_id,
                                    pd.products_name,
                                    pd.products_description,
                                    p.products_image,
                                    p.products_price,
                               	    p.products_vpe,
                               	    p.products_vpe_status,
                                    p.products_vpe_value,                                                          
                                    p.products_tax_class_id,
                                    p.products_date_added,
									p.products_quantity, 
									p.products_shippingtime, 
									p.products_weight, 
                                    m.manufacturers_name
                                    from ".TABLE_PRODUCTS." p
                                    left join ".TABLE_MANUFACTURERS." m
                                    on p.manufacturers_id = m.manufacturers_id
                                    left join ".TABLE_PRODUCTS_DESCRIPTION." pd
                                    on p.products_id = pd.products_id,
                                    ".TABLE_CATEGORIES." c,
                                    ".TABLE_PRODUCTS_TO_CATEGORIES." p2c 
                                    WHERE pd.language_id = '".(int) $_SESSION['languages_id']."'
                                    and c.categories_status=1
                                    and p.products_id = p2c.products_id
                                    and c.categories_id = p2c.categories_id
                                    and p.products_status = '1'
									and p.products_quantity > 0 
                                    ".$group_check."
                                    ".$fsk_lock."                                    
                                    ".$days."
                                    order
                                    by
                                    p.products_date_added DESC");
									
while ($products_new_export = xtc_db_fetch_array($products_new_export_query))
{									
	$products_id = $products_new_export['products_id'];

	$blueg_link = bluegate_link($products_id);
	
	$products_name = utf8_encode($products_new_export['products_name']);
	
	$products_description = $products_new_export['products_description'];
	$products_description = utf8_encode($products_description);
    $products_description = strip_tags($products_description, '<br /><br>');
	$products_description = str_replace("'",", ",$products_description);
	$products_description = str_replace("\n"," ",$products_description);
	$products_description = str_replace("\r"," ",$products_description);
	$products_description = str_replace("\t"," ",$products_description);
	$products_description = str_replace("\v"," ",$products_description);
	$products_description = str_replace("&quot;","\"",$products_description);
	$products_description = str_replace("&qout;","\"",$products_description);
	$products_description = str_replace(chr(13)," ",$products_description);
	$products_description = preg_replace('/\s\s+/', ' ', $products_description);
	
	$products_price = $xtPrice->xtcGetPrice($products_new_export['products_id'], $format=false, 1, $products_new_export['products_tax_class_id'], '');
	
	if ($products_new_export['products_image'] != '') 
	{
		$products_image = 'http://www.esoterikshopping.de/'.DIR_WS_THUMBNAIL_IMAGES.$products_new_export['products_image'];
	} 
	else 
	{
		$products_image = '';
	}
	$tax_rate = $xtPrice->TAX[$products_new_export['products_tax_class_id']];
	$tax_info = $tax_rate.' %';

	$ship_info='inkl. '.$tax_info.' MwSt. zzgl. <span class="shipping-costs-eso">Versandkosten</span>';
	
	$detail_button = '<a href="'.$blueg_link.'">'.xtc_image_button('button_details.gif', 'Mehr Details zum Produkt').'</a>';

	$shipping_status_name = $main->getShippingStatusName($products_new_export['products_shippingtime']);
	$shipping_status_image = $main->getShippingStatusImage($products_new_export['products_shippingtime']);	
	
		
	// Grundpreis p_listing
	$gpe_query = xtc_db_query("SELECT products_grundpreis_einheit FROM ".TABLE_PRODUCTS." WHERE products_id = '".$products_new_export['products_id']."' LIMIT 1");
	while ($gpe_data = xtc_db_fetch_array($gpe_query))
	{ 
		$gpe_id = $gpe_data['products_grundpreis_einheit'];
	}
	
	if($gpe_id != '' && $gpe_id != '0')
	{

		$gpen_query = xtc_db_query("SELECT products_grundpreis_einheit_name FROM ".TABLE_PRODUCTS_GRUNDPREIS_EINHEIT." WHERE products_grundpreis_einheit_id = '".$gpe_id."'");
		if(xtc_db_num_rows($gpen_query) > 0)
		{
			while ($gpen_data = xtc_db_fetch_array($gpen_query))
			{ 
				$gpe_name = $gpen_data['products_grundpreis_einheit_name'];
				$gpe_name_tmp = $gpen_data['products_grundpreis_einheit_name'];
			}
		
			$gpe = explode(' ', $gpe_name_tmp);
			if($products_new_export['products_weight'] > 0)
			{
				$multiplicate = $gpe[0] / ($products_new_export['products_weight']);
			}
		}
		if($multiplicate > 0)
		{
			$grundpreis = $products_price*$multiplicate;
			$grundpreis = number_format($grundpreis,2,'.',''). " EUR";
		}
		
		// Grundpreis: ende
	}
	else
	{
		$gpe_name = '';
		$grundpreis = '';
	}
	
	$products_name = str_replace('"', '""', $products_name);
	$products_name = '"' . $products_name . '"';
	$products_name = $products_name.';';
	
	$ship_info = str_replace('"', '""', $ship_info);
	$ship_info = '"' . $ship_info . '"';
	$ship_info = $ship_info.';';
	
	$tax_info = str_replace('"', '""', $tax_info);
	$tax_info = '"' . $tax_info . '"';	
	$tax_info = $tax_info.';';	
	
	$products_description = str_replace("\r", '', $products_description);
	$products_description = str_replace("\n", '', $products_description);
	$products_description = preg_replace('/<\s*html[^>]*>/', '', $products_description);
	$products_description = preg_replace('/<\s*head[^>]*>/', '', $products_description);
	$products_description = preg_replace('/<\s*\/\s*html[^>]*>/', '', $products_description);
	$products_description = preg_replace('/<\s*\/\s*head[^>]*>/', '', $products_description);
	$products_description = str_replace('"', '""', $products_description);
	$products_description = '"' . $products_description . '"';	
	$products_description = $products_description.';';	
	
	$products_price = number_format($products_price,2,'.',''). " EUR";
	$products_price = str_replace('"', '""', $products_price);
	$products_price = '"' . $products_price . '"';
	$products_price = $products_price.';';	
	
	$blueg_link = str_replace('"', '""', $blueg_link);
	$blueg_link = '"' . $blueg_link . '"';
	$blueg_link = $blueg_link.';';	
	
	$detail_button = str_replace('"', '""', $detail_button);
	$detail_button = '"' . $detail_button . '"';
	$detail_button = $detail_button.';';	
	
	$gpe_name = str_replace('"', '""', $gpe_name);
	$gpe_name = '"' . $gpe_name . '"';
	$gpe_name = $gpe_name.';';	
	
	$grundpreis = str_replace('"', '""', $grundpreis);
	$grundpreis = '"' . $grundpreis . '"';
	$grundpreis = $grundpreis.';';	
	
	$products_image = str_replace('"', '""', $products_image);
	$products_image = '"' . $products_image . '"';
	$products_image = $products_image.';';	
	
	$products_id = str_replace('"', '""', $products_id);
	$products_id = '"' . $products_id . '"';
	$products_id = $products_id.';';	
	
	$schema .=		$products_name.
            		$ship_info.
            		$tax_info.
            		$products_description.
					$products_price.
					$blueg_link.
					$detail_button.
					$gpe_name.
					$grundpreis.
            		$products_image.
					$products_id.
					"\r\n";
}

function bluegate_link($p_id)
{
	$bluegate_query = xtc_db_query("select url_text from bluegate_seo_url where products_id = '".$p_id."' LIMIT 1");
	while ($bluegate_data = xtc_db_fetch_array($bluegate_query))
	{
		$link_bluegate_modul = 'http://www.esoterikshopping.de/products/'.$bluegate_data['url_text'].'.html';
	}
	return $link_bluegate_modul;
}

function cleanStrings($name) 
{
 	/*
	$search_array=array('&auml;','&Auml;','&ouml;','&Ouml;','&uuml;','&Uuml;','&szlig;','&#8482;','&copy;','&reg;','&deg;','&acute;', '&nbsp;');
 	$replace_array=array('ä','Ä','ö','Ö','ü','Ü','ß','™','©','®','°','´', ' ');
 	$name=str_replace($search_array,$replace_array,$name);   	
	*/
    return $name;
}	

$file = 'products_new_export.csv';
	
// create File
$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/' . $file, "w+");
fputs($fp, $schema);
fclose($fp);

?>