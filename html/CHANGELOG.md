##1.3.1
* Added improved error feedback
* Updated Lib
* Added changelog

##1.3.0
* Added special handling for credit cards without cvc
* Added cvc tooltip reflecting the changes
* Fixed several minor bugs
* Changed the german display name of direct debit from "elektronische Lastschrift" to "ELV"

##1.2.1
* Improved logging behavior

##1.2.0
* Added log view accessible from the admin panel
* Fixed several minor bugs

##1.1.0
* Updated lib
* Fixed a bug regarding umlauts in the translation snippets
* Fixed a bug regarding credit card icons being displayed in the wrong spot
* Fixed a bug regarding duplicate transactions
* Added improved error display

##1.0.7
* Fixed a bug regarding pre authorization
* Fixed a bug regarding 3-D Secure

##1.0.6
* Added show paymill logo as an option
* Added pre authorization as an option
* Updated lib
* Fixed several minor bugs

##1.0.5
* Updated lib
* Fixed several minor bugs

##1.0.4
* Reworked logging
* Added translations
* Updated lib
* Reworked the checkout process
* Adjusted css path
* Removed API and bridge url as options

##1.0.3
* Updated Lib
* Fixed a bug regarding 3-D Secure

##1.0.2
* Modified line endings

##1.0.1
* Added PAYMILL order status

##1.0.0
* Initial release