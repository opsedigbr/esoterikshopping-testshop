<?php
/* --------------------------------------------------------------
   $Id: advertising.php 1133 2005-08-07 07:47:07Z gwinger $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------


   Released under the GNU General Public License 
   --------------------------------------------------------------*/

  require('includes/application_top.php');

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
a.button_sim {
	padding:2px;
	border:1px solid #333;
	cursor: pointer;
	-moz-box-shadow:    3px 3px 3px #bbb;
	-webkit-box-shadow: 3px 3px 3px #bbb;
	box-shadow:         3px 3px 3px #bbb;
}
</style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo xtc_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMER; ?></td>
				<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMER_MAIL; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCT; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_DATE_ADDED; ?></td>
                <td class="dataTableHeadingContent" align="right">L&ouml;schen?</td>
              </tr>
<?php

if($_GET['delete'] == 'true' && !empty($_GET['remind_id']))
{
	$cr_delete = xtc_db_query("DELETE FROM ".TABLE_CUSTOMERS_REMIND." WHERE remind_id = '".$_GET['remind_id']."' LIMIT 1");
}

  $customers_remind_query = xtc_db_query("select * from ".TABLE_CUSTOMERS_REMIND." order by remind_date_added desc");

  while ($customers_remind = xtc_db_fetch_array($customers_remind_query)) {

?>                                
                <td height="27" class="dataTableContent" ><?php 
				
				if($customers_remind['customers_id'] == '0') { ?>
					<font color="#FF0000"> <?php echo $customers_remind['customers_firstname']." ".$customers_remind['customers_lastname']; ?> </font>
				<?php } else { ?>
					<font color="#00CC33"> <?php echo $customers_remind['customers_firstname']." ".$customers_remind['customers_lastname']." [".$customers_remind['customers_id']."]"; ?> </font>
				<?php } ?>
				
				 &nbsp;</td>
				<td class="dataTableContent"><?php echo $customers_remind['customers_email_address']; ?>&nbsp;</td>
				<td class="dataTableContent"><?php echo '<a target="_blank" style="text-decoration:underline;color:blue;" href="http://www.esoterikshopping.de/product_info.php?products_id='.$customers_remind['products_id'].'">'.$customers_remind['products_name'].'</a>'; ?></td>
                <td class="dataTableContent" align="right"><?php echo date('d.m.Y H:i:s', strtotime($customers_remind['remind_date_added'])); ?></td>
                <td class="dataTableContent" align="right"><?php echo'<a title="L&ouml;schen" class="button_sim" href="?delete=true&remind_id='.$customers_remind['remind_id'].'">L&ouml;schen</a>'; ?></td>
              </tr>
			<?php
			  }
			?>
            </table></td>
          </tr>
        </table></td>
      </tr>
     <tr>
       <td align="right" style="font-family: Verdana, Arial, sans-serif; font-size: 10px;"><font color="#FF0000">nicht registrierter Kunde</font> | <font color="#00CC33">registrierter Kunde [Kundennummer]</font></td>
     </tr>
	 <tr>
       <td align="right">&nbsp;</td>
     </tr>
	 <tr>
       <td align="right" style="font-family: Verdana, Arial, sans-serif; font-size: 10px;">Kunden, die sich f�r eine Produkterinnerung eingetragen haben<br>bekommen eine automatische eMail-Benachrichtigung,<br>sobald der gew�nschte Artikel wieder auf Lager ist.</td>
     </tr>
    </table>
   </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>