<?php
define('CHECK_DATE_OF_BIRTH', 'false');
$error = false;

require('includes/application_top.php');
// include needed functions
require_once(DIR_FS_INC .'xtc_get_order_data.inc.php');
require_once(DIR_FS_INC .'xtc_get_attributes_model.inc.php');
require_once(DIR_FS_INC .'xtc_not_null.inc.php');
require_once(DIR_FS_INC .'xtc_format_price_order.inc.php');

include(DIR_WS_CLASSES . 'order.php');
$order = new order($_GET['oID']);

$oId = (int)$_GET['oID'];

function showCustomersData($custDataArray) {
    foreach($custDataArray as $customerData) {
        echo $customerData."<br />";
    }
}

function sendButton($orderId) {
    echo '<a style="position:relative;
  top:15px;
  padding:10px;
  background-color: #F1F1F1;
  border: 1px solid #000000;
  color: #000000;
  font-family: Verdana,Arial,sans-serif;
  font-size: 12px;
  font-weight: bold;
  line-height: 24px;
  text-decoration: none;" href="'.$_SERVER['PHP_SELF'].'?oID='.$orderId.'&send=1">Bonit&auml;t jetzt pr&uuml;fen?</a>';
}

function closeButton() {
    echo '<a class="ts-close-btn" style="position:relative;top:25px;padding:10px;background-color:red;
    border: 1px solid #666666;
    color: #ffffff;
    font-family: Verdana,Arial,sans-serif;
    font-size: 12px;
    font-weight: bold;
    line-height: 24px;
    text-decoration: none;" href="javascript:opener.location.reload();window.close()">Fenster schliessen!</a>';
}

function formatDateTime($d) {
    $d = trim($d);
    if(!empty($d)) {
        $d = explode(' ', $d);
        $dn = $d[0];
        $dn = explode('-', $dn);
        $dateTime = $dn[2].'.'.$dn[1].'.'.$dn[0];
    } 
    else {
        $dateTime = '';
    }
    return $dateTime;
}

$orderQuery = xtc_db_query("SELECT * FROM ".TABLE_ORDERS." WHERE orders_id='".$oId."' LIMIT 1");
$orderData = xtc_db_fetch_array($orderQuery);
$orderDataArray = array('orders_id' => $orderData['orders_id'], 
                        'customers_id' => $orderData['customers_id'], 
                        'billing_firstname' => $orderData['billing_firstname'],
                        'billing_lastname' => $orderData['billing_lastname'],
                        'billing_company' => $orderData['billing_company'],
                        'billing_street_address' => $orderData['billing_street_address'],
                        'billing_city' => $orderData['billing_city'],
                        'billing_postcode' => $orderData['billing_postcode'],
                        'billing_country' => $orderData['billing_country'],
                        'customers_email_address' => $orderData['customers_email_address']);


$showPreviewArray = array($orderData['billing_firstname'].' '.$orderData['billing_lastname'], 
                          $orderData['billing_street_address'], 
                          $orderData['billing_postcode'].' '.$orderData['billing_city'], 
                          $orderData['billing_country'],
                          $orderData['customers_email_address']);

// Wichtig f�r Datenbankeintr�ge (!!!)
$cId = $orderDataArray['customers_id'];

$customersQuery = xtc_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id ='".$orderDataArray['customers_id']."' LIMIT 1");
$customersData = xtc_db_fetch_array($customersQuery);
$customersDataArray = array('customers_dob' => $customersData['customers_dob']);



if(CHECK_DATE_OF_BIRTH == 'false') {
    $customersDataArray['customers_dob'] = '';
}


/* We also allow hexadecimal characters, because some servers do not have the right locale settings.
   C4 = �, E4 = �, D6 = �, F6 = �, DC = �, FC = �, DF = �
*/
if (!preg_match('/^([\w\xDF\xC4\xE4\xD6\xF6\xDC\xFC\-\. ]+?)[,]?[\/]?[ ]?(\d+ ?[a-zA-Z]?(?: ?[-\/] ?\d+ ?[a-zA-Z]?)*)$/', $orderDataArray['billing_street_address'], $matches)) {
    $street = $orderDataArray['billing_street_address'];
    $streetArray = array('street' => $street, 'houseNumber' => '');
}
else {
    $streetArray = array('street' => $matches[1], 'houseNumber' => preg_replace('/\s/', '', strtolower($matches[2])));
}

//$streetArray['houseNumber'] = '';

if(empty($streetArray['houseNumber'])) {
    $error = true;
    $message = "<li>Hausnummer konnte nicht ermittelt werden!</li>";
}

if(isset($_GET['send']) && $_GET['send'] == '1') { // Button Send

    if(isset($error) && $error === false) {

        /**
         * Diese Datei enth�lt ein Beispiel-Skript zur Abfrage einer Privatpersonen Bonit�tsauskunft �ber mediafinanz mit
         * PHP 5. Das Skript hat lediglich exemplarischen Charakter und diet keinesfalls als vollst�ndige Funktions-Referenz!
         * Die SOAP-Funktionalit�t steht nur dann zur Verf�gung, wenn PHP mit der Option --enable-soap konfiguriert wurde.
         */

        //Diese Funktion wird sp�ter verwendet, um alle Daten UTF8-codiert zu �bergeben:
        function makeUtf8(&$item, $key) {
            $item = utf8_encode($item);
        }



        //Lizenz-Schl�ssel der Applikation:
        $applicationLicence = '89c407285187e79847fd5631af9a19e5';

        //Lizenz-Schl�ssel des Mandanten:
        $clientLicence = 'c51848bffdbd5e81cdea5b0ae404fb45';

        //Mandanten-ID:
        $clientId = 33587;


        //Aus Applikations-Lizenz und Mandanten-Lizenz den Lizenzschl�ssel f�r die Autorisierung bilden:
        $licenceKey = md5($applicationLicence.$clientLicence);

        //Array f�r die Authentifizierung bauen:
        $auth = array(
            'clientId'   => $clientId,
            'licenceKey' => $licenceKey,
            'sandbox'    => false); //TRUE = Testbetrieb, FALSE = Produktivbetrieb

        //Optionen f�r das SoapClient-Objekt:
        $options = array(
            'trace'       => 1,     //so k�nnen sp�ter u.a. die Methoden __getLastRequest() und __getLastResponse() aufgerufen werden
            'compression' => true,  //Daten werden komprimiert zum Server �bertragen
            'exceptions'  => true); //SOAP-Faults werden als Exception weitergereicht


        //Nach der Erstellung eines SoapClient-Objekts mittels WSDL-Dokument, stehen die vom Web-Service
        //angebotenen Funktionen direkt als Methoden des Objektes zur Verf�gung:
        $soapClient = new SoapClient('https://soap.mediafinanz.de/buergel200.wsdl', $options);

        //Array mit Personen-Daten bauen:
        $suspect = array(
            'firstname'   => $orderDataArray['billing_firstname'],  //Vorname
            'lastname'    => $orderDataArray['billing_lastname'],   //Nachname
            'street'      => $streetArray['street'],                  //Stra�e
            'houseNumber' => $streetArray['houseNumber'],             //Hausnummer
            'postcode'    => $orderDataArray['billing_postcode'],   //PLZ
            'city'        => $orderDataArray['billing_city'],       //Ort
            'dateOfBirth' => formatDateTime($customersDataArray['customers_dob'])); //Geburtsdatum


        //Berechtigtes Interesse f�r die Bonit�tsabfrage:
        //M�gliche Werte:
        //- 1: Kreditanfrage
        //- 2: Gesch�ftsanbahnung
        //- 3: Bonit�tspr�fung
        //- 4: Forderung
        $justification = 3;

        //optionale Referenz, um die Anfrage besser zuordnen zu k�nnen
        $reference = $_SERVER['SERVER_NAME'];

        //alle Elemente in $suspect utf8-codieren:
        array_walk($suspect, 'makeUtf8');


        try
        {
            //Web-Service - Funktion zur �bergabe einer neuen Forderung aufrufen:
            $result = $soapClient->getConCheckScore($auth, $suspect, $justification, $reference);

            //R�ckgabewert auswerten:
            if (!empty($result->scoreResult))
            {
                echo "Die Bonit�tsauskunft wurde erfolgreich eingeholt<br />\n";

                //Details der Auskunft ausgeben:
                echo "Score: " . $result->scoreResult->score . "<br />\n";
                echo "Erkl�rung: " . utf8_decode($result->scoreResult->scoreText) . "<br />\n";

                echo "Adresse: <br />";
                echo utf8_decode($result->scoreResult->firstname) . " ".utf8_decode($result->scoreResult->lastname) . "<br />";
                echo utf8_decode($result->scoreResult->address->street) . " " . $result->scoreResult->address->houseNumber . "<br />";
                echo $result->scoreResult->address->postcode . ' ' . utf8_decode($result->scoreResult->address->city) . "<br />";

                echo "Adressherkunft: <br />";
                utf8_decode($result->scoreResult->address->originText) . "<br />";

                if (isset($result->scoreResult->reference))
                {
                    echo "Referenz: <br />";
                    echo utf8_decode($result->scoreResult->reference) . "<br />";
                }

                $negativeEntries  = isset($result->scoreResult->countOfNegativeEntries)  ? $result->scoreResult->countOfNegativeEntries  : 0;
                $relatedCompanies = isset($result->scoreResult->countOfRelatedCompanies) ? $result->scoreResult->countOfRelatedCompanies : 0;

                echo "Es sind $negativeEntries Negativmerkmale verf�gbar<br />\n";
                echo "Es gibt $relatedCompanies mit der Person in Beziehung stehende Firmen<br />\n";
                //Die Details der Negativmerkmale werden in diesem Demo-Skript nicht dargestellt!

                $datum = date("d.m.Y");
                $credit_rating = $datum.' - '.$result->scoreResult->score.' - '.utf8_decode($result->scoreResult->scoreText);
                xtc_db_query("UPDATE ".TABLE_ORDERS." SET buergel_bonitaet = 1 WHERE orders_id = '".$oId."' LIMIT 1");
                xtc_db_query("UPDATE ".TABLE_CUSTOMERS." SET credit_rating = '".$credit_rating."' WHERE customers_id = '".$cId."' LIMIT 1");

                closeButton();
            }
            else
            {
                echo "Die Bonit�tsauskunft konnte nicht eingeholt werden!";

                //alle Fehler ausgeben:
                foreach ($result->errorList as $errorMsg)
                {
                	echo "Fehler: ".$errorMsg."<br />";
                }

                echo $soapClient->__getLastRequest();
                echo $soapClient->__getLastResponse();
            }
        }
        catch (Exception $e)
        {
            echo "Es ist folgender Fehler aufgetreten: " . $e->getMessage();
        }
    }

    else {
        echo "<h1>Fehler</h1>";
        echo "<ul>";
        echo $message;
        echo "</ul>";
    }
} // if (Button Send)

else { // Button not Send
    showCustomersData($showPreviewArray);
    sendButton($oId, $toMailAdress);
}