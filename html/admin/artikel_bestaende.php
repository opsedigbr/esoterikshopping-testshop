<?php

/* --------------------------------------------------------------
   $Id: start.php 1235 2005-09-21 19:11:43Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project 
   (c) 2002-2003 osCommerce coding standards (a typical file) www.oscommerce.com
   (c) 2003      nextcommerce (start.php,1.5 2004/03/17); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

require ('includes/application_top.php');

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
.h2 {
  font-family: Trebuchet MS,Palatino,Times New Roman,serif;
  font-size: 13pt;
  font-weight: bold;
}

.h3 {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 9pt;
  font-weight: bold;
}
a.button_sim {
	padding:5px;
	border:1px solid #333;
	cursor: pointer;
	-moz-box-shadow:    3px 3px 3px #bbb;
	-webkit-box-shadow: 3px 3px 3px #bbb;
	box-shadow:         3px 3px 3px #bbb;
}
table {
	font-size:12px;
	font-family: arial;
}
td.rahmen {
	border:1px solid #666;
}
</style> 

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
	    <td class="pageHeading">Bestand < 1</td>
		</tr>
		</table>
        <br />
         <table border="0" width="300" cellspacing="0" cellpadding="0">
        <tr>
	    <td width="50%"><a href="?action=aktiv" class="button_sim">Aktive Artikel</a></td>
	    <td width="50%"><a href="?action=inaktiv" class="button_sim">Inaktive Artikel</a></td>
		</tr>
		</table>
        
        </td>
      </tr>
      <tr>
        <td>
        <?php include(DIR_WS_MODULES.FILENAME_SECURITY_CHECK); ?>
              
<br />  

<?php
if($_GET['action'] && $_GET['action'] != '')
{
	$p_status = '';

	switch($_GET['action'])
	{
		case 'aktiv':
			$p_status = '1';
			break;
		
		case 'inaktiv':
			$p_status = '0';
			break;
	}
	
	if($p_status != '')
	{
		$output_string = '<h2>'.strtoupper($_GET['action']).'</h2>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
		<td width="33%" class="rahmen"><strong>Artikelnummer</strong></td>
    	<td width="33%" class="rahmen"><strong>Artikelname</strong></td>
	    <td width="34%" class="rahmen"><strong>Vormerkungen / Produkterinnerung</strong></td>
		</tr>
		</table>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="2">';
		
		$bestand_query = xtc_db_query("SELECT * FROM products p, products_description pd WHERE p.products_id = pd.products_id and p.products_quantity < '1' and p.products_status = '".(int)$p_status."' order by p.products_model ASC");
		$rows_product = xtc_db_num_rows($bestand_query);
		while($bestand = xtc_db_fetch_array($bestand_query))
		{
			$products_id		= $bestand['products_id'];
			$products_model		= $bestand['products_model'];
			$products_name		= $bestand['products_name'];
			
			$cr_query = xtc_db_query("SELECT * FROM customers_remind WHERE products_id = '".(int)$products_id."'");
			// zählt die Einträge
			$cr_product = xtc_db_num_rows($cr_query);
			($cr_product > 0) ? $style='style="font-weight:bold; color:red;"' : $style='';
						
			$output_string.= '<tr>';
			$output_string.= '<td width="33%" class="rahmen" '.$style.'>'.$products_model.'</td>';
			$output_string.= '<td width="33%" class="rahmen" '.$style.'>'.$products_name.'</td>';
			($cr_product=='1') ? $output_string.= '<td width="34%" class="rahmen" '.$style.'>'.$cr_product.' Person</td>' : $output_string.= '<td width="34%" class="rahmen" '.$style.'>'.$cr_product.' Personen</td>';
			$output_string.= '</tr>';
		}
		$output_string.= '</table>';
	}
}

echo $output_string;

if($rows_product > 0)
	echo '<br /><br />'.$rows_product.' Eintr&auml;ge';

		
?>

     <tr>
      <td style="border: 0px solid; border-color: #ffffff;">
</td>
      </tr>		 
  
  
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>