<?php

/* --------------------------------------------------------------
   $Id: start.php 1235 2005-09-21 19:11:43Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project 
   (c) 2002-2003 osCommerce coding standards (a typical file) www.oscommerce.com
   (c) 2003      nextcommerce (start.php,1.5 2004/03/17); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

require ('includes/application_top.php');
//require_once 'includes/modules/carp/carp.php';

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
.h2 {
  font-family: Trebuchet MS,Palatino,Times New Roman,serif;
  font-size: 13pt;
  font-weight: bold;
}

.h3 {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 9pt;
  font-weight: bold;
}
</style> 

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td class="pageHeading">Umsatz-Statistik</td>
  </tr>
  <tr>
    <td class="main" valign="top">Und los...
</td>
  </tr>
</table></td>
      </tr>
      <tr>
        <td>
        <?php include(DIR_WS_MODULES.FILENAME_SECURITY_CHECK); ?>
              
<br />  
<table width="1000" border="1">
  <tr>
    <td><strong>Umsatzstatistik:</strong></td>
  </tr>
</table>
<table width="1000" border="1">
  <tr>
    <td width="20%"><strong>Zeitraum:</strong></td>
    <td width="20%"><strong>Bestellungen (bereinigt):</strong></td>
    <td width="20%"><strong>Nettoumsatz (bereinigt):</strong></td>
    <td width="20%"><strong>pro Bestellung (bereinigt):</strong></td>
    <td width="20%"><strong>Stornos:</strong></td>
  </tr>
<?php
$start_jahr = '2011';
$h_jahr = date('Y');
$h_monat = date('n');
$h_tag = date('d');

$month = array(	'1' => 'Januar',
				'2' => 'Februar',
				'3' => 'M&auml;rz',
				'4' => 'April',
				'5' => 'Mai',
				'6' => 'Juni',
				'7' => 'Juli',
				'8' => 'August',
				'9' => 'September',
				'10' => 'Oktober',
				'11' => 'November',
				'12' => 'Dezember');

?>

<?php
function calc_umsatz($jahr, $monat)
{
	$summe = '';
	$steuer = '';
	$versandkosten = '';
	$rabatt = '';
			
	if(strlen($monat) < 2) 
	{
		$monat = '0'.$monat;
	}
	
	$anfang_datum = $jahr.'-'.$monat.'-01 00:00:00';
	$end_datum = $jahr.'-'.$monat.'-31 23:59:59';
	
	$orders_query = xtc_db_query("select orders_id from " .TABLE_ORDERS. " WHERE date_purchased > '".$anfang_datum."' and date_purchased < '".$end_datum."' AND orders_status = '3'");
	while($orders = xtc_db_fetch_array($orders_query))
	{
		$count_orders++;
		
		$ot_query = xtc_db_query("select * from " .TABLE_ORDERS_TOTAL. " WHERE orders_id = '".$orders['orders_id']."'");
		while($ot = xtc_db_fetch_array($ot_query))
		{
			if($ot['class'] == 'ot_total')
			{
				$summe = $ot['value'];
			}
			if($ot['class'] == 'ot_tax')
			{
				if($steuer != '')
				{
					$steuer = $steuer+$steuer;
				}
				else
				{
					$steuer = $ot['value'];
				}
			}
			if($ot['class'] == 'ot_shipping')
			{
				$versandkosten = $ot['value'];
			}
			if($ot['class'] == 'ot_coupon')
			{
				$rabatt = $ot['value'];
			}			
			$endsumme = $endsumme + ($summe - ($steuer+$versandkosten+$rabatt));
			$summe = '';
			$steuer = '';
			$versandkosten = '';
			$rabatt = '';
		}
	}	
	return $endsumme; 
}

function n_format($number)
{
	return number_format($number,2,",",".");
}

function anzahl_best($jahr, $monat)
{
	if(strlen($monat) < 2) 
	{
		$monat = '0'.$monat;
	}
	
	$anfang_datum = $jahr.'-'.$monat.'-01 00:00:00';
	$end_datum = $jahr.'-'.$monat.'-31 23:59:59';
	
	$orders_query = xtc_db_query("select orders_id from " .TABLE_ORDERS. " WHERE date_purchased > '".$anfang_datum."' and date_purchased < '".$end_datum."' AND orders_status != '4'");
	while($orders = xtc_db_fetch_array($orders_query))
	{
		$count_orders++;
	}
	if(empty($count_orders) || ($count_orders == '') || ($count_orders == '0'))
	{
		$count_orders = '0';
	}
	return $count_orders;
}

function anzahl_stornos($jahr, $monat)
{
	if(strlen($monat) < 2) 
	{
		$monat = '0'.$monat;
	}
	
	$anfang_datum = $jahr.'-'.$monat.'-01 00:00:00';
	$end_datum = $jahr.'-'.$monat.'-31 23:59:59';
	
	$orders_query = xtc_db_query("select orders_id from " .TABLE_ORDERS. " WHERE date_purchased > '".$anfang_datum."' and date_purchased < '".$end_datum."' AND orders_status = '4'");
	while($orders = xtc_db_fetch_array($orders_query))
	{
		$count_stornos++;
	}
	if(empty($count_stornos) || ($count_stornos == '') || ($count_stornos == '0'))
	{
		$count_stornos = '0';
	}
	return $count_stornos;
}

while($h_jahr > $start_jahr)
{
	for($i=1;$i<=12;$i++)
	{
		echo '<tr>';
		echo '<td>'.$month[$i].' '.$start_jahr.'</td><td>'.anzahl_best($start_jahr, $i).'</td><td>'.n_format(calc_umsatz($start_jahr, $i)).' &euro;</td><td>'.n_format(calc_umsatz($start_jahr, $i)/anzahl_best($start_jahr, $i)).' &euro;</td><td>'.anzahl_stornos($start_jahr, $i).'</td>';
		echo '</tr>';
	}
	$start_jahr++;
}

if($h_jahr == $start_jahr)
{
	for($i=1;$i<=$h_monat;$i++)
	{
		$ab = anzahl_best($start_jahr, $i);
		($ab < 1) ? $ab = 1 : $ab=$ab;
		echo '<tr>';
		echo '<td>'.$month[$i].' '.$start_jahr.'</td><td>'.anzahl_best($start_jahr, $i).'</td><td>'.n_format(calc_umsatz($start_jahr, $i)).' &euro;</td><td>'.n_format(calc_umsatz($start_jahr, $i)/$ab).' &euro;</td><td>'.anzahl_stornos($start_jahr, $i).'</td>';
		echo '</tr>';
	}
}
echo '</table>';
?>

     <tr>
      <td style="border: 0px solid; border-color: #ffffff;">
</td>
      </tr>		 
  
  
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>