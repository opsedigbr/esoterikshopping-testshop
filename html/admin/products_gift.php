<?php
/*
 *  Modul: Products Gift v1.0
 *  created by Sergej Stroh.
 *	www.southbridge.de
 *	28.11.2005
 *	Copyright 2005 by Sergej Stroh.
 *
 * 	NEU: Kundengruppenabh�ngig
 */
 
  require('includes/application_top.php');
	require_once(DIR_FS_INC.'xtc_get_tax_rate.inc.php');
	require_once(DIR_WS_CLASSES.'currencies.php');
	
	$price_currencies = new currencies();

	// inaktive produkte automatisch beim Aufruf l�schen!
	$deleteNotActiveProductsQuery = xtc_db_query("SELECT products_id, products_gift_id FROM products_gift ORDER BY products_gift_id ASC;");
	while($deleteNotActiveProductsData = xtc_db_fetch_array($deleteNotActiveProductsQuery)) {
		$deleteProductsId = (int)$deleteNotActiveProductsData['products_id'];
		$deleteProductsGiftId = (int)$deleteNotActiveProductsData['products_gift_id'];

		$sql = xtc_db_query("SELECT products_id FROM products WHERE products_id = $deleteProductsId;");
		if(xtc_db_num_rows($sql) < 1) {
 			xtc_db_query("DELETE FROM ".TABLE_PRODUCTS_GIFT." WHERE products_gift_id = '".$deleteProductsGiftId."';");
		}
	}

	
// kundengruppen auslesen
	$customers_group_query = xtc_db_query("SELECT customers_status_id, customers_status_name 
		FROM ".TABLE_CUSTOMERS_STATUS." 
		WHERE language_id = '".(int)$_SESSION['languages_id']."'
		ORDER BY customers_status_id ASC");
	$count_group = xtc_db_num_rows($customers_group_query);	
	

  switch ($_GET['action']) {
    case 'delete': 
      xtc_db_query("DELETE FROM ".TABLE_PRODUCTS_GIFT." WHERE products_gift_id = '".(int)$_GET['id']."'");
			xtc_redirect(xtc_href_link(FILENAME_PRODUCTS_GIFT));
    break;	
		case 'update': 
			xtc_db_query("UPDATE ".TABLE_PRODUCTS_GIFT." SET 
				products_gift_sum = '".$_POST['products_gift_sum']."' 
				WHERE products_gift_id = '".(int)$_POST['products_gift_id']."'");
			xtc_redirect(xtc_href_link(FILENAME_PRODUCTS_GIFT));
		break;
    case 'new':
			// Kundengruppen einlesen
			$groups_anz = count($_POST['groups']);
			$groups_list = '';
			
			// Beim letzten element, keine Kommata
			for($i = 0; $i < $groups_anz; $i++){
				$groups_list .= $_POST['groups'][$i].($i+1 != $groups_anz ? ',' : '');
			}
			
			$products_gift_array = array(
				'products_id' 				=> xtc_db_prepare_input($_POST['products_gift']),
				'products_gift_sum' 	=> xtc_db_prepare_input($_POST['products_gift_sum']),
				'customers_groups' 		=> xtc_db_prepare_input($groups_list));
							
			xtc_db_perform(TABLE_PRODUCTS_GIFT, $products_gift_array);
			xtc_redirect(xtc_href_link(FILENAME_PRODUCTS_GIFT));
		break;
		default: // alle anzeigen
		// alle gratisartikel einlesen
		$products_gift = "SELECT 
				p.products_id, 
				p.products_price,
				p.products_tax_class_id,
				pd.products_name
			FROM ".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd
			WHERE p.products_gift = '1'
			AND p.products_id = pd.products_id
			AND pd.language_id = '".(int)$_SESSION['languages_id']."'
			ORDER BY p.products_id ASC";
			
		$products_gift_query = xtc_db_query($products_gift);
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>" />
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top">

      <table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
      <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
      </table>

    </td>
    <td class="boxCenter" width="100%" valign="top">

<!-- titel -->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="80" rowspan="2"><?php echo xtc_image(DIR_WS_ICONS.'heading_content.gif'); ?></td>
    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
  </tr>
  <tr>
    <td class="main" valign="top">XTC Tools</td>
  </tr>
</table>
<!-- titel eof -->
<table width="100%" border="0" cellspacing="1" cellpadding="2">
  <tr>
    <td class="main"><?php echo CONTENT_NOTE; ?></td>
  </tr>
</table>
<br>
<!-- auswahl -->
<?php
// pulldown bilden
	$products_array[] = array('id' => '', 'text' => TEXT_SELECT);
	while($products_gift = xtc_db_fetch_array($products_gift_query)){
		// pr�fen welche artikel bereits in der tabelle eingetragen sind und aus dem dropdown entfernen
		$products_gift_table_true = "SELECT products_gift_id, products_id
			FROM ".TABLE_PRODUCTS_GIFT." WHERE products_id = '".$products_gift['products_id']."'";
			
		$products_gift_table_true_query = xtc_db_query($products_gift_table_true);
		$products_gift_true = xtc_db_fetch_array($products_gift_table_true_query);
		
		if($products_gift_true['products_id'] != $products_gift['products_id']){
		// mwst. dazu rechnen
		$price_mwst = ($products_gift['products_price'] * (xtc_get_tax_rate($products_gift['products_tax_class_id']) / 100) + $products_gift['products_price']);
		$price_round = round($price_mwst, PRICE_PRECISION);
		
		$products_array[] = array(
			'id' => $products_gift['products_id'],
			'text' => $products_gift['products_id'].': '.$products_gift['products_name'].' - '.$price_round);
		}	
	}

	// Exception

?>
<?php echo xtc_draw_form('products_gift', FILENAME_PRODUCTS_GIFT, 'action=new', 'post','');?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr>
		<td class="header_gift" width="20%"><?php echo PRODUCTS_NAME; ?></td>
    <td class="header_gift" width="30%"><?php echo xtc_draw_pull_down_menu('products_gift', $products_array, '', 'style="width:300px;"'); ?></td>
		<td class="header_gift" width="15%"><?php echo PRODUCTS_GIFT_SUM; ?></td>
    <td class="header_gift" width="15%"><?php echo  xtc_draw_input_field('products_gift_sum', '', 'size="10"'); ?></td>
		<td class="header_gift"><?php echo '<input type="submit" class="button" onClick="this.blur();" value="' . BUTTON_SAVE . '"/>'; ?></td>
  </tr>
	<tr>
		<td class="header_gift"><?php echo PRODUCTS_GIFT_GROUP; ?></td>
		<td class="header_gift" colspan="4">
<table border="0">
	<tr>
			
<?php 
	while($customers_group = xtc_db_fetch_array($customers_group_query)){
		echo '<td>'.xtc_draw_selection_field('groups[]', 'checkbox', $customers_group['customers_status_id']) .'</td><td>'. $customers_group['customers_status_name'].'</td>';
	}	
?>
		
	</tr>
</table>	
		</td>
	</tr>
</table>	
</form>
<br>

<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center">
  <tr>
    <td class="header_gift" width="5%" align="center"><?php echo PRODUCTS_GIFT_ID; ?></td>
    <td class="header_gift" width="60%"><?php echo PRODUCTS_NAME; ?></td>
		<td class="header_gift" width="15%"><?php echo PRODUCTS_GIFT_SUM; ?></td>
    <td class="header_gift" width="10%" colspan="2"><?php echo PRODUCTS_GIFT_ACTION; ?></td>
  </tr>
<?php
// eingestellte Artikel anzeigen
	$products_gift_table = "SELECT 
				pg.products_gift_id, 
				pg.products_gift_sum,
				pg.customers_groups,
				p.products_tax_class_id,
				pd.products_name
			FROM ".TABLE_PRODUCTS." p,
				".TABLE_PRODUCTS_DESCRIPTION." pd,
				".TABLE_PRODUCTS_GIFT." pg
			WHERE pg.products_id = p.products_id
			AND p.products_id = pd.products_id
			AND pd.language_id = '".(int)$_SESSION['languages_id']."'
			ORDER BY pg.products_gift_sum ASC";
			
	$products_gift_table_query = xtc_db_query($products_gift_table);
	$i = 0;
  while($products_gift = xtc_db_fetch_array($products_gift_table_query)) {
?>
<?php echo xtc_draw_form('products_gift', FILENAME_PRODUCTS_GIFT, 'action=update', 'post','');?>
  <tr bgcolor="<?php echo ($i%2 ? '#eeeeee':'#f2f2f2')?>">
    <td class="content_gift" align="center"><?php echo $products_gift['products_gift_id'];?></td>
    <td class="content_gift">
<?php 
	echo $products_gift['products_name'] . xtc_draw_hidden_field('products_gift_id',$products_gift['products_gift_id']);
	echo '<br><span class="extra_fast">'.PRODUCTS_GIFT_GROUP.'</span>';
	$group_list = split('[,]', $products_gift['customers_groups']);
	$group_list_count = sizeof($group_list);
	
	for($a = 0; $a < $group_list_count; $a++){
		// Gruppennamen holen
		$current_customers_group_query = xtc_db_query("SELECT customers_status_name FROM ".TABLE_CUSTOMERS_STATUS." 
			WHERE customers_status_id = '".$group_list[$a]."' AND language_id = '".(int)$_SESSION['languages_id']."'");
		$current_customers_group = xtc_db_fetch_array($current_customers_group_query);	
		
		echo $current_customers_group['customers_status_name'].', ';
	}
?>
		</td>
		<td class="content_gift"><?php echo xtc_draw_input_field('products_gift_sum', $products_gift['products_gift_sum'], 'size="10"'); ?></td>
    <td class="content_gift" align="center"><?php echo '<input type="image" src="images/icons/edit_uni.gif" value="'.PRODUCTS_GIFT_SUM_UPDATE.'">';?></td>
		<td class="content_gift" align="center"><a href="<?php echo xtc_href_link(FILENAME_PRODUCTS_GIFT,'action=delete&amp;id='.$products_gift['products_gift_id']).'">'.xtc_image(DIR_WS_IMAGES.'icons/delete_uni.gif', PRODUCTS_GIFT_DELETE).'</a>';?></td>
  </tr>
</form>	
<?php
	$i++;
  }
?>
</table>

</body>
</html>