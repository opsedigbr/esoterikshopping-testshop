<?php

/* --------------------------------------------------------------
   $Id: start.php 1235 2005-09-21 19:11:43Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project 
   (c) 2002-2003 osCommerce coding standards (a typical file) www.oscommerce.com
   (c) 2003      nextcommerce (start.php,1.5 2004/03/17); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

require ('includes/application_top.php');

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
.h2 {
  font-family: Trebuchet MS,Palatino,Times New Roman,serif;
  font-size: 13pt;
  font-weight: bold;
}

.h3 {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 9pt;
  font-weight: bold;
}
a.button_sim {
	padding:5px;
	border:1px solid #333;
	cursor: pointer;
	-moz-box-shadow:    3px 3px 3px #bbb;
	-webkit-box-shadow: 3px 3px 3px #bbb;
	box-shadow:         3px 3px 3px #bbb;
}
table {
	font-size:12px;
	font-family: arial;
}
td.rahmen {
	border:1px solid #666;
}
</style> 

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
	    <td class="pageHeading">Newsletterkunden</td>
		</tr>
		</table>
        <br />
<?php
function ermittleAlter($b_string)
{
	$b_string = explode(' ', $b_string);
	$b_string = explode('-', $b_string[0]);
	$year_cust = $b_string[0];
	$month_cust = $b_string[1];
	$day_cust = $b_string[2];
	$h_jahr = date('Y');
	$h_monat = date('m');
	$h_tag = date('d');
	if($month_cust > $h_monat)
	{
		$abgleich_alter = '1';
	}
	elseif($month_cust < $h_monat)
	{
		$abgleich_alter = '0';
	}
	elseif($month_cust == $h_monat)
	{
		if($day_cust > $h_tag)
		{
			$abgleich_alter = '1';
		}
		elseif($day_cust < $h_tag)
		{
			$abgleich_alter = '0';
		}
		elseif($day_cust == $h_tag)
		{
			$abgleich_alter = '0';
		}		
		else
		{
			$alter = 'Fehler';
			return $alter;
		}				
	}
	else
	{
		$alter = 'Fehler';
		return $alter;
	}
	$alter = $h_jahr - $year_cust - $abgleich_alter;
	return $alter;
}
	

$kg[2] = 'Normale Kunden';
$kg[3] = 'H&auml;ndler';
$kg[5] = 'Firmenkunden';
$output = '';
$ordered_products_id = '';
$limit = 'LIMIT 0,1000';
##$limit = '';
?>        
        <table border="0" width="500" cellspacing="0" cellpadding="0">
        <tr>
	    <td width="33%"><a href="?action=kundengruppe2" class="button_sim"><?php echo $kg[2]; ?></a></td>
	    <td width="33%"><a href="?action=kundengruppe3" class="button_sim"><?php echo $kg[3]; ?></a></td>
        <td width="33%"><a href="?action=kundengruppe5" class="button_sim"><?php echo $kg[5]; ?></a></td>
		</tr>
		</table>
        
        </td>
      </tr>
      <tr>
        <td>
        <?php include(DIR_WS_MODULES.FILENAME_SECURITY_CHECK); ?>
              
<br />  

<?php
if($_GET['action'] && $_GET['action'] != '')
{
	$kundengruppe = '';
	$tabelle = '';

	switch($_GET['action'])
	{
		case 'kundengruppe2':
			$kundengruppe = '2';
			$tabelle = 'newsletter_kg_'.$kundengruppe;
			break;
		
		case 'kundengruppe3':
			$kundengruppe = '3';
			$tabelle = 'newsletter_kg_'.$kundengruppe;
			break;
			
		case 'kundengruppe5':
			$kundengruppe = '5';
			$tabelle = 'newsletter_kg_'.$kundengruppe;
			break;			
	}
	
	if($kundengruppe != '')
	{
		## Normale Kunden ##
		if($kundengruppe == '2')
		{
				
			$cust_query = xtc_db_query("SELECT customers_id, 
								customers_firstname, 
								customers_lastname, 
								customers_email_address, 
								customers_gender, 
								customers_dob, 
								customers_telephone
								FROM customers 
								WHERE customers_newsletter = '1' 
								AND customers_status = '2' 
								ORDER BY customers_id DESC 
								".$limit);
												
			while($cust = xtc_db_fetch_array($cust_query))
			{
				$ord_products = '';
				$customers_id = $cust['customers_id'];
				$customers_firstname = trim($cust['customers_firstname']);
				$customers_lastname = trim($cust['customers_lastname']);
				$customers_email_address = trim($cust['customers_email_address']);
				$customers_gender = trim($cust['customers_gender']);
				$customers_dob = ($cust['customers_dob']);
				$customers_telephone = trim($cust['customers_telephone']);
				$customers_firstname = ucfirst($customers_firstname);
				$customers_lastname = ucfirst($customers_lastname);
				$customers_alter = ermittleAlter($customers_dob);
				if($customers_alter < '1' || $customers_alter > '99') $customers_alter = '0'; 					
				
				## Hole Bestellungen
				$order_query = xtc_db_query("SELECT orders_id, 
														billing_street_address, 
														billing_city, 
														billing_postcode, 
														billing_country, 
														payment_method, 
														date_purchased
														FROM orders 
														WHERE customers_id = '".$customers_id."'
														ORDER BY orders_id DESC LIMIT 1");
														
				if(xtc_db_num_rows($order_query) > 0)
				{
					while($order = xtc_db_fetch_array($order_query))
					{
						$orders_id = $order['orders_id'];
						
						$content_query = xtc_db_query("SELECT customers_id, orders_id FROM ".$tabelle." WHERE customers_id = '".$customers_id."' ORDER BY orders_id DESC LIMIT 1");
						
						## Datensatz bereits vorhanden!
						if(xtc_db_num_rows($content_query) > 0)
						{
							while($content_data = xtc_db_fetch_array($content_query))
							{
								($content_data['orders_id'] == $orders_id) ? $do = 'nothing' : $do = 'update';
							}
						}
						## Neuer Datensatz!
						else
						{
							$do = 'insert';
						}
						
						if($do != 'nothing') {
						## Hole bestellte Produkte
						$ordered_products_query = xtc_db_query("SELECT products_id FROM orders_products WHERE orders_id = '".$orders_id."' ORDER BY orders_products_id DESC");
										
						while($ordered_products = xtc_db_fetch_array($ordered_products_query))
						{
							$row = 0;
							$ordered_products_id[$row] = $ordered_products['products_id'];
							$ord_products .= $ordered_products_id[$row].',';
							$row++;							
						}
						$ord_products = substr($ord_products,0, strlen($ord_products)-1);
						
						$billing_street_address = trim($order['billing_street_address']);
						$billing_city = trim($order['billing_city']);
						$billing_postcode = trim($order['billing_postcode']);
						$billing_country = trim($order['billing_country']);
						$payment_method = $order['payment_method'];
						$date_purchased = $order['date_purchased'];
						
						} // ende: if(do != 'nothing')
						
						if($do=='insert') 
						{
							$insert_query = xtc_db_query("INSERT INTO ".$tabelle." (id, 
																				customers_id, 
																				orders_id, 
																				customers_firstname, 
																				customers_lastname, 
																				customers_email_address, 
																				customers_gender, 
																				customers_alter,
																				customers_telephone, 
																				billing_street_address, 
																				billing_city, 
																				billing_postcode, 
																				billing_country,
																				payment_method, 
																				date_purchased, 
																				ord_products) 
																				VALUES
																				('', 
																				'".$customers_id."', 
																				'".$orders_id."',  
																				'".mysql_real_escape_string($customers_firstname)."', 
																				'".mysql_real_escape_string($customers_lastname)."', 
																				'".mysql_real_escape_string($customers_email_address)."', 
																				'".$customers_gender."', 
																				'".$customers_alter."', 
																				'".mysql_real_escape_string($customers_telephone)."', 
																				'".mysql_real_escape_string($billing_street_address)."', 
																				'".mysql_real_escape_string($billing_city)."', 
																				'".$billing_postcode."', 
																				'".mysql_real_escape_string($billing_country)."', 
																				'".$payment_method."', 
																				'".$date_purchased."', 
																				'".$ord_products."')");
						}
						
						if($do=='update') 
						{
							$update_query = xtc_db_query("UPDATE ".$tabelle." 
															SET orders_id = '".$orders_id."', 
															date_purchased = '".$date_purchased."', 
															ord_products = '".$ord_products."' 
															WHERE customers_id = '".$customers_id."'");
						}
						
					}
				}
			}
		}


		## Händler ##
		elseif($kundengruppe == '3')
		{
			$cust_query = xtc_db_query("SELECT c.customers_id, 
								c.customers_firstname, 
								c.customers_lastname, 
								c.customers_email_address, 
								c.customers_gender, 
								c.customers_dob, 
								c.customers_telephone,
								a.customers_id,
								a.entry_company 
								FROM 
								customers c,
								address_book a 
								WHERE c.customers_newsletter = '1' 
								AND c.customers_status = '3'  
								AND c.customers_id = a.customers_id 
								ORDER BY c.customers_id DESC  
								".$limit);
		
			while($cust = xtc_db_fetch_array($cust_query))
			{
				$ord_products = '';
				$customers_id = $cust['customers_id'];
				$customers_company = trim($cust['entry_company']);
				$customers_firstname = trim($cust['customers_firstname']);
				$customers_lastname = trim($cust['customers_lastname']);
				$customers_email_address = trim($cust['customers_email_address']);
				$customers_gender = trim($cust['customers_gender']);
				$customers_dob = ($cust['customers_dob']);
				$customers_telephone = trim($cust['customers_telephone']);
				$customers_firstname = ucfirst($customers_firstname);
				$customers_lastname = ucfirst($customers_lastname);
				$customers_alter = ermittleAlter($customers_dob);
				if($customers_alter < '1' || $customers_alter > '99') $customers_alter = '0'; 					
				
				## Hole Bestellungen
				$order_query = xtc_db_query("SELECT orders_id, 
														billing_street_address, 
														billing_city, 
														billing_postcode, 
														billing_country, 
														payment_method, 
														date_purchased
														FROM orders 
														WHERE customers_id = '".$customers_id."'
														ORDER BY orders_id DESC LIMIT 1");
														
				if(xtc_db_num_rows($order_query) > 0)
				{
					while($order = xtc_db_fetch_array($order_query))
					{
						$orders_id = $order['orders_id'];
						
						$content_query = xtc_db_query("SELECT customers_id, orders_id FROM ".$tabelle." WHERE customers_id = '".$customers_id."' ORDER BY orders_id DESC LIMIT 1");
						
						## Datensatz bereits vorhanden!
						if(xtc_db_num_rows($content_query) > 0)
						{
							while($content_data = xtc_db_fetch_array($content_query))
							{
								($content_data['orders_id'] == $orders_id) ? $do = 'nothing' : $do = 'update';
							}
						}
						## Neuer Datensatz!
						else
						{
							$do = 'insert';
						}
						
						if($do != 'nothing') {
						## Hole bestellte Produkte
						$ordered_products_query = xtc_db_query("SELECT products_id FROM orders_products WHERE orders_id = '".$orders_id."' ORDER BY orders_products_id DESC");
						
						while($ordered_products = xtc_db_fetch_array($ordered_products_query))
						{
							$row = 0;
							$ordered_products_id[$row] = $ordered_products['products_id'];
							$ord_products .= $ordered_products_id[$row].',';
							$row++;							
						}
						$ord_products = substr($ord_products,0, strlen($ord_products)-1);
						
						$billing_street_address = trim($order['billing_street_address']);
						$billing_city = trim($order['billing_city']);
						$billing_postcode = trim($order['billing_postcode']);
						$billing_country = trim($order['billing_country']);
						$payment_method = $order['payment_method'];
						$date_purchased = $order['date_purchased'];

						} // ende: if(do != 'nothing')
						
						if($do=='insert') 
						{
							$insert_query = xtc_db_query("INSERT INTO ".$tabelle." (id, 
																				customers_id, 
																				orders_id, 
																				customers_company, 
																				customers_firstname, 
																				customers_lastname, 
																				customers_email_address, 
																				customers_gender, 
																				customers_alter,
																				customers_telephone, 
																				billing_street_address, 
																				billing_city, 
																				billing_postcode, 
																				billing_country,
																				payment_method, 
																				date_purchased, 
																				ord_products) 
																				VALUES
																				('', 
																				'".$customers_id."', 
																				'".$orders_id."',  
																				'".mysql_real_escape_string($customers_company)."',
																				'".mysql_real_escape_string($customers_firstname)."', 
																				'".mysql_real_escape_string($customers_lastname)."', 
																				'".mysql_real_escape_string($customers_email_address)."', 
																				'".$customers_gender."', 
																				'".$customers_alter."', 
																				'".mysql_real_escape_string($customers_telephone)."', 
																				'".mysql_real_escape_string($billing_street_address)."', 
																				'".mysql_real_escape_string($billing_city)."', 
																				'".$billing_postcode."', 
																				'".mysql_real_escape_string($billing_country)."', 
																				'".$payment_method."', 
																				'".$date_purchased."', 
																				'".$ord_products."')");
						}
						
						if($do=='update') 
						{
							$update_query = xtc_db_query("UPDATE ".$tabelle." 
															SET orders_id = '".$orders_id."', 
															date_purchased = '".$date_purchased."', 
															ord_products = '".$ord_products."' 
															WHERE customers_id = '".$customers_id."'");
						}
						
					}
				}
			}
		}		
		
		## Firmenkunden ##
		elseif($kundengruppe == '5')
		{
			$cust_query = xtc_db_query("SELECT c.customers_id, 
								c.customers_firstname, 
								c.customers_lastname, 
								c.customers_email_address, 
								c.customers_gender, 
								c.customers_dob, 
								c.customers_telephone,
								a.customers_id,
								a.entry_company 
								FROM 
								customers c,
								address_book a 
								WHERE c.customers_newsletter = '1' 
								AND c.customers_status = '5'  
								AND c.customers_id = a.customers_id 
								ORDER BY c.customers_id DESC  
								".$limit);

			while($cust = xtc_db_fetch_array($cust_query))
			{
				$ord_products = '';
				$customers_id = $cust['customers_id'];
				$customers_company = trim($cust['entry_company']);
				$customers_firstname = trim($cust['customers_firstname']);
				$customers_lastname = trim($cust['customers_lastname']);
				$customers_email_address = trim($cust['customers_email_address']);
				$customers_gender = trim($cust['customers_gender']);
				$customers_dob = ($cust['customers_dob']);
				$customers_telephone = trim($cust['customers_telephone']);
				$customers_firstname = ucfirst($customers_firstname);
				$customers_lastname = ucfirst($customers_lastname);
				$customers_alter = ermittleAlter($customers_dob);
				if($customers_alter < '1' || $customers_alter > '99') $customers_alter = '0'; 					
				
				## Hole Bestellungen
				$order_query = xtc_db_query("SELECT orders_id, 
														billing_street_address, 
														billing_city, 
														billing_postcode, 
														billing_country, 
														payment_method, 
														date_purchased
														FROM orders 
														WHERE customers_id = '".$customers_id."'
														ORDER BY orders_id DESC LIMIT 1");
														
				if(xtc_db_num_rows($order_query) > 0)
				{
					while($order = xtc_db_fetch_array($order_query))
					{
						$orders_id = $order['orders_id'];
						
						$content_query = xtc_db_query("SELECT customers_id, orders_id FROM ".$tabelle." WHERE customers_id = '".$customers_id."' ORDER BY orders_id DESC LIMIT 1");
						
						## Datensatz bereits vorhanden!
						if(xtc_db_num_rows($content_query) > 0)
						{
							while($content_data = xtc_db_fetch_array($content_query))
							{
								($content_data['orders_id'] == $orders_id) ? $do = 'nothing' : $do = 'update';
							}
						}
						## Neuer Datensatz!
						else
						{
							$do = 'insert';
						}
						
						if($do != 'nothing') {
						## Hole bestellte Produkte
						$ordered_products_query = xtc_db_query("SELECT products_id FROM orders_products WHERE orders_id = '".$orders_id."' ORDER BY orders_products_id DESC");
									
						while($ordered_products = xtc_db_fetch_array($ordered_products_query))
						{
							$row = 0;
							$ordered_products_id[$row] = $ordered_products['products_id'];
							$ord_products .= $ordered_products_id[$row].',';
							$row++;							
						}
						$ord_products = substr($ord_products,0, strlen($ord_products)-1);
						
						$billing_street_address = trim($order['billing_street_address']);
						$billing_city = trim($order['billing_city']);
						$billing_postcode = trim($order['billing_postcode']);
						$billing_country = trim($order['billing_country']);
						$payment_method = $order['payment_method'];
						$date_purchased = $order['date_purchased'];

						} // ende: if(do != 'nothing')
						
						if($do=='insert') 
						{
							$insert_query = xtc_db_query("INSERT INTO ".$tabelle." (id, 
																				customers_id, 
																				orders_id, 
																				customers_company, 
																				customers_firstname, 
																				customers_lastname, 
																				customers_email_address, 
																				customers_gender, 
																				customers_alter,
																				customers_telephone, 
																				billing_street_address, 
																				billing_city, 
																				billing_postcode, 
																				billing_country,
																				payment_method, 
																				date_purchased, 
																				ord_products) 
																				VALUES
																				('', 
																				'".$customers_id."', 
																				'".$orders_id."',  
																				'".mysql_real_escape_string($customers_company)."',
																				'".mysql_real_escape_string($customers_firstname)."', 
																				'".mysql_real_escape_string($customers_lastname)."', 
																				'".mysql_real_escape_string($customers_email_address)."', 
																				'".$customers_gender."', 
																				'".$customers_alter."', 
																				'".mysql_real_escape_string($customers_telephone)."', 
																				'".mysql_real_escape_string($billing_street_address)."', 
																				'".mysql_real_escape_string($billing_city)."', 
																				'".$billing_postcode."', 
																				'".mysql_real_escape_string($billing_country)."', 
																				'".$payment_method."', 
																				'".$date_purchased."', 
																				'".$ord_products."')");
						}
						
						if($do=='update') 
						{
							$update_query = xtc_db_query("UPDATE ".$tabelle." 
															SET orders_id = '".$orders_id."', 
															date_purchased = '".$date_purchased."', 
															ord_products = '".$ord_products."' 
															WHERE customers_id = '".$customers_id."'");
						}
						
					}
				}
			}
		}		
	
		else
		{
			echo 'Fehler!';
		}
	}
}
		
?>

     <tr>
      <td style="border: 0px solid; border-color: #ffffff;">
</td>
      </tr>		 
  
  
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>