<?php
require('includes/application_top.php');
// include needed functions
require_once(DIR_FS_INC .'xtc_get_order_data.inc.php');
require_once(DIR_FS_INC .'xtc_get_attributes_model.inc.php');
require_once(DIR_FS_INC .'xtc_not_null.inc.php');
require_once(DIR_FS_INC .'xtc_format_price_order.inc.php');
require_once (DIR_FS_CATALOG.DIR_WS_CLASSES.'class.phpmailer.php');
require_once (DIR_FS_INC.'xtc_php_mail.inc.php');

function showPreview($c) {
    echo $c;
}

function sendButton($orderId, $mail) {
    echo '<a style="position:relative;
  top:15px;
  padding:10px;
  background-color: #F1F1F1;
  border: 1px solid #000000;
  color: #000000;
  font-family: Verdana,Arial,sans-serif;
  font-size: 12px;
  font-weight: bold;
  line-height: 24px;
  text-decoration: none;" href="'.$_SERVER['PHP_SELF'].'?oID='.$orderId.'&send=1">Jetzt an <span style="color:green;">"'.$mail.'"</span> senden?</a>';
}

function closeButton() {
    echo '<a class="ts-close-btn" style="position:relative;top:15px;padding:10px;background-color: #F1F1F1;
    border: 1px solid #000000;
    color: #000000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 12px;
    font-weight: bold;
    line-height: 24px;
    text-decoration: none;" href="javascript:opener.location.reload();window.close()">Fenster schliessen!</a>';
}

function mailSuccess($mail) {
    echo '<strong style="color:green;font-family:Arial;font-size:13px;">Die E-Mail wurde erfolgreich an die Adresse '.$mail.' versendet!</strong><br><br>';
    xtc_db_query("UPDATE ".TABLE_ORDERS." SET payment_method = 'moneyorder', payment_class = 'moneyorder' WHERE orders_id = '".(int)$_GET['oID']."' LIMIT 1");
}

function mailFailed() {
    echo '<strong style="color:red;font-family:Arial;font-size:13px;">Der Versand ist gescheitert!</strong><br><br>';
}

$contentHTML = file_get_contents(DIR_FS_DOCUMENT_ROOT.'/templates/'.CURRENT_TEMPLATE . '/admin/mail/'.$_SESSION['language'].'/bonitaetspruefung_mail_negative_bonitaet.html'); 
$contentTXT = file_get_contents(DIR_FS_DOCUMENT_ROOT.'/templates/'.CURRENT_TEMPLATE . '/admin/mail/'.$_SESSION['language'].'/bonitaetspruefung_mail_negative_bonitaet.txt');

include(DIR_WS_CLASSES . 'order.php');
$order = new order($_GET['oID']);

$orderQuery = xtc_db_query("SELECT * FROM ".TABLE_ORDERS." WHERE orders_id='".(int)$_GET['oID']."' LIMIT 1");
$orderData = xtc_db_fetch_array($orderQuery);
$orderDataArray = array('id' => $orderData['orders_id'], 
                        'customers_id' => $orderData['customers_id'], 
                        'customers_email_address' => $orderData['customers_email_address'], 
                        'payment_method' => $orderData['payment_method'], 
                        'date_purchased' => $orderData['date_purchased']);

$customersQuery = xtc_db_query("SELECT * FROM ".TABLE_CUSTOMERS." WHERE customers_id ='".$orderDataArray['customers_id']."' LIMIT 1");
$customersData = xtc_db_fetch_array($customersQuery);
$customersDataArray = array('id' => $customersData['customers_id'], 
                            'gender' => $customersData['customers_gender'], 
                            'firstname' => $customersData['customers_firstname'], 
                            'lastname' => $customersData['customers_lastname'], 
                            'dob' => $customersData['customers_dob'], 
                            'mail' => $customersData['customers_email_address'], 
                            'customers_date_added' => $customersData['customers_date_added']); 

$orderstotalQuery = xtc_db_query("SELECT text FROM ".TABLE_ORDERS_TOTAL." WHERE orders_id ='".(int)$_GET['oID']."' AND class = 'ot_total' LIMIT 1");
$orderstotalData = xtc_db_fetch_array($orderstotalQuery);
$orderstotalDataArray = array('summe' => $orderstotalData['text']);

// eMail Spezifikationen
$fromMailAdress = 'info@esoterikshopping.de';
$fromEmailName = "Esoterikshopping.de";
$toMailAdress = $orderDataArray['customers_email_address'];
$toName = $customersDataArray['firstname'].' '.$customersDataArray['lastname'];
$replyAdressName = "Esoterikshopping.de";
$subject = 'Informationen zu Ihrer Bestellung (Nr. '. $_GET['oID'].') bei Esoterikshopping.de - Bitte lesen!';

// Logo
$logo_path = HTTP_SERVER  . DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/';
$logo = '<tr><td><img src="'.$logo_path.'logo_email.jpg"></td></tr>';

switch($customersDataArray['gender']) {
    case 'm':
        $gender = 'Sehr geehrter Herr ';
        break;

    case 'f':
        $gender = 'Sehr geehrte Frau ';
        break;

    default: 
        $gender = 'Hallo ';
        break;
}

// Content HTML: Placeholders ersetzen
$contentHTML = str_replace('{#logo#}', $logo, $contentHTML);
$contentHTML = str_replace('{#oID#}', $_GET['oID'], $contentHTML);
$contentHTML = str_replace('{#gender#}', $gender, $contentHTML);
$contentHTML = str_replace('{#firstname#}', $customersDataArray['firstname'], $contentHTML);
$contentHTML = str_replace('{#lastname#}', $customersDataArray['lastname'], $contentHTML);
$contentHTML = str_replace('{#oId#}', $_GET['oID'], $contentHTML);
$contentHTML = str_replace('{#summe#}', $orderstotalDataArray['summe'], $contentHTML);

// Content TXT: Placeholders ersetzen
$contentTXT = str_replace('{#oID#}', $_GET['oID'], $contentTXT);
$contentTXT = str_replace('{#gender#}', $gender, $contentTXT);
$contentTXT = str_replace('{#firstname#}', $customersDataArray['firstname'], $contentTXT);
$contentTXT = str_replace('{#lastname#}', $customersDataArray['lastname'], $contentTXT);
$contentTXT = str_replace('{#oId#}', $_GET['oID'], $contentTXT);
$contentTXT = str_replace('{#summe#}', $orderstotalDataArray['summe'], $contentTXT);


if(isset($_GET['send']) && $_GET['send'] == '1') {
    $query=xtc_db_query("UPDATE ".TABLE_ORDERS." SET bonitaet_mail_flag = '1' WHERE orders_id = '".$_GET['oID']."' LIMIT 1");
    $sendMail = xtc_php_mail($fromMailAdress, $fromEmailName, $toMailAdress, $toName, '', $fromMailAdress, $replyAdressName, '', '', $subject, $contentHTML, $contentTXT);
    if($sendMail) {
        mailSuccess($toMailAdress);
    } else {
        mailFailed();
    }
    closeButton();
}
else {
    showPreview($contentHTML);
    sendButton($_GET['oID'], $toMailAdress);
}

?>