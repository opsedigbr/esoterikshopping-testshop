<?php
/* --------------------------------------------------------------
   *Hilfe und Kurzanleitung
   --------------------------------------------------------------*/
		echo '<div id="contentLayout">
		      <div id="contentContainer">
			  <p class="pageHeading">FAQ</p>
			  
			  <table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
				<td class="dataTableContent" valign="top" width="30"><b>1.</b></td>
				<td class="dataTableContent" valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
					Ich habe die direkte �bertragung gew�hlt. Warum kommen die Daten in meinem Yatego-Shop nicht an?
					</tr></tbody></table>
				<br>Ob Ihre Daten tats�chlich in der Warteschleife des Yatego-Importsystems angekommen sind, k�nnen Sie im Yatego-Administrationsbereich unter "Sortiment" > "Import/Export" > "Importstatistik" ersehen. Falls hier keine Eintr�ge im betroffenen Zeitraum vorhanden sind, pr�fen Sie bitte, ob die hinterlegten Zugangsdaten im Schnittstellen-Men�punkt "Einstellungen" richtig sind.</td>
				</tr></tbody></table>
			  
			  <table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
				<td class="dataTableContent" valign="top" width="30"><b>2.</b></td>
				<td class="dataTableContent" valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
					Im Men�punkt "Artikel ausw�hlen" erscheint nur eine leere Seite. Woran liegt das?
					</tr></tbody></table>
				<br>F�r diesen Funktionsbereich darf im Men�punkt "Einstellungen" die Option "Alle Artikel exportieren" nicht aktiviert sein. Deaktivieren Sie zuerst diese Funktion und wechseln Sie im Anschlu� in den Men�punkt "Alle Artikel exportieren" zur�ck.</td>
				</tr></tbody></table>
				
			  <table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
				<td class="dataTableContent" valign="top" width="30"><b>3.</b></td>
				<td class="dataTableContent" valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
					Die �berspielung in den Yatego-Shop hat funktioniert, aber einige oder alle Artikel werden nicht im �ffentlichen Bereich meines Yatego-Shops angezeigt. Woran liegt das?
					</tr></tbody></table>
				<br>Zum einen kann dies an der sogenannten Daten-Replikation liegen. Nach der �berspielung werden die Daten zun�chst alle zwei Stunden durch den YCO aufbereitet und an die Live-Server ausgeliefert. Das Aufbereiten ist notwendig, um die Artikel f�r interne und externe Suchprozesse optimal aufbereiten zu k�nnen. Es kann l�nger dauern, wenn Sie einen Artikel einpflegen nachdem gerade die Aufbereitungsphase begonnen hat. In diesem Fall muss diese erst beendet werden, bevor ein neuer Zyklus beginnen kann. Au�erdem kann es hin und wieder auch vorkommen, dass durch hohes Importaufkommen oder durch Arbeiten unseres Technik-Teams am Server Verz�gerungen auftreten. In der Regel dauer ein Replikations-Zyklus ca. 4 - 6 Stunden. Zum anderen kann es auch an den Lagerbest�nden liegen. Artikel (und Attribute bzw. Varianten) mit negativen oder Null-Lagerbest�nden k�nnen im Yatego-Shop nicht bestellt werden, da sie als ausverkauft gelten. Sofern Sie die Option "Lagerbest�nde exportieren" deaktivieren, werden die Artikel und Varianten auf -1 gestellt (-1 = ohne Limit verf�gbar).</td>
				</tr></tbody></table>			

			  <table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
				<td class="dataTableContent" valign="top" width="30"><b>4.</b></td>
				<td class="dataTableContent" valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
					Kann eine regelm��ige automatische �bertragung in den Yatego-Shop erfolgen?
					</tr></tbody></table>
				<br>F�r einen automatischen Import als kostenfreie Serviceleistung ist die Bereitstellung einer konzeptionell einwandfreien Datei  erforderlich. Bitte senden Sie daher folgenden Link zu Ihrer Datei an den Yatego Support (<a href="mailto:support@yatego.com">support@yatego.com</a>). Die Datei wird daraufhin hinsichtlich der Anforderungen �berpr�ft.<br><br>';
							
				echo (HTTP_SERVER . DIR_WS_CATALOG .'yatego.php').'?action=export&mode=download';
				
				echo '</td>
				</tr></tbody></table>	
			  </div></div>';		  
?>
