<?php
/* --------------------------------------------------------------
   * $Id: CYYatPref.php,v 1.4 2008/02/17 10:40:47 tobias Exp $
   * Konfiguration des Exports
   --------------------------------------------------------------*/

class CYYatPref {
	var $currency; // W�hrung, die exportiert wird
	var $customer_status; // Kundengruppe zur Preisberechnung
	var $language; // Sprache der Beschreibungen
	var $username; // Benutzername bei Yatego
	var $password; // Passwort bei Yatego
	var $quantities; // Export der Lagerbest�nde
	var $exportall; // Export aller Artikel
	var $h2longdesc; // Artikelname in <h2> vor Langbeschreibung
	var $genshortdesc; // Erstellen der Kurzbeschreibung
	var $genpackagesize; // Erstellen der Grundpreis
	var $importmode; // Importmode
	var $importpic; // Bilder aus der Langbeschreibung als Bilder verwenden
	
	function CYYatPref() {
		// Initialisierung mit den Werten aus der Datenbank
		$this->currency = YATEGO_CURRENCY;
		$this->customer_status = YATEGO_CUSTOMER_STATUS;
		$this->language = YATEGO_LANGUAGE;
		$this->username = YATEGO_USERNAME;
		$this->password = YATEGO_PASSWORD;
		$this->quantities = YATEGO_QUANTITIES;
		$this->exportall = YATEGO_EXPORTALL;
		$this->h2longdesc = YATEGO_H2LONGDESC;
		$this->genshortdesc = YATEGO_GENSHORTDESC;
		$this->genpackagesize = YATEGO_GENPACKAGESIZE;
		$this->importmode = YATEGO_IMPORTMODE;
		$this->importpic = YATEGO_IMPORTPIC;
	}

/* --------------------------------------------------------------
   * �ndern der W�hrung
   --------------------------------------------------------------*/
	function setCurrency($curr) {
		if($this->currency != $curr) {
			$this->currency = $curr;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->currency . "' WHERE configuration_key='YATEGO_CURRENCY'");
			return true;
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern der Kundengruppe
   --------------------------------------------------------------*/
	function setCustomerStatus($cust) {
		if($this->customer_status != $cust) {
			$this->customer_status = $cust;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->customer_status . "' WHERE configuration_key='YATEGO_CUSTOMER_STATUS'");
			return true;
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern der Sprache
   --------------------------------------------------------------*/
	function setLanguage($lang) {
		if($this->language != $lang) {
			$this->language = $lang;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->language . "' WHERE configuration_key='YATEGO_LANGUAGE'");
			return true;
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern des Yatego Benutzernamens
   --------------------------------------------------------------*/
	function setUsername($user) {
		if($this->username != $user) {
			$this->username = $user;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->username . "' WHERE configuration_key='YATEGO_USERNAME'");
			return true;
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern des Importmodes
   --------------------------------------------------------------*/
	function setImportmode($import) {
		if($this->importmode != $import) {
			$this->importmode = $import;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->importmode . "' WHERE configuration_key='YATEGO_IMPORTMODE'");
			return true;
		}
		return false;
	}	

/* --------------------------------------------------------------
   * �ndern des Importmodes
   --------------------------------------------------------------*/
	function setImportpic($import) {
		if($this->importpic != $import) {
			$this->importpic = $import;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->importpic . "' WHERE configuration_key='YATEGO_IMPORTPIC'");
			return true;
		}
		return false;
	}	
	
/* --------------------------------------------------------------
   * �ndern des Yatego Passworts
   --------------------------------------------------------------*/
	function setPassword($pass) {
		if($this->password != $pass) {
			$this->password = $pass;
			xtc_db_query("UPDATE configuration SET configuration_value='" . $this->password . "' WHERE configuration_key='YATEGO_PASSWORD'");
			return true;
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern der Lagerbest�nde
   --------------------------------------------------------------*/
	function setQuantities($quan) {
		if($this->quantities != $quan) {
			$this->quantities = $quan;
			if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->quantities . "' WHERE configuration_key='YATEGO_QUANTITIES'")) {
				return true;
			}
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern der Option zum Exportieren aller Artikel
   --------------------------------------------------------------*/
	function setExportAll($exp) {
		if($this->exportall != $exp) {
			$this->exportall = $exp;
			if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->exportall . "' WHERE configuration_key='YATEGO_EXPORTALL'")) {
				return true;
			}
			xtc_db_query("TRUNCATE TABLE yatego_articles");
		}
		return false;
	}

/* --------------------------------------------------------------
   * �ndern des Artikelnamens vor Langbeschreibung
   --------------------------------------------------------------*/
	function setH2longdesc($h2) {
		if($this->h2longdesc != $h2) {
			$this->h2longdesc = $h2;
			if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->h2longdesc . "' WHERE configuration_key='YATEGO_H2LONGDESC'")) {
				return true;
			}
		}
		return false;
	}
	
/* --------------------------------------------------------------
   * �ndern der Erstellung der Kurzbeschreibung
   --------------------------------------------------------------*/
	function setGenshortdesc($gen) {
		if($this->genshortdesc != $gen) {
			$this->genshortdesc = $gen;
			if(xtc_db_query("UPDATE configuration SET configuration_value='" . $this->genshortdesc . "' WHERE configuration_key='YATEGO_GENSHORTDESC'")) {
				return true;
			}
		}
		return false;
	}
	
	
/* --------------------------------------------------------------
   * �ndern der Erstellung der Grundpreis
   --------------------------------------------------------------*/
   function setGenpackagesize($gen) {
		if($this->genpackagesize != $gen) {
			$this->genpackagesize = $gen;
			if(xtc_db_query("UPDATE configuration SET configuration_value='".$this->genpackagesize."' WHERE configuration_key='YATEGO_GENPACKAGESIZE'")) {
				return true;
			}
		}
		return false;
	}	
	
/* --------------------------------------------------------------
   * Anzeige der Einstellungen
   * Auswahl wird per POST an yatego.php geschickt
   --------------------------------------------------------------*/
	function display() {
		
		$link_yatego = xtc_href_link('yatego.php');
		   if (strpos($link_yatego, '?') !== false)
		   {
		     	$link_yatego .= '&';
		   }
		   else
		   {
		   	    $link_yatego .= '?';
		   }
		
		echo '	<div id="contentLayout">
		<div id="contentContainer"><p class="pageHeading">Einstellungen</p>
<p><form action="' . $link_yatego . 'module=yatego&amp;section=preferences" method="post" accept-charset="' . $_SESSION['language_charset'] . '">';
?>
			<ol>
<table width="100%" cellpadding="4" cellspacing="0">
  <tbody>
<tr>
    <td class="dataTableContent" valign="top" width="200"><b>Sprache</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr>
        <td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
<?php
		// Es werden nur die Sprachen angezeigt, die auch installiert sind.
		// Hierf�r wird die Klasse des XT:Commerce verwendet
		if (!isset($lng) && !is_object($lng)) {
			include(DIR_WS_CLASSES . 'language.php');
			$lng = new language;
		}
		reset($lng->catalog_languages);
		while(list($key, $value) = each($lng->catalog_languages)) {
			echo xtc_draw_radio_field('yatego_language', $value['id'], $value['id']==$this->language?true:false).$value['name'];
		}
?>      </tr>
    </tbody></table>
    <br>W�hlen Sie hier die Sprache aus, in welcher Ihre Artikel exportiert werden sollen.</td>
  </tr>
  </tbody>
</table>
<table width="100%" cellpadding="4" cellspacing="0">
  <tbody>
<tr>
    <td class="dataTableContent" valign="top" width="200"><b>W�hrung</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr>
        <td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
<?php
		$currencies=xtc_db_query("SELECT title, code FROM ".TABLE_CURRENCIES);
		while($currencies_data=xtc_db_fetch_array($currencies)) {
			echo xtc_draw_radio_field('yatego_currency', $currencies_data['code'], $currencies_data['code']==$this->currency?true:false).$currencies_data['title'];
		}
?></tr>
    </tbody></table>
    <br>W�hlen Sie hier die zu exportierende W�hrung aus.</td>
  </tr>
  </tbody>
</table>
<table width="100%" cellpadding="4" cellspacing="0">
  <tbody>
<tr>
    <td class="dataTableContent" valign="top" width="200"><b>Kundengruppe</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr>
        <td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
<?php
		$customers_statuses_array = xtc_get_customers_statuses();
		echo xtc_draw_pull_down_menu('yatego_customer_status',$customers_statuses_array, $this->customer_status, 'id="yatego_customer_status"');
		echo '</tr>
    </tbody></table>
    <br>Aus dieser Kundengruppe werden die Preise f�r den Export bezogen.</td>
  </tr>
  </tbody>
</table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Yatego Benutzername</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			<input type="text" name="yatego_username" id="yatego_username" value="' . $this->username . '" />
		  </tr></tbody></table>
    <br>Hinterlegen Sie Ihren Benutzernamen inklusive .yatego.com</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Yatego Passwort</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			<input type="text" name="yatego_password" id="yatego_password" value="********" />
		</tr></tbody></table>
    <br>Tragen Sie hier Ihr Yatego Passwort ein, achten Sie dabei auf Gro�- und Kleinschreibung.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Import Modus</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			<select name="yatego_importmode" size=1 id="yatego_importmode"><option value="' . $this->importmode . '">' . $this->importmode . '</option><option value="0">---</option><option value="1">1</option><option value="0">0</option></select>
		</tr></tbody></table>
    <br>L�schen aller Daten die nicht in der Import-Datei vorhanden sind (1), nicht L�schen (0)</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Bilder aus Langbeschreibung �bernehmen</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			<select name="yatego_importpic" size=1 id="yatego_importpic"><option value="' . $this->importpic . '">' . $this->importpic . '</option><option value="0">---</option><option value="1">1</option><option value="0">0</option></select>
		</tr></tbody></table>
    <br>Alle (max. 5) Bilder aus der Langbeschreibung als Bilder verwenden (1), nicht verwenden (0)</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Lagerbest&auml;nde exportieren</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			' . xtc_draw_checkbox_field('yatego_quantities', 'true', $this->quantities=='false'?false:true, 'id=yatego_quantities') . '
			</tr></tbody></table>
    <br>Exportiert die hinterlegten Lagerbest�nde der Artikel. Wenn die Funktion nicht ausgew�hlt ist, wird immer ein unbegrenzter Bestand �bergeben.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Alle Artikel exportieren</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			' . xtc_draw_checkbox_field('yatego_exportall', 'true', $this->exportall=='false'?false:true, 'id=yatego_exportall') . '
		</tr></tbody></table>
    <br>W�hlt alle Artikel zum export aus. Unter dem Men�punkg "Artikel ausw�hlen" ist dann keine Funktion mehr verf�gbar.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Artikelname in &lt;h2&gt; vor Langbeschreibung</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			' . xtc_draw_checkbox_field('yatego_h2longdesc', 'true', $this->h2longdesc=='false'?false:true, 'id=yatego_h2longdesc') . '
		</tr></tbody></table>
    <br>Stellt den Artikelnamen im �berschriftenformat H2 vor die Langbeschreibung.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Kurzbeschreibung generieren</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			' . xtc_draw_checkbox_field('yatego_genshortdesc', 'true', $this->genshortdesc=='false'?false:true, 'id=yatego_genshortdesc') . '
			</tr></tbody></table>
    <br>Generiert bei Artikeln ohne Kurzbeschreibung einen Text aus dem Artikelnamen und dem Anfang der Langbeschreibung.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td class="dataTableContent" valign="top" width="200"><b>Grundpreis generieren</b></td>
    <td class="dataTableContent" valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr><td style="border: 1px solid rgb(167, 212, 240); background-color: rgb(191, 213, 255);" class="dataTableContent">
			' . xtc_draw_checkbox_field('yatego_genpackagesize', 'true', $this->genpackagesize=='false'?false:true, 'id=yatego_genpackagesize') . '
			</tr></tbody></table>
    <br>Exportiert den Grundpreis der Artikel.</td>
  </tr></tbody></table>
<table width="100%" cellpadding="4" cellspacing="0"><tbody><tr>
    <td valign="top" width="200"><b></b></td>
    <td valign="top">
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tbody><tr>
        <td>
			<li><br><input type="submit" value="Einstellungen speichern" /></li>
			</tr>
    </tbody></table></td>
  </tr>
  </tbody>
</table>
			</ol>
			</form>
			</p></div></div>';
	}
}
?>
