<?php

/* -----------------------------------------------------------------------------------------

   $Id: bluegateseoindex.php



   XT-Commerce - community made shopping

   http://www.xt-commerce.com



   Copyright (c) 2003 XT-Commerce

   -----------------------------------------------------------------------------------------

   based on: 

   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)

   (c) 2002-2003 osCommerce(cod.php,v 1.28 2003/02/14); www.oscommerce.com 

   (c) 2003	 nextcommerce (invoice.php,v 1.6 2003/08/24); www.nextcommerce.org



   Released under the GNU General Public License 

   ---------------------------------------------------------------------------------------

    

   Extension Direct URL (c) 2007 bluegate communications

   ---------------------------------------------------------------------------------------

   bluegate communications - http://www.bluegate.at

   Author: Ing. Michael F�rst

   */

defined( '_VALID_XTC' ) or die( 'Direct Access to this location is not allowed.' );



define('MODULE_BLUEGATE_SEO_INDEX_TEXT_DESCRIPTION', 'Der Indexierungsdienst erstellt die SUMA-freundlichen Direct URLs f�r Ihren Shop.');

define('MODULE_BLUEGATE_SEO_INDEX_TEXT_TITLE', 'bluegate.at Direct URL Indexierungsdienst');

define('MODULE_BLUEGATE_SEO_INDEX_STATUS_DESC','Modulstatus');

define('MODULE_BLUEGATE_SEO_INDEX_STATUS_TITLE','Status');

define('MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL_TITLE','Sprachabh�ngige URLs');

define('MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL_DESC','Die suchmaschinenfreundlichen URLs werden sprachabh�ngig kodiert');



define('MODULE_BLUEGATE_SEO_INDEX_AVOIDDUPLICATECONTENT_TITLE','Duplicate Content vermeiden?');

define('MODULE_BLUEGATE_SEO_INDEX_AVOIDDUPLICATECONTENT_DESC','Aktivieren Sie diese Option um Duplicate Content zu vermeiden (Anfragen an "alte" URL werden per 301 Redirect automatisch auf die neuen URL umgeleitet).');



define('MODULE_BLUEGATE_SEO_INDEX_CREATENEWINDEX_TITLE','Indexierung durchf�hren');

define('MODULE_BLUEGATE_SEO_INDEX_CREATENEWINDEX_DESC','Aktivieren Sie diese Option um beim Speichern alle Seiten neu zu indexieren.');



define('MODULE_BLUEGATE_SEO_INDEX_SEND404ERROR_TITLE','404 Header senden?');

define('MODULE_BLUEGATE_SEO_INDEX_SEND404ERROR_DESC','Wenn Sie diese Option aktivieren, liefert Direct URL automatisch einen 404 Header ("Seite nicht gefunden") an den Client, wenn ein Produkt, eine Kategorie oder ein Content Element in der Indextabelle nicht gefunden wird.');



define('MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET_TITLE','404 Zielseite');

define('MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET_DESC','Hier k�nnen Sie eine eigene Seite definieren, die angezeigt werden soll, wenn ein Produkt, eine Kategorie oder ein Content-Element nicht gefunden wird. Wird hier keine Seite angegeben, leitet Direct URL automatisch auf die Startseite des Shops um. Geben sie den Pfad IMMER absolut an, also z.B. "http://www.shop.com/404.html" (NICHT "404.html")');



define('INDEX_START','<strong>Indizierung starten</strong>');

define('INDEX_START_DESCRIPTION','Dr�cken Sie auf "OK" um die Einstellungen zu speichern. Ist die Option "Indizierung durchf�hren" auf "True" gesetzt, kann dieser Vorgang einige Zeit in Anspruch nehmen. Brechen Sie diese Operation keinesfalls ab!');



  class bluegateseoindex {

    var $code, $title, $description, $enabled;



    function bluegateseoindex() {

      global $order;



      $this->code = 'bluegateseoindex';

      $this->title = MODULE_BLUEGATE_SEO_INDEX_TEXT_TITLE;

      $this->description = MODULE_BLUEGATE_SEO_INDEX_TEXT_DESCRIPTION;

      $this->sort_order = MODULE_BLUEGATE_SEO_INDEX_SORT_ORDER;

      $this->enabled = ((MODULE_BLUEGATE_SEO_INDEX_STATUS == 'True') ? true : false);



    }





    function process($file) {

        // include needed functions

		require_once(DIR_FS_INC . 'bluegate_seo.inc.php');

  		!$bluegateSeo ? $bluegateSeo = new BluegateSeo() : false;

		

		if ($_REQUEST['startindex']=='true') {

		// Tabelle 'bluegate_seo_url' leeren - Vermeidung doppelter Content
		$truncateTableQuery = "TRUNCATE TABLE `bluegate_seo_url`";
		xtc_db_query($truncateTableQuery);
		
		$bluegateSeo->createSeoDBTable();

		}



    }



    function display() {

		$pulldown_option[0]['text']='Ja';

		$pulldown_option[0]['id']='true';

		$pulldown_option[1]['text']='Nein';

		$pulldown_option[1]['id']='false';

		

		return array('text' => 	'<br /><strong>Indexierung durchf�hren?</strong><br />Sollen alle Seiten beim Klick auf "OK" indiziert werden?<br /><br />'.

                            	xtc_draw_pull_down_menu('startindex',$pulldown_option,'Ja').'<br />'.

							

							'<br>' . xtc_button(BUTTON_REVIEW_APPROVE) . '&nbsp;' .

                            xtc_button_link(BUTTON_CANCEL, xtc_href_link(FILENAME_MODULE_EXPORT, 'set=' . $_GET['set'] . '&module=bluegateseoindex')));

    }



    function check() {

      if (!isset($this->_check)) {

        $check_query = xtc_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_BLUEGATE_SEO_INDEX_STATUS'");

        $this->_check = xtc_db_num_rows($check_query);

      }

      return $this->_check;

    }



    function install() {

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BLUEGATE_SEO_INDEX_STATUS', 'True',  '111', '1', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL', 'True',  '111', '2', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BLUEGATE_SEO_INDEX_AVOIDDUPLICATECONTENT', 'True',  '111', '3', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BLUEGATE_SEO_INDEX_SEND404ERROR', 'True',  '111', '4', 'xtc_cfg_select_option(array(\'True\', \'False\'), ', now())");

		xtc_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, set_function, date_added) values ('MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET', '',  '111', '5', '', now())");

		
		// Tabellenstruktur bluegate_seo_url anlegen, wenn noch nicht vorhanden
		$createTableQuery =	"CREATE TABLE IF NOT EXISTS `bluegate_seo_url` (

							`url_md5` varchar(32) NOT NULL default '',

							`url_text` varchar(255) NOT NULL default '',

							`products_id` int(11) default NULL,

							`categories_id` int(11) default NULL,

							`content_group` int(11) default NULL,

							`language_id` int(11) NOT NULL default '0',

							PRIMARY KEY  (`url_md5`),

							KEY `url_text` (`url_text`,`products_id`)

							) TYPE=MyISAM;";

		xtc_db_query($createTableQuery);

	}



    function remove() {

      xtc_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");

    }



    function keys() {

      return array('MODULE_BLUEGATE_SEO_INDEX_STATUS','MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL','MODULE_BLUEGATE_SEO_INDEX_AVOIDDUPLICATECONTENT','MODULE_BLUEGATE_SEO_INDEX_SEND404ERROR','MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET');

    }



  }

?>