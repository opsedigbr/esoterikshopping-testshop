<?php
/**
 * xtc-addons.de - PDFcat
 * Addon for xt:Commerce 3.04
 * compatible to SP2.1
 * 
 * www.xtc-addons.de
 * 
 * @author Damian Gawenda <info@xtc-addons.de>
 * @package xtc_addons_pdfcat
 */


class PDFcat extends FPDF{
	
	/**
	 * Sonderpreise
	 *
	 * @param unknown_type $pID
	 * @return unknown
	 */
	function xtcCheckSpecial($pID){
	    $product_query = "SELECT	specials_new_products_price 
	    					FROM " . TABLE_SPECIALS . " 
	    					WHERE products_id = '" . $pID . "' 
	    					AND status";
	    $product_query = xtc_db_query($product_query);
	    $product = xtc_db_fetch_array($product_query,true);
	    return $product['specials_new_products_price'];
	}

	/**
	 * Produktdaten einlesen
	 *
	 * @param array $selected_products	uebergebene Auswahl
	 */
	function LoadData($selected_products = '', $specials) {
		
		/* W�hrung auslesen */
		$query ="SELECT                       	c.symbol_left,
	                                            c.symbol_right
	                                            FROM " . TABLE_CURRENCIES . " c
	                                            WHERE value = '1'";
		$currency_query = xtc_db_query($query);
		$currency = xtc_db_fetch_array($currency_query);

		
		$query ="SELECT 	p.products_id,
							p.products_image,
							p.products_price,
							p.products_model,
							pd.products_name,
							cd.categories_name,
							cd.categories_id,
							p.products_tax_class_id,
							pd.products_short_description,
							p.products_quantity
							FROM " . TABLE_CATEGORIES . " c
							LEFT OUTER JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON  cd.categories_id = c.categories_id AND cd.language_id = $this->_pdf_lang
							LEFT OUTER JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " ptc ON ptc.categories_id = c.categories_id
							LEFT OUTER JOIN " . TABLE_PRODUCTS . " p ON p.products_id = ptc.products_id
							LEFT OUTER JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON pd.products_id = p.products_id AND pd.language_id = $this->_pdf_lang
							WHERE p.products_status = 1 AND  p.products_price > 0 AND pd.products_name > ''";
		
		
		if($selected_products != ''){
			$query .="AND p.products_id IN (";
						
			for($i_prod=0; $i_prod < count($selected_products); $i_prod++){

				if($i_prod == 0){
					$query .= $selected_products[$i_prod];
				}
				else{
					$query .= ",".$selected_products[$i_prod];
				}
			}
			$query .=")";
		}
									
		$query .="ORDER BY cd.categories_name";
		$product_query = xtc_db_query($query);

	    $xx=0;
		/* build array */
		while($product = xtc_db_fetch_array($product_query))
		{
			$data[$xx][0] = $product['products_name'];
		  	$data[$xx][1] = $product['products_image'];
		  	$data[$xx][2] = $product['products_model'];		  	
		  	$data[$xx][3] = $product['products_id'];
		  	
			if ($product['products_quantity']>0)  /* Vorbereitung f�r Ampel */
		    {
				$data[$xx][7] =   '1'; 			/* Lagerbestand (gr�ne Ampel) */
		    }
		    else
		    {
				$data[$xx][7] =   '0';			/* kein Lagerbestand (rote Ampel) */
		    }
			
		    /* Specialpreis abfragen */
		    if($this->xtcCheckSpecial($product['products_id']))
			{
				$xtPrice = $this->xtcCheckSpecial($product['products_id']);  
			}
			elseif($specials){
		    	unset($data[$xx]);
				continue;
		    }
			
			else /* Normal Preis verwenden */
			{
				$xtPrice = $product['products_price'];
			}
			
			/* Preis inkl. MwSt rechnen */
			$xtPrice 		=	$xtPrice*((xtc_get_tax_rate($product['products_tax_class_id'])/100)+1);
			$data[$xx][4] 	= 	$currency['symbol_left'].' '.number_format($xtPrice, 2, '.', '').' '.$currency['symbol_right'];

			$data[$xx][5] = trim($product['categories_name']);
					
			
			$short_description = str_replace (array("&auml;","&ouml;","&uuml;","&Auml;","&Ouml;","&Uuml;", "&amp;", "&szlig;", "&quot;"),
								 			  array("�","�","�","�","�","�","&","�",'"'), $product['products_short_description']);
			//echo "<pre>";
			//print_r($test);die;		
			
			$data[$xx][6] = strip_tags($short_description);
//			$data[$xx][6] = substr(html_entity_decode($product['products_short_description']),0,300); /* Short description auf 30 Zeichen k�rzen, eventuell auch mehr m�glich */
//			$data[$xx][6] = substr($product['products_short_description'],300); /* Short description auf 30 Zeichen k�rzen, eventuell auch mehr m�glich */

			$data[$xx][8] = trim($product['categories_id']);

			$xx++;
		}
		$data = $this->format_sortData($data);
		$this->data=$data;
	}

	/**
	 * Bei jeder neuen Seite wird das Hintergrundbild geladen
	 *
	 * @param unknown_type $ii
	 * @param unknown_type $neue_seite
	 */
	function create_header($ii,$neue_seite) 
	{
		if ($neue_seite) 
		{
		    $this->new_height=$this->default_height;
			unset($this->img_height); 
			unset($this->new_height_tmp);
			$this->AddPage(); /* Neue Seite erstellen */
			$this->Image("includes/modules/pdfkatalog/kopf_pdf.png",0,0,'','',''); /* Hintergrundbild */
			$this->format_title_h1(); /*Format f�r �berschrift aufrufen */
			$this->Text(10,$this->new_height,$this->data[$ii][5]);  /* Kategorienamen schreiben */
		}
		else
		{
			$this->format_title_h1(); /*Format f�r �berschrift aufrufen */
			$this->Text(10,$this->GETY(),$this->data[$ii][5]);  /* Kategorienamen schreiben */
		}
		
		
	}

	function format_text() {  /* Normaler Text */
	    $this->SetTextColor(0);
		$this->SetFont('Arial','',6);
	}
	function format_price() {  /* Preis */
	    $this->SetTextColor(204,51,51);
		$this->SetFont('Arial','B',10);
	}
	function format_title_h1() {  /* �berschrift / Kategorie */
	    $this->SetTextColor(0,102,152);
		$this->SetFont('Arial','B',18);
	}
	function format_title_h2() {  /* Produkte Name */
	    $this->SetTextColor(0,102,152);
		$this->SetFont('Arial','B',10);
	}

	
	function format_sortData($data){
		$categories = xtc_get_category_tree();
		for($i_cat=1;$i_cat<sizeof($categories);$i_cat++){
			for($i_products=0;$i_products<sizeof($data);$i_products++){
				if($categories[$i_cat]['id'] == $data[$i_products][8]){
					//unset($data[$i_products][8]);
					$sorted_products[] = $data[$i_products];
				}
			}
		}
		return $sorted_products;
	}
	
 	/**
 	 * Seite formatieren und mit Werten aus dem Array data abf�llen
 	 *
 	 */
	function format($prices) {
	 	$hoehe_bild_max 	=	15;  	//Maximale Bildh�he
		$breite_bild_max 	=	10; 	//Maximale Bildbreite
	  
		
		
 		/* Ganzes Array aufrufen */
		for($ii=0;$ii<count($this->data);$ii++) 
		{
			$this->new_height=$this->new_height_tmp;
			unset($this->img_height);
			unset($this->new_height_tmp);
			
			/* Maximalle Gr�sse erreicht? */
			if($this->max_height<$this->GetY()+$hoehe_bild_max+10) {
				
				$this->new_height=0;
			}

			/* Check ob eine neue Seite erstellt werden muss, wenn maximalle Gr�sse erreicht */
			if(($this->new_height==0)) { 
								
				$this->create_header($ii,1);
				$this->SETY($this->GETY()+20);
				$counter=0;
			}
			
			/* Mehere Kategorien pro Seite */
			if ($this->data[$ii][5]!=$this->data[$ii-1][5] and $counter <> 0) {
				$this->SETY($this->GETY()+15);
			    $this->create_header($ii,0);
				$counter=0;
			}

			/* Trennline einf�gen */
			$this->SETY($this->GETY()+2);
			if ($counter <> 0) {
				$this->SetLineWidth(0.2);
				$this->SetDrawColor(230,230,230);
				$this->line($this->GETX()+20,$this->GETY(),$this->GETX()+150,$this->GETY());
				$this->SETY($this->GETY()+2);
			}
			$counter++;

				
			/* Bild einf�gen */
			if($this->data[$ii][1])
			{
				/* Check ob ein JPG oder PNG Bild */
			    if ((substr($this->data[$ii][1],strlen($this->data[$ii][1])-3,3)=="PNG") or (substr($this->data[$ii][1],strlen($this->data[$ii][1])-3,3)=="png") or (substr($this->data[$ii][1],strlen($this->data[$ii][1])-3,3)=="JPG") or (substr($this->data[$ii][1],strlen($this->data[$ii][1])-3,3)=="jpg"))
			    {
			   		/* Check ob Bild im Filesystem existiert */
				    if (file_exists(DIR_FS_CATALOG_INFO_IMAGES.$this->data[$ii][1]))
				    {
						$img = @getimagesize(DIR_FS_CATALOG_INFO_IMAGES.$this->data[$ii][1]);
						$hoehe_img   =	$img[1];
						$breite_img  =	$img[0];

						/* H�he und BildVerh�ltnis berechnen */
						if ($hoehe_img > $hoehe_bild_max)
						{
						    $hoehe_faktor = $hoehe_img / $hoehe_bild_max;
						    $hoehe_img  = $hoehe_img /$hoehe_faktor;
						    $breite_img  = $breite_img / $hoehe_faktor;
						}
						
						/* Breite und BildVerh�ltnis berechnen */
						if ($breite_img > $breite_bild_max)
						{
						    $breite_faktor = $breite_img / $breite_bild_max;
						    $breite_img  = $breite_img / $breite_faktor;
						    $hoehe_img  = $hoehe_img / $breite_faktor;
						}

						/* Bild berechne */
						if($this->img_height < $img[1])
						{
							$this->new_height_tmp=($img[1]*0.3)+23+$this->new_height;
							$this->img_height = $img[1];
						}
						$__link = strtr($this->product_link,array("{product_id}" => $this->data[$ii][3]));

						/* Bild einf�gen */
						$this->Image(
						DIR_FS_CATALOG_INFO_IMAGES.$this->data[$ii][1],
						$this->getx(),
						($this->GetY())
						,$breite_img
						,$hoehe_img
						,''
						,$__link);
					}
					else{
						$this->new_height_tmp=($img[1]*0.3)+23+$this->new_height;
					}
				}
			}
			else{
				$this->new_height_tmp=($img[1]*0.3)+23+$this->new_height;
			}

			/* Artikelnummer zu Produktname */
			$artnr_name = $this->data[$ii][2]." - ".$this->data[$ii][0];
			
						
			/* Produktename einf�gen */
			$this->SETX($this->GetX()+$breite_bild_max+3);
			$this->format_title_h2();
			$this->MultiCell(130,10,$artnr_name,0,'L');			


			/* Shortinformation einf�gen */
			if ($this->shorttext == 1)
			{
				$this->format_text();
				$this->SetY($this->GetY());
				$this->SetX($this->GetX()+$breite_bild_max+3);
					$this->MultiCell(130,$hoehe_bild_max-10,$this->data[$ii][6],0,'L');	
			}

			if($prices){
				/* Price einf�gen */
				$this->format_price();
				$this->SetY($this->GetY()-$hoehe_bild_max);
				$this->SetX($this->GetX()+$breite_bild_max+133);
	    	   	$this->MultiCell(30,$hoehe_bild_max,$this->data[$ii][4],0,'L');	
			}
			
			/* Ampel einf�gen einf�gen */
			$this->format_price();
			$this->SetY($this->GetY()-$hoehe_bild_max);
			$this->SetX($this->GetX()+$breite_bild_max+133+30);

			/* Ampel einf�gen */
			if ($this->data[$ii][7]<>0) /* Wenn Bestand */
			{
				$this->Image(
				DIR_FS_ADMIN."gruen.jpg",
				$this->getx(),
				$this->getY()+($hoehe_bild_max-5)/2,
				8
				,5
				,''
				,'');
			}
			else /* Kein Bestand */
			{
				$this->Image(
				DIR_FS_ADMIN."rot.jpg",
				$this->getx(),
				$this->getY()+($hoehe_bild_max-5)/2,
				8
				,5
				,''
				,'');
			}
    	   	$this->MultiCell(30,$hoehe_bild_max,"",0,'L');	
		}
	}
}

?>