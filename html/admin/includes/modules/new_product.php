<?php

/* --------------------------------------------------------------
   $Id: new_product.php 897 2005-04-28 21:36:55Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(categories.php,v 1.140 2003/03/24); www.oscommerce.com
   (c) 2003  nextcommerce (categories.php,v 1.37 2003/08/18); www.nextcommerce.org

   Released under the GNU General Public License
   --------------------------------------------------------------
   Third Party contribution:
   Enable_Disable_Categories 1.3               Autor: Mikel Williams | mikel@ladykatcostumes.com
   New Attribute Manager v4b                   Autor: Mike G | mp3man@internetwork.net | http://downloads.ephing.com
   Category Descriptions (Version: 1.5 MS2)    Original Author:   Brian Lowe <blowe@wpcusrgrp.org> | Editor: Lord Illicious <shaolin-venoms@illicious.net>
   Customers Status v3.x  (c) 2002-2003 Copyright Elari elari@free.fr | www.unlockgsm.com/dload-osc/ | CVS : http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/elari/?sortby=date#dirlist

   Released under the GNU General Public License
   --------------------------------------------------------------*/

if (($_GET['pID']) && (!$_POST)) {
	$product_query = xtc_db_query("select *, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, date_format(p.products_date_published, '%Y-%m-%d') as products_date_published 
	                               from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd
                                  where p.products_id = '".(int) $_GET['pID']."'
                                  and p.products_id = pd.products_id
                                  and pd.language_id = '".$_SESSION['languages_id']."'");

	$product = xtc_db_fetch_array($product_query);
	$pInfo = new objectInfo($product);

}
elseif ($_POST) {
	$pInfo = new objectInfo($_POST);
	$products_name = $_POST['products_name'];
	$products_description = $_POST['products_description'];
	$products_short_description = $_POST['products_short_description'];
	$products_keywords = $_POST['products_keywords'];
	$products_meta_title = $_POST['products_meta_title'];
	$products_meta_description = $_POST['products_meta_description'];
	$products_meta_keywords = $_POST['products_meta_keywords'];
	$yatego_cat = $_POST['yatego_cat'];
	$products_url = $_POST['products_url'];
	$pInfo->products_startpage = $_POST['products_startpage'];
   $products_startpage_sort = $_POST['products_startpage_sort'];
} else {
	$pInfo = new objectInfo(array ());
}

$manufacturers_array = array (array ('id' => '', 'text' => TEXT_NONE));
$manufacturers_query = xtc_db_query("select manufacturers_id, manufacturers_name from ".TABLE_MANUFACTURERS." order by manufacturers_name");
while ($manufacturers = xtc_db_fetch_array($manufacturers_query)) {
	$manufacturers_array[] = array ('id' => $manufacturers['manufacturers_id'], 'text' => $manufacturers['manufacturers_name']);
}

$vpe_array = array (array ('id' => '', 'text' => TEXT_NONE));
$vpe_query = xtc_db_query("select products_vpe_id, products_vpe_name from ".TABLE_PRODUCTS_VPE." WHERE language_id='".$_SESSION['languages_id']."' order by products_vpe_name");
while ($vpe = xtc_db_fetch_array($vpe_query)) {
	$vpe_array[] = array ('id' => $vpe['products_vpe_id'], 'text' => $vpe['products_vpe_name']);
}

$products_grundpreis_einheit_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_grundpreis_einheit_query = xtc_db_query("select products_grundpreis_einheit_id, products_grundpreis_einheit_name from ".TABLE_PRODUCTS_GRUNDPREIS_EINHEIT." WHERE language_id='".$_SESSION['languages_id']."' order by products_grundpreis_einheit_name");
while ($products_grundpreis_einheit = xtc_db_fetch_array($products_grundpreis_einheit_query)) {
	$products_grundpreis_einheit_array[] = array ('id' => $products_grundpreis_einheit['products_grundpreis_einheit_id'], 'text' => $products_grundpreis_einheit['products_grundpreis_einheit_name']);
}

// produkthervorhebung
$products_produkthervorhebung_einheit_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_produkthervorhebung_einheit_query = xtc_db_query("select products_produkthervorhebung_einheit_id, products_produkthervorhebung_einheit_name from ".TABLE_PRODUCTS_PRODUKTHERVORHEBUNG_EINHEIT." WHERE language_id='".$_SESSION['languages_id']."' order by products_produkthervorhebung_einheit_name");
while ($products_produkthervorhebung_einheit = xtc_db_fetch_array($products_produkthervorhebung_einheit_query)) {
	$products_produkthervorhebung_einheit_array[] = array ('id' => $products_produkthervorhebung_einheit['products_produkthervorhebung_einheit_id'], 'text' => $products_produkthervorhebung_einheit['products_produkthervorhebung_einheit_name']);
}

// Buch: Autor (author)
$products_buch_autor_1_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_buch_autor_1_query = xtc_db_query("select products_buch_autor_id, products_buch_autor_name from ".TABLE_PRODUCTS_BUCH_AUTOR." WHERE language_id='".$_SESSION['languages_id']."' order by products_buch_autor_name");
while ($products_buch_autor_1 = xtc_db_fetch_array($products_buch_autor_1_query)) {
  $products_buch_autor_1_array[] = array ('id' => $products_buch_autor_1['products_buch_autor_id'], 'text' => $products_buch_autor_1['products_buch_autor_name']);
}

// Buch: Autor (author)
$products_buch_autor_2_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_buch_autor_2_query = xtc_db_query("select products_buch_autor_id, products_buch_autor_name from ".TABLE_PRODUCTS_BUCH_AUTOR." WHERE language_id='".$_SESSION['languages_id']."' order by products_buch_autor_name");
while ($products_buch_autor_2 = xtc_db_fetch_array($products_buch_autor_2_query)) {
  $products_buch_autor_2_array[] = array ('id' => $products_buch_autor_2['products_buch_autor_id'], 'text' => $products_buch_autor_2['products_buch_autor_name']);
}

// Buch: Autor (author)
$products_buch_autor_3_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_buch_autor_3_query = xtc_db_query("select products_buch_autor_id, products_buch_autor_name from ".TABLE_PRODUCTS_BUCH_AUTOR." WHERE language_id='".$_SESSION['languages_id']."' order by products_buch_autor_name");
while ($products_buch_autor_3 = xtc_db_fetch_array($products_buch_autor_3_query)) {
  $products_buch_autor_3_array[] = array ('id' => $products_buch_autor_3['products_buch_autor_id'], 'text' => $products_buch_autor_3['products_buch_autor_name']);
}

// Buch: Einband (binding)
$products_buch_einband_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_buch_einband_query = xtc_db_query("select products_buch_einband_id, products_buch_einband_name from ".TABLE_PRODUCTS_BUCH_EINBAND." WHERE language_id='".$_SESSION['languages_id']."' order by products_buch_einband_name");
while ($products_buch_einband = xtc_db_fetch_array($products_buch_einband_query)) {
  $products_buch_einband_array[] = array ('id' => $products_buch_einband['products_buch_einband_id'], 'text' => $products_buch_einband['products_buch_einband_name']);
}

// Musik: Produkttyp
$products_musik_produkttyp_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_musik_produkttyp_query = xtc_db_query("select products_musik_produkttyp_id, products_musik_produkttyp_name from ".TABLE_PRODUCTS_MUSIK_PRODUKTTYP." WHERE language_id='".$_SESSION['languages_id']."' order by products_musik_produkttyp_name");
while ($products_musik_produkttyp = xtc_db_fetch_array($products_musik_produkttyp_query)) {
  $products_musik_produkttyp_array[] = array ('id' => $products_musik_produkttyp['products_musik_produkttyp_id'], 'text' => $products_musik_produkttyp['products_musik_produkttyp_name']);
}

// Musik: Ausgabe
$products_musik_ausgabe_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_musik_ausgabe_query = xtc_db_query("select products_musik_ausgabe_id, products_musik_ausgabe_name from ".TABLE_PRODUCTS_MUSIK_AUSGABE." WHERE language_id='".$_SESSION['languages_id']."' order by products_musik_ausgabe_name");
while ($products_musik_ausgabe = xtc_db_fetch_array($products_musik_ausgabe_query)) {
  $products_musik_ausgabe_array[] = array ('id' => $products_musik_ausgabe['products_musik_ausgabe_id'], 'text' => $products_musik_ausgabe['products_musik_ausgabe_name']);
}

// Musik: Hersteller
$products_musik_hersteller_array = array (array ('id' => '', 'text' => TEXT_NONE));
$products_musik_hersteller_query = xtc_db_query("select products_musik_hersteller_id, products_musik_hersteller_name from ".TABLE_PRODUCTS_MUSIK_HERSTELLER." WHERE language_id='".$_SESSION['languages_id']."' order by products_musik_hersteller_name");
while ($products_musik_hersteller = xtc_db_fetch_array($products_musik_hersteller_query)) {
  $products_musik_hersteller_array[] = array ('id' => $products_musik_hersteller['products_musik_hersteller_id'], 'text' => $products_musik_hersteller['products_musik_hersteller_name']);
}

$tax_class_array = array (array ('id' => '0', 'text' => TEXT_NONE));
$tax_class_query = xtc_db_query("select tax_class_id, tax_class_title from ".TABLE_TAX_CLASS." order by tax_class_title");
while ($tax_class = xtc_db_fetch_array($tax_class_query)) {
	$tax_class_array[] = array ('id' => $tax_class['tax_class_id'], 'text' => $tax_class['tax_class_title']);
}
$shipping_statuses = array ();
$shipping_statuses = xtc_get_shipping_status();
$languages = xtc_get_languages();

switch ($pInfo->products_status) {
	case '0' :
		$status = false;
		$out_status = true;
		break;
	case '1' :
	default :
		$status = true;
		$out_status = false;
}

if ($pInfo->products_startpage == '1') { $startpage_checked = true; } else { $startpage_checked = false; }

?>
<link rel="stylesheet" type="text/css" href="includes/javascript/spiffyCal/spiffyCal_v2_1.css">
<script type="text/javascript" src="includes/javascript/spiffyCal/spiffyCal_v2_1.js"></script>
<script type="text/javascript">
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "new_product", "products_date_available","btnDate1","<?php echo $pInfo->products_date_available; ?>",scBTNMODE_CUSTOMBLUE);
  var datePublished = new ctlSpiffyCalendarBox("datePublished", "new_product", "products_date_published","btnDate1","<?php echo $pInfo->products_date_published; ?>",scBTNMODE_CUSTOMBLUE);
</script>

<tr><td>
<?php $form_action = ($_GET['pID']) ? 'update_product' : 'insert_product'; ?>
<?php $fsk18_array=array(array('id'=>0,'text'=>NO),array('id'=>1,'text'=>YES)); ?>
<?php echo xtc_draw_form('new_product', FILENAME_CATEGORIES, 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID'] . '&action='.$form_action, 'post', 'enctype="multipart/form-data"'); ?>
<span class="pageHeading"><?php echo sprintf(TEXT_NEW_PRODUCT, xtc_output_generated_category_path($current_category_id)); ?></span><br />
<table width="100%"  border="0">
  <tr>
    <td>
      <span class="main"><?php echo TEXT_PRODUCTS_STATUS; ?> <?php echo xtc_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . xtc_draw_radio_field('products_status', '1', $status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . xtc_draw_radio_field('products_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE; ?>
      </span>
      <br />
      <br />
      
      <table width="100%" border="0">
        <tr>
          <td class="main" width="127"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?><br />
              <small>(YYYY-MM-DD)</small>
          </td>
          <td class="main"><?php echo xtc_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?>
              <script type="text/javascript">dateAvailable.writeControl(); dateAvailable.dateFormat="yyyy-MM-dd";</script>
          </td>
        </tr>
      </table>
      <span class="main"><br /></span>

      <!-- Publish Date: Manuelle Einstellung um den Artikel gezielt an eine vordere Position bei "Neue Produkte", "Special", usw. zu heben -->
      <table width="100%" border="0">
        <tr>
          <td class="main" width="127"><?php echo TEXT_PRODUCTS_DATE_PUBLISHED; ?><br />
              <small>(YYYY-MM-DD)</small>
          </td>
          <td class="main"><?php echo xtc_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?>
              <script type="text/javascript">datePublished.writeControl(); datePublished.dateFormat="yyyy-MM-dd";</script>
          </td>
        </tr>
      </table>
      <span class="main"><br /></span>

    </td><td>
    <table bgcolor="f3f3f3" style="border: 1px solid; border-color: #cccccc;" "width="100%"  border="0">
    <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_STARTPAGE; ?> <?php echo TEXT_PRODUCTS_STARTPAGE_YES . xtc_draw_radio_field('products_startpage', '1', $startpage_checked) . '&nbsp;' . TEXT_PRODUCTS_STARTPAGE_NO . xtc_draw_radio_field('products_startpage', '0', !$startpage_checked) ?></span></td>
        <td><span class="main"><?php echo TEXT_PRODUCTS_STARTPAGE_SORT; ?>&nbsp;<?php echo  xtc_draw_input_field('products_startpage_sort', $pInfo->products_startpage_sort ,'size=3'); ?></span></td>
    </tr>        
    <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_SORT; ?>&nbsp;<?php echo  xtc_draw_input_field('products_sort', $pInfo->products_sort,'size=3'); ?></span></td>
        <td><span class="main"><?php echo TEXT_PRODUCTS_QUANTITY; ?>&nbsp;<?php echo xtc_draw_input_field('products_quantity', $pInfo->products_quantity,'size=5'); ?></span></td>
      </tr>
    <tr>
      <td><span class="main"><?php echo TEXT_PRODUCTS_MODEL; ?></span></td>
      <td><span class="main"><?php echo  xtc_draw_input_field('products_model', $pInfo->products_model); ?></span></td>
    </tr>
                    <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_EAN; ?></span></td>
        <td><span class="main"><?php echo  xtc_draw_input_field('products_ean', $pInfo->products_ean); ?></span></td>
      </tr>
      <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_MANUFACTURER; ?></span></td>
        <td><span class="main"><?php echo xtc_draw_pull_down_menu('manufacturers_id', $manufacturers_array, $pInfo->manufacturers_id); ?></span></td>
      </tr>
      <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_VPE_VISIBLE.xtc_draw_selection_field('products_vpe_status', 'checkbox', '1',$pInfo->products_vpe_status==1 ? true : false); ?>
        <?php echo TEXT_PRODUCTS_VPE_VALUE.xtc_draw_input_field('products_vpe_value', $pInfo->products_vpe_value,'size=4'); ?></span></td>
        <td><span class="main"><?php echo TEXT_PRODUCTS_VPE. xtc_draw_pull_down_menu('products_vpe', $vpe_array, $pInfo->products_vpe='' ?  DEFAULT_PRODUCTS_VPE_ID : $pInfo->products_vpe); ?></span></td>
      </tr>
      <tr>
        <td><span class="main"><?php echo TEXT_FSK18; ?>&nbsp;<?php echo xtc_draw_pull_down_menu('fsk18', $fsk18_array, $pInfo->products_fsk18); ?></span></td>
        <td><span class="main"><?php echo TEXT_PRODUCTS_WEIGHT; ?><?php echo xtc_draw_input_field('products_weight', $pInfo->products_weight,'size=4'); ?>(g / ml)<?php //echo TEXT_PRODUCTS_WEIGHT_INFO; ?></span></td>
      </tr>
      <tr>
<?php if (ACTIVATE_SHIPPING_STATUS=='true') { ?>
        <td><span class="main"><?php echo BOX_SHIPPING_STATUS.':'; ?></span></td>
        <td><span class="main"><?php echo xtc_draw_pull_down_menu('shipping_status', $shipping_statuses, $pInfo->products_shippingtime); ?></span></td>
      </tr>
<?php } ?>
<!-- gift: new field -->
      <tr>
        <td><span class="main"><?php echo TEXT_PRODUCTS_GIFT; ?></span></td>
        <td><span class="main"><?php echo xtc_draw_selection_field('products_gift', 'checkbox', '1',$pInfo->products_gift==1 ? true : false); ?></span></td>
      </tr>
<!-- gift: new field -->
	  
	  <tr>
          <?php

$files = array ();
if ($dir = opendir(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/module/product_info/')) {
	while (($file = readdir($dir)) !== false) {
		if (is_file(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/module/product_info/'.$file) and ($file != "index.html")) {
			$files[] = array ('id' => $file, 'text' => $file);
		} //if
	} // while
	closedir($dir);
}
$default_array = array ();
// set default value in dropdown!
if ($content['content_file'] == '') {
	$default_array[] = array ('id' => 'default', 'text' => TEXT_SELECT);
	$default_value = $pInfo->product_template;
	$files = array_merge($default_array, $files);
} else {
	$default_array[] = array ('id' => 'default', 'text' => TEXT_NO_FILE);
	$default_value = $pInfo->product_template;
	$files = array_merge($default_array, $files);
}
echo '<td class="main">'.TEXT_CHOOSE_INFO_TEMPLATE.':</td>';
echo '<td><span class="main">'.xtc_draw_pull_down_menu('info_template', $files, $default_value);
?>
        </span></td>
      </tr>
      <tr>


          <?php

$files = array ();
if ($dir = opendir(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/module/product_options/')) {
	while (($file = readdir($dir)) !== false) {
		if (is_file(DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/module/product_options/'.$file) and ($file != "index.html")) {
			$files[] = array ('id' => $file, 'text' => $file);
		} //if
	} // while
	closedir($dir);
}
// set default value in dropdown!
$default_array = array ();
if ($content['content_file'] == '') {
	$default_array[] = array ('id' => 'default', 'text' => TEXT_SELECT);
	$default_value = $pInfo->options_template;
	$files = array_merge($default_array, $files);
} else {
	$default_array[] = array ('id' => 'default', 'text' => TEXT_NO_FILE);
	$default_value = $pInfo->options_template;
	$files = array_merge($default_array, $files);
}
echo '<td class="main">'.TEXT_CHOOSE_OPTIONS_TEMPLATE.':'.'</td>';
echo '<td><span class="main">'.xtc_draw_pull_down_menu('options_template', $files, $default_value);
?>
        </span></td>
      </tr>

<tr>
  <td><span class="main"><?php echo TEXT_PRODUCTS_DELIVERY_FOR_FREE; ?></span></td>
  <td><span class="main"><?php echo xtc_draw_selection_field('products_delivery_for_free', 'checkbox', '1',$pInfo->products_delivery_for_free==1 ? true : false); ?></span></td>
</tr>

<tr>
<td style="padding:3px;border:1px solid #c00;"><span class="main">Grundpreis:&nbsp;</span><span class="main"><?php echo xtc_draw_selection_field('products_grundpreis', 'checkbox', '1',$pInfo->products_grundpreis==1 ? true : false); ?></span></td>
<td class="main" style="padding:3px;border:1px solid #c00;">Grundpreiseinheit:&nbsp;<?php echo xtc_draw_pull_down_menu('products_grundpreis_einheit', $products_grundpreis_einheit_array, $pInfo->products_grundpreis_einheit='' ?  DEFAULT_PRODUCTS_GRUNDPREIS_EINHEIT_ID : $pInfo->products_grundpreis_einheit); ?></td>
</tr>

<tr>
<td style="padding:3px;border:1px solid #c00;"><span class="main">Produkthervorhebung:&nbsp;</span><span class="main"><?php echo xtc_draw_selection_field('products_produkthervorhebung', 'checkbox', '1',$pInfo->products_produkthervorhebung==1 ? true : false); ?></span></td>
<td class="main" style="padding:3px;border:1px solid #c00;">Hervorhebungstext:&nbsp;<?php echo xtc_draw_pull_down_menu('products_produkthervorhebung_einheit', $products_produkthervorhebung_einheit_array, $pInfo->products_produkthervorhebung_einheit='' ?  DEFAULT_PRODUCTS_PRODUKTHERVORHEBUNG_EINHEIT_ID : $pInfo->products_produkthervorhebung_einheit); ?></td>
</tr>
    </table></td>
  </tr>
</table>
  <br /><br />

<?php
// Amazon: Buch / Tarotkarten
if ($product['amazon_category_name'] == 'BUCH') {
?>
<div class="amazon-styles-caption">
  Amazon: B&uuml;cher / Tarots
</div>

<table class="amazon-styles">
  <tr>
  <td class="main">Autor Nr.1:</td>
  <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_buch_autor_1', $products_buch_autor_1_array, $pInfo->products_buch_autor_1='' ?  DEFAULT_PRODUCTS_BUCH_AUTOR_ID : $pInfo->products_buch_autor_1); ?></td>
</tr>  
<tr>
  <td class="main">Autor Nr.2:</td>
  <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_buch_autor_2', $products_buch_autor_2_array, $pInfo->products_buch_autor_2='' ?  DEFAULT_PRODUCTS_BUCH_AUTOR_ID : $pInfo->products_buch_autor_2); ?></td>
</tr>  
<tr>
  <td class="main">Autor Nr.3:</td>
  <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_buch_autor_3', $products_buch_autor_3_array, $pInfo->products_buch_autor_3='' ?  DEFAULT_PRODUCTS_BUCH_AUTOR_ID : $pInfo->products_buch_autor_3); ?></td>
</tr>
<tr>
  <td class="main">Einband:</td>
  <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_buch_einband', $products_buch_einband_array, $pInfo->products_buch_einband='' ?  DEFAULT_PRODUCTS_BUCH_EINBAND_ID : $pInfo->products_buch_einband); ?></td>
</tr>   
<tr>
  <td><span class="main">Besondere Merkmale:</span></td>
  <td><span class="main huge-input-field"><?php echo xtc_draw_input_field('products_besondere_merkmale', $pInfo->products_besondere_merkmale); ?></span></td>
</tr>   
</table>
<br /><br />

<?php 
// Amazon: Musik
} else if ($product['amazon_category_name'] == 'MUSIK') {
?>  

<div class="amazon-styles-caption">
  Amazon: Musik
</div>

<table class="amazon-styles">
  <tr>
    <td class="main">Produkttyp:</td>
    <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_musik_produkttyp', $products_musik_produkttyp_array, $pInfo->products_musik_produkttyp='' ?  DEFAULT_MUSIK_PRODUKTTYP_ID : $pInfo->products_musik_produkttyp); ?></td>
  </tr>
  <tr>
    <td class="main">Ausgabe:</td>
    <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_musik_ausgabe', $products_musik_ausgabe_array, $pInfo->products_musik_ausgabe='' ?  DEFAULT_MUSIK_AUSGABE_ID : $pInfo->products_musik_ausgabe); ?></td>
  </tr>    
  <tr>
  <tr>
    <td class="main">Hersteller:</td>
    <td class="normal-input-field"><?php echo xtc_draw_pull_down_menu('products_musik_hersteller', $products_musik_hersteller_array, $pInfo->products_musik_hersteller='' ?  DEFAULT_MUSIK_HERSTELLER_ID : $pInfo->products_musik_hersteller); ?></td>
  </tr>    
  <tr>    
    <td><span class="main">Besondere Merkmale:</span></td>
    <td><span class="main huge-input-field"><?php echo  xtc_draw_input_field('products_besondere_merkmale', $pInfo->products_besondere_merkmale); ?></span></td>
  </tr>   
</table>
<br /><br />

<?php 
}
?>

  <?php for ($i = 0, $n = sizeof($languages); $i < $n; $i++) { ?>
  <table width="100%" border="0">
  <tr>
  <td bgcolor="000000" height="10"></td>
  </tr>
  <tr>
    <td bgcolor="#FFCC33" valign="top" class="main"><?php echo xtc_image(DIR_WS_LANGUAGES . $languages[$i]['directory'] .'/'. $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;<?php echo TEXT_PRODUCTS_NAME; ?><?php echo xtc_draw_input_field('products_name[' . $languages[$i]['id'] . ']', (($products_name[$languages[$i]['id']]) ? stripslashes($products_name[$languages[$i]['id']]) : xtc_get_products_name($pInfo->products_id, $languages[$i]['id'])),'size=60'); ?></td>
  </tr>
  <tr>
    <td class="main"><?php echo TEXT_PRODUCTS_URL . '&nbsp;<small>' . TEXT_PRODUCTS_URL_WITHOUT_HTTP . '</small>'; ?><?php echo xtc_draw_input_field('products_url[' . $languages[$i]['id'] . ']', (($products_url[$languages[$i]['id']]) ? stripslashes($products_url[$languages[$i]['id']]) : xtc_get_products_url($pInfo->products_id, $languages[$i]['id'])),'size=60'); ?></td>
  </tr>
</table>

<!-- input boxes desc, meta etc -->
<table width="100%" border="0">
  <tr>
    <td class="main">
        <strong><?php echo TEXT_PRODUCTS_DESCRIPTION; ?></strong><br />
        <?php echo xtc_draw_textarea_field('products_description_' . $languages[$i]['id'], 'soft', '103', '30', (($products_description[$languages[$i]['id']]) ? stripslashes($products_description[$languages[$i]['id']]) : xtc_get_products_description($pInfo->products_id, $languages[$i]['id']))); ?>
    </td>
  </tr>
  <tr>
    <td class="main" valign="top">
    
    <table>
    <tr>
     <td width="60%" valign="top" class="main">
        <strong><?php echo TEXT_PRODUCTS_SHORT_DESCRIPTION; ?></strong><br />
        <?php echo xtc_draw_textarea_field('products_short_description_' . $languages[$i]['id'], 'soft', '103', '20', (($products_short_description[$languages[$i]['id']]) ? stripslashes($products_short_description[$languages[$i]['id']]) : xtc_get_products_short_description($pInfo->products_id, $languages[$i]['id']))); ?>
     </td>
     <td class="main" valign="top" style="padding: 15px;">
        <?php echo TEXT_PRODUCTS_KEYWORDS; ?><br />
        <?php echo xtc_draw_input_field('products_keywords[' . $languages[$i]['id'] . ']',(($products_keywords[$languages[$i]['id']]) ? stripslashes($products_keywords[$languages[$i]['id']]) : xtc_get_products_keywords($pInfo->products_id, $languages[$i]['id'])), 'size=25 maxlenght=255'); ?><br />     
        <?php echo TEXT_META_TITLE; ?><br />
        <?php echo xtc_draw_input_field('products_meta_title[' . $languages[$i]['id'] . ']',(($products_meta_title[$languages[$i]['id']]) ? stripslashes($products_meta_title[$languages[$i]['id']]) : xtc_get_products_meta_title($pInfo->products_id, $languages[$i]['id'])), 'size=25 maxlenght=50'); ?><br />
        <?php echo TEXT_META_DESCRIPTION; ?><br />
        <?php echo xtc_draw_input_field('products_meta_description[' . $languages[$i]['id'] . ']',(($products_meta_description[$languages[$i]['id']]) ? stripslashes($products_meta_description[$languages[$i]['id']]) : xtc_get_products_meta_description($pInfo->products_id, $languages[$i]['id'])), 'size=25 maxlenght=50'); ?><br />
        <?php echo TEXT_META_KEYWORDS; ?><br />
        <?php echo xtc_draw_input_field('products_meta_keywords[' . $languages[$i]['id'] . ']', (($products_meta_keywords[$languages[$i]['id']]) ? stripslashes($products_meta_keywords[$languages[$i]['id']]) : xtc_get_products_meta_keywords($pInfo->products_id, $languages[$i]['id'])), 'size=25 maxlenght=50'); ?> <br />
        <?php echo TEXT_SEO_URLS; ?><br />
        <?php echo xtc_draw_input_field('products_seo[' . $languages[$i]['id'] . ']', (($products_seo[$languages[$i]['id']]) ? stripslashes($products_seo[$languages[$i]['id']]) : xtc_get_products_seo($pInfo->products_id, $languages[$i]['id'])), 'size=25'); ?> <br />
        <?php //opsedi: Anzeige der Yatego Kategorien im Produkt // ?>
        <div style="border: 2px dashed red;margin-top: 10px;padding: 5px;">
        	Yatego Kategorien:<br />
	        <?php echo xtc_draw_input_field('yatego_cat[' . $languages[$i]['id'] . ']', (($yatego_cat[$languages[$i]['id']]) ? stripslashes($yatego_cat[$languages[$i]['id']]) : xtc_get_yatego_cats($pInfo->products_id, $languages[$i]['id'])), 'size=45'); ?> 
        </div>


     </td>
    </tr>
    </table>
   
   </td>
  </tr>
</table>

<?php } ?>
<table width="100%"><tr><td style="border-bottom: thin dashed Gray;">&nbsp;</td></tr></table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td><span class="main" style="padding-left: 10px;"><?php echo HEADING_PRODUCT_IMAGES; ?></span></td></tr>
<tr><td><br />
<table width="100%" border="0" bgcolor="f3f3f3" style="border: 1px solid; border-color: #cccccc;">

<?php

include (DIR_WS_MODULES.'products_images.php');
?>
    <tr><td colspan="4"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td></tr>
<?php

if (GROUP_CHECK == 'true') {
	$customers_statuses_array = xtc_get_customers_statuses();
	$customers_statuses_array = array_merge(array (array ('id' => 'all', 'text' => TXT_ALL)), $customers_statuses_array);
?>
<tr>
<td style="border-top: 1px solid; border-color: #ff0000;" valign="top" class="main" ><?php echo ENTRY_CUSTOMERS_STATUS; ?></td>
<td style="border-top: 1px solid; border-left: 1px solid; border-color: #ff0000;"  bgcolor="#FFCC33" class="main">
<?php

	for ($i = 0; $n = sizeof($customers_statuses_array), $i < $n; $i ++) {
		$code = '$id=$pInfo->group_permission_'.$customers_statuses_array[$i]['id'].';';
		eval ($code);
		
		if ($id==1) {

			$checked = 'checked ';
			
		} else {
			$checked = '';
		}
		echo '<input type="checkbox" name="groups[]" value="'.$customers_statuses_array[$i]['id'].'"'.$checked.'> '.$customers_statuses_array[$i]['text'].'<br />';
	}

?>
</td>
</tr>
<?php

}
?>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0">
        <tr>
          <td colspan="4"><?php include(DIR_WS_MODULES.'group_prices.php'); ?></td>
        </tr>
        <tr>
          <td colspan="4"><?php echo xtc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
        </tr>
</table>
</td></tr>

    <tr>
     <td class="main" align="right">
      	<?php

echo xtc_draw_hidden_field('products_date_added', (($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')));
echo xtc_draw_hidden_field('products_id', $pInfo->products_id);
?>
      	<input type="submit" class="button" value="<?php echo BUTTON_SAVE; ?>" onclick="return confirm('<?php echo SAVE_ENTRY; ?>')">
      	&nbsp;&nbsp;
      	<?php echo '<a class="button" href="' . xtc_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&pID=' . $_GET['pID']) . '">' . BUTTON_CANCEL . '</a>'; ?>
  	 </td>
    </tr></form>