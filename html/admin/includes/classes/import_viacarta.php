<?php
/* --------------------------------------------------------------
$Id: import.php 2008-09-21 André Estel $

Estelco - Ebusiness and more
Copyright (c) 2008 Estelco

--------------------------------------------------------------
based on:
(c) 2003 xtcommerce (import.php 1319 2005-10-23 10:35:15Z mz); www.xt-commerce.com

Released under the GNU General Public License
--------------------------------------------------------------
*/
defined('_VALID_XTC') or die('Direct Access to this location is not allowed.');
//$LIMIT = 'LIMIT 0,150';
$LIMIT = '';

if($_GET['action'] == 'export_viacarta')
{
	$sep = ';';
	include('includes/configure.php');
	$server = DB_SERVER;
	$username = DB_SERVER_USERNAME;
	$password = DB_SERVER_PASSWORD;
	$database = DB_DATABASE;
	$link = mysql_connect($server, $username, $password);
	mysql_select_db($database, $link);
	
	// products
	$file_p = 'products.csv';
	$schema_p = 'products_id_p;products_ean_p;products_quantity_p;products_shippingtime_p;products_model_p;group_permission_0_p;group_permission_1_p;group_permission_2_p;group_permission_3_p;products_sort_p;products_image_p;products_price_p;products_discount_allowed_p;products_date_added_p;products_last_modified_p;products_date_available_p;products_weight_p;products_status_p;products_tax_class_id_p;product_template_p;options_template_p;manufacturers_id_p;products_ordered_p;products_fsk18_p;products_vpe_p;products_vpe_status_p;products_vpe_value_p;products_startpage_p;products_startpage_sort_p;group_permission_4_p;group_permission_5_p'."\n";				
	$sql_export_p = "SELECT * FROM ".TABLE_PRODUCTS." ".$LIMIT."";
	$result_export_p = mysql_query($sql_export_p, $link);
	for($y_p=0;$y_p<mysql_num_rows($result_export_p);$y_p++) { $ergebnis_export_p[$y_p]=mysql_fetch_array($result_export_p); }
	for($y_p=0;$y_p<count($ergebnis_export_p);$y_p++)
	{	
		$products_id_p[$y_p] = $ergebnis_export_p[$y_p]['products_id'];
		$products_ean_p[$y_p] = $ergebnis_export_p[$y_p]['products_ean'];
		$products_quantity_p[$y_p] = '99999';
		$products_shippingtime_p[$y_p] = $ergebnis_export_p[$y_p]['products_shippingtime'];
		$products_model_p[$y_p] = $ergebnis_export_p[$y_p]['products_model'];
		$group_permission_0_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_0'];
		$group_permission_1_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_1'];
		$group_permission_2_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_2'];
		$group_permission_3_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_3'];
		$products_sort_p[$y_p] = $ergebnis_export_p[$y_p]['products_sort'];
		$products_image_p[$y_p] = $ergebnis_export_p[$y_p]['products_image'];
		
		// Steuersatz
		$products_tax_class_id_p[$y_p] = $ergebnis_export_p[$y_p]['products_tax_class_id'];
		
		// Anpassen Preis
		$aufschlag = '1.15';
		$products_price_p[$y_p] = $ergebnis_export_p[$y_p]['products_price'];
		$products_price_p[$y_p] = $products_price_p[$y_p] * $aufschlag;
		if($products_tax_class_id_p[$y_p] == '1') // 19%
		{
			$products_price_p[$y_p] = $products_price_p[$y_p] * '1.19';
			$products_price_p[$y_p] = explode('.', $products_price_p[$y_p]);
			$products_price_p[$y_p] = $products_price_p[$y_p][0].'.99';
			$products_price_p[$y_p] = $products_price_p[$y_p] / '1.19';
		}
		else if($products_tax_class_id_p[$y_p] == '2') // 7%
		{
			$products_price_p[$y_p] = $products_price_p[$y_p] * '1.07';
			$products_price_p[$y_p] = explode('.', $products_price_p[$y_p]);
			$products_price_p[$y_p] = $products_price_p[$y_p][0].'.99';
			$products_price_p[$y_p] = $products_price_p[$y_p] / '1.07';
		}		
		
		$products_price_p[$y_p] = str_replace('.', ',', $products_price_p[$y_p]);
		
		$products_discount_allowed_p[$y_p] = $ergebnis_export_p[$y_p]['products_discount_allowed'];
		$products_date_added_p[$y_p] = '';
		$products_last_modified_p[$y_p] = '';
		$products_date_available_p[$y_p] = '';
		$products_weight_p[$y_p] = $ergebnis_export_p[$y_p]['products_weight'];
		$products_status_p[$y_p] = $ergebnis_export_p[$y_p]['products_status'];
		$product_template_p[$y_p] = 'product_info_v1.html';
		$options_template_p[$y_p] = 'default';
		$manufacturers_id_p[$y_p] = $ergebnis_export_p[$y_p]['manufacturers_id'];
		$products_ordered_p[$y_p] = $ergebnis_export_p[$y_p]['products_ordered'];
		$products_fsk18_p[$y_p] = $ergebnis_export_p[$y_p]['products_fsk18'];
		$products_vpe_p[$y_p] = $ergebnis_export_p[$y_p]['products_vpe'];
		$products_vpe_status_p[$y_p] = $ergebnis_export_p[$y_p]['products_vpe_status'];
		$products_vpe_value_p[$y_p] = $ergebnis_export_p[$y_p]['products_vpe_value'];
		$products_startpage_p[$y_p] = $ergebnis_export_p[$y_p]['products_startpage'];
		$products_startpage_sort_p[$y_p] = $ergebnis_export_p[$y_p]['products_startpage_sort'];
		$group_permission_4_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_4'];
		$group_permission_5_p[$y_p] = $ergebnis_export_p[$y_p]['group_permission_5'];
		
		$schema_p .= $products_id_p[$y_p].$sep.
					  $products_ean_p[$y_p].$sep.
					  $products_quantity_p[$y_p].$sep.
					  $products_shippingtime_p[$y_p].$sep.
					  $products_model_p[$y_p].$sep.
					  $group_permission_0_p[$y_p].$sep.
					  $group_permission_1_p[$y_p].$sep.
					  $group_permission_2_p[$y_p].$sep.
					  $group_permission_3_p[$y_p].$sep.
					  $products_sort_p[$y_p].$sep.
					  $products_image_p[$y_p].$sep.
					  $products_price_p[$y_p].$sep.
					  $products_discount_allowed_p[$y_p].$sep.
					  $products_date_added_p[$y_p].$sep.
					  $products_last_modified_p[$y_p].$sep.
					  $products_date_available_p[$y_p].$sep.
					  $products_weight_p[$y_p].$sep.
					  $products_status_p[$y_p].$sep.
					  $products_tax_class_id_p[$y_p].$sep.
					  $product_template_p[$y_p].$sep.
					  $options_template_p[$y_p].$sep.
					  $manufacturers_id_p[$y_p].$sep.
					  $products_ordered_p[$y_p].$sep.
					  $products_fsk18_p[$y_p].$sep.
					  $products_vpe_p[$y_p].$sep.
					  $products_vpe_status_p[$y_p].$sep.
					  $products_vpe_value_p[$y_p].$sep.
					  $products_startpage_p[$y_p].$sep.
					  $products_startpage_sort_p[$y_p].$sep.
					  $group_permission_4_p[$y_p].$sep.
					  $group_permission_5_p[$y_p].
					  "\n";	

		// create file
		$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/viacarta/' . $file_p, "w+");
       	fputs($fp, $schema_p);
       	fclose($fp);					
	}
		
	// products_description
	$file_pd = 'products_description.csv';
	$schema_pd = 'products_id_pd;products_name_pd;products_description_pd;products_short_description_pd;products_keywords_pd'."\n";			
	$sql_export_pd = "SELECT * FROM ".TABLE_PRODUCTS_DESCRIPTION." ".$LIMIT."";
	$result_export_pd = mysql_query($sql_export_pd, $link);
	for($y_pd=0;$y_pd<mysql_num_rows($result_export_pd);$y_pd++) { $ergebnis_export_pd[$y_pd]=mysql_fetch_array($result_export_pd); }
	for($y_pd=0;$y_pd<count($ergebnis_export_pd);$y_pd++)
	{	
		$products_id_pd[$y_pd] = $ergebnis_export_pd[$y_pd]['products_id'];
		
		$products_name_pd[$y_pd] = $ergebnis_export_pd[$y_pd]['products_name'];
		$products_name_pd[$y_pd] = str_replace("&uacute;", "&uacute,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("center;", "center,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&acute;", "&acute,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&lt;", "&lt,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&gt;", "&gt,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&ccedil;", "&ccedil,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&laquo;", "&laquo,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&raquo;", "&raquo,", $products_name_pd[$y_pd]);				
		$products_name_pd[$y_pd] = str_replace("&nbsp;", " ", $products_name_pd[$y_pd]);
 		$products_name_pd[$y_pd] = str_replace("&uuml;", "&uuml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&auml;", "&auml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&ouml;", "&ouml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&Uuml;", "&Uuml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&Auml;", "&Auml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&Ouml;", "&Ouml,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&amp;", "&amp,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&quot;", "&quot,",$products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&qout;", "&qout,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&szlig;", "&szlig,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&reg;", "&reg,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("&deg;", "&deg,", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("  ", " ", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("\r", "", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace("\n", "", $products_name_pd[$y_pd]);			
		$products_name_pd[$y_pd] = str_replace(chr(13), "", $products_name_pd[$y_pd]);
		$products_name_pd[$y_pd] = str_replace(";", ",", $products_name_pd[$y_pd]);
				
		$products_description_pd[$y_pd] = $ergebnis_export_pd[$y_pd]['products_description'];
		$products_description_pd[$y_pd] = str_replace("&uacute;", "&uacute,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("center;", "center,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&acute;", "&acute,", $products_description_pd[$y_pd]);		
		$products_description_pd[$y_pd] = str_replace("&lt;", "&lt,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&gt;", "&gt,", $products_description_pd[$y_pd]);		
		$products_description_pd[$y_pd] = str_replace("&ccedil;", "&ccedil,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&laquo;", "&laquo,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&raquo;", "&raquo,", $products_description_pd[$y_pd]);						
		$products_description_pd[$y_pd] = str_replace("&nbsp;", " ", $products_description_pd[$y_pd]);
 		$products_description_pd[$y_pd] = str_replace("&uuml;", "&uuml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&auml;", "&auml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&ouml;", "&ouml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&Uuml;", "&Uuml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&Auml;", "&Auml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&Ouml;", "&Ouml,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&amp;", "&amp,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&quot;", "&quot,",$products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&qout;", "&qout,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&szlig;", "&szlig,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&reg;", "&reg,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("&deg;", "&deg,", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("  ", " ", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("\r", "", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace("\n", "", $products_description_pd[$y_pd]);			
		$products_description_pd[$y_pd] = str_replace(chr(13), "", $products_description_pd[$y_pd]);
		$products_description_pd[$y_pd] = str_replace(";", ",", $products_description_pd[$y_pd]);
		
		$products_short_description_pd[$y_pd] = $ergebnis_export_pd[$y_pd]['products_short_description'];
		$products_short_description_pd[$y_pd] = str_replace("&uacute;", "&uacute,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("center;", "center,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&acute;", "&acute,", $products_short_description_pd[$y_pd]);		
		$products_short_description_pd[$y_pd] = str_replace("&lt;", "&lt,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&gt;", "&gt,", $products_short_description_pd[$y_pd]);				
		$products_short_description_pd[$y_pd] = str_replace("&ccedil;", "&ccedil,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&laquo;", "&laquo,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&raquo;", "&raquo,", $products_short_description_pd[$y_pd]);					
		$products_short_description_pd[$y_pd] = str_replace("&nbsp;", " ", $products_short_description_pd[$y_pd]);
 		$products_short_description_pd[$y_pd] = str_replace("&uuml;", "&uuml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&auml;", "&auml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&ouml;", "&ouml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&Uuml;", "&Uuml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&Auml;", "&Auml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&Ouml;", "&Ouml,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&amp;", "&amp,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&quot;", "&quot,",$products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&qout;", "&qout,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&szlig;", "&szlig,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&reg;", "&reg,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("&deg;", "&deg,", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("  ", " ", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("\r", "", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace("\n", "", $products_short_description_pd[$y_pd]);			
		$products_short_description_pd[$y_pd] = str_replace(chr(13), "", $products_short_description_pd[$y_pd]);
		$products_short_description_pd[$y_pd] = str_replace(";", ",", $products_short_description_pd[$y_pd]);
		
		$products_keywords_pd[$y_pd] = $products_name_pd[$y_pd];
		
		$schema_pd .= $products_id_pd[$y_pd].$sep.$products_name_pd[$y_pd].$sep.$products_description_pd[$y_pd].$sep.$products_short_description_pd[$y_pd].$sep.$products_keywords_pd[$y_pd]."\n";	

		// create file
		$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/viacarta/' . $file_pd, "w+");
       	fputs($fp, $schema_pd);
       	fclose($fp);					
	}
	
	// products_to_categories
	$file_ptc = 'products_to_categories.csv';
	$schema_ptc = 'products_id_ptc;categories_id_ptc;'."\n";		
	$sql_export_ptc = "SELECT * FROM ".TABLE_PRODUCTS_TO_CATEGORIES." ".$LIMIT."";
	$result_export_ptc = mysql_query($sql_export_ptc, $link);
	for($y_ptc=0;$y_ptc<mysql_num_rows($result_export_ptc);$y_ptc++) { $ergebnis_export_ptc[$y_ptc]=mysql_fetch_array($result_export_ptc); }
	for($y_ptc=0;$y_ptc<count($ergebnis_export_ptc);$y_ptc++)
	{	
		$products_id_ptc[$y_ptc] = $ergebnis_export_ptc[$y_ptc]['products_id'];
		$categories_id_ptc[$y_ptc] = $ergebnis_export_ptc[$y_ptc]['categories_id'];
		
		$schema_ptc .=$products_id_ptc[$y_ptc].$sep.$categories_id_ptc[$y_ptc]."\n";	

		// create file
		$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/viacarta/' . $file_ptc, "w+");
       	fputs($fp, $schema_ptc);
       	fclose($fp);			
	}		
	
	// categories
	$file_c = 'categories.csv';
	$schema_c = 'categories_id_c;categories_image_c;parent_id_c;categories_status_c;categories_template_c;group_permission_0_c;group_permission_1_c;group_permission_2_c;group_permission_3_c;listing_template_c;sort_order_c;products_sorting_c;products_sorting2_c;date_added_c;last_modified_c;group_permission_4_c;group_permission_5_c'."\n";	
	$sql_export_c = "SELECT * FROM ".TABLE_CATEGORIES." ".$LIMIT."";
	$result_export_c = mysql_query($sql_export_c, $link);
	for($y_c=0;$y_c<mysql_num_rows($result_export_c);$y_c++) { $ergebnis_export_c[$y_c]=mysql_fetch_array($result_export_c); }
	for($y_c=0;$y_c<count($ergebnis_export_c);$y_c++)
	{	
		$categories_id_c[$y_c] = $ergebnis_export_c[$y_c]['categories_id'];
		$categories_image_c[$y_c] = $ergebnis_export_c[$y_c]['categories_image'];
		$parent_id_c[$y_c] = $ergebnis_export_c[$y_c]['parent_id'];
		$categories_status_c[$y_c] = $ergebnis_export_c[$y_c]['categories_status'];
		$categories_template_c[$y_c] = $ergebnis_export_c[$y_c]['categories_template'];
		$group_permission_0_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_0'];
		$group_permission_1_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_1'];
		$group_permission_2_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_2'];
		$group_permission_3_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_3'];
		$listing_template_c[$y_c] = $ergebnis_export_c[$y_c]['listing_template'];
		$sort_order_c[$y_c] = $ergebnis_export_c[$y_c]['sort_order'];
		$products_sorting_c[$y_c] = $ergebnis_export_c[$y_c]['products_sorting'];
		$products_sorting2_c[$y_c] = $ergebnis_export_c[$y_c]['products_sorting2'];
		$date_added_c[$y_c] = '';
		$last_modified_c[$y_c] = '';
		$group_permission_4_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_4'];
		$group_permission_5_c[$y_c] = $ergebnis_export_c[$y_c]['group_permission_5'];
		
		$schema_c .= $categories_id_c[$y_c].$sep.$categories_image_c[$y_c].$sep.$parent_id_c[$y_c].$sep.$categories_status_c[$y_c].$sep.$categories_template_c[$y_c].$sep.
					$group_permission_0_c[$y_c].$sep.$group_permission_1_c[$y_c].$sep.$group_permission_2_c[$y_c].$sep.$group_permission_3_c[$y_c].$sep.$listing_template_c[$y_c].
					$sep.$sort_order_c[$y_c].$sep.$products_sorting_c[$y_c].$sep.$products_sorting2_c[$y_c].$sep.$date_added_c[$y_c].$sep.$last_modified_c[$y_c].$sep.$group_permission_4_c[$y_c].
					$sep.$group_permission_5_c[$y_c]."\n";
		
		// create file
		$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/viacarta/' . $file_c, "w+");
       	fputs($fp, $schema_c);
       	fclose($fp);
	}			

	// categories_description
	$file_cd = 'categories_description.csv';
	$schema_cd = 'categories_id;language_id;categories_name;categories_heading_title;categories_description_cd;categories_meta_title;categories_meta_description;categories_meta_keywords'."\n";
	$sql_export_cd = "SELECT * FROM ".TABLE_CATEGORIES_DESCRIPTION." ".$LIMIT."";
	$result_export_cd = mysql_query($sql_export_cd, $link);
	for($y_cd=0;$y_cd<mysql_num_rows($result_export_cd);$y_cd++) { $ergebnis_export_cd[$y_cd]=mysql_fetch_array($result_export_cd); }
	for($y_cd=0;$y_cd<count($ergebnis_export_cd);$y_cd++)
	{	
		$categories_id_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_id'];
		$language_id_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['language_id'];
		
		$categories_name_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_name'];
		$categories_name_cd[$y_cd] = str_replace("&uacute;", "&uacute,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("center;", "center,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&acute;", "&acute,", $categories_name_cd[$y_cd]);		
		$categories_name_cd[$y_cd] = str_replace("&lt;", "&lt,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&gt;", "&gt,", $categories_name_cd[$y_cd]);				
		$categories_name_cd[$y_cd] = str_replace("&ccedil;", "&ccedil,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&laquo;", "&laquo,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&raquo;", "&raquo,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&nbsp;", " ", $categories_name_cd[$y_cd]);
 		$categories_name_cd[$y_cd] = str_replace("&uuml;", "&uuml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&auml;", "&auml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&ouml;", "&ouml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&Uuml;", "&Uuml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&Auml;", "&Auml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&Ouml;", "&Ouml,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&amp;", "&amp,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&quot;", "&quot,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&qout;", "&qout,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&szlig;", "&szlig,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&reg;", "&reg,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("&deg;", "&deg,", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("  ", " ", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("\r", "", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace("\n", "", $categories_name_cd[$y_cd]);			
		$categories_name_cd[$y_cd] = str_replace(chr(13), "", $categories_name_cd[$y_cd]);
		$categories_name_cd[$y_cd] = str_replace(";", ",", $categories_name_cd[$y_cd]);
		
		$categories_heading_title_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_heading_title'];
		$categories_heading_title_cd[$y_cd] = str_replace("&uacute;", "&uacute,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("center;", "center,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&acute;", "&acute,", $categories_heading_title_cd[$y_cd]);			
		$categories_heading_title_cd[$y_cd] = str_replace("&lt;", "&lt,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&gt;", "&gt,", $categories_heading_title_cd[$y_cd]);					
		$categories_heading_title_cd[$y_cd] = str_replace("&ccedil;", "&ccedil,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&laquo;", "&laquo,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&raquo;", "&raquo,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&nbsp;", " ", $categories_heading_title_cd[$y_cd]);
 		$categories_heading_title_cd[$y_cd] = str_replace("&uuml;", "&uuml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&auml;", "&auml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&ouml;", "&ouml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&Uuml;", "&Uuml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&Auml;", "&Auml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&Ouml;", "&Ouml,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&amp;", "&amp,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&quot;", "&quot,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&qout;", "&qout,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&szlig;", "&szlig,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&reg;", "&reg,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("&deg;", "&deg,", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("  ", " ", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("\r", "", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace("\n", "", $categories_heading_title_cd[$y_cd]);			
		$categories_heading_title_cd[$y_cd] = str_replace(chr(13), "", $categories_heading_title_cd[$y_cd]);
		$categories_heading_title_cd[$y_cd] = str_replace(";", ",", $categories_heading_title_cd[$y_cd]);
		
		$categories_description_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_description'];
		$categories_description_cd[$y_cd] = str_replace("&uacute;", "&uacute,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("center;", "center,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&acute;", "&acute,", $categories_description_cd[$y_cd]);			
		$categories_description_cd[$y_cd] = str_replace("&lt;", "&lt,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&gt;", "&gt,", $categories_description_cd[$y_cd]);					
		$categories_description_cd[$y_cd] = str_replace("&ccedil;", "&ccedil,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&laquo;", "&laquo,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&raquo;", "&raquo,", $categories_description_cd[$y_cd]);		
		$categories_description_cd[$y_cd] = str_replace("&nbsp;", " ", $categories_description_cd[$y_cd]);
 		$categories_description_cd[$y_cd] = str_replace("&uuml;", "&uuml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&auml;", "&auml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&ouml;", "&ouml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&Uuml;", "&Uuml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&Auml;", "&Auml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&Ouml;", "&Ouml,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&amp;", "&amp,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&quot;", "&quot,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&qout;", "&qout,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&szlig;", "&szlig,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&reg;", "&reg,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&deg;", "&deg,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("&#65533;", "&#65533,", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("  ", " ", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("\r", "", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace("\n", "", $categories_description_cd[$y_cd]);			
		$categories_description_cd[$y_cd] = str_replace(chr(13), "", $categories_description_cd[$y_cd]);
		$categories_description_cd[$y_cd] = str_replace(";", ",", $categories_description_cd[$y_cd]);
				
		$categories_meta_title_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_meta_title'];
		$categories_meta_description_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_meta_description'];
		$categories_meta_keywords_cd[$y_cd] = $ergebnis_export_cd[$y_cd]['categories_meta_keywords'];
		
		$schema_cd .= 	$categories_id_cd[$y_cd].$sep.$language_id_cd[$y_cd].$sep.$categories_name_cd[$y_cd].$sep.$categories_heading_title_cd[$y_cd].$sep.$categories_description_cd[$y_cd].$sep.
					$categories_meta_title_cd[$y_cd].$sep.$categories_meta_description_cd[$y_cd].$sep.$categories_meta_keywords_cd[$y_cd]."\n";
		
		// create file
		$fp = fopen(DIR_FS_DOCUMENT_ROOT.'export/viacarta/' . $file_cd, "w+");
       	fputs($fp, $schema_cd);
       	fclose($fp);
	}
							
}
?>