<?php
/*
admin/includes/news_info_images.php	14.11.2005
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/
$news = new image_manipulation(
					DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . $news_image_name, 
					NEWS_IMAGE_INFO_WIDTH, 
					NEWS_IMAGE_INFO_HEIGHT, 
					DIR_FS_CATALOG_INFO_NEWS_IMAGES . $news_image_name, 
					IMAGE_QUALITY,
					'');			
$news->create();
?>