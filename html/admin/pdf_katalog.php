<?php
/**
 * xtc-addons.de - PDFcat
 * Addon for xt:Commerce 3.04
 * compatible to SP2.1
 * 
 * Copyright by Damian Gawenda
 * www.xtc-addons.de
 * 
 * @author Damian Gawenda <info@xtc-addons.de>
 * @package xtc_addons_pdfcat
 */

$ebene = '';

require('includes/application_top.php');
require_once(DIR_FS_INC . 'xtc_add_tax.inc.php');
require_once (DIR_FS_INC.'xtc_get_subcategories.inc.php');
define('FPDF_FONTPATH',DIR_WS_CLASSES.'fpdf/font/');
require(DIR_WS_CLASSES . 'fpdf/fpdf.php');
require_once(DIR_WS_MODULES . 'pdfkatalog/PDFcat.php');

// ACTIONS //
if ( $_GET['action'] == 'generate' ) {
	if($_POST['saveprofile'] == "on"){
		if(!$_POST["specials"]){ 
			$_POST["specials"] = 0; 
		}
		$query = 'INSERT INTO pdfkatalog_profile ( pdfkatalog_profile_name, pdfkatalog_profile_deckblatt, pdfkatalog_profile_beiblatt, pdfkatalog_profile_prices, pdfkatalog_profile_specials) VALUES ("'.$_POST["profilename"].'",'.$_POST["deckblatt"].','.$_POST["beiblatt"].','.$_POST["prices"].','.$_POST["specials"].')';
		xtc_db_query($query);
		$profile_id = xtc_db_insert_id();
		for($i_profile=0;$i_profile<sizeof($_POST["products"]);$i_profile++){
			$query = 'INSERT INTO pdfkatalog_profile_products ( pdfkatalog_profile_id, products_id) VALUES ("'.$profile_id.'",'.$_POST["products"][$i_profile].')';
			xtc_db_query($query);
		}
		for($i_cat=0;$i_cat<sizeof($_POST["categories"]);$i_cat++){
			$query = 'INSERT INTO pdfkatalog_profile_cat ( pdfkatalog_profile_id, cat_id) VALUES ("'.$profile_id.'",'.$_POST["categories"][$i_cat].')';
			xtc_db_query($query);
		}
	}
	// print catalog
	$pdf=new PDFcat();
	$pdf->data=$data;
	$pdf->_pdf_lang=2;
	$pdf->default_height=30;
	$pdf->max_height=280;
	$pdf->shorttext	=	1;  		//inklusiv Shorttext
	$pdf->doc_name	=	"Katalog_".$_POST['profilename'];   //DokumentName
	$pdf->domain 	=	$_SERVER['HTTP_HOST'];    		//Website
	$pdf->filesplit	=	$filesplit;    		//Mehere PDF Dokumente
	$pdf->product_link = "http://".$pdf->domain."/".$ebene."product_info.php?products_id={product_id}?language=".$pdf_lang['code'];
	if($_POST['deckblatt']){
		$pdf->AddPage();
		$pdf->Image("includes/modules/pdfkatalog/deckblatt_pdf.png",0,0,'','','');
	}
	$pdf->LoadData($_POST['products'], $_POST['specials']);
	$pdf->format($_POST['prices']);
	if($_POST['beiblatt']){
		$pdf->AddPage();
		$pdf->Image("includes/modules/pdfkatalog/beiblatt_pdf.png",0,0,'','','');
	}
	$pdf->Output($pdf->doc_name.".pdf", "D");
  }
// ACTIONS //
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script type="text/javascript" src="includes/general.js"></script>
<script language="javascript" type="text/javascript">
<!-- 
function select_cat(cat, value){
	var anzahl = document.getElementsByName("products[]").length;
	var elements = document.getElementsByName("products[]");
		for(i=0;i<anzahl;i++){
		if(elements[i].id == cat){
				elements[i].checked = value
		}
	}
}
function selectall(value){
	var anzahl_p = document.getElementsByName("products[]").length;
	var elements_p = document.getElementsByName("products[]");
	for(i=0;i<anzahl_p;i++){
		elements_p[i].checked = value;
	}
	var anzahl_c = document.getElementsByName("categories[]").length;
	var elements_c = document.getElementsByName("categories[]");
	for(i=0;i<anzahl_c;i++){
		elements_c[i].checked = value;
	}
}
//-->
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top">
    	<table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
			<!-- left_navigation //-->
			<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
			<!-- left_navigation_eof //-->
    	</table>
    </td>
    
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top">
    	<table border="0" width="100%" cellspacing="0" cellpadding="2">
	      <tr>
	        <td width="100%">
	        	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		          <tr>
		            <td class="pageHeading">PDF-Katalog erstellen</td>
		            <td class="pageHeading" align="right"><?php echo xtc_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
		          </tr>
       			</table>
       		</td>
      	</tr>
      </table>
      <hr>      
      <form name="pdfkatalog" <?php echo 'action="' . xtc_href_link('pdf_katalog.php', 'action=generate', 'NONSSL') . '"'; ?> method="post">
      <!-- saved profiles -->
      <table class="dataTableHeadingContent" border="0" width="100%" cellspacing="0" cellpadding="2">
      	<tr><td width="90%">Gespeicherte Profile:&nbsp;
      	<?php 
      		if($_GET['del_profile']){
      			$query_del = "DELETE FROM pdfkatalog_profile";
      			xtc_db_query($query_del);
      			$query_del2 = "DELETE FROM pdfkatalog_profile_products";
      			xtc_db_query($query_del2);
      			$query_del3 = "DELETE FROM pdfkatalog_profile_cat";
      			xtc_db_query($query_del3);
      		}
      		if($_GET['profile_id']){
				$query_p1 = 'SELECT * FROM pdfkatalog_profile WHERE pdfkatalog_profile_id = '.$_GET['profile_id'];
				$profile_query_p1 = xtc_db_query($query_p1);
				while($profile = xtc_db_fetch_array($profile_query_p1)){
					$profile_beiblatt = $profile['pdfkatalog_profile_beiblatt'];
					$profile_deckblatt = $profile['pdfkatalog_profile_deckblatt'];
					$profile_prices = $profile['pdfkatalog_profile_prices'];
					$profile_specials = $profile['pdfkatalog_profile_specials'];
				}
				$query_p2 = 'SELECT * FROM pdfkatalog_profile_products WHERE pdfkatalog_profile_id = '.$_GET['profile_id'];
				$profile_query_p2 = xtc_db_query($query_p2);
				while($profile = xtc_db_fetch_array($profile_query_p2)){
					$profile_products[] = $profile['products_id'];
				}
				$query_p3 = 'SELECT * FROM pdfkatalog_profile_cat WHERE pdfkatalog_profile_id = '.$_GET['profile_id'];
				$profile_query_p3 = xtc_db_query($query_p3);
				while($profile = xtc_db_fetch_array($profile_query_p3)){
					$profile_categories[] = $profile['cat_id'];
				}
			}
			$query_p4 = 'SELECT * FROM pdfkatalog_profile';
			$profile_query_p4 = xtc_db_query($query_p4);
			while($profile = xtc_db_fetch_array($profile_query_p4)){
				?>
				    <i><a href="<?php echo $PHP_SELF?>?profile_id=<?php echo $profile['pdfkatalog_profile_id'] ?>"><?php echo $profile['pdfkatalog_profile_name'] ?></a></i>&nbsp;|&nbsp;
				<?php
			}	
     	?>
     	</td>
	     	<td>
	     		<a class="button" href="<?php echo $PHP_SELF?>?del_profile=true">Profile l&ouml;schen</a></i>
	     	</td>
     	</tr>
      </table>
      <!-- saved profiles -->
       <div style="height:2px"></div>
      	<!-- Produktauswahl -->
      <table class="dataTableHeadingContent" border="0" width="100%" cellspacing="0" cellpadding="2">
      	<?php 
       	$query ="SELECT 		p.products_id,
								p.products_image,
								p.products_price,
								pd.products_name,
								cd.categories_name,
								cd.categories_id,
								p.products_tax_class_id,
								pd.products_short_description,
								p.products_quantity
								FROM " . TABLE_CATEGORIES . " c
								LEFT OUTER JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON  cd.categories_id = c.categories_id AND cd.language_id = 2
								LEFT OUTER JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " ptc ON ptc.categories_id = c.categories_id
								LEFT OUTER JOIN " . TABLE_PRODUCTS . " p ON p.products_id = ptc.products_id
								LEFT OUTER JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON pd.products_id = p.products_id AND pd.language_id = 2
								WHERE p.products_status = 1 AND  p.products_price > 0 AND pd.products_name > '' 
								ORDER BY cd.categories_id
								";
		$product_query = xtc_db_query($query);
		while($product = xtc_db_fetch_array($product_query)){
			$products[] = $product;
		}
		$categories = xtc_get_category_tree();
		?>
			<tr>
			    <td>1. W�hlen Sie die Artikel aus, die in Ihren PDF-Katalog gedruckt werden sollen:</td>
			</tr>
			<tr>
			    <td width="20%">
			 	   <input type="checkbox" name="categorie_all" onclick="selectall(this.checked);" value="all">alle Artikel
				</td>
			</tr>
		<?php
		for($i_cat=1;$i_cat<sizeof($categories);$i_cat++){
			$count = substr_count($categories[$i_cat]['text'], '&nbsp;'); 
			unset($count_cat);
			unset($count_prod);
			if($count > 0){
				$count_cat = (int)$count *3;
				$count_prod = (int)$count *5;
				$categorie_text = substr (strrchr ($categories[$i_cat]['text'], ";"), 1);
				$spacer = '<img src="includes/modules/pdfkatalog/pixel.png" height="1px" width="'.$count_cat.'px">';
				$spacer2 = '<img src="includes/modules/pdfkatalog/pixel.png" height="1px" width="'.$count_prod.'px">';
				$arrow = '<img src="includes/modules/pdfkatalog/pfeil.png"/>';
			}
			else{
				$categorie_text = $categories[$i_cat]['text'];
				$spacer = '';
				$arrow = '';
			}
			?>
				<tr>
				    <td>
				    	<?php echo $spacer?>
				 	   <input type="checkbox" onclick="select_cat(this.value, this.checked)" name="categories[]" value="<?php echo $categories[$i_cat]['id']?>"
				 	   <?php
					 	   	if($profile_categories){
					 	   		if(in_array($categories[$i_cat]['id'],$profile_categories)){
					 	   			?>checked="checked"
					  	 				<?php
					 	   		}
				  	 		}
				  	 		?>
				 	   ><?php echo $categorie_text?>
					</td>
				</tr>
			<?php
			for($i_products=0;$i_products<sizeof($products);$i_products++){
				if($products[$i_products]['categories_id'] == $categories[$i_cat]['id']){
					?>
					<tr>
					    <td>
					    <?php echo $spacer2?>
					 	   <input type="checkbox" id="<?php echo $categories[$i_cat]['id']?>" name="products[]" value="<?php echo $products[$i_products]['products_id']?>"
					 	   <?php
					 	   	if($profile_products){
					 	   		if(in_array($products[$i_products]['products_id'],$profile_products)){
					 	   			?>checked="checked"
					  	 				<?php
					 	   		}
				  	 		}
				  	 		?>
					 	   ><?php echo $products[$i_products]['products_name']?>
						</td>
					</tr>
					<?php
				}
			}
		}
	  	 ?>
      </table>
      <!-- Produktauswahl -->
      <div style="height:2px"></div>
      <!-- Options -->	
      <table class="dataTableHeadingContent" border="0" width="100%" cellspacing="0" cellpadding="2">
      	<tr>
		    <td colspan="3">2. W�hlen Sie die gew&uuml;nschten Optionen:
			</td>
		</tr>
		<tr align="left">
		    	<th width="15%"><input type="radio" name="deckblatt" value="1"
	  	 		<?php
	  	 		if($profile_deckblatt){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
	  	 		>Mit Deckblatt</th>
		    	<th width="20%"><input type="radio" name="beiblatt" value="1"
		    	<?php
	  	 		if($profile_beiblatt){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
		    	>Mit Beiblatt (Fax, AGB..)</th>
	  	 		<th width="15%"><input type="radio" name="prices" value="1"
		    	<?php
	  	 		if($profile_prices){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
		    	>Mit Preise</th>
		</tr>
		<tr align="left">
		    	<th width="15%"><input type="radio" name="deckblatt" value="0"
		    	<?php
	  	 		if(!$profile_deckblatt){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
	  	 		>Ohne Deckblatt</th>
		    	<th width="20%"><input type="radio" name="beiblatt" value="0"
		    	<?php
	  	 		if(!$profile_beiblatt){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
		    	>Ohne Beiblatt (nur Artikelliste)</th>
	  	 		<th width="55%"><input type="radio" name="prices" value="0"
		    	<?php
	  	 		if(!$profile_prices){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
		    	>Ohne Preise</th>
		</tr>
		<tr align="left">    	
		    	<th width="15%"><input type="checkbox" name="specials" value="1"
		    	<?php
	  	 		if($profile_specials){
	  	 		?>checked
	  	 		<?php
	  	 		}
	  	 		?>
		    	>Nur Sonderpreise</th>
		</tr>
      </table>
      <!-- Options -->
       <div style="height:2px"></div>
      <!-- Profil -->     
      <?php $date = getdate();?>
      <table class="dataTableHeadingContent" border="0" width="100%" cellspacing="0" cellpadding="2">
      	<!-- Profil speichern -->
      	<tr>
		    <td>
		 	  3. Benennen Sie Ihre Auswahl
			</td>
		</tr>
		<tr align="left">
			<th width="20%"><input type="text" size="20" name="profilename" value="<?php echo $date['month'].$date['year'] ?>">&nbsp;
			<input type="checkbox" name="saveprofile">Profil speichern</th>
			<!--<th><input type="button" class="button" onclick="pdfkatalog.submit();" value="Auswahl speichern"></th>-->
		</tr>
      	  <tr align="left">
			<th><input type="button" class="button" onclick="pdfkatalog.submit();" value="Katalog generieren"></th>
		</tr>
      </table>
      <!-- Profil --> 
      </form>
    </td>
  </tr>
</table></td>
<!-- body_text_eof //-->
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
