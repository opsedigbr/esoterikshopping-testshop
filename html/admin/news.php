<?php
/*
admin/news.php	
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/
require ('includes/application_top.php');
require_once ('includes/classes/' . FILENAME_IMAGEMANIPULATOR);
require (DIR_FS_INC . 'xtc_image_button_news.inc.php');
require (DIR_FS_INC . 'xtc_parse_input_field_data.inc.php');
require (DIR_FS_INC . 'xtc_image_submit.inc.php');
require_once (DIR_FS_INC . 'xtc_wysiwyg.inc.php');
$languages = xtc_get_languages();
switch ($_POST['action']) { 
  case 'offline':
    $news_db_array = array('status' => '1');
    xtc_db_perform(TABLE_NEWS, $news_db_array, 'update', "news_id = '" . $_POST['news_id'] . "'");
    xtc_redirect(xtc_href_link(FILENAME_MODULE_NEWS));
  break;
  case 'online':
    $news_db_array = array('status' => '0');
    xtc_db_perform(TABLE_NEWS, $news_db_array, 'update', "news_id = '" . $_POST['news_id'] . "'");
    xtc_redirect(xtc_href_link(FILENAME_MODULE_NEWS));
  break;
  case 'delete':
    $lang = sizeof($languages);
    for ($i = 0;$i < $lang;$i++) {
      $languages_id = $languages[$i]['id'];
      $image_query = xtc_db_query("-- admin/news.php
				SELECT image 
				FROM " . TABLE_NEWS_DESCRIPTION . " 
				WHERE news_id = '" . (int)$_POST['news_id'] . "'
				AND language_id = '" . (int)$languages_id . "'");
      $delete_image = xtc_db_fetch_array($image_query);
      @unlink(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . $delete_image['image']);
      @unlink(DIR_FS_CATALOG_INFO_NEWS_IMAGES . $delete_image['image']);
    }
    xtc_db_query("DELETE FROM " . TABLE_NEWS . " WHERE news_id = '" . (int)$_POST['news_id'] . "'");
    xtc_db_query("DELETE FROM " . TABLE_NEWS_DESCRIPTION . " WHERE news_id = '" . (int)$_POST['news_id'] . "'");
    xtc_redirect(xtc_href_link(FILENAME_MODULE_NEWS));
  break;
  case 'insert':
    $lang = sizeof($languages);
    $status = xtc_db_prepare_input($_POST['status']);
    $show_date = xtc_db_prepare_input($_POST['show_date']);
    if ($_POST['news_id'] != '') {
      xtc_db_query("
				UPDATE " . TABLE_NEWS . " 
				SET 
					status = '" . $status . "',
					show_date = '" . $show_date . "' 
				WHERE news_id = '" . (int)$_POST['news_id'] . "'
			");
      for ($i = 0;$i < $lang;$i++) {
        $languages_id = $languages[$i]['id'];
        if ($_POST['delete_image' . $languages_id] == '') {
          $image_name = $_FILES['image' . $languages_id]['name'];
          if ($image_name != '') {
            if ($image = xtc_try_upload('image' . $languages_id, DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES, '777', '')) {
              $image_suffix = explode('.', $image_name);
              $suffix = array_pop($image_suffix);
              $news_image_name = $_POST['news_id'] . '_' . $languages_id . '.' . $suffix;
              rename(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $image_name, DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $news_image_name);
              @copy(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $news_image_name, DIR_FS_CATALOG_INFO_NEWS_IMAGES . '/' . $news_image_name);
            }
            require (DIR_WS_INCLUDES . 'news_info_images.php');
            $news_image_query = ", image = '" . $news_image_name . "'";
          } 
        } elseif ($_POST['delete_image' . $languages_id] != '') {
          @unlink(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . $_POST['delete_image' . $languages_id]);
          @unlink(DIR_FS_CATALOG_INFO_NEWS_IMAGES . $_POST['delete_image' . $languages_id]);
          $news_image_query = ", image = ''";
        } elseif ($_FILES['image' . $languages_id]['name'] == '') {
          $news_image_query = "";
        }
        xtc_db_query("
					UPDATE " . TABLE_NEWS_DESCRIPTION . " 
					SET 
						title = '" . xtc_db_prepare_input($_POST['title'][$languages_id]) . "',
						subtitle = '" . xtc_db_prepare_input($_POST['subtitle'][$languages_id]) . "',
						shorttext = '" . xtc_db_prepare_input($_POST['shorttext'][$languages_id]) . "',
						text = '" . xtc_db_prepare_input($_POST['text'][$languages_id]) . "', 
            products_link = '" . xtc_db_prepare_input($_POST['products_link'][$languages_id]) . "' 
						" . $news_image_query . "
					WHERE news_id = '" . (int)$_POST['news_id'] . "'
					AND language_id = '" . (int)$languages_id . "'");
      }
    } else {
      $next_query = xtc_db_query("-- admin/news.php
				SELECT MAX(news_id) AS ID 
				FROM " . TABLE_NEWS);
      $next_id = xtc_db_fetch_array($next_query);
      $next_id = $next_id['ID'];
      if ($next_id != 0 ? $next_id+= 1 : $next_id = 1);
      $date_now = date("Y-m-d H:i:s");
      $news_array = array(
				'news_id' => $next_id, 
				'date' => $date_now, 
				'status' => $status, 
				'show_date' => $show_date
			);
      for ($i = 0;$i < $lang;$i++) {
        $languages_id = $languages[$i]['id'];
        $image_name = $_FILES['image' . $languages_id]['name'];
        if ($image_name != '') {
          if ($image = xtc_try_upload('image' . $languages_id, DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES, '777', '')) {
            $image_suffix = explode('.', $image_name);
            $suffix = array_pop($image_suffix);
            $news_image_name = $next_id . '_' . $languages_id . '.' . $suffix;
            rename(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $image_name, DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $news_image_name);
            @copy(DIR_FS_CATALOG_ORIGINAL_NEWS_IMAGES . '/' . $news_image_name, DIR_FS_CATALOG_INFO_NEWS_IMAGES . '/' . $news_image_name);
          }
          require (DIR_WS_INCLUDES . 'news_info_images.php');
        }
        $news_desc_array = array(
					'news_id' => $next_id, 
					'language_id' => xtc_db_prepare_input($languages_id), 
					'title' => xtc_db_prepare_input($_POST['title'][$languages_id]), 
					'subtitle' => xtc_db_prepare_input($_POST['subtitle'][$languages_id]), 
					'shorttext' => xtc_db_prepare_input($_POST['shorttext'][$languages_id]), 
					'text' => xtc_db_prepare_input($_POST['text'][$languages_id]), 
          'products_link' => xtc_db_prepare_input($_POST['products_link'][$languages_id]), 
					'image' => $news_image_name
				);
        xtc_db_perform(TABLE_NEWS_DESCRIPTION, $news_desc_array);
      } 
      xtc_db_perform(TABLE_NEWS, $news_array);
    } 
    xtc_redirect(xtc_href_link(FILENAME_MODULE_NEWS));
  default:
    define('ITEMS_PER_PAGE', '15');
    $news = xtc_db_query("-- admin/news.php
			SELECT COUNT(*) AS anzahl 
			FROM news");
    $anzahl = mysql_result($news, 0);
    $start = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $num_pages = ceil($anzahl/ITEMS_PER_PAGE);
    if (!$num_pages) {
      $num_pages = 1;
    }
    if ($start < 1) {
      $start = 1;
    }
    if ($start > $num_pages) {
      $start = $num_pages;
    }
    $offset = ($start-1) *ITEMS_PER_PAGE;
    $all_news_query = xtc_db_query("-- admin/news.php
			SELECT 
				n.news_id,
				n.status,
        DATE_FORMAT(n.date, '%d.%m.%Y, %H:%i:%s') AS DATE,
				left(nd.title, 60) AS SHORT_TEXT
			FROM " . TABLE_NEWS . " n
			INNER JOIN " . TABLE_NEWS_DESCRIPTION . " nd ON n.news_id = nd.news_id		
      WHERE nd.language_id = '" . (int)$_SESSION['languages_id'] . "'
      ORDER BY n.news_id DESC
      LIMIT " . $offset . "," . ITEMS_PER_PAGE . ";");
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>" />
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<?php
  if (USE_WYSIWYG == 'true') {
    $query = xtc_db_query("-- admin/news.php
			SELECT code 
			FROM " . TABLE_LANGUAGES . " 
			WHERE languages_id='" . (int)$_SESSION['languages_id'] . "'");
    $data = xtc_db_fetch_array($query);
?>
<script type="text/javascript" src="includes/modules/fckeditor/fckeditor.js"></script>
<script type="text/javascript">
	window.onload = function(){
<?php
    if ($_POST['action'] == 'new' || $_POST['action'] == 'edit') {
      for ($i = 0;$i < sizeof($languages);$i++) {
        echo xtc_wysiwyg('news_content', $data['code'], $languages[$i]['id']);
      }
    }
?>
}
</script>
<?php
  }
?>
</head>
<body>
<?php require (DIR_WS_INCLUDES . 'header.php'); ?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top">
      <table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft"><?php require (DIR_WS_INCLUDES . 'column_left.php'); ?></table>
    </td>
    <td class="boxCenter" width="100%" valign="top">       
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="80" rowspan="2"><?php echo xtc_image(DIR_WS_ICONS . 'heading_content.gif'); ?></td>
          <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
        </tr>
        <tr> 
          <td class="main" valign="top">XTC Tools</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="1" cellpadding="2">
        <tr>
          <td class="pageHeading_news"><br /><?php echo HEADING_CONTENT; ?></td>
        </tr>
        <tr>
          <td class="main"><?php echo CONTENT_NOTE; ?></td>
        </tr>  
      </table>
<?php
if ($_POST['action'] == 'edit' || $_POST['action'] == 'new') {
?>
<form name="news" method="post" enctype="multipart/form-data" action="<?php echo FILENAME_MODULE_NEWS; ?>">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
<?php
    $count_languages = sizeof($languages);
    for ($i = 0;$i < $count_languages;$i++) {
      $edit_news_query = xtc_db_query("-- admin/news.php
				SELECT 
					n.news_id,
					n.status,
					n.show_date,
					nd.title, 
					nd.subtitle,
					nd.shorttext,
					nd.text,
					nd.image,
          nd.products_link 
      	FROM " . TABLE_NEWS . " n
				INNER JOIN " . TABLE_NEWS_DESCRIPTION . " nd ON n.news_id = nd.news_id
      	WHERE n.news_id = '" . $_POST['news_id'] . "'
				AND nd.language_id = '" . (int)$languages[$i]['id'] . "'");
      $edit_news = xtc_db_fetch_array($edit_news_query);
?>
  <tr>
    <td width="20%" class="news_header" colspan="2">
<?php
      echo xtc_draw_hidden_field('action', 'insert');
      echo xtc_image(DIR_WS_ADMIN . 'images/icons/' . $languages[$i]['code'] . '_icon.gif', 'Language') . '&nbsp;' . $languages[$i]['name'] . xtc_draw_hidden_field('news_id', $edit_news['news_id']);
?></td>
  </tr>
  <tr>
    <td width="20%" class="news_heading">&nbsp;<?php echo TABLE_HEADING_TITLE; ?></td>
    <td class="news_content"><?php echo xtc_draw_input_field('title[' . $languages[$i]['id'] . ']', $edit_news['title'], 'size="75"'); ?></td>
  </tr>
  <tr>
    <td class="news_heading">&nbsp;<?php echo TABLE_HEADING_SUBTITLE; ?></td>
    <td class="news_content"><?php echo xtc_draw_input_field('subtitle[' . $languages[$i]['id'] . ']', $edit_news['subtitle'], 'size="75"'); ?></td>
  </tr>
  <tr>
    <td class="news_heading">&nbsp;Link zum Produkt</td>
    <td class="news_content"><?php echo xtc_draw_input_field('products_link[' . $languages[$i]['id'] . ']', $edit_news['products_link'], 'size="100"'); ?></td>
  </tr>  
<?php
      if ($edit_news['image'] != '') {
?>
  <tr>
    <td class="news_heading" width="25%">&nbsp;</td>
    <td class="news_content" width="75%">
<?php echo xtc_image(DIR_WS_CATALOG_INFO_NEWS_IMAGES . $edit_news['image'], $edit_news['image']); ?>
<div><?php echo xtc_draw_selection_field('delete_image' . $languages[$i]['id'], 'checkbox', $edit_news['image']) . ' ' . SELECTION_DEL; ?></div>
</td>
  </tr>
<?php
      }
?>  
  <tr>
    <td class="news_heading">&nbsp;<?php echo TABLE_HEADING_IMAGE; ?></td>
    <td class="news_content"><?php echo xtc_draw_file_field('image' . $languages[$i]['id']); ?></td>
  </tr>
  <tr>
    <td class="news_content" colspan="2" height="30">&nbsp;
    <strong><?php echo TABLE_HEADING_TEXT_SHORT . '</strong> ' . TABLE_TEXT_SHORT_MAX; ?></td>
  </tr>
  <tr>
    <td class="news_content" colspan="2"><?php echo xtc_draw_textarea_field('shorttext[' . $languages[$i]['id'] . ']', '', '100', '5', $edit_news['shorttext'], 'style="width:99%;"'); ?></td>
  </tr>  
  <tr>
    <td class="news_content" colspan="2" height="30">&nbsp;<strong><?php echo TABLE_HEADING_TEXT; ?></strong></td>
  </tr>  
  <tr>
    <td class="news_content" colspan="2"><?php echo xtc_draw_textarea_field('text[' . $languages[$i]['id'] . ']', '', '100', '25', $edit_news['text'], 'style="width:99%;"'); ?></td>
  </tr>
  <tr>
    <td class="news_content" colspan="2" height="30">&nbsp;</td>
  </tr>  	
<?php
    }
?>
  <tr>
    <td class="news_content" colspan="2" height="30">&nbsp;<strong><?php echo TABLE_HEADING_OPTIONS; ?></strong></td>
  </tr>
  <tr>
    <td class="news_content" colspan="2" height="30">&nbsp;</td>
  </tr>	
  <tr>
    <td class="news_heading">&nbsp;<?php echo TABLE_HEADING_OPTIONS_OFFLINE; ?></td>
    <td class="news_content">
<?php
    if ($edit_news['status'] == '1') {
      echo xtc_draw_checkbox_field('status', '1', true);
    } else {
      echo xtc_draw_checkbox_field('status', '1', false);
    }
?>      
    </td>
  </tr>
  <tr>
    <td class="news_heading">&nbsp;<?php echo TABLE_HEADING_OPTIONS_DATE; ?></td>
    <td class="news_content">
<?php
    if ($edit_news['show_date'] == '1') {
      echo xtc_draw_checkbox_field('show_date', '1', true);
    } else {
      echo xtc_draw_checkbox_field('show_date', '1', false);
    }
?>     
    </td>
  </tr>
  <tr>
    <td class="news_header" colspan="2" height="40"></td>
	</tr>
	<tr>
		<td><?php echo '<input type="submit" class="button" onclick="this.blur();" value="' . BUTTON_SAVE . '"/>'; ?>
<a class="button" onclick="this.blur();" href="<?php echo xtc_href_link(FILENAME_MODULE_NEWS); ?>"><?php echo BUTTON_BACK; ?></a></td>
	</tr>
</table>
		</td>
  </tr>
</table>
</form>


<?php
  } else {
?>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center" class="magic">
  <tr>
    <td class="main">
<form name="newsnew" method="post" action="<?php echo FILENAME_MODULE_NEWS; ?>">
<?php echo xtc_draw_hidden_field('action', 'new'); ?>   
<?php echo xtc_image_submit_news('button_news_new.gif', NEWS_IMAGE_SUBMIT_NEW); ?>
</form>

<form name="newsnew" method="post" action="configuration.php?gID=3">
<?php echo xtc_image_submit_news('button_news_config.gif', NEWS_IMAGE_SUBMIT_CONFIG); ?>
</form>
    </td>
<?php
    if ($num_pages > 1) {
?>    
    <td align="right" class="main"><small>Seiten</small> 
<?php
      for ($i = 1;$i <= $num_pages;$i++) {
        if ($i == $start) {
          echo '<span class="items">&nbsp;' . $i . '&nbsp;</span>&nbsp;';
        } else {
          echo '<span class="items">&nbsp;<a class="item_link" href="' . FILENAME_MODULE_NEWS . '?section=news&amp;page=' . $i . '">' . $i . '</a>&nbsp;</span>&nbsp;';
        }
      }
?>
    </td>
<?php
    }
?>  
  </tr> 
</table>
<table width="98%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="#cccccc">
  <tr>
    <td class="news_header" width="69%">&nbsp; <?php echo TABLE_HEADING_TITLE; ?></td>
    <td class="news_header" width="5%">&nbsp;<?php echo TABLE_HEADING_STATUS; ?>&nbsp;</td>
    <td class="news_header" width="25%">&nbsp; <?php echo TABLE_HEADING_EDIT; ?></td>
  </tr>
<?php
    while ($news_data = xtc_db_fetch_array($all_news_query)) {
?>
  <tr>
    <td width="69%" class="news_content2">&nbsp;<span class="news_link"><?php echo $news_data['SHORT_TEXT']; ?></span><br />
    &nbsp;- <?php echo TABLE_HEADING_ID . $news_data['news_id']; ?>&nbsp;|&nbsp;<?php echo $news_data['DATE']; ?></td>
    <td class="news_content2" width="5%" align="center">
<?php
      if ($news_data['status'] == '1') {
        echo '<strong>Offline</strong>';
      } else {
        echo 'Online';
      }
?>
    </td>
    <td class="news_content2" width="25%">
<form name="news" method="post" action="<?php echo FILENAME_MODULE_NEWS; ?>">
<table border="0" cellspacing="5" cellpadding="1" align="center">
  <tr>
    <td>
<?php
      echo xtc_draw_hidden_field('news_id', $news_data['news_id']);
      echo xtc_draw_hidden_field('update', '1');
?>  
<select name="action" class="news_selection">
  <option value="edit"><?php echo SELECTION_EDIT; ?></option>
<?php
      if ($news_data['status'] == '1') {
        echo '<option value="online">' . SELECTION_ONLINE . '</option>';
      } else {
        echo '<option value="offline">' . SELECTION_OFFLINE . '</option>';
      }
?>
  <option value="delete"><?php echo SELECTION_DEL; ?></option>
</select>

    </td>
    <td><input type="submit" name="edit" value="Go" /></td>
  </tr>
</table>
</form>
    </td>
  </tr> 
<?php
    }
?>	
</table>
<?php
  }
?>
        </td>
      </tr>
</table>

<?php
  if (USE_SPAW == 'true') {
?>
<script type="text/javascript">
  HTMLArea.loadPlugin("SpellChecker");
  HTMLArea.loadPlugin("TableOperations");
  HTMLArea.loadPlugin("CharacterMap");
  HTMLArea.loadPlugin("ContextMenu");
  HTMLArea.loadPlugin("ImageManager");
      
HTMLArea.onload = function() {
  var editor_short = new HTMLArea("shorttext");
  editor_short.generate();

  var editor = new HTMLArea("text");
  editor.registerPlugin(TableOperations);
  editor.registerPlugin(ContextMenu);
  editor.registerPlugin(CharacterMap);
  editor.registerPlugin(ImageManager);
  editor.generate();
};
HTMLArea.init();
</script>
<?php
  }
?>
</body>
</html>