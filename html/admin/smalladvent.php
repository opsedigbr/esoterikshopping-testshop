<?php
/* --------------------------------------------------------------
  

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(promo.php,v 1.26 2003/05/17); www.oscommerce.com 
   (c) 2003	 nextcommerce (promo.php,v 1.9 2003/08/18); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

  require('includes/application_top.php');

  if ($_GET['action']) {
    switch ($_GET['action']) {
      case 'insert':
        $smalladvent_id = xtc_db_prepare_input($_POST['smalladvent_id']);
        $smalladvent_day = xtc_db_prepare_input($_POST['smalladvent_day']);
        $smalladvent_product = xtc_db_prepare_input($_POST['smalladvent_product']);

        xtc_db_query("insert into " . TABLE_SMALLADVENT . " (smalladvent_id,smalladvent_day,smalladvent_product) values ('" . xtc_db_input($smalladvent_id) . "', '" . xtc_db_input($smalladvent_day) . "', '" . xtc_db_input($smalladvent_product) . "')");
        xtc_redirect(xtc_href_link(FILENAME_SMALLADVENT));
        break;
      case 'save':
        $smalladvent_id = xtc_db_prepare_input($_GET['cID']);
        $smalladvent_day = xtc_db_prepare_input($_POST['smalladvent_day']);
        $smalladvent_product = xtc_db_prepare_input($_POST['smalladvent_product']);

        xtc_db_query("update " . TABLE_SMALLADVENT . " set smalladvent_day = '" . xtc_db_input($smalladvent_day) . "', smalladvent_product = '" . xtc_db_input($smalladvent_product) . "' where smalladvent_id = '" . xtc_db_input($smalladvent_id) . "'");
        xtc_redirect(xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $smalladvent_id));
        break;
      case 'deleteconfirm':
        $smalladventid = xtc_db_prepare_input($_GET['cID']);
        xtc_db_query("delete from " . TABLE_SMALLADVENT . " where smalladvent_id = '" . xtc_db_input($smalladvent_id) . "'");
        xtc_redirect(xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page']));
        break;
      case 'setlflag':
        $smalladvent_id = xtc_db_prepare_input($_GET['cID']);
        $status = xtc_db_prepare_input($_GET['flag']);      
        xtc_db_query("update " . TABLE_SMALLADVENT . " set status = '" . xtc_db_input($status) . "' where smalladvent_id = '" . xtc_db_input($smalladvent_id) . "'");
        xtc_redirect(xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $smalladvent_id));      
      break;  
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script type="text/javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="80" rowspan="2"><?php echo xtc_image(DIR_WS_ICONS.'heading_configuration.gif'); ?></td>
    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
  </tr>
  <tr>
    <td class="main" valign="top"><a href="http://www.XT-Shopservice.de" target="_blank">www.XT-Shopservice.de</a></td>
  </tr>
</table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
			  <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DAY; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCT; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $smalladvent_query_raw = "select smalladvent_id, smalladvent_day, smalladvent_product, status from " . TABLE_SMALLADVENT . " order by smalladvent_id";
  $smalladvent_split = new splitPageResults($_GET['page'], '24', $smalladvent_query_raw, $smalladvent_query_numrows);
  $smalladvent_query = xtc_db_query($smalladvent_query_raw);
  while ($promo = xtc_db_fetch_array($smalladvent_query)) {
    if (((!$_GET['cID']) || (@$_GET['cID'] == $promo['smalladvent_id'])) && (!$cInfo) && (substr($_GET['action'], 0, 3) != 'new')) {
      $cInfo = new objectInfo($promo);
    }

    if ( (is_object($cInfo)) && ($promo['smalladvent_id'] == $cInfo->smalladvent_id) ) {
      echo '                  <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $cInfo->smalladvent_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $promo['smalladvent_id']) . '\'">' . "\n";
    }
?>
				<td class="dataTableContent"><?php echo $promo['smalladvent_day']; ?></td>
                <td class="dataTableContent"><?php echo $promo['smalladvent_product']; ?></td>
                <td class="dataTableContent" align="right"><?php if ( (is_object($cInfo)) && ($promo['smalladvent_id'] == $cInfo->smalladvent_id) ) { echo xtc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $promo['smalladvent_id']) . '">' . xtc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
             
            </table></td>
<?php
  $heading = array();
  $contents = array();
  switch ($_GET['action']) {
    case 'edit':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_PROMO . '</b>');

      $contents = array('form' => xtc_draw_form('promo', FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $cInfo->smalladvent_id . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
	  $contents[] = array('text' => '<br />' . TEXT_INFO_DAY . '<br />'. xtc_draw_input_field('smalladvent_day', $cInfo->smalladvent_day, 'readonly="readonly"'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTID . '<br />' . xtc_draw_input_field('smalladvent_product', $cInfo->smalladvent_product));
      $contents[] = array('align' => 'center', 'text' => '<br /><input type="submit" class="button" onClick="this.blur();" value="' . BUTTON_UPDATE . '"/>&nbsp;<a class="button" onClick="this.blur();" href="' . xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $cInfo->smalladvent_id) . '">' . BUTTON_CANCEL . '</a>');
      break;
   
    default:
      if (is_object($cInfo)) {
        $heading[] = array('text' => '<b>' . $cInfo->smalladvent_day . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<a class="button" onClick="this.blur();" href="' . xtc_href_link(FILENAME_SMALLADVENT, 'page=' . $_GET['page'] . '&cID=' . $cInfo->smalladvent_id . '&action=edit') . '">' . BUTTON_EDIT . '</a> ');
        $contents[] = array('text' => '<br />' . TEXT_INFO_DAY . ' ' . $cInfo->smalladvent_day);
        $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTID . ' ' . $cInfo->smalladvent_product);
      }
      break;
  }

  if ( (xtc_not_null($heading)) && (xtc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
	  <tr><td><?php echo INFO_TEXT; ?></td></tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>