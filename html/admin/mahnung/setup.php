<?
/*

*/

// In dieser Datei werden alle Einstellungen getroffen:

$firma = 'Esoterikshopping.de'; // Eigener Firmenname

$sprach_id_de = '2'; // Welche ID gilt f�r deutsche Sprache (Test.php ausf�hren)
$sprach_id_en = '1'; // Welche ID gilt f�r englische Sprache (Test.php ausf�hren)
$bestellstatus_nopayment = '1'; // Welche Bestellstatus ID gilt f�r Nicht bezahlt (Standard: 1) (Test.php ausf�hren)
$bestellstatus_mahnung1 = '8'; // ID f�r Bestellstatus Mahnung (Test.php ausf�hren)
$bestellstatus_mahnung2 = '9'; // ID f�r Bestellstatus �bergabe an Rechtsabteilung (Test.php ausf�hren)
$bestellstatus_storno = '4'; // ID f�r Bestellstatus �bergabe an Rechtsabteilung (Test.php ausf�hren)

$zeitraum_mahnung1 = '7'; // Nach X Tagen Mahnung 1
$zeitraum_mahnung2 = '14'; // Nach X Tagen �bergabe an Rechtsabteilung
$zeitraum_storno = '30'; // Nach X Tagen �bergabe an Rechtsabteilung
$verjaehrungsfrist = '90'; // Alte Bestellungen werden nicht behandelt (ab X Tagen)

$email_kunde = '1'; // Mahnung per eMail an Kunden (zul�ssige Werte: 0 und 1)
$email_admin = '0'; // Mahnung per eMail an Admin (zul�ssige Werte: 0 und 1)
$email_rechtsabteilung = '0'; // Mahnung per eMail an Rechtsabteilung (zul�ssige Werte: 0 und 1)
$email_rechtsabteilung_kund = '0'; // Kunde bekommt Hinweis wenn sein Fall an Rechtsabteilung weitergeleitet wird
$email_addy_admin = 'info@esoterikshopping.de'; // Email von Admin
$email_addy_rechtsabteilung = 'info@esoterikshopping.de'; // Email der Rechtsabteilung

$email_betreff_mahnung1_de = 'Zahlungserinnerung'; // eMail Betreff der Mahnung 1 in deutsch
$email_betreff_mahnung1_en = 'Payment Reminder:'; // eMail Betreff der Mahnung 1 in englisch
$email_betreff_mahnung2_de = 'Letzte Zahlungserinnerung'; // eMail Betreff der Mahnung 1 in deutsch
$email_betreff_mahnung2_en = 'Payment Reminder 2:'; // eMail Betreff der Mahnung 1 in englisch
$email_betreff_mahnung2 = 'Nicht erfolgte Zahlung'; // eMail Betreff an Rechtsabteilung

$mails = '1'; // Hier k�nnen die eMails testweise deaktiviert werden
$db_uptdate = '1'; // Hier kann der Datenbank update testweise deaktiviert werden
?>