<?php

/* --------------------------------------------------------------
   $Id: start.php 1235 2005-09-21 19:11:43Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project 
   (c) 2002-2003 osCommerce coding standards (a typical file) www.oscommerce.com
   (c) 2003      nextcommerce (start.php,1.5 2004/03/17); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

require ('includes/application_top.php');
//require_once 'includes/modules/carp/carp.php';

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
.h2 {
  font-family: Trebuchet MS,Palatino,Times New Roman,serif;
  font-size: 13pt;
  font-weight: bold;
}

.h3 {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 9pt;
  font-weight: bold;
}
</style> 

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="80" rowspan="2"><?php echo xtc_image(DIR_WS_ICONS.'heading_news.gif'); ?></td>
    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
  </tr>
  <tr>
    <td class="main" valign="top">xt:Commerce News
</td>
  </tr>
</table></td>
      </tr>
      <tr>
        <td>
        <?php include(DIR_WS_MODULES.FILENAME_SECURITY_CHECK); ?>

<!-- Startstatistik Anfang kunigunde -->
<?php
$customers_query = xtc_db_query("select count(*) as count from " .TABLE_CUSTOMERS);
$customers = xtc_db_fetch_array($customers_query);
$newsletter_query = xtc_db_query("select count(*) as count from " .TABLE_CUSTOMERS. " where customers_newsletter = '1'");
$newsletter = xtc_db_fetch_array($newsletter_query);
$products_query = xtc_db_query("select count(*) as count from " .TABLE_PRODUCTS . " where products_status = '1'");
$products = xtc_db_fetch_array($products_query);
$products1_query = xtc_db_query("select count(*) as count from " .TABLE_PRODUCTS . " where products_status = '0'");
$products1 = xtc_db_fetch_array($products1_query);
$products2_query = xtc_db_query("select count(*) as count from " .TABLE_PRODUCTS . "");
$products2 = xtc_db_fetch_array($products2_query);
$orders0_query = xtc_db_query("select count(*) as count from " .TABLE_ORDERS . " where orders_status = '0'");
$orders0 = xtc_db_fetch_array($orders0_query);
$orders1_query = xtc_db_query("select count(*) as count from " .TABLE_ORDERS . " where orders_status = '1'");
$orders1 = xtc_db_fetch_array($orders1_query);
$orders2_query = xtc_db_query("select count(*) as count from " .TABLE_ORDERS . " where orders_status = '2'");
$orders2 = xtc_db_fetch_array($orders2_query);
$orders3_query = xtc_db_query("select count(*) as count from " .TABLE_ORDERS . " where orders_status = '3'");
$orders3 = xtc_db_fetch_array($orders3_query);
$specials_query = xtc_db_query("select count(*) as count from " .TABLE_SPECIALS);
$specials = xtc_db_fetch_array($specials_query);
?>
<div align="left">
<h2>Statistik &Uuml;bersicht</h2>
<table width="30%" border="1">
<tr>
<td colspan="2" align="left"><strong>Kunden</strong></td>
</tr>
<tr>
<td>Kunden:</td>
<td><?php echo $customers['count']; ?></td>
</tr>
<tr>
<td>Newsletter Abo:</td>
<td><?php echo $newsletter['count']; ?></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td align="left" colspan="2"><strong>Artikel</strong></td>
</tr>
<tr>
<td>Aktive Artikel:</td>
<td><?php echo $products['count']; ?></td>
</tr>
<tr>
<td>Inaktive Artikel:</td>
<td><?php echo $products1['count']; ?></td>
</tr>
<tr>
<td>Artikel gesamt:</td>
<td><?php echo $products2['count']; ?></td>
</tr>
<tr>
<td>Sonderangebote:</td>
<td><?php echo $specials['count']; ?></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td align="left" colspan="2"><strong>Bestellungen</strong></td>
</tr>
<tr>
<td>Offen:</td>
<td><?php echo $orders1['count']; ?></td>
</tr>
<tr>
<td>In Bearbeitung:</td>
<td><?php echo $orders2['count']; ?></td>
</tr>
<tr>
<td>Versendet:</td>
<td><?php echo $orders3['count']; ?></td>
</tr>
<tr>
<td>nicht Best&auml;tigt:</td>
<td><?php echo $orders0['count']; ?></td>
</tr>
</table>
<br />
</div>
<!-- Startstatistik Ende kunigunde -->


     <tr>
      <td style="border: 0px solid; border-color: #ffffff;">
</td>
      </tr>		 
  
  
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>