<?php

/* --------------------------------------------------------------
   $Id: start.php 1235 2005-09-21 19:11:43Z mz $

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project 
   (c) 2002-2003 osCommerce coding standards (a typical file) www.oscommerce.com
   (c) 2003      nextcommerce (start.php,1.5 2004/03/17); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

require ('includes/application_top.php');
//require_once 'includes/modules/carp/carp.php';

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['language_charset']; ?>"> 
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
.h2 {
  font-family: Trebuchet MS,Palatino,Times New Roman,serif;
  font-size: 13pt;
  font-weight: bold;
}

.h3 {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 9pt;
  font-weight: bold;
}
</style> 

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td class="columnLeft2" width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td class="boxCenter" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td class="pageHeading">Zur L&ouml;schung</td>
  </tr>
  <tr>
    <td class="main" valign="top">Und los...
</td>
  </tr>
</table></td>
      </tr>
      <tr>
        <td>
        <?php include(DIR_WS_MODULES.FILENAME_SECURITY_CHECK); ?>
              
<br />  
<?php
	/************ zur Löschung ***************/
	$op_verjaehrungsfrist = 30 * '86400'; // Sekunden pro Tag
	$to_delete_query = xtc_db_query("SELECT * FROM orders WHERE orders_status = '9' order by orders_id desc");
	while($to_delete = xtc_db_fetch_array($to_delete_query))
	{
		$op_orders_id = $to_delete['orders_id'];
		$dp = $to_delete['date_purchased'];
		$op_delta = time() - strtotime($dp);
		
		if($op_delta > $op_verjaehrungsfrist)
		{
			//mysql_query("UPDATE orders SET orders_status=$bestellstatus_mahnung1 WHERE orders_id=$orders_id[$i]") or die(mysql_error());
			xtc_db_query("UPDATE orders SET orders_status='999' WHERE orders_id='".$op_orders_id."'");
			DELETE FROM orders_total WHERE orders_id = '.$op_orders_id.'
			echo'Bestellnummer: '.$op_orders_id.' wurde zur L&ouml;schung freigegeben.<br />';
		}
	}
	/************ zur Löschung ***************/?>

     <tr>
      <td style="border: 0px solid; border-color: #ffffff;">
</td>
      </tr>		 
  
  
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>