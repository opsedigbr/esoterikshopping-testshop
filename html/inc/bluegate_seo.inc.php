<?PHP



/*

Extension Direct URL (c) 2007 bluegate communications

---------------------------------------------------------------------------------------

bluegate communications - http://www.bluegate.at

Author: Ing. Michael F�rst

*/



class BluegateSeo {

	

	// *************************** Class Constructor ************************* //

	function BluegateSeo() {

		require_once(DIR_FS_INC . 'xtc_get_product_path.inc.php');

		require_once(DIR_FS_INC . 'xtc_get_category_path.inc.php');

		require_once(DIR_FS_INC . 'xtc_get_parent_categories.inc.php');

		require_once(DIR_FS_INC . 'xtc_db_num_rows.inc.php');

	}

	

	// *************************** Build the product link for xtc_href_link.inc.php ************************* //

	function getProductLink($parameters,$connection='NONSSL',$language) {

		$explodedParams=explode('&',$parameters);

		

		// Extract product id and product name from file parameter

		foreach($explodedParams as $value) {

			if (substr($value,0,5) == 'info=') {

				$xtcProductParameter=substr($value,5,strlen($value));

				$productId=substr($xtcProductParameter,1,(strpos($xtcProductParameter,'_')-1));

			}

		}
		
		
		//echo $connection;
		
		$op_ssl = $_SERVER['SERVER_PORT'];
		if($op_ssl == '443')
			$connection = 'SSL';	

		if ($connection == 'SSL' && ENABLE_SSL) {

        	$link = HTTPS_SERVER.DIR_WS_CATALOG;

        } else {

            $link = HTTP_SERVER.DIR_WS_CATALOG ;

        }

		

		// Get SUMA friendly product Link from Database
		
		$product_link_query   = "SELECT url_text FROM bluegate_seo_url WHERE products_id = '".$productId."' AND language_id='".$language."' ".$queryCatID;

		$product_link_query   = xtc_db_query($product_link_query);

		$product_link		  = xtc_db_fetch_array($product_link_query,false);



		return $link.'products/'.$product_link['url_text'].'.html';

	}

	

	// *************************** Build the category link for xtc_href_link.inc.php ************************* //

	function getCategoryLink($parameters,$connection='NONSSL',$language) {

		$explodedParams=explode('&',$parameters);

		

		// Extract category id and file parameter

		foreach($explodedParams as $value) {

			if (substr($value,0,5) == 'cat=c') {

				$xtcCategoryParameter=substr($value,5,strlen($value));

				$categoryId=substr($xtcCategoryParameter,0,(strpos($xtcCategoryParameter,'_')));

			}

		}

		$op_ssl = $_SERVER['SERVER_PORT'];
		if($op_ssl == '443')
			$connection = 'SSL';		

		if ($connection == 'SSL' && ENABLE_SSL) {

        	$link = HTTPS_SERVER.DIR_WS_CATALOG;

        } else {

            $link = HTTP_SERVER.DIR_WS_CATALOG ;

        }

		

		// Get SUMA friendly category Link from Database

		$category_link_query   = "SELECT url_text FROM bluegate_seo_url WHERE categories_id = '".$categoryId."' AND language_id='".$language."'";

		$category_link_query   = xtc_db_query($category_link_query);

		$category_link		  = xtc_db_fetch_array($category_link_query,false);



		return $link . $category_link['url_text'];

	}

	

	// *************************** Build the content link for xtc_href_link.inc.php ************************* //

	function getContentLink($parameters,$connection='NONSSL',$language) {

		$explodedParams=explode('&',$parameters);



		// Extract category id and file parameter

		foreach($explodedParams as $value) {

			if (substr($value,0,4) == 'coID') {

				$contentGroupId=substr($value,5,strlen($value));

			}

		}

		
		$op_ssl = $_SERVER['SERVER_PORT'];
		if($op_ssl == '443')
			$connection = 'SSL';	

		if ($connection == 'SSL' && ENABLE_SSL) {

        	$link = HTTPS_SERVER.DIR_WS_CATALOG;

        } else {

            $link = HTTP_SERVER.DIR_WS_CATALOG;

        }

		

		// Get SUMA friendly content Link from Database

		$content_link_query   = "SELECT url_text FROM bluegate_seo_url WHERE content_group = '".$contentGroupId."' AND language_id='".$language."'";

		$content_link_query   = xtc_db_query($content_link_query);

		$content_link		  = xtc_db_fetch_array($content_link_query,false);



		return $link . 'content/'.$content_link['url_text'].'.html';

	}

	

	// *************************** Build the language change link for xtc_href_link.inc.php ************************* //

	function getLanguageChangeLink($page,$parameters,$connection='NONSSL') {

		

		// Get language ISO Code AND ID from parameters

		$languageParamStartPos = strpos($parameters,'language=');

		$language=substr($parameters,($languageParamStartPos+9),2);

		$languageId_query   = "SELECT languages_id 

								FROM languages 

								WHERE code='".$language."'";

		$languageId_query   = xtc_db_query($languageId_query);

		$languageId_result	= xtc_db_fetch_array($languageId_query,false);

		

		$op_ssl = $_SERVER['SERVER_PORT'];
		if($op_ssl == '443')
			$connection = 'SSL';	
					

		if ($connection == 'SSL' && ENABLE_SSL) {

        	$link = HTTPS_SERVER.DIR_WS_CATALOG;

        } else {

            $link = HTTP_SERVER.DIR_WS_CATALOG ;

        }

		

		// Get IDs

		$explodedParams=explode('&',$parameters);

			

		switch ($page) {

			// Extract product ID and build SQL Query parts

			case 'product_info.php': 

				foreach($explodedParams as $value) {

					if (substr($value,0,5) == 'info=') {

						$categoryId=' IS NULL';

						$productId='=\''.substr($value,5,strlen($value)).'\'';

						$coID=' IS NULL';

					}

				}		

				

				break;

			// Extract category id and build SQL Query parts

			case 'index.php':

				$catIdFound=false;

				foreach($explodedParams as $value) {

					if (substr($value,0,4) == 'cat=') {

						$categoryId='=\''.substr($value,4,strlen($value)).'\'';

						$productId=' IS NULL';

						$coID=' IS NULL';

						$catIdFound=true;

					}

				}

				

				// Workaround for Language-Link on Start-Page (no Category ID in parameter list)

				if (!$catIdFound) {

					return $link.$language;

				}

				break;

			// Extract content id and build SQL Query parts

			case 'shop_content.php': 

				foreach($explodedParams as $value) {

					if (substr($value,0,5) == 'coID=') {

						$categoryId=' IS NULL';

						$productId=' IS NULL';

						$coID='=\''.substr($value,5,strlen($value)).'\'';

					}

				}

				break;

		}

		

		

		

		// Get SUMA friendly category Link from Database

		$link_query   = "SELECT url_text 

						FROM bluegate_seo_url 

						WHERE 1>0 

						AND products_id".$productId." 

						AND categories_id".$categoryId." 

						AND content_group".$coID." 

						AND language_id=".$languageId_result['languages_id'];

		$link_query   = xtc_db_query($link_query);

		$link_result  = xtc_db_fetch_array($link_query,false);

		

		// Return type-depending URL

		switch ($page) {

			// product

			case 'product_info.php':

				return $link.'products/'.$link_result['url_text'].'.html';

				break;

			// Category

			case 'index.php': 

				return $link . $link_result['url_text'];

				break;

			// Content

			case 'shop_content.php': 

				return $link . 'content/'.$link_result['url_text'].'.html';

				break;

		}

	}

	

	

	// *************************** Create the category link path for a product ************************* //

	function getCategoryPathForProduct($productId,$p2catID,$language) {

		$xtcProductPath = xtc_get_product_path($productId, $p2catID);

		$pathExploded = explode('_',$xtcProductPath);

		foreach($pathExploded as $value) {

			$productPath .= $this->getCategoryNameForId($value, $language);

			$pathExploded[0] <> '' ? $productPath .= '/' : false;

		}

		return $productPath;

	}

	

	// *************************** Create the category link path for a product ************************* //

	function getCategoryPathForCategory($categoryId, $language) {

		$xtcCategoryPath = xtc_get_category_path($categoryId);

		$pathExploded = explode('_',$xtcCategoryPath);

		foreach($pathExploded as $value) {

			$pathExploded[0] <> '' ? $categoryPath .= '/' : false;

			$categoryPath .= $this->getCategoryNameForId($value, $language);

		}

		return $categoryPath;

	}

	

	// *************************** Gets the category name for its ID ************************* //

	function getCategoryNameForId($categoryId,$language)

	{

    	$category_query   = "SELECT categories_name FROM " . TABLE_CATEGORIES_DESCRIPTION . " WHERE categories_id = '" . $categoryId . "' and language_id = '" . $language . "'";

   	 	$category_query   = xtc_db_query($category_query);

		$category         = xtc_db_fetch_array($category_query,false);



   		return $this->getUrlFriendlyText($category['categories_name']);

	}

	

	// *************************** Initiates and updates the whole seo_url table in the database ************************* //

	function createSeoDBTable () {

		

		$useLanguageUrl=false;

		

		// First delete the whole data records from the table

		//$bluegate_seo_query   = "TRUNCATE TABLE `bluegate_seo_url`";

   	 	//$bluegate_seo_query   = xtc_db_query($bluegate_seo_query);

		

		// ******* Products Index ********//

		// Get all products from catalog

		$product_query   = "SELECT t_productdescription.products_id, t_productdescription.language_id, t_productdescription.products_name, t_languages.code FROM " . TABLE_PRODUCTS_DESCRIPTION ." AS t_productdescription, ". TABLE_LANGUAGES ." as t_languages WHERE t_productdescription.language_id  = t_languages.languages_id";

   	 	$product_query   = xtc_db_query($product_query);

		

		// Pr�fen, ob der Language ISO Code in die URL soll	

		if ($_REQUEST['configuration']['MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL'] == 'True') {

			$useLanguageUrl=true;

		} else if (!$_REQUEST['configuration']['MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL'] && MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True') {

			$useLanguageUrl=true;

		}

		

		

		// Produkte durchlaufen und Indexierung f�r SEO Tabelle vornehmen

		while ($productList = xtc_db_fetch_array($product_query, false)) 
		{
			// Alle Cat's zu einem Produkt holen
			$getCatQuery = xtDBquery("SELECT * FROM products_to_categories where products_id = '".$productList['products_id']."'");
			while ($getCatData = xtc_db_fetch_array($getCatQuery, true)) 
			{
				$productPath = $this->getCategoryPathForProduct($productList['products_id'],$getCatData['categories_id'],$productList['language_id']);
				// URL mit oder ohne ISO Code anlegen
				if ($useLanguageUrl) 
				{
					$productLink = $productList['code'].'/'. $productPath .$this->getUrlFriendlyText($productList['products_name']);
				} 
				else 
				{
					$productLink = $productPath .$this->getUrlFriendlyText($productList['products_name']);
				
				}
				
				
				
				$productLink = $this->validateDBKeyLink ($productLink,'');
				
				
				
				$product_seo_array = array('url_md5' => md5($productLink),
				
											 'url_text' => $productLink,
				
											 'products_id' => $productList['products_id'],
				
											 'language_id' => $productList['language_id'],
											 
											 'categ_id' => $getCatData['categories_id']													 
				
											);
				
				
				
				
				$check_qry = xtc_db_query("SELECT products_id FROM bluegate_seo_url WHERE products_id='".$productList['products_id']."' and language_id  = '".$productList['language_id']."' and categ_id ='".$getCatData['categories_id']."'");
				
				if (!xtc_db_num_rows($check_qry)) 
				{
					xtc_db_perform('bluegate_seo_url', $product_seo_array );
				}
				else
				{
					xtc_db_perform('bluegate_seo_url', $product_seo_array , 'update', 'products_id = \''.$productList['products_id'].'\' and language_id  = \''.$productList['language_id'].'\' ');
				}
				
			}			
		}

		

		// ******* Categories Index ********//

		// Get all categories from catalog

		$category_query   = "SELECT t_categoriesdescription.categories_id, t_categoriesdescription.language_id, t_categoriesdescription.categories_name, t_languages.code FROM " . TABLE_CATEGORIES_DESCRIPTION . " AS t_categoriesdescription, ". TABLE_LANGUAGES ." as t_languages WHERE t_categoriesdescription.language_id = t_languages.languages_id";

   	 	$category_query   = xtc_db_query($category_query);

		

		// Kategorien durchlaufen und Indexierung f�r SEO Tabelle vornehmen

		while ($categoryList = xtc_db_fetch_array($category_query, false)) {

			$categoryPath = $this->getCategoryPathForCategory($categoryList['categories_id'], $categoryList['language_id']);			

		

			// URL mit oder ohne ISO Code anlegen

			if ($useLanguageUrl) {

				$categoryLink = $categoryList['code'] . $categoryPath;

			} else {

				// Remove leading Slash from URL (/)

				$categoryLink = substr($categoryPath,1);

			}

			

			$categoryLink = $this->validateDBKeyLink ($categoryLink,'');

			

			$category_seo_array = array('url_md5' => md5($categoryLink),

														 'url_text' => $categoryLink,

														 'categories_id' => $categoryList['categories_id'],

 														 'language_id' => $categoryList['language_id']													 

														);

		

			$check_qry = xtc_db_query("SELECT categories_id FROM bluegate_seo_url WHERE categories_id='".$categoryList['categories_id']."'  and language_id  = '".$categoryList['language_id']."'");

			if (!xtc_db_num_rows($check_qry)) {

				xtc_db_perform('bluegate_seo_url', $category_seo_array );

			}else{

				xtc_db_perform('bluegate_seo_url', $category_seo_array , 'update', 'categories_id = \''.$categoryList['categories_id'].'\' and language_id  = \''.$categoryList['language_id'].'\' ');

			}			

			

		}

		

		// ******* Content Index ********//

		$content_query   = "SELECT t_contentmanager.content_group, t_contentmanager.languages_id, t_contentmanager. content_title, t_languages.code FROM " . TABLE_CONTENT_MANAGER . " AS t_contentmanager, ". TABLE_LANGUAGES ." as t_languages WHERE t_contentmanager.languages_id = t_languages.languages_id";

   	 	$content_query   = xtc_db_query($content_query);

		

		// Content Datens�tze durchlaufen und Indexierung f�r SEO Tabelle vornehmen

		while ($contentList = xtc_db_fetch_array($content_query, false)) {

			

			// URL mit oder ohne ISO Code anlegen

			if ($useLanguageUrl) {

				$contentLink = $contentList['code'].'/'.$this->getUrlFriendlyText($contentList['content_title']);

			} else {

				$contentLink = $this->getUrlFriendlyText($contentList['content_title']);

			}

			

			$contentLink = $this->validateDBKeyLink ($contentLink,'');

			

			$content_seo_array = array('url_md5' => md5($contentLink),

														 'url_text' => $contentLink,

														 'content_group' => $contentList['content_group'],

 														 'language_id' => $contentList['languages_id']													 

														);

		

			$check_qry = xtc_db_query("SELECT content_group FROM bluegate_seo_url WHERE content_group='".$contentList['content_group']."'  and language_id  = '".$contentList['languages_id']."'");

			if (!xtc_db_num_rows($check_qry)) {

				xtc_db_perform('bluegate_seo_url', $content_seo_array );

			}else{

				xtc_db_perform('bluegate_seo_url', $content_seo_array , 'update', 'content_group = \''.$contentList['content_group'].'\' and language_id  = \''.$contentList['languages_id'].'\' ');

			}				

			

		}

		

	}

	

	// *************************** Aktualisiert einzelne Datens�tze bei einem Update in der bluegate_seo_url Tabelle ************************* //

	function updateSeoDBTable ($elementType, $operation, $id) {

		

		// Return type-depending URL

		switch ($elementType) {

			// ******* Product Update ********//

			case 'product':

				// ******* Get Product Information ********//

				$result_query   = "SELECT t_productdescription.products_id, t_productdescription.language_id, t_productdescription.products_name, t_languages.code FROM " . TABLE_PRODUCTS_DESCRIPTION ." AS t_productdescription, ". TABLE_LANGUAGES ." as t_languages WHERE t_productdescription.products_id=".$id." AND t_productdescription.language_id  = t_languages.languages_id";

		   	 	$result_query   = xtc_db_query($result_query);

				

				while ($resultList = xtc_db_fetch_array($result_query, false)) {

					$productPath = $this->getCategoryPathForProduct($resultList['products_id'],$resultList['language_id']);

					

					// URL mit oder ohne ISO Code anlegen

					if (MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True') {

						$productLink = $resultList['code'].'/'. $productPath .$this->getUrlFriendlyText($resultList['products_name']);

					} else {

						$productLink = $productPath .$this->getUrlFriendlyText($resultList['products_name']);

					}

			

					// Update Dataset of this element

					// We first have to delete the old element and then insert the new one, cause

					// otherwise the validateDBKeyLink doesn't work

					$bluegate_delete_seo_query =   "DELETE FROM `bluegate_seo_url`

													WHERE `products_id`='".$id."'";

					$bluegate_delete_seo_query   = xtc_db_query($bluegate_delete_seo_query);

										

					$this->insertSeoDBTable ('product');

					

				}

				break;

				

			// ******* Category Update ********//

			case 'category': 

				// ******* Get Categorie Information ********//

				$result_query   = "SELECT t_categoriesdescription.categories_id, t_categoriesdescription.language_id, t_categoriesdescription.categories_name, t_languages.code FROM " . TABLE_CATEGORIES_DESCRIPTION . " AS t_categoriesdescription, ". TABLE_LANGUAGES ." as t_languages WHERE t_categoriesdescription.categories_id=".$id." AND t_categoriesdescription.language_id = t_languages.languages_id";

   	 			$result_query   = xtc_db_query($result_query);

				

				while ($resultList = xtc_db_fetch_array($result_query, false)) {

					$categoryPath = $this->getCategoryPathForCategory($resultList['categories_id'], $resultList['language_id']);

					// URL mit oder ohne ISO Code anlegen

					if (MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True') {

						$categoryLink = $resultList['code'] . $categoryPath;

					} else {

					// Remove leading Slash from URL (/)

						$categoryLink = substr($categoryPath,1);

					}

					

					// Update Dataset of this element

					// We first have to delete the old element and then insert the new one, cause

					// otherwise the validateDBKeyLink doesn't work

					$bluegate_delete_seo_query =   "DELETE FROM `bluegate_seo_url`

													WHERE `categories_id`='".$id."'";

					$bluegate_delete_seo_query   = xtc_db_query($bluegate_delete_seo_query);

										

					$this->insertSeoDBTable ('category');

				}

				break;

			// Content

			case 'content': 



				break;

		}	

	}

	

	// *************************** F�gt einen neuen Datensetz in die Tabelle bluegate_seo_url ein ************************* //

	function insertSeoDBTable ($elementType) {

		// Create type-depending URL

		switch ($elementType) {

			

			// ******* Insert Product ********//

			case 'product':

				// Ermitteln, welche Produkte existieren, die noch nicht in bluegate_seo_url indiziert sind

				$result_query   = "SELECT products_description.products_id, products_description.language_id, products_description.products_name, languages.code

								   FROM " . TABLE_PRODUCTS_DESCRIPTION ." AS products_description

								   LEFT JOIN `bluegate_seo_url` AS `bluegate_seo_url`

								   ON products_description.products_id=bluegate_seo_url.products_id

								   INNER JOIN ". TABLE_LANGUAGES ." AS  `languages`

								   ON products_description.language_id=languages.languages_id

								   WHERE bluegate_seo_url.products_id IS NULL";   

								

		   	 	$result_query   = xtc_db_query($result_query);

				

				// Anlegen der neuen Datens�tze

				while ($resultList = xtc_db_fetch_array($result_query, false)) {

				

					$productPath = $this->getCategoryPathForProduct($resultList['products_id'],$resultList['language_id']);

			

					// URL mit oder ohne ISO Code anlegen

					if (MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True') {

						$productLink = $resultList['code'].'/'. $productPath .$this->getUrlFriendlyText($resultList['products_name']);

					} else {

						$productLink = $productPath .$this->getUrlFriendlyText($resultList['products_name']);

					}

			

					$productLink = $this->validateDBKeyLink ($productLink,'');

		

					$product_seo_query   = "INSERT INTO `bluegate_seo_url` 

									(`url_md5`,`url_text`,`products_id`,`language_id`) 

									VALUES 

									('".md5($productLink)."','".$productLink."','".$resultList['products_id']."','".$resultList['language_id']."')";

				

					$product_seo_query   = xtc_db_query($product_seo_query);

				}

			break;

			

			// ******* Insert Category ********//

			case 'category':

				// Ermitteln, welche Kategorien existieren, die noch nicht in bluegate_seo_url indiziert sind

				$result_query   = "SELECT categories_description.categories_id, categories_description.language_id, categories_description.categories_name, languages.code

								   FROM " . TABLE_CATEGORIES_DESCRIPTION ." AS categories_description

								   LEFT JOIN `bluegate_seo_url` AS `bluegate_seo_url`

								   ON categories_description.categories_id=bluegate_seo_url.categories_id

								   INNER JOIN ". TABLE_LANGUAGES ." AS  `languages`

								   ON categories_description.language_id=languages.languages_id

								   WHERE bluegate_seo_url.categories_id IS NULL"; 

		   	 	$result_query   = xtc_db_query($result_query);

				

				while ($resultList = xtc_db_fetch_array($result_query, false)) {

					$categoryPath = $this->getCategoryPathForCategory($resultList['categories_id'], $resultList['language_id']);			

		

					// URL mit oder ohne ISO Code anlegen

					if (MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True') {

						$categoryLink = $resultList['code'] . $categoryPath;

					} else {

						// Remove leading Slash from URL (/)

						$categoryLink = substr($categoryPath,1);

					}

			

					$categoryLink = $this->validateDBKeyLink ($categoryLink,'');

			

					$category_seo_query   = "INSERT INTO `bluegate_seo_url` 

											(`url_md5`,`url_text`,`categories_id`,`language_id`) 

											VALUES 

											('".md5($categoryLink)."','".$categoryLink."','".$resultList['categories_id']."','".$resultList['language_id']."')";

					$category_seo_query   = xtc_db_query($category_seo_query);

				}

			break;

			

		}

	}



	

	// *************************** Pr�ft rekursiv ob ein Key mit selben Bezeichner / Pfad bereits existiert und liefert gegebenenfalls einen anderen freien Key zur�ck ************************* //

	function validateDBKeyLink ($urlKey,$counter) {

		$product_query   = "SELECT url_text FROM bluegate_seo_url where url_md5='".md5($urlKey.$counter)."' ";

		$product_query   = xtc_db_query($product_query);

		

		if (xtc_db_num_rows($product_query)==0) {

			return $urlKey.$counter;

		} else {

			$counter++;

			return $this->validateDBKeyLink ($urlKey,$counter);

		}			

	}

	

	// *************************** Retrives the ID for a given URL from seo_url table in the database ************************* //

	function getIdForURL($linkUrl='',$type) {



		$query   = "SELECT products_id, categories_id, content_group FROM bluegate_seo_url WHERE url_md5_old = '" . md5($_SERVER['REQUEST_URI']) . "' LIMIT 0,1";



		$query   = xtc_db_query($query);

		if (xtc_db_num_rows($query)==1) {

	

			$id      = xtc_db_fetch_array($query);



			if($id['products_id'] != 0){

			

			$parameters='info=p'.$id['products_id'].'_';

			$redirectLink = $this->getProductLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

				

			}elseif($id['categories_id'] != 0){

			

			$parameters='cat=c'.$id['categories_id'].'_';

			$redirectLink = $this->getCategoryLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

				

			}else{

			

			$parameters='coID='.$id['content_group'];

			$redirectLink = $this->getContentLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

				

			}

			

		}

		else {

		

		$query   = "SELECT products_id, categories_id, content_group FROM bluegate_seo_url WHERE url_md5 = '" . md5($linkUrl) . "' LIMIT 0,1";

   	 	$query   = xtc_db_query($query);

		$id      = xtc_db_fetch_array($query);

		

		}



							

		// *******************************************************************

		// * 404 PROCESSING **************************************************

		// *******************************************************************

		$pageNotFound = false;

		

		if (!is_array($id)) {

			// Don't send 404 if the root (/) is called

			if (!(basename(basename($_SERVER['SCRIPT_FILENAME']))=='index.php' && $id=='' && $linkUrl=='')) {

				// Don't send 404 it the language on the root page is switched

				if (!(MODULE_BLUEGATE_SEO_INDEX_LANGUAGEURL=='True' && strlen($linkUrl)==2 && in_array($linkUrl,$this->getLanguagesISOCodes()))) {

					$pageNotFound = true;

				}

			}

		}

		

		

		// *******************************************************************

		// * SEND 404 HEADER *************************************************

		// *******************************************************************

		if ($pageNotFound==true && MODULE_BLUEGATE_SEO_INDEX_SEND404ERROR=='True') {
			header('HTTP/1.1 404 Not Found', true, 404);
			header("X-Robots-Tag: noindex,nofollow");
			//header("Location: http://www.esoterikshopping.de/content/404.html"); 
			header("Connection: close");
		}

		

		// *******************************************************************

		// * SEND 404 REDIRECT ***********************************************

		// *******************************************************************

		

		/*
		if ($pageNotFound==true && strlen(MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET)>0) {

			header('location: '.MODULE_BLUEGATE_SEO_INDEX_404ERRORTARGET);

		}*/

				

				

		// Check the URL type

		/*

		switch ($type) {

			case 'product':

				return array('id'=>$id['products_id'], 'type'=>'info', 'redirect'=>$redirect);

			case 'category':

				return array('id'=>$id['categories_id'], 'type'=>'cat', 'redirect'=>$redirect);

			case 'content':

				return array('id'=>$id['content_group'], 'type'=>'coID', 'redirect'=>$redirect);

		}

		

		*/

		

				// Check the URL type

		switch ($type) {

			case 'product':

				return $id['products_id'];

			case 'category':

				return $id['categories_id'];

			case 'content':

				return $id['content_group'];

		}

	}

	

	// *************************** Masks a string to become url friendly ************************* //

	function getUrlFriendlyText ($string) {

		

		$search = array();

		$replace = array();

		$this->getRegExps($search, $replace);



    	// <br> durch - ersetzen

    	$validUrl  = preg_replace("/<br>/i","-",$string);



    	// HTML Tags entfernen

    	$validUrl  = strip_tags($validUrl);

		

		// Schr�gstriche entfernen

	    $validUrl  = preg_replace("/\//","-",$validUrl);



    	// Definierte Zeichen entfernen (Arraydefinition)

    	$validUrl  = preg_replace($search,$replace,$validUrl);



    	// Die nun noch (komisch aussehenden) doppelten Bindestriche entfernen

    	$validUrl  = preg_replace("/(-){2,}/","-",$validUrl);

		

		// Nun alles entfernen, was nicht [a-Z][0-9] oder ein Bindestrich ist

		$validUrl = preg_replace("/[^a-z0-9-]/i", "", $validUrl);

		

		// String URL-codieren

    	$validUrl  = urlencode($validUrl);

	 

	    return($validUrl);

	}

	

	function getRegExps(&$search, &$replace) {

    $search     = array(

                        "'\s&\s'",          // remove ampersant

						"'[\r\n\s]+'",	    // strip out white space

						"'&(quote|#34);'i",	// replace html entities

						"'&(amp|#38);'i",

						"'&(lt|#60);'i",

						"'&(gt|#62);'i",

						"'&(nbsp|#160);'i",

						"'&(iexcl|#161);'i",

						"'&(cent|#162);'i",

						"'&(pound|#163);'i",

						"'&(copy|#169);'i",

                        "'&'",              // ampersant in + konvertieren

                        "'%'",              //-- % entfernen

                        "/[\[\({]/",        //--�ffnende Klammern nach Bindestriche entfernen

                        "/[\)\]\}]/",       //--schliessende Klammern entfernen

                        "/�/",              //--Umlaute entfernen

                        "/�/",              

                        "/�/",              

                        "/�/",              

                        "/�/",              

                        "/�/",              

                        "/�/",              

                        "/'|\"|�|`/",       //--Anf�hrungszeichen entfernen

                        "/[:,\.!?\*\+]/",   //--Doppelpunkte, Komma, Punkt, asterisk entfernen

                        );

    $replace    = array(

                        "-",

						"-",

					    "\"",

						"-",

						"<",

						">",

						"",

						chr(161),

						chr(162),

						chr(163),

						chr(169),

                        "-",

						"+",

                        "-",

                        "",

                        "ss",

                        "ae",

                        "ue",

                        "oe",

                        "Ae",

                        "Ue",

                        "Oe",

                        "",

                        ""

                        );



	}

	

	// *************************** Retrives the ID from a given XTC URL (e.g. [info] => p2_Demoprodukt-Template--Red-River-.html - Required for 301 redirection ************************* //

	function getIdForXTCSumaFriendlyURL($fileName) {

		

		// set the language (necessary to find the correct product by language ID)

		// Set ONLY if a 301 Redirect follows. Else a duplicate Inclusion of the language Object may happen

		if (($fileName=='product_info.php' && $_GET['info']<>'' && $_GET['action']=='') || ($fileName == 'index.php' && $_GET['cat']<>'' && $_GET['page']=='' && $_GET['action']=='') || ($fileName == 'index.php' && $_GET['cPath']<>'' && $_GET['page']=='' && $_GET['action']=='') || ($fileName == 'shop_content.php' && $_GET['coID']<>'' && $_GET['action']=='')) {

			require_once(DIR_WS_CLASSES.'language.php');

			!$temp_lng ? $temp_lng = new language(xtc_input_validation($_GET['language'], 'char', '')) : false;

		

			if (!isset ($_SESSION['language']) || isset ($_GET['language'])) {



				if (!isset ($_GET['language']))

					$temp_lng->get_browser_language();

				$_SESSION['languages_id'] = $temp_lng->language['id'];

			

			}



			if (isset($_SESSION['language']) && !isset($_SESSION['language_charset'])) {

				$_SESSION['languages_id'] = $temp_lng->language['id'];

		

			}

		}



		// *******************************************************************

		// * PRODUCT 301 REDIRECT ********************************************

		// *******************************************************************



		// Create 301 redirect for PRODUCT LINKS without xtc:Suma friendly URLS

		// e.g.: http://www.shopname.com/product_info.php?info=p124_Produkt-1.html

		if ($fileName=='product_info.php' && $_GET['info']<>'' && $_GET['products_id']=='' && $_GET['action']=='') {

			$parameters='info='.$_GET['info'];

			$redirectLink = $this->getProductLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

		}

		

		// Create 301 redirect for PRODUCT LINKS with xtc:Suma friendly URLS

		// e.g.: http://www.shopname.com/product_info.php/products_id/124

		if ($fileName=='product_info.php' && $_GET['products_id']<>'' && $_GET['info']==''&& $_GET['action']=='') {

			$parameters='info=p'.$_GET['products_id'].'_';

			$redirectLink = $this->getProductLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

		}

		

		// *******************************************************************

		// * CATEGORY 301 REDIRECT *******************************************

		// *******************************************************************



		// Create 301 redirect for CATEGORY LINKS without xtc:Suma friendly URLS

		// e.g.: http://www.shopname.com/index.php?cat=c10_Kategorie-5.html

		if ($fileName == 'index.php' && $_GET['cat']<>'' && $_GET['cPath']=='' && $_GET['page']=='' && $_GET['action']=='') {

			$parameters='cat='.$_GET['cat'];

			$redirectLink = $this->getCategoryLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

		}



		// New Redirect

		

		if ($fileName == 'index.php' && $_GET['cat_id']<>'' && $_GET['page']=='' && $_GET['action']=='') {

			$categoryId = $this->getIdForURL($_GET['linkurl'],'category');

		}



		// Create 301 redirect for CATEGORY LINKS with xtc:Suma friendly URLS (cPath)

		// e.g.: http://www.shopname.com/index.php/cPath/10

		if ($fileName == 'index.php' && $_GET['cPath']<>'' && $_GET['page']=='' && $_GET['action']=='') {

			

			// Extract last Category ID if subcategories are submitted

			// e.g.: http://www.shopname.com/index.php/cPath/77_78

			// (78 is the subcategory)

			$explodedCategoryParameters = explode('_',$_GET['cPath']);

			$_GET['cPath']=end($explodedCategoryParameters);			



			$parameters='cat=c'.$_GET['cPath'].'_';

			$redirectLink = $this->getCategoryLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

		}

		

		// *******************************************************************

		// * CONTENT 301 REDIRECT ********************************************

		// *******************************************************************

		if ($fileName == 'shop_content.php' && $_GET['coID']<>'' && $_GET['action']=='') {

			$parameters='coID='.$_GET['coID'];

			$redirectLink = $this->getContentLink($parameters,$connection,$_SESSION['languages_id']);

			header("HTTP/1.1 301 Moved Permanently");

			header("Location: ".$redirectLink);

			die('This page has moved to: <a href="'.$redirectLink.'">'.$redirectLink.'</a>');

		}

		

		return false;

	}

	

	// *************************** Returns all available language ISO-2 Codes as Array************************* //

	function getLanguagesISOCodes() {

		

		$languageIsoArray = array();

		$languageIsoQuery = "SELECT code 

							 FROM languages";

							 

		$languageIsoResult = xtc_db_query($languageIsoQuery);

		

		while ($languageIsoRow = mysql_fetch_array($languageIsoResult, MYSQL_ASSOC)) {

			array_push($languageIsoArray, $languageIsoRow['code']);

		}

		return $languageIsoArray;

		

	}

}

?>