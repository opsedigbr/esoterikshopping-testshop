<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_get_products_produkthervorhebung_einheit_name.inc.php 

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
   
   
   function xtc_get_products_produkthervorhebung_einheit_name($products_produkthervorhebung_einheitID) {
   	
   	  $products_produkthervorhebung_einheit_query="SELECT products_produkthervorhebung_einheit_name FROM " . TABLE_PRODUCTS_PRODUKTHERVORHEBUNG_EINHEIT . " WHERE language_id='".(int)$_SESSION['languages_id']."' and products_produkthervorhebung_einheit_id='".$products_produkthervorhebung_einheitID."'";
   	  $products_produkthervorhebung_einheit_query = xtDBquery($products_produkthervorhebung_einheit_query);
   	  $products_produkthervorhebung_einheit = xtc_db_fetch_array($products_produkthervorhebung_einheit_query,true);
   	  return $products_produkthervorhebung_einheit['products_produkthervorhebung_einheit_name'];
   	
   }
   
    
?>
