<?php
/* -----------------------------------------------------------------------------------------
   $Id: xtc_get_products_grundpreis_einheit_name.inc.php 

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/
   
   
   function xtc_get_products_grundpreis_einheit_name($products_grundpreis_einheitID) {
   	
   	  $products_grundpreis_einheit_query="SELECT products_grundpreis_einheit_name FROM " . TABLE_PRODUCTS_GRUNDPREIS_EINHEIT . " WHERE language_id='".(int)$_SESSION['languages_id']."' and products_grundpreis_einheit_id='".$products_grundpreis_einheitID."'";
   	  $products_grundpreis_einheit_query = xtDBquery($products_grundpreis_einheit_query);
   	  $products_grundpreis_einheit = xtc_db_fetch_array($products_grundpreis_einheit_query,true);
   	  return $products_grundpreis_einheit['products_grundpreis_einheit_name'];
   	
   }
   
    
?>
