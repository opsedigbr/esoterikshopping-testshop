<?php

/* -----------------------------------------------------------------------------------------
   $Id: shop_content.php 1303 2005-10-12 16:47:31Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(conditions.php,v 1.21 2003/02/13); www.oscommerce.com 
   (c) 2003	 nextcommerce (shop_content.php,v 1.1 2003/08/19); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

require ('includes/application_top.php');
// create smarty elements
$smarty = new Smarty;
// include boxes
require (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/boxes.php');

// include needed functions
require_once (DIR_FS_INC.'xtc_validate_email.inc.php');
require_once (DIR_FS_INC.'xtc_cleanName.inc.php');

if (GROUP_CHECK == 'true') 
{
	$group_check = "and group_ids LIKE '%c_".$_SESSION['customers_status']['customers_status_id']."_group%'";
}

$shop_content_query = xtc_db_query("SELECT
                     content_id,
                     content_title,
                     content_heading,
                     content_text,
                     content_file 
                     FROM ".TABLE_CONTENT_MANAGER."
                     WHERE content_group='".(int) $_GET['coID']."' ".$group_check."
                     AND languages_id='".(int) $_SESSION['languages_id']."'");
$shop_content_data = xtc_db_fetch_array($shop_content_query);

//$breadcrumb->add($shop_content_data['content_title'], xtc_href_link('product-informations.php', '?coID='.(int) $_GET['coID']));

require (DIR_WS_INCLUDES.'header.php');

$smarty->assign('CONTENT_HEADING', $shop_content_data['content_heading']);

if ($shop_content_data['content_file'] != '') 
{

	ob_start();

	if (strpos($shop_content_data['content_file'], '.txt'))
		echo '<pre>';
	include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
	if (strpos($shop_content_data['content_file'], '.txt'))
		echo '</pre>';
	$smarty->assign('file', ob_get_contents());
	ob_end_clean();

} 
else 
{
	$content_body = $shop_content_data['content_text'];
}
$smarty->assign('CONTENT_BODY', $content_body);

//$smarty->assign('BUTTON_CONTINUE', '<a href="javascript:history.back(1)">'.xtc_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>');
$smarty->assign('language', $_SESSION['language']);


/******************************************************************************************************/
/******************************* get footer - weitere produktnews *************************************/
/******************************************************************************************************/
define('MAX_PRODUCTINFORMATION_NEWS_1', '10');
define('MAX_TEXT_LENGTH_1', '265');

$module_content = array ();

$pinQery = xtDBquery("select * from ".TABLE_CONTENT_MANAGER." where file_flag = '10' and content_status = '1' order by content_id DESC limit ".MAX_PRODUCTINFORMATION_NEWS_1);
if (xtc_db_num_rows($pinQery,true) > 0) 
{
	$row = 0;
	while ($pin = xtc_db_fetch_array($pinQery,true)) 
	{
		$row ++;
		
		// Text auslesen //
		$pin_content = '';
		$pin['content_title'] = xtc_cleanName($pin['content_title']);
		$pin_content = file_get_contents(HTTP_SERVER.DIR_WS_CATALOG . 'media/content/' . $pin['content_file']);
		
		// Überschrift auslesen //
		if(empty($pin['content_heading']))
		{
			if(preg_match("/<h1>(.*)<\/h1>/", $pin_content, $match))
			{
				$pin_headline = $match[1];
			}
			else
			{
				$pin_headline = $pin['content_heading'];
			}
		}
		else
		{
			$pin_headline = $pin['content_heading'];
		}
		
		// 1. Bild auslesen //
		//$s_pattern = '/(img|src)=("|\')[^"\'>]+/i';
		$s_pattern = '/<img src=("|\')(.*)["\|\']/';
		
		if(preg_match($s_pattern, $pin_content, $img_match))
		{
			$pin_images = $img_match[0].' />';
		}
		else
		{
			$pin_images = '';
		}
		
		// Daten aufbereiten
		$pin_content = trim(strip_tags(str_replace('<br />', "\n", $pin_content)));
		//$pin_content = substr($pin_content, 0, MAX_TEXT_LENGTH_1);
		$pin['content_cpaths'] = str_replace(',','_',$pin['content_cpaths']);
				
		$pin_headline_1 = $pin['content_title'];
		$pin_headline_2 = $pin_headline;
		
		/*
		$module_content[] = array (
		'PIN_LINK' => FILENAME_PRODUCT_INFORMATIONS.'?coID='.$pin['content_group'].'&cPath='.$pin['content_cpaths'].'&product='.$pin['content_title'],
		'PIN_LINK_MORE' => '...mehr...',
		'PIN_HEADLINE_1' => $pin_headline_1,
		'PIN_HEADLINE_2' => $pin_headline_2,
		'PIN_CONTENT' => $pin_content, 
		'PIN_IMG' => $pin_images
		);
		*/
		$module_content[] = array (
		'PIN_LINK' => 'l-pg/'.$pin['content_group'].'/'.$pin['content_cpaths'].'/'.$pin['content_title'].'-guenstig-hier-im-shop-kaufen.html',
		'PIN_LINK_MORE' => '...mehr...',
		'PIN_HEADLINE_1' => $pin_headline_1,
		'PIN_HEADLINE_2' => $pin_headline_2,
		'PIN_CONTENT' => $pin_content, 
		'PIN_IMG' => $pin_images
		);		
	}

	$smarty->assign('module_content', $module_content);
}
/******************************************************************************************************/


// set cache ID
if (!CacheCheck()) 
{
	$smarty->caching = 0;
	$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/product-informations.html');
} 
else 
{
	$smarty->caching = 1;
	$smarty->cache_lifetime = CACHE_LIFETIME;
	$smarty->cache_modified_check = CACHE_CHECK;
	$cache_id = $_SESSION['language'].$shop_content_data['content_id'];
	$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/product-informations.html', $cache_id);
}

$smarty->assign('language', $_SESSION['language']);
$smarty->assign('main_content', $main_content);
$smarty->caching = 0;
if (!defined(RM))
	$smarty->load_filter('output', 'note');
$smarty->display(CURRENT_TEMPLATE.'/index.html');
include ('includes/application_bottom.php');
?>