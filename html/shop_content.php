<?php

/* -----------------------------------------------------------------------------------------
   $Id: shop_content.php 1303 2005-10-12 16:47:31Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(conditions.php,v 1.21 2003/02/13); www.oscommerce.com 
   (c) 2003	 nextcommerce (shop_content.php,v 1.1 2003/08/19); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

require ('includes/application_top.php');
// create smarty elements
$smarty = new Smarty;
// include boxes
require (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/boxes.php');

// include needed functions
require_once (DIR_FS_INC.'xtc_validate_email.inc.php');

if (GROUP_CHECK == 'true') {
	$group_check = "and group_ids LIKE '%c_".$_SESSION['customers_status']['customers_status_id']."_group%'";
}

$shop_content_query = xtc_db_query("SELECT
                     content_id,
                     content_title,
                     content_heading,
                     content_text,
                     content_file
                     FROM ".TABLE_CONTENT_MANAGER."
                     WHERE content_group='".(int) $_GET['coID']."' ".$group_check."
                     AND languages_id='".(int) $_SESSION['languages_id']."'");
$shop_content_data = xtc_db_fetch_array($shop_content_query);

$breadcrumb->add($shop_content_data['content_title'], xtc_href_link(FILENAME_CONTENT.'?coID='.(int) $_GET['coID']));

if ($_GET['coID'] != 7) {
	require (DIR_WS_INCLUDES.'header.php');
}
if ($_GET['coID'] == 7 && $_GET['action'] == 'success') {
	require (DIR_WS_INCLUDES.'header.php');
}

$smarty->assign('CONTENT_HEADING', $shop_content_data['content_heading']);

if ($_GET['coID'] == 7) {

	$error = false;
	if (isset ($_GET['action']) && ($_GET['action'] == 'send')) {
		if (xtc_validate_email(trim($_POST['email']))) {

			xtc_php_mail($_POST['email'], $_POST['name'], CONTACT_US_EMAIL_ADDRESS, CONTACT_US_NAME, CONTACT_US_FORWARDING_STRING, $_POST['email'], $_POST['name'], '', '', CONTACT_US_EMAIL_SUBJECT, nl2br($_POST['message_body']), $_POST['message_body']);

			if (!isset ($mail_error)) {
				xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'action=success&coID='.(int) $_GET['coID']));
			} else {
				$smarty->assign('error_message', $mail_error);

			}
		} else {
			// error report hier einbauen
			$smarty->assign('error_message', ERROR_MAIL);
			$error = true;
		}

	}

	$smarty->assign('CONTACT_HEADING', $shop_content_data['content_title']);
	if (isset ($_GET['action']) && ($_GET['action'] == 'success')) {
		$smarty->assign('success', '1');
		$smarty->assign('BUTTON_CONTINUE', '<a href="'.xtc_href_link(FILENAME_DEFAULT).'">'.xtc_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE).'</a>');

	} else {
		if ($shop_content_data['content_file'] != '') {
			ob_start();
			if (strpos($shop_content_data['content_file'], '.txt'))
				echo '<pre>';
			include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
			if (strpos($shop_content_data['content_file'], '.txt'))
				echo '</pre>';
		$contact_content = ob_get_contents();
		ob_end_clean();
		} else {
			$contact_content = $shop_content_data['content_text'];
		}
		require (DIR_WS_INCLUDES.'header.php');
		$smarty->assign('CONTACT_CONTENT', $contact_content);
		$smarty->assign('FORM_ACTION', xtc_draw_form('contact_us', xtc_href_link(FILENAME_CONTENT, 'action=send&coID='.(int) $_GET['coID'])));
		$smarty->assign('INPUT_NAME', xtc_draw_input_field('name', ($error ? $_POST['name'] : $first_name)));
		$smarty->assign('INPUT_EMAIL', xtc_draw_input_field('email', ($error ? $_POST['email'] : $email_address)));
		$smarty->assign('INPUT_TEXT', xtc_draw_textarea_field('message_body', 'soft', 50, 15, $_POST['']));
		$smarty->assign('BUTTON_SUBMIT', xtc_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE));
		$smarty->assign('FORM_END', '</form>');
	}

	$smarty->assign('language', $_SESSION['language']);

	$smarty->caching = 0;
	$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/contact_us.html');

} 
// Widerrufsformular
else if ($_GET['coID'] == 10002) {

	if (!isset ($_SESSION['customer_id'])) {
		$smarty->assign('WIDERRUFSFORMUALR_LOGIN_FIRST', 'true');
		$smarty->assign('FORM_ACTION', xtc_draw_form('login', xtc_href_link(FILENAME_ACCOUNT, '', 'SSL')));
		$smarty->assign('FORM_END', '</form>');
		require_once(DIR_WS_INCLUDES.'header.php');
	}
	else {
		$sql = xtc_db_query("SELECT o.billing_name, o.billing_street_address, o.billing_postcode, o.billing_city, o.billing_country, c.customers_email_address FROM ".TABLE_ORDERS." o LEFT JOIN ".TABLE_CUSTOMERS." c ON o.customers_id = c.customers_id WHERE c.customers_id = ".intval($_SESSION['customer_id'])." ORDER BY o.orders_id DESC LIMIT 0,1;");
		$data = xtc_db_fetch_array($sql);
		$name_verbraucher = $data['billing_name'];
		$anschrift = $data['billing_street_address'].', '.$data['billing_postcode'].' '.$data['billing_city'].', '.$data['billing_country'];
		$datum = date('d.m.Y');
		$customers_email_address = $data['customers_email_address'];

		$error = false;

		if (isset ($_GET['action']) && ($_GET['action'] == 'send')) {

			// 1. Schritt
			$ware = xtc_db_prepare_input($_POST['ware']);
			$bestellnummer = xtc_db_prepare_input($_POST['bestellnummer']);
			$bestellt_am = xtc_db_prepare_input($_POST['bestellt_am']);
			$name_verbraucher = xtc_db_prepare_input($_POST['name_verbraucher']);
			$anschrift = xtc_db_prepare_input($_POST['anschrift']);
			$datum = xtc_db_prepare_input($_POST['datum']);
			$dateEmailSent = date('d.m.Y H:i');

			$dataArray = array($ware, $bestellt_am, $name_verbraucher, $anschrift, $datum);
			//print_r($dataArray);

			// 2. Schritt
			$error = false;
			$required = false;
			
			// 3. Schritt
			if (strlen($ware) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie an um welche Ware es sich handelt.';
				$required['ware'] = 'required';
			}

			if (strlen($bestellnummer) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie Ihre Bestellnummer an.';
				$required['bestellnummer'] = 'required';
			}			

			if (strlen($bestellt_am) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie an wann Sie die Ware bei uns bestellt haben.';
				$required['bestellt_am'] = 'required';
			}

			if (strlen($name_verbraucher) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie den Namen des Verbraucher an.';
				$required['name_verbraucher'] = 'required';
			}		

			if (strlen($anschrift) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie Ihre Anschrift an.';
				$required['anschrift'] = 'required';
			}	

			if (strlen($datum) < 1) {
				$error = true;
				$messageWiderrufsformular[] = 'Bitte geben Sie das Datum an.';
				$required['datum'] = 'required';
			}

			$adminEmail = 'info@esoterikshopping.de';
			$adminEmailName = 'Esoterikshopping.de';

			if($error == false)	{
				$messageHtml = '';
				$messageHtml .= "Ihr Widerruf<br /><br />";
				$messageHtml .= "Hallo $name_verbraucher,<br />";
				$messageHtml .= "sie haben unser Online-Widerrufsformular ausgef&uuml;llt, dessen Empfang wir hiermit best&auml;tigen.<br />";
				$messageHtml .= "Wir werden uns ggf. mit Ihnen in Verbindung setzen um etwaige Details zu kl&auml;ren.<br /><br />";
				$messageHtml .= "Ware, die widerrufen wurde:<br />";
				$messageHtml .= "$ware<br /><br />";
				$messageHtml .= "Bestellnummer:<br />";
				$messageHtml .= "$bestellnummer<br /><br />";
				$messageHtml .= "Bestellt am:<br />";				
				$messageHtml .= "$bestellt_am<br /><br />";
				$messageHtml .= "Name des Verbrauchers:<br />";
				$messageHtml .= "$name_verbraucher<br /><br />";
				$messageHtml .= "Anschrift des Verbrauchers:<br />";
				$messageHtml .= "$anschrift<br /><br />";
				$messageHtml .= "Datum des Widerrufs:<br />";
				$messageHtml .= "$datum<br /><br />";
				$messageHtml .= "E-Mail Zeitstempel:<br />";
				$messageHtml .= "$dateEmailSent<br /><br />";
				$messageHtml .= "Ihr Team von Esoterikshopping.de<br />Esoterikshopping ist eine Domain der Spart Bares GbR<br />Vertretungsberechtigte Gesellschafter: Holger Klinzmann und Uta Dittrich<br />Humboldtstrasse 134<br />90459 N&uuml;rnberg<br />Umsatzsteuer-Identifikationsnummer: DE 234465241<br />E-mail: info@esoterikshopping.de<br />Tel.: 08404 - 939 264<br />Fax: 08404 - 939 265";

				$messageTxt = $messageHtml;

				// Mail an Admin
				xtc_php_mail($customers_email_address, $name_verbraucher, $adminEmail, $adminEmailName, '', 'info@esoterikshopping.de', 'Esoterikshopping.de', '', '', 'Widerruf', $messageHtml, $messageTxt);

				// Mail an Kunden
				xtc_php_mail('info@esoterikshopping.de', 'Esoterikshopping.de', $customers_email_address, $name_verbraucher, '', 'info@esoterikshopping.de', 'Esoterikshopping.de', '', '', 'Widerruf', $messageHtml, $messageTxt);

				xtc_redirect(xtc_href_link(FILENAME_CONTENT, 'action=success&coID='.(int) $_GET['coID']));
			}	
			
			else {
				$messageWiderrufsformular = implode('<br />', $messageWiderrufsformular);
				$smarty->assign('error_message', $messageWiderrufsformular);
			}
			
		}

		//echo "<br />Error: $error ||| Action: ".$_GET['action']."<br />";

		if (isset ($_GET['action']) && ($_GET['action'] == 'success')) {
			$smarty->assign('success', '1');
			$smarty->assign('FORM_ACTION', xtc_draw_form('widerrufsformular', xtc_href_link(FILENAME_DEFAULT, '')));
			$smarty->assign('FORM_END', '</form>');
		} 

		else {
			if ($shop_content_data['content_file'] != '') {
				ob_start();
				
				if (strpos($shop_content_data['content_file'], '.txt')) {
					echo '<pre>';
				}

				include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
				
				if (strpos($shop_content_data['content_file'], '.txt')) {
					echo '</pre>';
				}

				$contact_content = ob_get_contents();
				ob_end_clean();
			} 
			else {
				$contact_content = $shop_content_data['content_text'];
			}

			$smarty->assign('WIDERRUFSFORMUALR_HEADING_INFO', '(Wenn Sie den Vertrag widerrufen wollen, dann f&uuml;llen Sie bitte dieses Formular aus)');
			$smarty->assign('FORM_ACTION', xtc_draw_form('widerrufsformular', xtc_href_link(FILENAME_CONTENT, 'action=send&coID='.(int) $_GET['coID'])));
			$smarty->assign('INPUT_WARE', '<textarea class="textfield '.$required['ware'].'" name="ware" style="width:400px;height:150px;">'.$ware.'</textarea>');
			$smarty->assign('INPUT_BESTELLNUMMER', '<input class="textfield '.$required['bestellnummer'].'" type="text" name="bestellnummer" value="'.$bestellnummer.'">');
			$smarty->assign('INPUT_BESTELLT_AM', '<input class="textfield '.$required['bestellt_am'].'" type="text" name="bestellt_am" value="'.$bestellt_am.'">');
			$smarty->assign('INPUT_NAME_VERBRAUCHER', '<input class="textfield '.$required['name_verbraucher'].'" type="text" name="name_verbraucher" value="'.$name_verbraucher.'">');
			$smarty->assign('INPUT_ANSCHRIFT_VERBRAUCHER', '<input class="textfield '.$required['anschrift'].'" type="text" name="anschrift" value="'.$anschrift.'">');
			$smarty->assign('INPUT_DATUM', '<input class="textfield '.$required['datum'].'" type="text" name="datum" value="'.$datum.'">');
			$smarty->assign('FORM_END', '</form>');
			require_once(DIR_WS_INCLUDES.'header.php');
		}
	}

	$smarty->assign('WIDERRUFSFORMUALR_HEADING', $shop_content_data['content_title']);
	
	$smarty->assign('language', $_SESSION['language']);

	$smarty->caching = 0;
	$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/widerrufsformular.html');
}
else {

	if ($shop_content_data['content_file'] != '') {

		ob_start();

		if (strpos($shop_content_data['content_file'], '.txt'))
			echo '<pre>';
		include (DIR_FS_CATALOG.'media/content/'.$shop_content_data['content_file']);
		if (strpos($shop_content_data['content_file'], '.txt'))
			echo '</pre>';
		$smarty->assign('file', ob_get_contents());
		ob_end_clean();

	} else {
		$content_body = $shop_content_data['content_text'];
	}
	$smarty->assign('CONTENT_BODY', $content_body);

	$smarty->assign('BUTTON_CONTINUE', '<a href="javascript:history.back(1)">'.xtc_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>');
	$smarty->assign('language', $_SESSION['language']);

	// set cache ID
	 if (!CacheCheck()) {
		$smarty->caching = 0;
		$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/content.html');
	} else {
		$smarty->caching = 1;
		$smarty->cache_lifetime = CACHE_LIFETIME;
		$smarty->cache_modified_check = CACHE_CHECK;
		$cache_id = $_SESSION['language'].$shop_content_data['content_id'];
		$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/content.html', $cache_id);
	}

}

$smarty->assign('language', $_SESSION['language']);
$smarty->assign('main_content', $main_content);
$smarty->caching = 0;
if (!defined(RM))
	$smarty->load_filter('output', 'note');
$smarty->display(CURRENT_TEMPLATE.'/index.html');
include ('includes/application_bottom.php');
?>