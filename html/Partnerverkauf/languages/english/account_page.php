<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$tlinks_1 = "Standard Linking Code";
$menu_1 = "General Statistics";
$menu_2 = "2nd Tier Statistics";
$menu_4 = "Banner Linking Code";
$menu_11 = "Text Ad Linking Code";
$menu_12 = "Recurring Sales";
$tb_60 = "Recurring Sale Earnings";

$tb_1	= "Account Status";
$tb_app = "Approved";
$tb_non = "Pending Approval";
$tlinks_2 = "Copy/Paste Your Code Into Your Site";
$tlinks_alt_1 = "Your Account Status Is Currently Pending Approval";
$tlinks_alt_2 = "Once we have approved your account, you can login to get your code.";
$tb_2	= "Total Transactions";
$tb_3	= "Standard Earnings";
$tb_4	= "2nd Tier Earnings";
$tb_5	= "Earnings To Date";
$tb_inc = "includes bonus";
$menu_3 = "Payment History";
$menu_5 = "2nd Tier Linking Code";
$menu_6 = "Send Link To Friend";
$menu_7 = "Edit My Account";
$menu_8 = "Change Password";
$menu_9 = "Browse Other Affiliates";
$menu_10 = "Frequently Asked Questions";
$msmatch = "Passwords Do Not Match";
$text_p = "Password Successfully Changed";
$un_1	= "Account Status Alert";
$un_2	= "Your Account Status Is Currently Pending Approval";
$un_3	= "Once we have approved your account, you can login to get your code.";
?>