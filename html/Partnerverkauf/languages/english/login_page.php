<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$column_heading = "Account Login";
$p1 = "Enter your username and password to gain access to your account statistics, banners, linking code, FAQ and more.";
$p2 = "If you can't remember your password, enter your username and we'll send your login information to you via email.";
$page_heading = "Login To Your Account";
$text_user = "Username";
$text_pass = "Password";
$log_button = "Login Now";
$pass_button = "Send Password";
$lost	= "Lost Your Password?";
$lost_enter	= "Enter Your Username";
$bad_name = "Invalid Username";
$pass_sent = "Password Sent To Email";
$text_no_auth = "Invalid Username or Password";
?>