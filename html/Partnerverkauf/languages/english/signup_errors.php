<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$e1 = "The username you have entered is already in use.";
$e2 = "Your username needs to be at least 4 characters long.";
$e3 = "Your username can only be 12 characters long.";
$e4 = "Your password needs to be at least 4 characters long.";
$e5 = "Your password can only be 12 characters long.";
$e6 = "Your password needs to be at least 4 characters long.";
$e7 = "Your password can only be 12 characters long.";
$e8 = "Your passwords do not match.";
$e9 = "You need to enter who checks should be made to.";
$e10 = "You need to enter your Federal Tax ID or Social Security # - Non USA Residents use: International.";
$e11 = "You need to enter your first name.";
$e12 = "You need to enter your last name.";
$e13 = "You need to enter your email address.";
$e14 = "Your email address is invalid - be sure it's all lowercase.";
$e15 = "You need to enter your street address.";
$e16 = "You need to enter your City name.";
$e17 = "You need to enter your County/State.";
$e18 = "You need to enter your zip code.";
$e19 = "You need to enter your phone number.";
$e20 = "You need to enter your website URL.";
$e21 = "You chose to receive PayPal.com payments, you must enter your PayPal.com account.";
?>