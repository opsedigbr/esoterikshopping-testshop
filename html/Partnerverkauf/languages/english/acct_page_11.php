<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$box1_8 = "Using the provided linking code, you can adjust the <b>color scheme</b>, <b>height</b> and <b>width</b> of your Text Ad to easily integrate with your site!";
$box1_9 = "If you choose a specific product, your customer will be sent straight into the product page.";
$box1_1 = "Text Ad Linking Code";
$box1_2 = "Choose A Product Group";
$box1_4 = "Source Code - Copy/Paste Into Your Site";
$box1_5 = "Choose A Product Group Above";
$box1_6 = "No Group Selected";
$box1_7 = "Display Text Ads";
?>