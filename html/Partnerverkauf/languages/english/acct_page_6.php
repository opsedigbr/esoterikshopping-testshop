<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$box1_1 = "Send Your Affiliate Link To A Friend";
$box1_2 = "Name";
$box1_3 = "Email Address";
$box1_4 = "Friend";
$box1_5 = "required";
$box1_6 = "optional";
$box1_7 = "Subject";
$box1_8 = "Header";
$box1_9 = "Dear [Friend Name Auto-Inserted],";
$box1_10 = "Message Body";
$box1_11 = "Auto Insert";
$box1_12 = "Your Affiliate Text Link";
$box1_13 = "Signature";
$box1_14 = "Email Notes";
$box1_15 = "This message will be sent using your email address";
$box1_16 = "HTML Format Only: Your actual linking code with affiliate ID will not be displayed.";
$box1_17 = "HTML Format Only: Affiliate link displayed as";
$box1_18 = "Click Here To Visit";
$box1_19 = "Send In HTML Format";
$box1_20 = "Send In Plain Text Format";
$button = "Send Email";
$em1 = "Please Enter A Name For Friend";
$em2 = "Please Enter An Email Address For Friend";
$em3 = "Invalid Email Address For Friend";
$em4 = "Try Again Here";
$em5 = "Message Sent To";
$em6 = "Send Another Email Message";
?>