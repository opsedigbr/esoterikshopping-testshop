<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$box2_6 = "Your sales total and ratio only reflect directly generated sales<BR>and do not include 2nd tier or<BR>recurring sales.";
$box1_10 = "Recurring Sale Earnings";

$box1_1 = "Current Sales";
$box1_2 = "Transactions";
$box1_3 = "Standard Earnings";
$box1_4 = "Earnings To Date";
$box1_5 = "View Payment History";
$box1_6 = "includes bonus";
$box1_7 = "2nd Tier Earnings";
$box1_8 = "N/A";
$box1_9 = "Current Earnings";
$box2_1 = "Traffic Statistics";
$box2_2 = "Visitors";
$box2_3 = "Unique Visitors";
$box2_4 = "Total Sales";
$box2_5 = "Sales Ratio";
$box3_1 = "Payout Type";
$box3_2 = "Level";
$box3_3s = "of every sale you generate.";
$box3_3c = "for every unique click you generate.";
$box3_4 = "Current Payout Level";
$payt_1 = "Pay-Per-Sale";
$payt_2 = "Pay-Per-Click";
?>