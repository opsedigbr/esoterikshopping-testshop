<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$page_heading_4 = "Optional Payment - PayPal.com";
$paypal_1 = "Pay Me Via PayPal.com";
$paypal_2 = "PayPal.com Account";
$checkbx = "check box";
$pp_msg_1 = "Receiving A PayPal Payment From Us Is Optional";
$pp_msg_2 = "If you choose not to receive PayPal payments from us, we'll send you a paper check in the mail.";
$pp_msg_3 = "You can always add a PayPal account later if you don't have one yet!";
?>