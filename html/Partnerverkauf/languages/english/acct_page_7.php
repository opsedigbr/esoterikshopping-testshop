<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$tag_1 = "Edit Your Account";
$tag_2 = "We do not allow username or account ID changes.";
$tag_3 = "Change your password here";
$tag_4 = "Email Address";
$tag_5 = "Checks Payable To";
$tag_6 = "First Name";
$tag_7 = "Tax ID or SSN";
$tag_8 = "Last Name";
$tag_9 = "Phone #";
$tag_10 = "Street Address";
$tag_11 = "Fax #";
$tag_12 = "City";
$tag_13 = "Postal Code/Zip";
$tag_14 = "County/State";
$tag_15 = "Country";
$tag_16 = "Website URL";
$button = "Edit My Account";
?>