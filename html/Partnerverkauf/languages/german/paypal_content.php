<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
$page_heading_4 = "Wahlweise Paypal-Zahlung";
$paypal_1 = "Bitte zahlen Sie mich �ber Paypal aus";
$paypal_2 = "PayPal Konto";
$checkbx = "�berpr�fung Kasten";
$pp_msg_1 = "Sie k�nnen wahlweise PayPal Zahlung oder �berweisungen von uns erhalten.";
$pp_msg_2 = "Wenn Sie keine PayPal Zahlung w�nschen, senden Sie uns bitte Ihre Bankdaten f�r �berweisungen.";
$pp_msg_3 = "Sie k�nnen jederzeit Ihre Auszahlung auf PayPal umstellen.";
?>