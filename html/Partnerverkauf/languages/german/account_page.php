<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

$tlinks_1 = "Quelltext f�r Textlink (Copy/Paste)";
$menu_1 = "Allgemeine Statistik";
$menu_2 = "2. Level Statistik";
$menu_4 = "Banner Code";
$menu_11 = "Textlink Code";
$menu_12 = "Wiederkehrenden Umsatz";
$tb_60 = "Wiederkehrende Verk�ufeverdienste";

$tb_1 = "Konto-Status";
$tb_app = "genehmigte";
$tb_non = "W�hrend Zustimmung";
$tlinks_2 = "Kopieren Sie per Copy/Paste den Quelltext in Ihre Seite";
$tlinks_alt_1 = "Ihr Konto-Status ist z.Zt. in Schwebender Zustimmung";
$tlinks_alt_2 = "Sobald wir Ihr Konto genehmigt haben, k�nnen Sie sich im LOGIN Bereich den Quelltext kopieren.";
$tb_2 = "Anzahl aller K�ufe";
$tb_3 = "Einkommen";
$tb_4 = "2. Reihe-Einkommen";
$tb_5 = "Einkommen Bis jetzt";
$tb_inc = "schlie�t Pr�mie ein";
$menu_3 = "Zahlungsverhalten";
$menu_5 = "Quelltext f�r 2. Level Partner";
$menu_6 = "Schicken Sie Dem Freund";
$menu_7 = "Kontodaten �ndern";
$menu_8 = "Kennwort �ndern";
$menu_9 = "URL�s von anderen Partnern";
$menu_10 = "H�ufig gestellte Fragen";
$msmatch = "Kennw�rter Passen Nicht Zusammen";
$text_p = "Kennwort erfolgreich ge�ndert";
$un_1 = "Konto-Status-Alarm";
$un_2 = "Ihr Konto-Status Ist Z.Z. Schwebende Zustimmung";
$un_3 = "Sobald wir Ihr Konto genehmigt haben, k�nnen Sie sich �ber LOGIN einloggen.";
?>