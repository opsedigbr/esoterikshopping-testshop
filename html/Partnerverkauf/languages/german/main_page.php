<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
$par_heading_1 = "Willkommen bei";
$par_heading_2 = "Partnerprogramm!";
$par_heading_3 = "Wie Funktioniert es?";
$par_heading_4 = "Realzeitstatistiken und Berichte.";
$par1_line_1 = "Unser Programm ist einfach einzubinden und erfordert kein technisches Wissen.";
$par1_line_2 = "Partnerprogramme bieten Ihnen die M�glichkeit, mit Ihrer eigenen Webseite Geld zu verdienen.";
$par1_line_3s = "Wenn �ber Ihre Webseite Verk�ufe in unserem Online-Shop verzeichnet werden, bekommen Sie einen Prozentsatz der Verk�ufe.<br>
<br><b>*NEU*</b><br>Bauen Sie sich eine eigene Struktur an Partnern auf und verdienen Sie so zus�tzlich <b>50%</b> der Provisionsums�tze, die Ihr Partner macht!";
$par1_line_3c = "Teilnehmer erzeugen Verkehr f�r unseren Online-Shop und empfangen eine Proklick Verg�tung.";
$par1_line_3r = "Teilnehmer erzeugen Verk�ufe f�r unseren Online-Shop und bekommen eine Provision f�r alle verzeichneten Verk�ufe.";
$par2_line_1 = "Wenn Sie bei dem";
$par2_line_2 = "Partnerprogramm mitmachen, werden Sie mit Bannern und Textverlinkungen beliefert, die Sie auf Ihre Webseite einbinden.";
$par2_line_3 = "Wenn ein User auf unseren Link bei Ihnen klickt";
$par2_line_4 = "wird diese T�tigkeit durch unsere Partnerprogramm-Software aufgezeichnet.";
$par2_line_5s = "Wenn dieser User einen Kauf in unserem Online-Shop abschlie�t, wird Ihrem Konto eine Verkaufs-Provision gutgeschrieben. Diese Provision wird ausgezahlt, sobald der Kunde das Geld f�r die eingekauften Waren bei uns bezahlt hat.";
$par2_line_5c = "f�r jeden Klick von unterschiedlichen Usern wird Ihr Konto einen Bonus gutgeschrieben.";
$par3_line_1 = "Sie k�nnen sich jederzeit einloggen, um Ihre Verk�ufe, Verkehr, Kontostand zu �berpr�fen.<br><br><b>Extrem lange Cookie Laufzeit</b><br>In vielen Partnerprogrammen ist nur eine extrem kurze Laufzeit von Cookies gegeben. Damit werden Wiederholungskunden und Kunden, die sich nur den Link zum Shop speichern komplett ausgeschlossen.<br>Das ist bei uns nicht der Fall!<br>Unser Cookie ist auf den 31.12.2015 gesetzt, damit auch wirklich alle Kunden, die von Ihren Seiten kommen, provisioniert werden k�nnen.<br><br><b>Ausnahmen der Provisionierung</b><br>Wenn sich Kunden als Wiederverk�ufer (Reseller) bei uns anmelden und gewerblichen Handel treiben, k�nnen diese Verk�ufe leider nicht mehr provisioniert werden, da Reseller einen speziellen Rabatt bekommen, der zusammen mit einer eventuellen Partner-Verkaufsprovision gr�sser w�re, als unsere Gewinnspanne. <br>Hierf�r bitten wir f�r Verst�ndnis!<br><br>
<b>Bannererstellung</b><br>Wir haben eine grosse Liste an Banner- und Textanzeigen f�r Sie erstellt. Wenn Sie f�r ein bestimmtes Produkt werben m�chten, oder Sie einen Banner verwenden m�chten, das auf Ihre Webseite abgestimmt ist, setzen sie sich mit uns in Verbindung, wir erstellen gemeinsam mit Ihnen die f�r Sie passende Werbeform!<br><br>
<b>Auszahlung</b><br>Sobald Ihre Werbeeinnahmen den Betrag von 10,00 &euro; �bersteigen, �berweisen wir auf Wunsch Ihr Guthaben zum n�chsten 1. des Folgemonats, oder Sie geben uns bescheid, ab welcher Summe ausgezahlt werden soll. Sie bekommen von uns eine Abrechnung mit Provisions- und Steuernummer. Auch als Privatperson!<br><br>
<b>Fragen und Kontakt</b><br>Falls noch Fragen offen sein sollten, scheuen Sie sich nicht, uns zu kontaktieren! <br>Per email unter info@esoterikshopping.de oder telefonisch, Mo-Fr von 09-18 Uhr und Sa von 10-16 Uhr unter: 0911 - 89 17 950";
$text_0 = "Subvention-Art";
$text_1 = "Subvention-Menge";
$text_2 = "Ausgangsdesposit";
$text_3 = "Subvention-Schwelle";
$text_4 = "Subvention-Dauer";
$text_5 = "Teilnehmer-Programm-Details";
$text_and = "und";
$text_signup = "";
$line_click = "Zahlung pro Klick";
$line_sale = "Zahlung pro Verkauf";
$text_firsts = "f�r jeden Verkauf, den Sie erzeugen";
$text_firstc = "f�r jedes unterschiedliche Klicks, das Sie erzeugen";
$text_seconds = "von allen Verkaufsprovisionen Ihrer angeworbenen Partner.";
$text_secondc = "von allen unterschiedlichen Klicks erzeugt von den 2. Stufe Verk�ufern";
$text_init = "gerade f�r oben unterzeichnen!";
$text_bal = "Mindestbetrag f�r Auszahlung";
$text_mail_1 = "Zahlungen werden am";
$text_mail_2 = "von jedem Monat, f�r den vorhergehenden Monat ausgezahlt.";
$signup_1 = "Jetzt mitmachen!";
$signup_2 = "Und der Anfang ist getan, um heute noch Geld mit uns zu verdienen!";
?>
