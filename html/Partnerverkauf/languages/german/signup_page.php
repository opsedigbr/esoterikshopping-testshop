<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
$heading_1 = "Willkommen, Partner!";
$heading_2 = "Partner-Einkommen:";
$heading_3 = "Erwerben Sie Sogar Mehr!";
$heading_4 = "Konto-Aktivierung";
$page_heading_1 = "Erstellen Sie Ihr Konto";
$page_heading_2 = "Pers�nliche Informationen";
$p1 = "Machen Sie mit, bei unserem Partnerprogramm von Esoterikshopping.de! Setzen Sie einfach unsere Banner- oder Textlinks auf Ihre Seite und verdienen an den Kunden mit, die �ber Ihre Seite bei uns landen.";
$p2 = "Lehnen Sie sich zur�ck und sehen zu, wie Ihr Konto w�chst, w�hrend Ihre Besucher unsere Kunden werden! ";
$p3 = "erwerben Sie sogar mehr Geld, indem Sie Ihre 2. Reihe zu anderen webmasters f�rdern.";
$text_p1 = "Wir zahlen Ihnen";
$text_p2a = "f�r jeden";
$text_p2b = "Verkauf,";
$text_p2bc = "einzigartiges Klicken";
$text_p2c = "den Sie erzeugen!";
$text_p3 = "wir beginnen sogar Ihre Kontobalance weg mit";
$text_p4 = "gerade f�r oben unterzeichnen!";
$text_p5 = "Sie erwerben ein zus�tzliches";
$text_p6 = "von jedem Verkauf erzeugt von Ihren 2. Reiheteilnehmern!";
$app_no = "Ihr Konto wird in 24-48 Stunden genehmigt - Wir senden Ihnen dann eine Best�tigungsmail";
$app_yes_1 = "Ihr Konto ist automatisch anerkannt.";
$app_yes_2 = "Sie k�nnen Geld, mit uns in einigen Minuten zu erwerben beginnen!";
$text_pp = "Lassen Sie sich Ihre Provisionen bequem �ber PayPal auszahlen";
$text_notes_1 = "merkt";
$text_notes_2 = "Ihr username und Kennwort m�ssen 4-12 Buchstaben sein.";
$text_notes_3 = "Gewerbetreibende bitte die Steuer-Nummer eintragen!";
$text_user = "Benutzername";
$text_pass = "Kennwort";
$text_pass_2 = "Kennwort Wiederholen";
$text_payable = "Auszahlungsart(PayPal oder �berweisung)";
$text_tax = "Steuer-Nr (Privatpersonen=PRIVAT)";
$text_web = "Ihre URL";
$text_fname = "Vorname";
$text_lname = "Nachname";
$text_street = "Adresse";
$text_street_2 = "Adresse 2";
$text_city = "Stadt";
$text_state = "Bundesland";
$text_email = "Email address";
$text_phone = "Telefon";
$text_fax = "Fax";
$text_zip = "PLZ";
$text_country = "Land";
$text_button = "Erstellen Sie Mein Konto";
$submit_msg_1 = "Ihr Konto Ist erstellt worden!";
$submit_msg_2 = "wir haben Ihnen eine mail gesendet";
$submit_msg_3 = "mit Ihrem Benutzernamen und Kennwort.";
$submit_msg_4 = "Sie sollten dieses in einem sicheren Platz als zuk�nftiger Referenz halten.";
$submit_msg_5 = "Handhaben Sie Ihr Konto";
$submit_msg_6 = "Die Folgenden Fehler wurden ermittelt";
$submit_msg_7 = "in rot angezeigt";
?>