<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
$column_heading = "Konto Login";
$p1 = "tragen Sie Ihren Benutzernamen und Kennwort ein, um zu Ihren Kontostatistiken und Banner- und Textlink Code zu gelangen.";
$p2 = "Wenn Sie nicht an Ihr Kennwort erinnern können, senden Sie uns Ihren Benutzernamen und wir versenden Ihnen Ihr Passwort.";
$page_heading = "LOGIN zu Ihrem Konto";
$text_user = "Benutzername";
$text_pass = "Kennwort";
$log_button = "LOGIN Jetzt";
$pass_button = "Senden Sie mir mein Kennwort";
$lost = "Kennwort vergessen?";
$lost_enter = "Tragen Ihr Benutzernamen ein";
$bad_name = "Unzulässiges Benutzername!";
$pass_sent = "Kennwort per mail verschickt!";
$text_no_auth = "unzulässiger Benutzername oder Kennwort!";
?>