<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

print "<!-- IF YOU WANT TO ADJUST CONTENT ON THIS PAGE, USE THE LANGUAGE FILES - /languages/ -->";

$inibal = (number_format($initialbalance,2));
print "<b>$heading_1</b><BR>";
print "$p1&nbsp;&nbsp;$p2";
print "<BR><BR><b>$heading_2</b>";

if ($paytype == 1) {
$paydisplay = $text_p2b;
} elseif ($paytype == 2) {
$paydisplay = $text_p2bc;
} elseif ($paytype == 3) {
$paydisplay = $text_p2b; }

if ($paytype == 1) {
$paylvl = $paylvl * 100;
if ($paylevels > 1) { $prefix = $paymax * 100;
print "<BR>$text_p1 $paylvl% - $prefix% $text_p2a $paydisplay $text_p2c";
} else {
print "<BR>$text_p1 $paylvl% $text_p2a $paydisplay $text_p2c"; }
} elseif ($paytype == 2) {
print "<BR>$text_p1 $cur_sym$ppc $currency $text_p2a $paydisplay $text_p2c";
} elseif ($paytype == 3) {
print "<BR>$text_p1 $cur_sym$pps_fr $currency $text_p2a $paydisplay $text_p2c"; }


if ($initialbalance != 0) { print "&nbsp;&nbsp;$text_p3 $cur_sym$inibal $currency $text_p4"; }
if ($usepaypal == 1) { print "<BR><BR><b><center><font color=$red_text>$text_pp</font></center></b>"; }
if ($second_tier == 1) { print "<BR><b>$heading_3</b><BR>$p3&nbsp;&nbsp;$text_p5 $pay_total% $text_p6<BR>"; }

if ($account_approval == 1) { print "<BR><b>$heading_4</b><BR>$app_no"; } else {
print "<BR><b>$heading_4</b><BR>$app_yes_1&nbsp;&nbsp;$app_yes_2"; }
?>