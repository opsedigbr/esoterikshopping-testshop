<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
include "../config/config.php";
if (session_is_registered("valid_admin")) {
$link9 = "setup.php?action=9";
$link10 = "setup.php?action=10";
$link11 = "setup.php?action=11";
} else {
$link9 = "setup.php";
$link10 = "setup.php";
$link11 = "setup.php"; }
$scount = mysql_query("select record from idevaff_sales where approved = 0");
$scnt = mysql_num_rows($scount);
$acount = mysql_query("select id from idevaff_affiliates where approved = 1");
$acnt = mysql_num_rows($acount);
$aacount = mysql_query("select id from idevaff_affiliates where approved != 1");
$aacnt = mysql_num_rows($aacount);
$tcount = mysql_query("select id from idevaff_affiliates where tier > 0");
$tcnt = mysql_num_rows($tcount);
$appcount = mysql_query("select id from idevaff_sales where approved = 0");
$appcnt = mysql_num_rows($appcount);
if ($appcnt > 0) { $appfont = "#CC0000"; } else { $appfont = "#999999"; }
$reccount = mysql_query("select id from idevaff_recurring");
$reccnt = mysql_num_rows($reccount);
?>
<html>
<head>
<title>Partnerverkauf - Admin</title>
<link rel="STYLESHEET" type="text/css" href="../templates/template.css">
<style type="text/css"> 
<!-- 
td.back { background: #F0F4FF; }
td.over { background: #D4E2FD; }
--> 
</style>
</head>
<body>
<? include("help/help_script.txt"); ?>
<center><table border="0" cellpadding="2" cellspacing="0" width="750">
<td width="100%" background="../images/back.gif" height=20>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="20%" align=center><b><font color="#FFFFFF">Partnerverkauf Admin Center</font></b></td>
<td width="80%" align=right>
<b><font color="#FFFFFF">Admin Logged: <? if (session_is_registered("valid_admin")) { echo "$valid_admin - <a href=\"index.php?logout=1\"><font color=\"#DFE3EC\"><b>Logout Here</b></font></a>&nbsp;"; } else { echo "None&nbsp;"; } ?></font></b>
</td>
</tr>
</table>
</div>
</td></table><center>
<table border="0" cellspacing="0" width="750" class=outline_color>
<tr><td>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr></center><td width="20%" class=menu_cells valign="top" align="center">
<div align="center">
<table border="0" cellpadding="0" cellspacing="1" width="99%" class=menu_cells>
<tr><td width="100%" colspan="2" height="5" class=menu_cells_alt></td></tr>
<tr>
<td width="100%" colspan="2" class=menu_cells_back>&nbsp;<b>Manage Accounts</b></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_blue.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="account_list.php">View Accounts</a> </td>
        <td width="20%" align="center"><font color="#999999">(<?=$acnt?>)</font></td>
      </tr>
    </table>
    </center>
  </div>
 </td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_blue.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="approve_accts.php">Approve Accounts</a> </td>
        <td width="20%" align="center"><font color="#999999">(<?=$aacnt?>)</font></td>
      </tr>
    </table>
    </center>
  </div>
 </td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_blue.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="tier_list.php">View Tier Structure</a></td>
        <td width="20%" align="center"><font color="#999999">(<?=$tcnt?>)</font></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_blue.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="email.php">Email Affiliates</a> </td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
 </td>
</tr>

<tr>
<td width="100%" colspan="2" class=menu_cells_back>&nbsp;<b>Sales & Accounting</b></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="pay_list.php">Pay Affiliates</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>

<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="approve_sales.php">Approve Sales</a> </td>
        <td width="20%" align="center"><font color="<?=$appfont?>">(<?=$appcnt?>)</font></td>
      </tr>
    </table>
    </center>
  </div>
 </td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="recurring.php">Recurring Sales</a> </td>
        <td width="20%" align="center"><font color="#999999">(<?=$reccnt?>)</font></td>
      </tr>
    </table>
    </center>
  </div>
 </td>
</tr>
<tr>
<td width="100%" colspan="2" class=menu_cells_back>&nbsp;<b>Affiliate Marketing</b></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_white.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="groups.php">Product Groups</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_green.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="banner_add.php">Add A Banner</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_green.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="banner_edit.php">Edit Banners</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>





<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_green_alt.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="textad_add.php">Create A Text Ad</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>




<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_green_alt.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>
  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td width="80%">&nbsp;<a href="textad_edit.php">Edit A Text Ad</a></td>
        <td width="20%" align="center"></td>
      </tr>
    </table>
    </center>
  </div>
</td>
</tr>




<tr>
<td width="100%" colspan="2" class=menu_cells_back>&nbsp;<b>Stats & Reporting</b></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>&nbsp;<a href=report_traffic.php>Incoming Traffic Logs</a></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>&nbsp;<a href=report_sales.php>Current Sales</a></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>&nbsp;<a href=report_payouts.php>Accounting History</a></td>
</tr>
<tr>
<td width="100%" colspan="2" class=menu_cells_back>&nbsp;<b>Configuration & Settings</b></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>&nbsp;<a href=setup.php>Partnerverkauf Setup</a></td>
</tr>
<tr>
<td width="12%" align="center" class=menu_cells_back><img border="0" src="../images/bullet.gif" width="8" height="8"></td>
<td width="88%" class=menu_cells_back>&nbsp;<a href=setup.php?action=14>General Information</a></td>
</tr>
<tr><td width="100%" colspan="2" height="5" class=menu_cells_alt></td></tr>
</table>
</div>
</td><td width="75%" valign="top" class=white_back>
<div align="center">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td width="100%">
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="100%">
