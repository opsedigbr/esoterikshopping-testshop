<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back">
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr>
<td width="100%"><b>Pay-Per-Sale (Percentage Payouts)</b></td>
</tr>
<tr>
<td width="100%">If you're going to offer multiple payout levels, we
recommend <b>increasing</b> the payout amount for each level.&nbsp; This
allows you to reward your higher producing affiliates with increased
commissions.</td>
</tr>
<tr>
<td width="100%">&nbsp;</td>
</tr>
<tr>
<td width="100%"><b>Sample Payout Levels</b></td>
</tr>
<tr>
<td width="100%">Level 1: 25% (default level)</td>
</tr>
<tr>
<td width="100%">Level 2: 40%</td>
</tr>
<tr>
<td width="100%">Level 3: 50%</td>
</tr>
<tr>
<td width="100%">&nbsp;</td>
</tr>
<tr>
<td width="100%">The criteria you choose to increment an affiliate level
is completely up to you.&nbsp; We recommend using a scale based on
number of sales, total revenue, etc.&nbsp; These are just
ideas, it's up to you to decide when and why an affiliate gets an
increase.&nbsp; Multiple levels are also useful for partnership
agreements with other sites.&nbsp; Example: You offer one site 30% and
another site 35%.&nbsp; Just set each account accordingly.&nbsp;
Adjusting an affiliates payout level can be done by clicking <b>edit</b>
on the <b>view accounts</b> page.</td>
</tr>
</table>
</div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>