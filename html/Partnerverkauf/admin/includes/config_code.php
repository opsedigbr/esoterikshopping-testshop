<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
mysql_query("update idevaff_config set set_5 = '1'");
if ($paytype == 1) { $typetag = "Pay-Per-Sale (Percentage)"; }
if ($paytype == 3) { $typetag = "Pay-Per-Sale (Flat Rate)"; }
if ($paytype == 2) { $typetag = "Pay-Per-Click"; }
if ($use_return == 1) {
?>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<div align="center"><table border="0" cellspacing="0" width="95%" class="outline_color"><tr>
<td width="100%"><div align="center"><table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color"><b><font color="#FFFFFF">&nbsp;<?=$typetag?></font></b></td>
</tr><tr><td width="100%">&nbsp;</td></tr><tr><td width="100%">
<div align="center"><table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%"><b>How Processing Works</b></td>
</tr>
<tr><td width="100%">You're using the auto return feature.&nbsp; After a
    customer makes a purchase, they will be sent to a file called idevsale.php.&nbsp;
    The affiliate (if one exists) will be processed then your customer will
    automatically be sent to <font color=#CC0000><?=$idev_return?></font>.&nbsp; This URL is defined in your <a href="setup.php?action=1">Site Configuration</a>.&nbsp; Use the following code for your return URL in your merchant account or shopping cart.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Return URL Code</b></td>
</tr>
<tr><td width="100%"><textarea rows="2" name="code" cols="60"><?=$base_url?>/idevsale.php</textarea></td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr>
<td width="100%"><b>More Information On Processing Methods</b></td>
</tr>
<tr><td width="100%"><img border="0" src="../images/bullet_green_alt.gif" width="8" height="8">&nbsp;&nbsp;Consult the <font color=#CC0000>docs/README_manual.txt</font> file included in your distrubution.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
  </table></div></td></tr>
    </table></div></td></tr></table></div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>



<? } else {
if ($paytype == 2) { ?>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<div align="center"><table border="0" cellspacing="0" width="95%" class="outline_color"><tr>
<td width="100%"><div align="center"><table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color"><b><font color="#FFFFFF">&nbsp;<?=$typetag?></font></b></td>
</tr><tr><td width="100%">&nbsp;</td></tr><tr><td width="100%">
<div align="center"><table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%">You're using Pay-Per-Click payouts.&nbsp; You don't need to place any special code into your existing site pages.&nbsp; Partnerverkauf is completely self dependent for this payout style.</td>
</tr></table></div></td></tr><tr><td width="100%">&nbsp;</td></tr>
</table></div></td></tr></table></div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>

<? } elseif ($paytype == 3) { ?>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<div align="center"><table border="0" cellspacing="0" width="95%" class="outline_color"><tr>
<td width="100%"><div align="center"><table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color"><b><font color="#FFFFFF">&nbsp;<?=$typetag?></font></b></td>
</tr><tr><td width="100%">&nbsp;</td></tr><tr><td width="100%">
<div align="center"><table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%"><b>How Processing Works</b></td>
</tr>
<tr><td width="100%">You'll need to place some code into your sale confirmation page.&nbsp;
    This is the page where your customers return to after making a purchase.&nbsp;
    The very basic code is provided below. <a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This code will call an Partnerverkauf file called sale.php.&nbsp; If your visitor came in from an affiliate link, this file will credit the affiliate account with a sale.',WIDTH,'250',CAPTION,'Pay-Per-Sale Processing Code')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>&nbsp;
    Partnerverkauf will then be included with all your sales.&nbsp; If an
    affiliate exists for the customer, the affiliate will be credited
    appropriately.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Processing Code</b></td>
</tr>
<tr><td width="100%"><textarea rows="5" name="code" cols="60"><!-- Start Partnerverkauf Processing Code -->
&lt;? include("<?=$path?>/sale.php"); ?&gt;
<!-- End Partnerverkauf Processing Code --></textarea></td>
</tr>
<tr><td width="100%"><font color="#cc0000">This code needs placed into a page
    that has a .php extension.</font><br>
    Example: order_success.php</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Processing Code - JavaScript Version</b></td>
</tr>
<tr><td width="100%"><textarea rows="5" name="code" cols="60"><!-- Start Partnerverkauf Processing Code -->
<script language="JavaScript" type="text/javascript" src="path/to/Partnerverkauf/sale.php"></script>
<!-- End Partnerverkauf Processing Code --></textarea></td>
</tr>
<tr><td width="100%"><font color="#cc0000">Be sure to adjust the path as necessary.</font></td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Processing Code - Image Tag Version</b></td>
</tr>
<tr><td width="100%"><textarea rows="5" name="code" cols="60"><!-- Start Partnerverkauf Processing Code -->
<img src="path/to/Partnerverkauf/sale.php" width="1" height="1">
<!-- End Partnerverkauf Processing Code --></textarea></td>
</tr>
<tr><td width="100%"><font color="#cc0000">Be sure to adjust the path as necessary.</font></td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr>
<td width="100%"><b>More Information On Processing Methods</b></td>
</tr>
<tr><td width="100%"><img border="0" src="../images/bullet_green_alt.gif" width="8" height="8">&nbsp;&nbsp;Consult the <font color=#CC0000>docs/README_manual.txt</font> file included in your distrubution.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
</table></div></td></tr>
    </table></div></td></tr></table></div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>





<? } elseif ($paytype == 2) { ?>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<div align="center"><table border="0" cellspacing="0" width="90%" class="outline_color"><tr>
<td width="100%"><div align="center"><table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color"><b><font color="#FFFFFF">&nbsp;<?=$typetag?></font></b></td>
</tr><tr><td width="100%">&nbsp;</td></tr><tr><td width="100%">
<div align="center"><table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%">You're using Pay-Per-Click payouts.&nbsp; You don't need to place any special code into your existing site pages.&nbsp; Partnerverkauf is completely self dependent for this payout style.</td>
</tr></table></div></td></tr><tr><td width="100%">&nbsp;</td></tr>
</table></div></td></tr></table></div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>

<? } elseif ($paytype == 1) { ?>

<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<div align="center"><table border="0" cellspacing="0" width="95%" class="outline_color"><tr>
<td width="100%"><div align="center"><table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color"><b><font color="#FFFFFF">&nbsp;<?=$typetag?></font></b></td>
</tr><tr><td width="100%">&nbsp;</td></tr><tr><td width="100%">
<div align="center"><table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%"><b>How Processing Works</b></td>
</tr>
<tr><td width="100%">You'll need to place some code into your sale confirmation page.&nbsp;
    This is the page where your customers return to after making a purchase.&nbsp;
    The very basic code is provided below. <a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This code will call an Partnerverkauf file called sale.php.&nbsp; If your visitor came in from an affiliate link, this file will credit the affiliate account with a sale.',WIDTH,'250',CAPTION,'Pay-Per-Sale Processing Code')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>&nbsp;
    Partnerverkauf will then be included with all your sales.&nbsp; If an
    affiliate exists for the customer, the affiliate will be credited
    appropriately.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Percentage Processing Note</b></td>
</tr>
<tr><td width="100%">In order for Partnerverkauf to calculate the payout, you'll
    need to have the sale amount present on your sale confirmation page.&nbsp;
    This can come from your merchant account, shopping cart or any other custom
    setup as long as the variable you're using is the same name you set in your <a href="setup.php?action=4">Payout
    Settings</a>.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Custom Processing Setups <font color="#CC0000">(Important)</font></b></td>
</tr>
<tr><td width="100%">If you don't know the name of the variable your merchant
    account or shopping cart passes into your sale confirmation page, check
    their support forums as they are always listed.&nbsp;.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr><td width="100%"><b>Processing Code</b></td>
</tr>
<tr><td width="100%"><textarea rows="5" name="code" cols="60"><!-- Start Partnerverkauf Processing Code -->
&lt;? include("<?=$path?>/sale.php"); ?&gt;
<!-- End Partnerverkauf Processing Code --></textarea></td>
</tr>
<tr><td width="100%"><font color="#CC0000">This code needs placed into a page that has a .php extension.</font><BR>Example: order_success.php</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
<tr>
<td width="100%"><b>More Information On Processing Methods</b></td>
</tr>
<tr><td width="100%"><img border="0" src="../images/bullet_green_alt.gif" width="8" height="8">&nbsp;&nbsp;Consult the <font color=#CC0000>docs/README_manual.txt</font> file included in your distrubution.</td>
</tr>
<tr><td width="100%">&nbsp;</td>
</tr>
</table></div></td></tr>
    </table></div></td></tr></table></div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>


<? } } ?>