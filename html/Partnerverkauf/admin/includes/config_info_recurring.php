<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back">
<div align="center">
  <table border="0" cellpadding="0" cellspacing="0" width="95%">
    <tr>
      <td width="100%">
The recurring sales feature will take the current sale and duplicate it exactly
as it was originally created.&nbsp; You can add a current sale into the "<a href="recurring.php">recurring
sales</a>" list upon approving the sale.&nbsp; You define the amount of
time (in days) that you want the affiliate to be credited again (usually every
30 days).&nbsp; Partnerverkauf will credit the affiliate indefinitely until you
remove/delete the recurring sale entry.<BR><BR>
If the sale has a 2nd Tier affiliate attached to it, the 2nd Tier affiliate will
also get credit on the recurring rotation.&nbsp; When removing a recurring sale,
you can either terminate the crediting process immediately or allow the system
to credit the affiliate one more time, then terminate.&nbsp; This is good for
service based items vs. product based where you know the customer is terminating
their service agreement or membership, etc.<BR><BR>
<font color="#CC0000">
More information on recurring sales can be found in your readme.txt file.</font>
</td>
    </tr>
  </table>
</div>


</td></tr>
<tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>