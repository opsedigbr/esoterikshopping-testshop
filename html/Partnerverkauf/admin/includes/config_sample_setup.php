<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back">
<div align="center"><center><table border="0" cellspacing="0" width="95%">
<tr>
<td width="100%" align=center>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
<tr><td width="100%">
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="48%" valign="top">
<div align="center">
<table border="0" cellspacing="0" width="100%" cellpadding="0" class=outline_color>
<tr>
<td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
<td width="100%" class=outline_color colspan="3">
&nbsp;<b><font color="#FFFFFF">Pay-Per-Sale Example Configuration</font></b></td>
</tr>
<tr>
<td width="10%" align="center" class=cell_back_super_light>
<img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="70%" class=cell_back_super_light>&nbsp;Approve New
Sales</td>
<td width="20%" class=cell_back_super_light align="center">Yes</td>
</tr>
<tr>
<td width="10%" align="center" class=white_back>
<img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="70%" class=white_back>&nbsp;Enable 2nd Tier</td>
<td width="20%" class=white_back align="center">Yes</td>
</tr>
<tr>
<td width="10%" align="center" class=cell_back_super_light>
<img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="70%" class=cell_back_super_light>&nbsp;Notify Admin
of New Sale</td>
<td width="20%" class=cell_back_super_light align="center">Yes</td>
</tr>
<tr>
<td width="10%" align="center" class=white_back>
<img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="70%" class=white_back>&nbsp;Notify Affiliate of New
Sale</td>
<td width="20%" class=white_back align="center">Yes</td>
</tr>
</table>
</div></td>
</tr>
</table>
</div>

</td>
<td width="4%" valign="top"></td>
<td width="48%" valign="top">
<div align="center">
<table border="0" cellspacing="0" width="100%" cellpadding="0" class=outline_color>
<tr>
<td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
<td width="100%" class=outline_color colspan="3">
&nbsp;<b><font color="#FFFFFF">Pay-Per-Click Example Configuration</font></b></td>
</tr>
<tr>
<td width="10%" align="center" class=cell_back_super_light>
<img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="70%" class=cell_back_super_light>&nbsp;Approve New Sales</td>
<td width="20%" class=cell_back_super_light align="center">No</td>
</tr>
<tr>
<td width="10%" align="center" class=white_back>
<img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="70%" class=white_back>&nbsp;Enable 2nd Tier</td>
<td width="20%" class=white_back align="center">No</td>
</tr>
<tr>
<td width="10%" align="center" class=cell_back_super_light>
<img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="70%" class=cell_back_super_light>&nbsp;Notify Admin
of New Sale</td>
<td width="20%" class=cell_back_super_light align="center">No</td>
</tr>
<tr>
<td width="10%" align="center" class=white_back>
<img border="0" src="../images/bullet_purple.gif" width="8" height="8"></td>
<td width="70%" class=white_back>&nbsp;Notify Affiliate of New
Sale</td>
<td width="20%" class=white_back align="center">No</td>
</tr>
</table>
</div></td>
</tr>
</table>
</div>

</td>
</tr>
</table>
</center>
</div>
</table>
</td>
</tr></table></center></div>
</td></tr>
<tr><td width="100%" class="white_back">&nbsp;

</td></tr>
<tr><td width="100%" class="white_back">
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr>
<td width="100%"><font color=#CC0000><b>Pay-Per-Click Users Only</b></font></td>
</tr>
<tr>
<td width="100%">The reason we recommend features as being selected
&quot;no&quot; is because every time an affiliate sends in a new
visitor, these features will be activated.&nbsp; This can lead to a lot
of email sent to both the admin and affiliate as well as heavily
increased maintenance in your admin center (approving sales).&nbsp;
Enabling your 2nd Tier for Pay-Per-Click will pay your Top-Tier
affiliates only fractions of a penny because most Pay-Per-Click setups
only pay a few cents (maximum) to being with.</td>
</tr>
</table>
</div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>