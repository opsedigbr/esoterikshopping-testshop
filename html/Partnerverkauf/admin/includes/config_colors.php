<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center><table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr>
<td width="100%"><div align="center"><center>
<table border="0" cellpadding="0" cellspacing="1" width="95%">
<form method="POST" action="setup.php?action=9&cfg=4">

<tr><td width="100%" colspan="2" height="2" class="white_back"></td></tr><tr>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Page Background:</b></td>
<td width="25%"><input type="text" name="page_back" size="7" value="<?=$page_back?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$page_back?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the overall page background color.',WIDTH,'250',CAPTION,'Page Background')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div>
</td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Page Border:</b></td>
<td width="25%"><input type="text" name="page_border" size="7" value="<?=$page_border?>">
</td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$page_border?>"><tr><td></td></tr></table>
</td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('These are the lines on the left and right side of each page.They should probably match the header and footer color.',WIDTH,'250',CAPTION,'Page Border')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Header Background:</b></td>
<td width="25%"><input type="text" name="cp_head_back" size="7" value="<?=$cp_head_back?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$cp_head_back?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the colored bar with the text links at the top of each page.',WIDTH,'250',CAPTION,'Header Background')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Lightest Cells:</b></td>
<td width="25%"><input type="text" name="lighter_cells" size="7" value="<?=$lighter_cells?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$lighter_cells?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('These cells contain the lightest background color in your scheme.',WIDTH,'250',CAPTION,'Lightest Cells')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Footer Background:</b></td>
<td width="25%"><input type="text" name="cp_foot_back" size="7" value="<?=$cp_foot_back?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$cp_foot_back?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the colored bar with the text links at the bottom of each page.It should probably match your header background.',WIDTH,'250',CAPTION,'Footer Background')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Light Cells:</b></td>
<td width="25%"><input type="text" name="light_cells" size="7" value="<?=$light_cells?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$light_cells?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('These cells are just a bit darker than the lightest cells.This color should probably be of the same color family.',WIDTH,'250',CAPTION,'Light Cells')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Header/Footer Links:</b></td>
<td width="25%"><input type="text" name="top_bot_links" size="7" value="<?=$top_bot_links?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$top_bot_links?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of any links in the header or footer bar on each page.',WIDTH,'250',CAPTION,'Header/Footer Links')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Dark Cells:</b></td>
<td width="25%"><input type="text" name="mid_cells" size="7" value="<?=$mid_cells?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$mid_cells?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is just a bit darker than light and lighter cells.This color should probably be of the same color family.',WIDTH,'250',CAPTION,'Dark Cells')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Alt Head/Foot Links:</b></td>
<td width="25%"><input type="text" name="alt_links" size="7" value="<?=$alt_links?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$alt_links?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('These are the other links in the header and footer bars.Example: Username when logged, link to your site, etc.',WIDTH,'250',CAPTION,'Alternate Header/Footer Links')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Left Column:</b></td>
<td width="25%"><input type="text" name="left_column" size="7" value="<?=$left_column?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$left_column?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the background color for any pages with a left column.',WIDTH,'250',CAPTION,'Left Column Background Color')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Header/Footer Text:</b></td>
<td width="25%"><input type="text" name="top_bot_text" size="7" value="<?=$top_bot_text?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$top_bot_text?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of any normal text in the header or footer bars.',WIDTH,'250',CAPTION,'Header/Footer Text')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Logged Menu Cells:</b></td>
<td width="25%"><input type="text" name="logged_menu" size="7" value="<?=$logged_menu?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$logged_menu?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of each cell on the menu shown when the user is logged in.',WIDTH,'250',CAPTION,'Logged Menu Cells')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Page Links:</b></td>
<td width="25%"><input type="text" name="page_links" size="7" value="<?=$page_links?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$page_links?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of any normal links in a page.',WIDTH,'250',CAPTION,'Page Links')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</center>
</div>
</td>
<td width="50%" class=white_back>
<div align="center">
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Logged Menu Links:</b></td>
<td width="25%"><input type="text" name="menu_links" size="7" value="<?=$menu_links?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$menu_links?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of the links in the logged menu.',WIDTH,'250',CAPTION,'Logged Menu Links')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div>
</div>
</td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Red Text:</b></td>
<td width="25%"><input type="text" name="red_text" size="7" value="<?=$red_text?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$red_text?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Any red text will appear in this color.',WIDTH,'250',CAPTION,'Red Text')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Content Background:</b></td>
<td width="25%"><input type="text" name="white_back" size="7" value="<?=$white_back?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$white_back?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the background for the internal content on each page.',WIDTH,'250',CAPTION,'Content Background')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Section Head Text:</b></td>
<td width="25%"><input type="text" name="section_head_txt" size="7" value="<?=$section_head_txt?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$section_head_txt?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of the title for each informational box.',WIDTH,'250',CAPTION,'Section Header Text')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Dimmed Text:</b></td>
<td width="25%"><input type="text" name="dim_font" size="7" value="<?=$dim_font?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$dim_font?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This text only appears in two place and is activated when if you do not use the 2nd Tier option.',WIDTH,'250',CAPTION,'Dimmed Text')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="55%"><b>&nbsp;Message Tags:</b></td>
<td width="25%"><input type="text" name="tag_color" size="7" value="<?=$tag_color?>"></td>
<td width="10%"><table border="1" cellpadding="0" cellspacing="0" width="30" bordercolorlight="#000000" height="15" bordercolordark="#C0C0C0" bgcolor="<?=$tag_color?>"><tr><td></td></tr></table></td>
<td width="10%" align="center"><a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the color of the text displayed when someone updates their password.',WIDTH,'250',CAPTION,'Message Tags')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a></td>
</tr>
</table>
</div></td>
<td width="50%" class=white_back>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td width="100%" colspan=3 align=center>Admin Colors Can Be Changed In: <b>/template.css/</b></td>
</tr>
</table>
</div></td>
</tr><tr><td width="100%" height="2" colspan="2" class="white_back"></td></tr>

<tr><td width="100%" colspan="2" class="white_back" align="center"><BR>
<div align="center"><center><table border="0" cellpadding="2" cellspacing="0" width="80%">
<tr><td align="right" width="40%"><input type="checkbox" name="original" value="1">
</td><td width="60%">Restore Default Settings</td></tr></table></center></div>
<BR><input type="submit" value="Update Control Panel Color Scheme"><BR><BR></td></form></tr>

</table></center></div></td></tr><tr><td width="100%">
<font size="1">&nbsp;</font></td></tr></table></center></div>