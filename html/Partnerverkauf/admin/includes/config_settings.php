<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr>
<td width="100%">
<font size="1">&nbsp;</font>
<tr>
<td width="100%">
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="1" width="95%">
<form method="POST" action="setup.php?action=1&cfg=1">
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>&nbsp;Name of Company/Website:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="sitename" size="45" value="<?=$sitename?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Enter your Company or Website name.This name will be placed throughout the affiliate control panel.',WIDTH,'250',CAPTION,'Name of Company or Website')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>&nbsp;Website URL:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="siteurl" size="45" value="<?=$siteurl?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the URL to your main Website.Include the trailing slash (/).',WIDTH,'250',CAPTION,'Website URL')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>&nbsp;Partnerverkauf Installation URL:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="base_url" size="45" value="<?=$base_url?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is URL to your affiliate installation.Do not include the trailing slash (/).',WIDTH,'250',CAPTION,'iDevAfffiliate Installation URL')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Incoming Traffic Page:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="full_path" size="45" value="<?=$full_path?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('The is the URL incoming affiliate traffic will come into.Be sure to include the actual page name with extension, not just a root or sub directory.',WIDTH,'250',CAPTION,'Incoming Traffic Page')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Cookie URL:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="cookie_url" size="45" value="<?=$cookie_url?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Be sure to include the dot (.) before the domain name.This will ensure the cookie is regardless the domain/sub domain.Do not include the prefix (http://www).',WIDTH,'250',CAPTION,'Cookie URL')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Administrative Email:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="admin_email" size="45" value="<?=$admin_email?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the email address you will receive all affiliate related email at including email generated automatically by Partnerverkauf.',WIDTH,'250',CAPTION,'Administrative Email')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Main Logo/Image:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="main_logo" size="45" value="<?=$main_logo?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the logo/image that is displayed at the top of you Partnerverkauf control panel.',WIDTH,'250',CAPTION,'Main Logo/Image')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Control Panel Language:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="language">
<option value="1" <? if ($lang == 1) { print "selected"; } ?>>English</option>
<option value="3" <? if ($lang == 3) { print "selected"; } ?>>Spanish</option>
<option value="2" <? if ($lang == 2) { print "selected"; } ?>>German</option>
<option value="4" <? if ($lang == 4) { print "selected"; } ?>>French</option>
<option value="5" <? if ($lang == 5) { print "selected"; } ?>>Italian</option>
<option value="6" <? if ($lang == 6) { print "selected"; } ?>>Dutch
(Netherlands)</option>
<option value="7" <? if ($lang == 7) { print "selected"; } ?>>Portuguese</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Select a language you would like your control panel to be in.',WIDTH,'250',CAPTION,'Control Panel Language')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Currency:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="currency">
<option value="ARS" <?if($currency == "ARS"){print "selected";}?>>ARS</option>
<option value="AUD" <?if($currency == "AUD"){print "selected";}?>>AUD</option>
<option value="BRL" <?if($currency == "BRL"){print "selected";}?>>BRL</option>
<option value="BSD" <?if($currency == "BSD"){print "selected";}?>>BSD</option>
<option value="CAD" <?if($currency == "CAD"){print "selected";}?>>CAD</option>
<option value="CHF" <?if($currency == "CHF"){print "selected";}?>>CHF</option>
<option value="CLP" <?if($currency == "CLP"){print "selected";}?>>CLP</option>
<option value="CNY" <?if($currency == "CNY"){print "selected";}?>>CNY</option>
<option value="COP" <?if($currency == "COP"){print "selected";}?>>COP</option>
<option value="CZK" <?if($currency == "CZK"){print "selected";}?>>CZK</option>
<option value="DKK" <?if($currency == "DKK"){print "selected";}?>>DKK</option>
<option value="EUR" <?if($currency == "EUR"){print "selected";}?>>EUR</option>
<option value="FJD" <?if($currency == "FJD"){print "selected";}?>>FJD</option>
<option value="GBP" <?if($currency == "GBP"){print "selected";}?>>GBP</option>
<option value="GHC" <?if($currency == "GHC"){print "selected";}?>>GHC</option>
<option value="HKD" <?if($currency == "HKD"){print "selected";}?>>HKD</option>
<option value="HNL" <?if($currency == "HNL"){print "selected";}?>>HNL</option>
<option value="HUF" <?if($currency == "HUF"){print "selected";}?>>HUF</option>
<option value="IDR" <?if($currency == "IDR"){print "selected";}?>>IDR</option>
<option value="ILS" <?if($currency == "ILS"){print "selected";}?>>ILS</option>
<option value="INR" <?if($currency == "INR"){print "selected";}?>>INR</option>
<option value="ISK" <?if($currency == "ISK"){print "selected";}?>>ISK</option>
<option value="JPY" <?if($currency == "JPY"){print "selected";}?>>JPY</option>
<option value="KRW" <?if($currency == "KRW"){print "selected";}?>>KRW</option>
<option value="LKR" <?if($currency == "LKR"){print "selected";}?>>LKR</option>
<option value="MAD" <?if($currency == "MAD"){print "selected";}?>>MAD</option>
<option value="MXN" <?if($currency == "MXN"){print "selected";}?>>MXN</option>
<option value="MYR" <?if($currency == "MYR"){print "selected";}?>>MYR</option>
<option value="NOK" <?if($currency == "NOK"){print "selected";}?>>NOK</option>
<option value="NZD" <?if($currency == "NZD"){print "selected";}?>>NZD</option>
<option value="PAB" <?if($currency == "PAB"){print "selected";}?>>PAB</option>
<option value="PEN" <?if($currency == "PEN"){print "selected";}?>>PEN</option>
<option value="PHP" <?if($currency == "PHP"){print "selected";}?>>PHP</option>
<option value="PKR" <?if($currency == "PKR"){print "selected";}?>>PKR</option>
<option value="PLN" <?if($currency == "PLN"){print "selected";}?>>PLN</option>
<option value="RUR" <?if($currency == "RUR"){print "selected";}?>>RUR</option>
<option value="SEK" <?if($currency == "SEK"){print "selected";}?>>SEK</option>
<option value="SGD" <?if($currency == "SGD"){print "selected";}?>>SGD</option>
<option value="SIT" <?if($currency == "SIT"){print "selected";}?>>SIT</option>
<option value="SKK" <?if($currency == "SKK"){print "selected";}?>>SKK</option>
<option value="THB" <?if($currency == "THB"){print "selected";}?>>THB</option>
<option value="TND" <?if($currency == "TND"){print "selected";}?>>TND</option>
<option value="TRL" <?if($currency == "TRL"){print "selected";}?>>TRL</option>
<option value="TTD" <?if($currency == "TTD"){print "selected";}?>>TTD</option>
<option value="TWD" <?if($currency == "TWD"){print "selected";}?>>TWD</option>
<option value="USD" <?if($currency == "USD"){print "selected";}?>>USD</option>
<option value="VEB" <?if($currency == "VEB"){print "selected";}?>>VEB</option>
<option value="XAF" <?if($currency == "XAF"){print "selected";}?>>XAF</option>
<option value="XCD" <?if($currency == "XCD"){print "selected";}?>>XCD</option>
<option value="XDR" <?if($currency == "XDR"){print "selected";}?>>XDR</option>
<option value="XPF" <?if($currency == "XPF"){print "selected";}?>>XPF</option>
<option value="ZAR" <?if($currency == "ZAR"){print "selected";}?>>ZAR</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Select the currency you will be using for your affiliate program.',WIDTH,'250',CAPTION,'Default Currency Setting')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Currency Symbol:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="cur_sym" size="2" value="<?=$cur_sym?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Select the currency symbol you would like displayed next to all money related information.Examples: $, �, �, etc.',WIDTH,'250',CAPTION,'Currency Symbol')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Max. Banner Upload Size:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="max_size">
<option value="10000" <? if ($max_size == 10000) { ?> selected <? }
?>>10K</option>
<option value="25000" <? if ($max_size == 25000) { ?> selected <? }
?>>25K</option>
<option value="50000" <? if ($max_size == 50000) { ?> selected <? }
?>>50K</option>
<option value="100000" <? if ($max_size == 100000) { ?> selected <? }
?>>100K</option>
<option value="1000000" <? if ($max_size == 1000000) { ?> selected <? }
?>>Unlimited</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the largest banner size you can upload in your Partnerverkauf administration center.',WIDTH,'250',CAPTION,'Maximum Banner Upload Size')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Approve New Accounts?&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="account_approval" style="width=60;">
<option value="1" <? if ($account_approval == 1) { ?> selected <? }
?>>Yes</option>
<option value="0" <? if ($account_approval == 0) { ?> selected <? }
?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('By setting this option to NO, new accounts will automatically be approved and affiliate can get their linking code immediately.Setting this option to YES will require that you login and manually approve the account.At the time of approval, you have the option of notifying the affiliate of your decision via email.',WIDTH,'250',CAPTION,'New Account Approvals')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Initial Account Balance:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="initialbalance" size="10" value="<?=$initialbalance?>" style="width=60;">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Setting an initial account balance creates incentive for webmasters join your program.  Set to 0 if you don\'t want to offer an initial incentive balance.',WIDTH,'250',CAPTION,'Initial Account Balance')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Allow Affiliate Browsing?&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="browse" style="width=60;">
<option value="1" <? if ($browse == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($browse == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This option will create a list of other affiliates in the affilite control panel and is primarily used to create a reciprocal link for your affiliates.  This is completely optional.',WIDTH,'250',CAPTION,'Affiliate Browsing')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Delay Minutes:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="delay_minutes" size="2" value="<?=$delay_minutes?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the amount of time that must pass in order for the same affiliate to be credited with a second sale from the same customer.  This prevents immediate page refreshes (duplicate sales entries).  This only applies to Pay-Per-Sale.  Pay-Per-Click automatically records only UNIQUE hits by default.',WIDTH,'250',CAPTION,'Delay Minutes')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Require SSN/Tax ID?&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<select size="1" name="ssn_req" style="width=60;">
<option value="1" <? if ($ssn_req == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($ssn_req == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Setting this feature to YES will force the affiliate to enter their SSN or TAX ID on the affiliate signup form.',WIDTH,'250',CAPTION,'SSN/Tax ID Form Field Requirement')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="100%" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="100%" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="100%" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Use Auto Return:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="checkbox" name="use_return" value=1 <? if ($use_return == 1) {print "checked"; } ?>> (optional)
</td>
</tr>
<tr>
<td width="30%" align="right" class=white_back><b>Auto Return URL:&nbsp;&nbsp;</b></td>
<td width="70%" class=white_back>&nbsp;<input type="text" name="idev_return" size="45" value="<?=$idev_return?>">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is an optional setting and should only be used if you have a <b>return url</b> setup on your merchant account.',WIDTH,'250',CAPTION,'Auto Return Setup')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="30%" align="right" class=white_back></td>
<td width="70%" class=white_back>&nbsp;See <font color="#CC0000">docs/README_manual.txt</font>
  for more information.
</td>
</tr>
<tr>
<td width="100%" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="100%" class=white_back colspan="2" height="2"></td>
</tr>
<tr>
<td width="100%" height="2" class=white_back colspan="2"></td>
</tr>
<tr>
<td width="30%" align="right" class=white_back></td>
<td width="70%" class=white_back>&nbsp;<input type="submit" value="Update Configuration"></td></form>
</tr>
<tr>
<td width="100%" align="right" height="2" class=white_back colspan="2"></td>
</tr>
</table>
</center>
</div>
<tr>
<td width="100%">
<font size="1">&nbsp;</font>
</table>
