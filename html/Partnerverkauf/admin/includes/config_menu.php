<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back">
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr>
<td width="100%" colspan="2">The following pages show basic configuration samples and general information about some of the advanced features Partnerverkauf offers.</td>
</tr>
<tr>
<td width="100%" colspan="2" height="20"></td>
</tr>
<tr>
<td width="2%"><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="98%">&nbsp;<a href="setup.php?action=12">How Product Groups Work</a></td>
</tr>
<tr>
<td width="2%"><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="98%">&nbsp;<a href="setup.php?action=13">How Recurring Sales Work</a></td>
</tr>
<tr>
<td width="2%"><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="98%">&nbsp;<a href="setup.php?action=8">Percentage Payout Levels</a></td>
</tr>
<tr>
<td width="2%"><img border="0" src="../images/bullet_red.gif" width="8" height="8"></td>
<td width="98%">&nbsp;<a href="setup.php?action=7">Sample Payout Configurations</a></td>
</tr>
<tr>
<td width="100%" colspan="2" height="20"></td>
</tr>
</table>
</div>
</td></tr><tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>