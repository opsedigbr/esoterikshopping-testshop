<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr><tr>
<td width="100%">
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="1" width="95%">
<form method="POST" action="setup.php?action=3&cfg=2">
<tr>
<td width="100%" align="right" colspan="3" height="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class=white_back><b>Payout Style:&nbsp;&nbsp;</b></td>
<td width="60%" class=white_back>&nbsp;<select size="1" name="paystyle" style="width=190;">
<option value=1 <? if ($paytype == 1) { ?> selected <? } ?>>Pay-Per-Sale (Percentage)</option>
<option value=3 <? if ($paytype == 3) { ?> selected <? } ?>>Pay-Per-Sale (Flat Rate)</option>
<option value=2 <? if ($paytype == 2) { ?> selected <? } ?>>Pay-Per-Click</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('The Pay-Per-Sale option will only record an affiliate sale when someone actually purchases something from your site.The Pay-Per-Click option will record a sale for the affiliate everytime they send in a new (unique) visitor.',WIDTH,'250',CAPTION,'Select A Payout Style')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Approve New Sales?</b><b>&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="sale_approval" style="width=60;">
<option value="1" <? if ($sale_approval == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($sale_approval == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('By setting this option to NO, sales will automatically be approved.This is not advised.We recommend manually approving each sale to ensure they are valid.You might even implement a time-line to approve in case orders are rejected, returned or charged back.',WIDTH,'250',CAPTION,'New Sale Approval')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Pay Day (day of the month):&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="payday" style="width=60;">
<option value="1st" <? if ($payday == "1st") { ?> selected <? } ?>>1st</option>
<option value="2nd" <? if ($payday == "2nd") { ?> selected <? } ?>>2nd</option>
<option value="3rd" <? if ($payday == "3rd") { ?> selected <? } ?>>3rd</option>
<option value="4th" <? if ($payday == "4th") { ?> selected <? } ?>>4th</option>
<option value="5th" <? if ($payday == "5th") { ?> selected <? } ?>>5th</option>
<option value="6th" <? if ($payday == "6th") { ?> selected <? } ?>>6th</option>
<option value="7th" <? if ($payday == "7th") { ?> selected <? } ?>>7th</option>
<option value="8th" <? if ($payday == "8th") { ?> selected <? } ?>>8th</option>
<option value="9th" <? if ($payday == "9th") { ?> selected <? } ?>>9th</option>
<option value="10th" <? if ($payday == "10th") { ?> selected <? }?>>10th</option>
<option value="11th" <? if ($payday == "11th") { ?> selected <? }?>>11th</option>
<option value="12th" <? if ($payday == "12th") { ?> selected <? }?>>12th</option>
<option value="13th" <? if ($payday == "13th") { ?> selected <? }?>>13th</option>
<option value="14th" <? if ($payday == "14th") { ?> selected <? }?>>14th</option>
<option value="15th" <? if ($payday == "15th") { ?> selected <? }?>>15th</option>
<option value="16th" <? if ($payday == "16th") { ?> selected <? }?>>16th</option>
<option value="17th" <? if ($payday == "17th") { ?> selected <? }?>>17th</option>
<option value="18th" <? if ($payday == "18th") { ?> selected <? }?>>18th</option>
<option value="19th" <? if ($payday == "19th") { ?> selected <? }?>>19th</option>
<option value="20th" <? if ($payday == "20th") { ?> selected <? }?>>20th</option>
<option value="21st" <? if ($payday == "21st") { ?> selected <? }?>>21st</option>
<option value="22nd" <? if ($payday == "22nd") { ?> selected <? }?>>22nd</option>
<option value="23rd" <? if ($payday == "23rd") { ?> selected <? }?>>23rd</option>
<option value="24th" <? if ($payday == "24th") { ?> selected <? }?>>24th</option>
<option value="25th" <? if ($payday == "25th") { ?> selected <? }?>>25th</option>
<option value="26th" <? if ($payday == "26th") { ?> selected <? }?>>26th</option>
<option value="27th" <? if ($payday == "27th") { ?> selected <? }?>>27th</option>
<option value="28th" <? if ($payday == "28th") { ?> selected <? }?>>28th</option>
<option value="29th" <? if ($payday == "29th") { ?> selected <? }?>>29th</option>
<option value="30th" <? if ($payday == "30th") { ?> selected <? }?>>30th</option>
<option value="31st" <? if ($payday == "31st") { ?> selected <? }?>>31st</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the day of each month you will pay your affiliates.',WIDTH,'250',CAPTION,'Pay Day')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>

<tr>
<td width="40%" align="right" class="white_back">
<b>Enable PayPal.com Payments?&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="usepaypal" style="width=60;">
<option value="1" <? if ($usepaypal == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($usepaypal == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('Setting this option to YES will allow your affiliates to enter their PayPal account and enable you to pay them via PayPal each month.',WIDTH,'250',CAPTION,'Enable PayPal Payments')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>

<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Payout Balance Required:&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<input type="text" name="balance" size="5" value="<?=$balance?>" style="width=60;">
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the minimum balance the affiliate account must have in order to be paid.If the account does not meet your minimum balance requirement, the balance will carry over from month-to-month until it does.',WIDTH,'250',CAPTION,'Payout Balance Required')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Enable 2nd Tier Sales?</b><b>&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="second_tier" style="width=60;">
<option value="1" <? if ($second_tier == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($second_tier == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This option will allow your affiliates to promote your affiliate program to other affiliates and earn a percentage their commissions as well.',WIDTH,'250',CAPTION,'Enable Your Second Tier')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Tier Payout Percentage:&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="tier_payout" style="width=60;">
<option value=1 <? if ($tier_payout == 1) { ?> selected <? } ?>>1%</option>
<option value=2 <? if ($tier_payout == 2) { ?> selected <? } ?>>2%</option>
<option value=3 <? if ($tier_payout == 3) { ?> selected <? } ?>>3%</option>
<option value=4 <? if ($tier_payout == 4) { ?> selected <? } ?>>4%</option>
<option value=5 <? if ($tier_payout == 5) { ?> selected <? } ?>>5%</option>
<option value=6 <? if ($tier_payout == 6) { ?> selected <? } ?>>6%</option>
<option value=7 <? if ($tier_payout == 7) { ?> selected <? } ?>>7%</option>
<option value=8 <? if ($tier_payout == 8) { ?> selected <? } ?>>8%</option>
<option value=9 <? if ($tier_payout == 9) { ?> selected <? } ?>>9%</option>
<option value=10 <? if ($tier_payout == 10) { ?> selected <? } ?>>10%</option>
<option value=11 <? if ($tier_payout == 11) { ?> selected <? } ?>>11%</option>
<option value=12 <? if ($tier_payout == 12) { ?> selected <? } ?>>12%</option>
<option value=13 <? if ($tier_payout == 13) { ?> selected <? } ?>>13%</option>
<option value=14 <? if ($tier_payout == 14) { ?> selected <? } ?>>14%</option>
<option value=15 <? if ($tier_payout == 15) { ?> selected <? } ?>>15%</option>
<option value=16 <? if ($tier_payout == 16) { ?> selected <? } ?>>16%</option>
<option value=17 <? if ($tier_payout == 17) { ?> selected <? } ?>>17%</option>
<option value=18 <? if ($tier_payout == 18) { ?> selected <? } ?>>18%</option>
<option value=19 <? if ($tier_payout == 19) { ?> selected <? } ?>>19%</option>
<option value=20 <? if ($tier_payout == 20) { ?> selected <? } ?>>20%</option>
<option value=21 <? if ($tier_payout == 21) { ?> selected <? } ?>>21%</option>
<option value=22 <? if ($tier_payout == 22) { ?> selected <? } ?>>22%</option>
<option value=23 <? if ($tier_payout == 23) { ?> selected <? } ?>>23%</option>
<option value=24 <? if ($tier_payout == 24) { ?> selected <? } ?>>24%</option>
<option value=25 <? if ($tier_payout == 25) { ?> selected <? } ?>>25%</option>
<option value=26 <? if ($tier_payout == 26) { ?> selected <? } ?>>26%</option>
<option value=27 <? if ($tier_payout == 27) { ?> selected <? } ?>>27%</option>
<option value=28 <? if ($tier_payout == 28) { ?> selected <? } ?>>28%</option>
<option value=29 <? if ($tier_payout == 29) { ?> selected <? } ?>>29%</option>
<option value=30 <? if ($tier_payout == 30) { ?> selected <? } ?>>30%</option>
<option value=31 <? if ($tier_payout == 31) { ?> selected <? } ?>>31%</option>
<option value=32 <? if ($tier_payout == 32) { ?> selected <? } ?>>32%</option>
<option value=33 <? if ($tier_payout == 33) { ?> selected <? } ?>>33%</option>
<option value=34 <? if ($tier_payout == 34) { ?> selected <? } ?>>34%</option>
<option value=35 <? if ($tier_payout == 35) { ?> selected <? } ?>>35%</option>
<option value=36 <? if ($tier_payout == 36) { ?> selected <? } ?>>36%</option>
<option value=37 <? if ($tier_payout == 37) { ?> selected <? } ?>>37%</option>
<option value=38 <? if ($tier_payout == 38) { ?> selected <? } ?>>38%</option>
<option value=39 <? if ($tier_payout == 39) { ?> selected <? } ?>>39%</option>
<option value=40 <? if ($tier_payout == 40) { ?> selected <? } ?>>40%</option>
<option value=41 <? if ($tier_payout == 41) { ?> selected <? } ?>>41%</option>
<option value=42 <? if ($tier_payout == 42) { ?> selected <? } ?>>42%</option>
<option value=43 <? if ($tier_payout == 43) { ?> selected <? } ?>>43%</option>
<option value=44 <? if ($tier_payout == 44) { ?> selected <? } ?>>44%</option>
<option value=45 <? if ($tier_payout == 45) { ?> selected <? } ?>>45%</option>
<option value=46 <? if ($tier_payout == 46) { ?> selected <? } ?>>46%</option>
<option value=47 <? if ($tier_payout == 47) { ?> selected <? } ?>>47%</option>
<option value=48 <? if ($tier_payout == 48) { ?> selected <? } ?>>48%</option>
<option value=49 <? if ($tier_payout == 49) { ?> selected <? } ?>>49%</option>
<option value=50 <? if ($tier_payout == 50) { ?> selected <? } ?>>50%</option>
<option value=51 <? if ($tier_payout == 51) { ?> selected <? } ?>>51%</option>
<option value=52 <? if ($tier_payout == 52) { ?> selected <? } ?>>52%</option>
<option value=53 <? if ($tier_payout == 53) { ?> selected <? } ?>>53%</option>
<option value=54 <? if ($tier_payout == 54) { ?> selected <? } ?>>54%</option>
<option value=55 <? if ($tier_payout == 55) { ?> selected <? } ?>>55%</option>
<option value=56 <? if ($tier_payout == 56) { ?> selected <? } ?>>56%</option>
<option value=57 <? if ($tier_payout == 57) { ?> selected <? } ?>>57%</option>
<option value=58 <? if ($tier_payout == 58) { ?> selected <? } ?>>58%</option>
<option value=59 <? if ($tier_payout == 59) { ?> selected <? } ?>>59%</option>
<option value=60 <? if ($tier_payout == 60) { ?> selected <? } ?>>60%</option>
<option value=61 <? if ($tier_payout == 61) { ?> selected <? } ?>>61%</option>
<option value=62 <? if ($tier_payout == 62) { ?> selected <? } ?>>62%</option>
<option value=63 <? if ($tier_payout == 63) { ?> selected <? } ?>>63%</option>
<option value=64 <? if ($tier_payout == 64) { ?> selected <? } ?>>64%</option>
<option value=65 <? if ($tier_payout == 65) { ?> selected <? } ?>>65%</option>
<option value=66 <? if ($tier_payout == 66) { ?> selected <? } ?>>66%</option>
<option value=67 <? if ($tier_payout == 67) { ?> selected <? } ?>>67%</option>
<option value=68 <? if ($tier_payout == 68) { ?> selected <? } ?>>68%</option>
<option value=69 <? if ($tier_payout == 69) { ?> selected <? } ?>>69%</option>
<option value=70 <? if ($tier_payout == 70) { ?> selected <? } ?>>70%</option>
<option value=71 <? if ($tier_payout == 71) { ?> selected <? } ?>>71%</option>
<option value=72 <? if ($tier_payout == 72) { ?> selected <? } ?>>72%</option>
<option value=73 <? if ($tier_payout == 73) { ?> selected <? } ?>>73%</option>
<option value=74 <? if ($tier_payout == 74) { ?> selected <? } ?>>74%</option>
<option value=75 <? if ($tier_payout == 75) { ?> selected <? } ?>>75%</option>
<option value=76 <? if ($tier_payout == 76) { ?> selected <? } ?>>76%</option>
<option value=77 <? if ($tier_payout == 77) { ?> selected <? } ?>>77%</option>
<option value=78 <? if ($tier_payout == 78) { ?> selected <? } ?>>78%</option>
<option value=79 <? if ($tier_payout == 79) { ?> selected <? } ?>>79%</option>
<option value=80 <? if ($tier_payout == 80) { ?> selected <? } ?>>80%</option>
<option value=81 <? if ($tier_payout == 81) { ?> selected <? } ?>>81%</option>
<option value=82 <? if ($tier_payout == 82) { ?> selected <? } ?>>82%</option>
<option value=83 <? if ($tier_payout == 83) { ?> selected <? } ?>>83%</option>
<option value=84 <? if ($tier_payout == 84) { ?> selected <? } ?>>84%</option>
<option value=85 <? if ($tier_payout == 85) { ?> selected <? } ?>>85%</option>
<option value=86 <? if ($tier_payout == 86) { ?> selected <? } ?>>86%</option>
<option value=87 <? if ($tier_payout == 87) { ?> selected <? } ?>>87%</option>
<option value=88 <? if ($tier_payout == 88) { ?> selected <? } ?>>88%</option>
<option value=89 <? if ($tier_payout == 89) { ?> selected <? } ?>>89%</option>
<option value=90 <? if ($tier_payout == 90) { ?> selected <? } ?>>90%</option>
<option value=91 <? if ($tier_payout == 91) { ?> selected <? } ?>>91%</option>
<option value=92 <? if ($tier_payout == 92) { ?> selected <? } ?>>92%</option>
<option value=93 <? if ($tier_payout == 93) { ?> selected <? } ?>>93%</option>
<option value=94 <? if ($tier_payout == 94) { ?> selected <? } ?>>94%</option>
<option value=95 <? if ($tier_payout == 95) { ?> selected <? } ?>>95%</option>
<option value=96 <? if ($tier_payout == 96) { ?> selected <? } ?>>96%</option>
<option value=97 <? if ($tier_payout == 97) { ?> selected <? } ?>>97%</option>
<option value=98 <? if ($tier_payout == 98) { ?> selected <? } ?>>98%</option>
<option value=99 <? if ($tier_payout == 99) { ?> selected <? } ?>>99%</option>
<option value=100 <? if ($tier_payout == 100) { ?> selected <? } ?>>100%</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This is the percentage amount your will pay on your second tier.It is calculated from the original affiliate commission, not the total sale.',WIDTH,'250',CAPTION,'Tier Percentage Payout')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Enable Recurring Sales?</b><b>&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="use_rec" style="width=60;">
<option value="1" <? if ($use_rec == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($use_rec == 0) { ?> selected <? } ?>>No</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This option will allow you to set a sale to recurring and will display the recurring sales stats in the affiliate control center.',WIDTH,'250',CAPTION,'Enable Your Second Tier')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"><b>Crediting Style (Pay-Per-Sale Only):</b><b>&nbsp;&nbsp;</b></td>
<td width="60%" class="white_back">&nbsp;<select size="1" name="aff_lock" style="width=220;">
<option value="0" <? if ($aff_lock == 0) { ?> selected <? } ?>>First To Send Visitor Gets Credit</option>
<option value="1" <? if ($aff_lock == 1) { ?> selected <? } ?>>Last To Send Visitor Gets Credit</option>
</select>
<a href="#" OnClick="javascript:return false;" onMouseOver="toolTip('This determines which affiliate gets credit in the event a visitor comes in through more than 1 of your affiliate links.  Do you want the very first affiliate to always get that credit or should it be the very last affiliate? PPC users can ignore this option.',WIDTH,'250',CAPTION,'Enable Your Second Tier')" onMouseOut="toolTip();"><img src="../images/help.gif" width="12" height="12" border="0"></a>
</td>
</tr>
<tr>
<td width="55%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="40%" align="right" class="white_back"></td>
<td width="60%" class="white_back">&nbsp;<input type="submit" value="Update Settings">
</td></form>
</tr>
<tr>
<td width="100%" align="right" colspan="3" height="2" class="white_back"></td>
</tr>
</table>
</center>
</div>
</td>
</tr>
<tr>
<td width="100%">
<font size="1">&nbsp;</font>
</td>
</tr>
</table>
</center>
</div>