<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back" align="center">
<form method="POST" action="setup.php?action=10&cfg=7">
<div align="center"><table border="0" cellspacing="0" width="90%" class="outline_color">
<tr><td width="100%"><div align="center">
<table border="0" cellspacing="0" width="100%" cellpadding="0" class="white_back">
<tr><td width="100%" class="outline_color" colspan="2"><font color=#FFFFFF><b>&nbsp;</b><b>Setup Your Order Tracking Code Variable</b></font></td>
</tr><tr><td width="100%" colspan="2">&nbsp;</td></tr>
<tr><td width="100%" colspan="2">
    <div align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="95%">
        <tr>
          <td width="100%"><font color="#CC0000">This feature is completely
            optional.</font>&nbsp; Most people use it to pass the order number
            into Partnerverkauf.&nbsp; This aids in the tracking of each
            affiliate sale and is only available for Pay-Per-Sale users.</td>
        </tr>
      </table>
    </div>
  </td></tr>
<tr><td width="100%" colspan="2">&nbsp;</td></tr>
<tr><td width="40%" align="right">Variable Name:&nbsp;&nbsp;</td><td width="60%">&nbsp;<input type="text" name="idev_tracking" size="20" value="<?=$idev_tracking?>"></td></tr>
<tr><td width="40%" height="6" align="right"></td><td width="60%" height="6"></td></tr>
<tr><td width="40%" align="right">Use Order Tracking:&nbsp;&nbsp;</td><td width="60%">&nbsp;<input type="checkbox" name="use_tracking" value="1" <? if ($use_tracking == 1) { print "checked"; } ?>> (check box)</td></tr><tr><td width="40%" height="6" align="right"></td><td width="60%" height="6"></td></tr><tr><td width="40%" align="right"></td><td width="60%">&nbsp;<input type="submit" value="Set My Variable Name"></td></form></tr><tr><td width="40%" align="right"></td><td width="60%">&nbsp;</td></tr><tr><td width="100%" colspan="2" align="center">
    <div align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="95%">
        <tr>
          <td width="100%" align="center">
    If you use this feature, you'll need to make sure the above named variable is
    present on your sale confirmation page.&nbsp; This is the page where
    customers are sent after they make a purchase.<BR><BR>
<font color=#CC0000>This feature is only available for Pay-Per-Sale (flat rate or percentage payouts).</font></td>
        </tr>
      </table>
    </div>
  </td></tr><tr><td width="100%" colspan="2">&nbsp;</td></tr></table></div></td></tr></table></div>
</td></tr>
<tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>