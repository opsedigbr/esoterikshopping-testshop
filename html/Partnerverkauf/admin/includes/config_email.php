<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr>
<td width="100%">
<font size="1">&nbsp;</font>
</td>
</tr>
<tr>
<td width="100%">
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="1" width="95%">
<form method="POST" action="setup.php?action=5&cfg=5">
<tr>
<td width="100%" colspan="2" height="2" class="white_back"></td>
</tr>
<tr>
<td width="60%" align="right" class="white_back"><b>&nbsp;Notify affiliate when commission checks are sent?&nbsp;&nbsp;</b></td>
<td width="40%" class="white_back">&nbsp;<select size="1" name="notify" style="width=60;">
<option value="1" <? if ($notify == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($notify == 0) { ?> selected <? } ?>>No</option>
</select></td>
</tr>
<tr>
<td width="100%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr><td width="60%" align="right" class="white_back"><b>&nbsp;Send welcome email to new affiliates who join?&nbsp;&nbsp;</b></td>
<td width="40%" class="white_back">&nbsp;<select size="1" name="we" style="width=60;">
<option value="1" <? if ($we == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($we == 0) { ?> selected <? } ?>>No</option>
</select>
</td></tr><tr><td width="100%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr><td width="60%" align="right" class="white_back"><b>&nbsp;Notify admin when a new affiliate joins?&nbsp;&nbsp;</b></td>
<td width="40%" class="white_back">&nbsp;<select size="1" name="mailadmin" style="width=60;">
<option value="1" <? if ($mailadmin == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($mailadmin == 0) { ?> selected <? } ?>>No</option>
</select>
</td></tr><tr><td width="100%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="60%" align="right" class="white_back"><b>&nbsp;Notify admin each time an affiliate sale occurs?&nbsp;&nbsp;</b></td>
<td width="40%" class="white_back">&nbsp;<select size="1" name="sale_notify" style="width=60;">
<option value="1" <? if ($sale_notify == 1) { ?> selected <? } ?>>Yes</option>
<option value="0" <? if ($sale_notify == 0) { ?> selected <? } ?>>No</option>
</select>
</td>
</tr>
<tr>
<td width="100%" align="right" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="60%" align="right" class="white_back"><b>&nbsp;Notify affiliates when they generate an approved sale?&nbsp;&nbsp;</b></td>
<td width="40%" class="white_back">&nbsp;<select size="1" name="sale_notify_affiliate" style="width=60;">
<option value="1" <? if ($sale_notify_affiliate == 1) { ?> selected <? }
?>>Yes</option>
<option value="0" <? if ($sale_notify_affiliate == 0) { ?> selected <? }
?>>No</option>
</select>
</td>
</tr>
<tr>
<td width="100%" height="2" colspan="2" class="white_back"></td>
</tr>
<tr>
<td width="60%" class="white_back"></td>
<td width="40%" class="white_back">&nbsp;<input type="submit" value="Update Settings">
</td></form>
</tr>
<tr>
<td width="100%" align="center" colspan="2" class="white_back"><BR>Each email notification has a template that can be adjusted in the <a href="setup.php?action=6">email templates</a> settings.<BR><BR></td>
</tr>
</table>
</center>
</div>
</td>
</tr>
<tr>
<td width="100%">
<font size="1">&nbsp;</font>
</td>
</tr>
</table>
</center>
</div>