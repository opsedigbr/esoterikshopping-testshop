<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
?>
<div align="center"><center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="cell_back_super_light">
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr>
<tr><td width="100%"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<tr><td width="100%" class="white_back">&nbsp;</td></tr>
<tr><td width="100%" class="white_back">
<div align="center">
  <table border="0" cellpadding="0" cellspacing="0" width="95%">
    <tr>
      <td width="100%">
The use of product groups allows your affiliates to promote single products on
your site as well as all your products.&nbsp; When you create a product group,
the affiliate will be able to pick text ads and banners that promote only the
product itself.&nbsp; You can also define the incoming traffic page for each
product so when the visitor comes in to your site, they are immediately taken
directly to that product page.<BR><BR>

It is also highly recommend that you use a product group called "all products".&nbsp; This allows affiliates to promote all your products and the visitors from these
links will come straight into your default "<a href="setup.php?action=1">incoming
traffic page</a>".&nbsp; This page is usually the main/index page of your
site.<BR><BR>


To create product groups, simply click "<a href="groups.php">Product Groups</a>"
on the left column.&nbsp; You can create unlimited groups and define product
pages for each group.&nbsp; The URL accepted can be any type, full path
(http://www...) or relative (../productpage.html).&nbsp; Once you're finished,
click on either "<a href="banner_add.php">Add A Banner</a>" or "<a href="textad_add.php">Create
A Text Ad</a>".&nbsp; You will now see your product groups listed and can
create unlimited text ads and upload unlimited banners.</td>
    </tr>
  </table>
</div>


</td></tr>
<tr><td width="100%" class="white_back">&nbsp;</td></tr></table></div></td></tr>
<tr><td width="100%"><font size="1">&nbsp;</font></td></tr></table></center></div>