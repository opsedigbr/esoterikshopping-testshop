<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
include "../../config/config.php";
$alldata=mysql_query("select * from idevaff_affiliates where id = '$id'");
$indv_data=mysql_fetch_array($alldata);
$uname=$indv_data[username];
$upass=$indv_data[password];
$payto=$indv_data[payable];
$utax=$indv_data[tax_id_ssn];
$ufname=$indv_data[f_name];
$ulname=$indv_data[l_name];
$uemail=$indv_data[email];
$ad1=$indv_data[address_1];
$ad2=$indv_data[address_2];
$c=$indv_data[city];
$s=$indv_data[state];
$z=$indv_data[zip];
$coun=$indv_data[country];
$phone=$indv_data[phone];
$fax=$indv_data[fax];
$url=$indv_data[url];
$pp=$indv_data[pp];
$hits=$indv_data[hits_in];
$sales=$indv_data[sales];
$tsales=$indv_data[tier_sales];
$earnings1 = mysql_query("select SUM(amount) AS total from idevaff_payments where id = '$id'"); 
$row1 = mysql_fetch_array( $earnings1 );
$sexact = $row1['total'];
$earnings2 = mysql_query("select SUM(payment) AS total from idevaff_sales where id = '$id' and approved = '1'"); 
$row2 = mysql_fetch_array( $earnings2 );
$pexact = $row2['total'];
$totalsales = $sexact + $pexact;
$appsales = mysql_query("select id from idevaff_sales where id = '$id' and tier = '' and approved = '0'"); 
$salestotal = mysql_num_rows($appsales);
$ipstat = mysql_query("select SUM(acct_id) AS total from idevaff_iptracking where id = '$id'"); 
$row = mysql_fetch_array( $ipstat );
$unique = $row['total'];
$bonu = mysql_query("select bonus from idevaff_sales where id = '$id' and bonus = '1' and approved = '1'"); 
$bon = mysql_num_rows($bonu);
?>

<div align="center"><center><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td width="100%" bgcolor="#EEEEEE" height="20"><div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="98%"><tr>
<td width="50%"><b>Affiliate Record</b></td><td width="50%" align="right"><b>Partnerverkauf Report</b></td>
</tr></table></div></td></tr></table></center></div><hr>

<div align="center">
  <center>
  <table border="0" cellspacing="0" width="100%" cellpadding="2">
    <tr>
      <td width="20%"">&nbsp;Username:</td>
      <td width="40%""><?=$uname?></td>
      <td width="22%"">Total Hits In</td>
      <td width="18%"" colspan="2"><? print (number_format($hits, 0)); ?></td>
    </tr>
    <tr>
      <td width="20%">&nbsp;Password:</td>
      <td width="40%"><?=$upass?></td>
      <td width="22%">Unique Hits In</td>
      <td width="18%" colspan="2"><? print (number_format($unique, 0)); ?></td>
    </tr>
    <tr>
      <td width="20%"">&nbsp;Real Name:</td>
      <td width="40%""><?=$ufname?> <?=$ulname?></td>
      <td width="22%""># of Sales</td>
      <td width="18%"" colspan="2"><? print (number_format($sales, 0)); ?></td>
    </tr>
    <tr>
      <td width="20%">&nbsp;Email:</td>
      <td width="40%"><a href=mailto:<?=$uemail?>><?=$uemail?></a></td>
      <td width="22%"># of Tier Sales</td>
      <td width="18%" colspan="2"><? print (number_format($tsales, 0)); ?></td>
    </tr>
    <tr>
      <td width="20%"">&nbsp;Site URL:</td>
      <td width="40%""><a href=<?=$url?> target=_blank><?=$url?></a></td>
      <td width="22%"">Current Sales</td>
      <td width="2%""></td>
      <td width="16%"" align="right">

<?
if ($bon > 0) {
print " <font color=#CC0000>(bonus)&nbsp;</font>"; }
print "$cur_sym";
print (number_format($pexact, 2));
print "&nbsp;";
?>
</td>
    </tr>
    <tr>
      <td width="20%">&nbsp;Phone:</td>
      <td width="40%"><?=$phone?></td>
      <td width="22%">Previous Payouts</td>
      <td width="2%">
</td>
      <td width="16%" align="right">
<?
print "$cur_sym";
print (number_format($sexact, 2));
print "&nbsp;";
?>
</td>
    <tr>
      <td width="20%"">&nbsp;Fax:</td>
      <td width="40%""><?=$fax?></td>
      <td width="22%""><b>Grand Total Sales</b></td>
      <td width="2%""></td>
      <td width="16%"" align="right"><b>
<?
print "$cur_sym";
print (number_format($totalsales, 2));
print "&nbsp;";
?>
</b></td>
    </tr>
    <tr>
      <td width="20%">&nbsp;Federal Tax ID/SSN</td>
      <td width="40%"><?=$utax?></td>
      <td width="22%">Payment Preference</td>
      <td width="18%" colspan="2">
<? if ($pp == 1) {
print "<font color=CC0000>PayPal.com</font>";
} elseif ($pp == 0) {
print "Paper Check"; } ?></td>
    </tr>
  </table>
  </center>
</div>
<div align="center">
  <center>
  <table border="0" cellspacing="0" width="100%" cellpadding="2">
    <tr>
      <td width="50%""><font size="1">&nbsp;</font></td>

      <td width="50%" align="center"" rowspan="9">
      </td>
   </tr>
    <tr>
      <td width="50%""><b>&nbsp;Paper Checks Are Mailed To:</b></td>
    </tr>
    <tr>
      <td width="50%"">&nbsp;<?=$payto?></td>
    </tr>
    <tr>
      <td width="50%"">&nbsp;<?=$ad1?></td>
    </tr>
<? if ($ad2) { ?>
    <tr>
      <td width="50%"">&nbsp;<?=$ad2?></td>
    </tr>
<? } ?>
    <tr>
      <td width="50%"">&nbsp;<?=$c?>, <?=$s?> <?=$z?> - <?=$coun?></td>
    </tr>
    <tr>
      <td width="50%""><font size="1">&nbsp;</font></td>
    </tr>
  </table>
  </center>
</div>