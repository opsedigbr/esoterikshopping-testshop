<?
#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################
session_start();
if (!session_is_registered("valid_admin")) {
header("Location: setup.php"); } else {
require("templates/header.php");
if ($add == 'yes') {
if(strlen(trim($size1)) < 1) {
$fail = 1;
$fail_message .= "Missing Enter Banner Dimension\n"; }
if(strlen(trim($size2)) < 1) {
$fail = 1;
$fail_message .= "Missing Enter Banner Dimension\n"; }
if(strlen(trim($file)) < 1) {
$fail = 1;
$fail_message .= "Missing Banner File Name\n"; }
if ($fail != 1) {
if (!isset($HTTP_POST_FILES['file'])) exit;
if (is_uploaded_file($HTTP_POST_FILES['file']['tmp_name'])) {
if ($HTTP_POST_FILES['file']['size']>$max_size) { echo "<center><BR><b>The File Size Is Too Large<BR></b><BR>The maximum file size can be $max_size bytes.&nbsp;&nbsp;If you want to adjust this setting, you can do so in the <a href=setup.php>Partnerverkauf Setup</a>.<BR><BR>If you'd like to try another banner, you can do so here... <a href=banner_add.php>Add A New Banner</a>.</center><br>\n"; exit; }
if (($HTTP_POST_FILES['file']['type']=="image/gif") || ($HTTP_POST_FILES['file']['type']=="image/pjpeg") || ($HTTP_POST_FILES['file']['type']=="image/jpeg")) {
if (file_exists($path . $HTTP_POST_FILES['file']['name'])) { echo "<center><BR><b>The Filename Already Exists<BR></b><BR>For security purposes, Partnerverkauf does not remove files from your server (when you delete banners, etc).&nbsp;&nbsp;We only remove the entry from the database.&nbsp;&nbsp;If this is a banner that has once been in your affiliate program (on the server), you'll need to manually remove it from the <b>affiliates/banners/</b> directory and try again or simply rename the banner you're trying to upload.&nbsp;&nbsp;If you'd like to rename the banner and try again, you can do so here... <a href=banner_add.php>Add A New Banner</a>.</center><br>\n"; exit; }
$res = copy($HTTP_POST_FILES['file']['tmp_name'], "$path/banners/" .
$HTTP_POST_FILES['file']['name']);
if (!$res) { echo "<center><BR><b><font color=#CC0000>Upload Failed!</font><BR></b><BR>Try another banner here... <a href=banner_add.php>Add A New Banner</a>.</center><br>\n"; exit; } else {
$fname = $HTTP_POST_FILES['file']['name'];
mysql_query("insert into idevaff_banners VALUES ('', '$group', '$size1', '$size2','$fname','$desc')");
mysql_query("update idevaff_groups set contains = contains+1 where id = '$group'");
echo "<center><div align=center><center><table border=0 cellpadding=2 cellspacing=0 width=100%><tr><td width=100% class=cell_back_section>&nbsp;<b><font color=#FFFFFF>Banner Upload Successful!</font></b></td></tr></table></div></center><div align=center><table border=0 cellpadding=2 cellspacing=0 width=100%><tr><td width=100% class=cell_back_super_light><BR>&nbsp;A.) The banner has been added to the database.<BR>&nbsp;B.) The banner has been uploaded to the server into the /banners/ directory.<BR>&nbsp;C.) Individual banner linking code has been created for your affiliates.<BR><BR>&nbsp;<a href=banner_edit.php>Edit Your Banners Here</a><BR><BR></td></tr></table></div><br>\n"; 
} } else { echo "<center><BR><b>Incorrect File Type<BR></b><BR>Partnerverkauf only recognizes <font color=CC0000><b>.gif</b></font>, <font color=CC0000><b>.jpg</b></font> and <font color=CC0000><b>.jpeg</b></font> files.<BR><BR>You'll need to either convert your banners to one of these formats or manually upload the banner to the server AND manually enter the banner information into the database using a tool such as phpMySQL.<BR><BR>Try another banner here... <a href=banner_add.php>Add A New Banner</a>.</center><br>\n"; exit; } } } }
$max_size = (number_format($max_size,0));
?>
<center><div align="center"><center>
<table border="0" cellpadding="2" cellspacing="0" width="100%"><tr>
<td width="100%" class=cell_back_section>&nbsp;<font color="#FFFFFF"><b>Add A New Banner</b>
</font></td></tr></table></div></center>
<div align="center">
<center>
<table border="0" cellpadding="2" cellspacing="0" width="100%" class=cell_back_super_light>
<FORM ENCTYPE="multipart/form-data" ACTION="banner_add.php" METHOD="POST">
<input type=hidden name=add value=yes>
<tr>
<td width="60%" height="10"></td>
<td width="40%" align="center" height="10"></td>
</tr>
<tr>
<td width="60%"><b>&nbsp;Banner Size:</b></td>
<td width="40%" align="center" rowspan="12" valign="top"><input type="submit" value="Add This Banner">
<BR><BR>
<? if (($fail == 1) || ($ufail_message)){ ?>
<div align="center"><center>
<table border="0" cellpadding="4" cellspacing="0" width="100%" bgcolor="#CC0000">
<tr><td width="100%" align="center"><b><font color="#FFFFFF" size="4">ERROR</font></b><BR><b><font color="#FFFFFF"><?=$fail_message?></font></b></td>
</tr></table></center></div>
<? } ?>
<BR><BR>Banners must be <font color=CC0000><b>.gif</b></font>, <font color=CC0000><b>.jpeg</b></font> or <font color=CC0000><b>.jpg</b></font> and must not be larger than <?=$max_size?> bytes in size.&nbsp;&nbsp;These settings are adjustable in <a href=setup.php>Partnerverkauf Setup</a>.<BR><BR><b>Processing Make Take A Few Seconds</b>
</td>
</tr>
<tr>
<td width="60%"><b>&nbsp;<input type="text" name="size1" size="4" value=<?=$size1?>> X <input type="text" name="size2" size="4" value=<?=$size2?>></b>&nbsp;(Example: <b>120 X 60</b>, <b>468 X 60</b>, etc.)</td>
</tr>
<tr>
<td width="60%" height="10"></td>
</tr>
<tr>
<td width="60%"><b>&nbsp;Product Group:</b></td>
</tr>
<tr>
<td width="60%">&nbsp;<select size=1 name=group>
<?
$groups = mysql_query("select * from idevaff_groups order by name"); 
if (mysql_num_rows($groups)) {
while ($qry = mysql_fetch_array($groups)) {
$groupid=$qry[id];
$groupname=$qry[name];
print "<option value='$groupid'>$groupname</option>\n"; } }
?>
</select> (<a href=groups.php>manage groups</a>)</td>
</tr>
<tr>
<td width="60%" height="10"></td>
</tr>
<tr>
<td width="60%"><b>&nbsp;Banner Upload:</b></td>
</tr>
<tr>
<td width="60%"><b>&nbsp;<input type="file" name="file" size="20"></b></td>
</tr>
<tr>
<td width="60%" height="10"></td>
</tr>
<tr>
<td width="60%"><b>&nbsp;Description (optional):</b></td>
</tr>
<tr>
<td width="60%">&nbsp;<textarea rows="3" name="desc" cols="35"><?=$desc?></textarea></td></form>
</tr>
<tr>
<td width="60%">&nbsp;(Example: Our most popular banner.)</td>
</tr>
<tr>
<td width="60%" height="10"></td>
<td width="40%" align="center" height="10"></td>
</tr>
</table></center></div>
<? } include("templates/footer.php"); ?>
