#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

IMPORTANT INFORMATION IS CONTAINED IN THIS DOCUMENT, PLEASE READ EVERYTHING!
IMPORTANT INFORMATION IS CONTAINED IN THIS DOCUMENT, PLEASE READ EVERYTHING!
IMPORTANT INFORMATION IS CONTAINED IN THIS DOCUMENT, PLEASE READ EVERYTHING!
IMPORTANT INFORMATION IS CONTAINED IN THIS DOCUMENT, PLEASE READ EVERYTHING!

Copyright (C) 2001-2004
Jim Webster

This program is NOT FREE to use.  If you have a copy and have not paid for it, you are
using an illegal copy.  

------------------------------------------------------------	
A NOTE BEFORE INSTALLING:
We highly recommend installing into a top level directory named Partnerverkauf/.

Example:
http://www.yoursite.com/Partnerverkauf/
------------------------------------------------------------	
INSTALLATION:

1.)	Create a new MySQL database (naming it "Partnerverkauf" is fine).
	This is typically done at the command line or via a "control panel" if your
	hosting solution provides one.  If not, we recommend using our hosting solution.

2.)	Open config/config.php and adjust the full path to your server. Save your
	settings and close the file.

	UNIX/LINUX Platform Will Look Something Like This
	--------------------------------------------------
	$path = "/home/yoursite/public_html/Partnerverkauf";
	--
	
	WINDOWS/ASP Platform Will Look Something Like This
	--------------------------------------------------
	$path = "C:\home\yoursite\yoursite.com\Partnerverkauf";
	--
	
3.)	Open config/database.php and adjust your database connectivity settings.

		$dbhost = "localhost";
		$dbuser = "yourdatabaseusername";
		$dbpass = "yourdatabasepassword";
		$dbname = "yourdatabasename";

	Leaving the host set to "localhost" will work in most cases.
	Save your settings and close the file.

4.)	Create a directory in your Document Root called 'Partnerverkauf'.
	Example: public_html/Partnerverkauf or http/Partnerverkauf (each server looks different).

5.)	Using your favorite FTP program, upload all of the files in their present
	directory structure to your hosting account (into your newly created directory
	called 'Partnerverkauf').
	
		Important:
		ALL .php and .txt files MUST be uploaded in ASCII mode.
		ALL images (.gif/.jpg/etc.) MUST be uploaded in BINARY mode.

6.)	Change permissions on /banners/ to allow write access (777).

7.)	From your web browser, run the 'install.php' program.  This program will
	automatically add the appropriate tables into your new database created
	in step (B).  To execute the file simply type the following line into
	your web browser:

	http://www.yoursitenamehere.com/Partnerverkauf/install.php

8.)	Login to your admin center and complete your configuration.

9.)	Complete your affiliate configuration in this order:
	1.) Site Configuration
	2.) Sales Settings
	3.) Payout Settings
	4.) Email Settings
	5.) Email Templates
	6.) Processing Code (place this code into your existing site).
	
See README_manual.txt for more information on setting up your payment processing.
