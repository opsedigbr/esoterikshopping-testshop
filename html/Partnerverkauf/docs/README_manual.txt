#############################################################
## Partnerverkauf Version 4.0 - Copyright by iDevDirect
## Nullified by [GTT]
#############################################################

Note: This manual only covers the most obvious features you may have questions about.  We
do not have a manual that covers every single feature as most of the features are fairly
self-explanatory.  If you have questions, please do not hesitate to contact us directly.
PS. If anyone is interested in writing a comprehensive features manual, please
contact us.  We'd like to talk with you.

SETTING UP YOUR PAYMENT PROCESSING
In order to process affiliates, Partnerverkauf must be included in your sales processing.

You can do this by using one of two methods.

A) Send your customers through idevsale.php after they make a purchase.  This method uses
   the auto return feature.  You should use this option if you're existing site is setup
   to use the auto return feature on your merchant account.  You'll simply set your return
   URL to http://www.yoursite.com/Partnerverkauf/idevsale.php.  This script will process the
   affiliate (if one exists) then auto-forward your customer to whatever page you define in
   your "Site Configuration" (Partnerverkauf Admin Center).  NOTE: If your merchant account
   is integrated with your shopping cart, you probably don't want to use this method as the
   return page is probably already setup in your cart.  This feature is good for standalone
   type checkouts like the basic PayPal buynow button.

   To Use This Method:
   1. Check the Use Auto Return option in "Site Configuration" (Partnerverkauf Admin Center).
   2. Click on "Processing Code" (Partnerverkauf Admin Center) to retrieve the return URL code.
   3. For percentage payouts, make sure the variable you named in "Payout Settings"
      (Partnerverkauf Admin Center) is being passed after the purchase is complete.

   - OR -

B) Add processing code to the page customers return to after they make a purchase.  Use this
   style if you have a complete shopping cart and merchant account integrated.  You will need
   to place some code on your sale confirmation page.  This is the page your customer returns
   to after they make a purchase.  All we're going to do here is include sale.php on that
   page so the affiliate is processed (if one exists).  You can choose your include type and
   get the code by clicking on "Processing Code" (Partnerverkauf Admin Center).

   To Use This Method:
   1. UNcheck the Use Auto Return option in "Site Configuration" (Partnerverkauf Admin Center).
   2. Click on "Processing Code" (Partnerverkauf Admin Center) to retrieve the code to place
      into your sale confirmation page.
   3. For percentage payouts, make sure the variable you entered into "Payout Settings"
      (Partnerverkauf Admin Center) is being passed into this page after the purchase is
      complete.

   Percentage Payout Users:
   It's possible you may need a custom setup for processing percentage payouts.  This
   depends on the merchant account and/or shopping cart you're using.  More information is
   below under "Custom Processing".

PAY-PER-SALE (Flat Rate)
Implement one of the above options and you are done. 

PAY-PER-SALE (Percentages)
In order for you to process percentage payouts, the sale amount must be passed into
either your idevsale.php file or your final sale confirmation page (depending on what method
you're using above).  The sale amount is passed using a variable name.  This variable name
is different for every shopping cart and merchant account.  You can usually find the name
by checking the Companies support forums.  

Note 1: You'll need to set this variable name as your pass-through variable by clicking on
"Payout Settings" (Partnerverkauf Admin Center).

Note 2: You'll need to make sure this variable name is actually passing the sale amount.


   CUSTOM PROCESSING
   Some carts and merchant accounts require some tweaking to the standard processing code
   provided in the Partnerverkauf Admin Center.  

   If you know the total sale amount variable is already on your sale confirmation page,
   simply place your Processing Code (Partnerverkauf Admin Center) into this page and you are
   done.  Any style processing code you choose should work without problems.


PAY-PER-CLICK
Partnerverkauf will automatically credit an affiliate (if one exists) with the
commission amount set for clicks into your site.  Only UNIQUE clicks will be credited.
Partnerverkauf self manages this type of payout, no processing code needs placed.

----------------------------------

RECURRING SALES
In order to use recurring sales, you must set "Approve New Sales?" to YES in your
sales settings.  This is the only place you can set a sale to recurring.  When a recurring
sale is triggered, it will appear on the "Approve Sales" page.

The recurring sale trigger is built-in to the Text Ad boxes.  In order for the "next
credit" time to be ticked off each day, at least ONE Text Ad needs to get an impression each
day.  If you have a low traffic site or very few affiliates, we recommend placing a Text
Ad box on your OWN highest traffic page.  When you start generating affiliates, any text ads
they are running will automatically complete this requirement for you.

----------------------------------

PRODUCT GROUPS
You can create unlimited product groups and allow your affiliates to promote individual
products or all products.  If a product group has an incoming traffic page set, customers
will come into your site and immediately be redirected to the defined page.  If no incoming
traffic page is set, customers will come into your default incoming traffic page.

----------------------------------

PAYING AFFILIATES
When you click on "Pay Affiliates", you will get a list of affiliates that need paid.
This list will only show affiliates who have met your payout balance requirement set in
"Sales Settings".  The balance of any affiliate who has not met this requirement will
simply carry over from month-to-month until they meet it.  You should only pay affiliates
on the day you have selected in "Sales Settings".

After you have either A) sent their PayPal payment or B) sent their check via mail, click
on "Archive This Payment".  Partnerverkauf will archive the payment and make all the sales
as being paid.  The affiliate balance will be dropped back to zero.

