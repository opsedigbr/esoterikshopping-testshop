Wir freuen uns &uuml;ber Ihr Interesse an unserer Website. Der Schutz Ihrer Privatsph&auml;re ist f&uuml;r uns
sehr wichtig. Nachstehend informieren wir Sie ausf&uuml;hrlich &uuml;ber den Umgang mit Ihren Daten.<br />
<br />
<strong>Erhebung, Verarbeitung und Nutzung personenbezogener Daten</strong><br />
Sie k&ouml;nnen unsere Seite besuchen, ohne Angaben zu Ihrer Person zu machen. Wir speichern
lediglich Zugriffsdaten ohne Personenbezug wie z.B. den Namen Ihres Internet Service
Providers, die Seite, von der aus Sie uns besuchen oder den Namen der angeforderten
Datei. Diese Daten werden ausschlie&szlig;lich zur Verbesserung unseres Angebotes ausgewertet
und erlauben keinen R&uuml;ckschluss auf Ihre Person.

Personenbezogene Daten werden nur erhoben, wenn Sie uns diese im Rahmen Ihrer Warenbestellung 
oder bei Er&ouml;ffnung eines Kundenkontos  freiwillig mitteilen. Wir verwenden die von ihnen mitgeteilten Daten ohne Ihre
gesonderte Einwilligung ausschlie&szlig;lich zur Erf&uuml;llung und Abwicklung Ihrer Bestellung. Mit
vollst&auml;ndiger Abwicklung des Vertrages und vollst&auml;ndiger Kaufpreiszahlung werden Ihre Daten
f&uuml;r die weitere Verwendung gesperrt und nach Ablauf der steuer- und handelsrechtlichen
Aufbewahrungsfristen gel&ouml;scht, sofern Sie nicht ausdr&uuml;cklich in die weitere Nutzung 
Ihrer Daten eingewilligt haben.<br />
<br />
<strong>Verwendung von Cookies</strong><br />
Um den Besuch unserer Website attraktiv zu gestalten und die Nutzung bestimmter Funktionen
zu erm&ouml;glichen, verwenden wir auf verschiedenen Seiten sogenannte Cookies. Hierbei handelt es sich um kleine Textdateien, die auf Ihrem Endger&auml;t abgelegt werden. Einige
der von uns verwendeten Cookies werden nach dem Ende der Browser-Sitzung, also nach 
Schlie&szlig;en Ihres Browsers, wieder gel&ouml;scht (sog. Sitzungs-Cookies). Andere Cookies verbleiben 
auf Ihrem Endger&auml;t und erm&ouml;glichen uns oder unseren Partnerunternehmen, Ihren
Browser beim n&auml;chsten Besuch wiederzuerkennen (persistente Cookies). 
Sie k&ouml;nnen Ihren Browser so einstellen, dass Sie &uuml;ber das Setzen von Cookies informiert
werden und einzeln &uuml;ber deren Annahme entscheiden oder die Annahme von Cookies f&uuml;r
bestimmte F&auml;lle oder generell ausschlie&szlig;en. Bei der Nichtannahme von Cookies kann die
Funktionalit&auml;t unserer Website eingeschr&auml;nkt sein.<br />
<br />
<strong>Erstellung pseudonymer Nutzungsprofile zur Webanalyse</strong><br />
<div class="image-float right">
    <a href="http://www.etracker.com/privacy_info?et=7sgmS9" target="_blank"><img alt="etracker more than analytics " title="etracker more than analytics " border="0" src="http://www.etracker.com/privacy_signet?et=7sgmS9" height="117" width="100" /></a><br />
</div>
Auf dieser Website werden mit Technologien der etracker GmbH (<a href="http://www.etracker.com">www.etracker.com</a>) Daten zu Marketing- und Optimierungszwecken gesammelt und gespeichert. Aus diesen Daten k&ouml;nnen unter einem Pseudonym Nutzungsprofile erstellt werden. Hierzu k&ouml;nnen Cookies eingesetzt werden. Bei Cookies handelt es sich um kleine Textdateien, die lokal im Zwischenspeicher des Internet-browsers des Seitenbesuchers gespeichert werden. Die Cookies erm&ouml;glichen die Wiedererkennung des Internetbrowsers. 
Die mit den etracker-Technologien erhobenen Daten werden ohne die gesondert erteilte Zustimmung des Betroffenen nicht dazu benutzt, den Besucher dieser Website pers&ouml;nlich zu identifizieren und nicht mit personenbezogenen Daten &uuml;ber den Tr&auml;ger des Pseudonyms zusammengef&uuml;hrt. Der Datenerhebung und -speicherung kann jederzeit mit Wirkung f&uuml;r die Zukunft <a target="_blank" href="https://www.etracker.de/privacy?et=7sgmS9">widersprochen</a> werden.
<div class="clear"></div>
<br />
Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (&bdquo;Google&ldquo;). Google Analytics verwendet sog. &bdquo;Cookies&ldquo;, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm&ouml;glichen. Die durch den Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA &uuml;bertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europ&auml;ischen Union oder in anderen Vertragsstaaten des Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt. Nur in Ausnahmef&auml;llen wird die volle IP-Adresse an einen Server von Google in den USA &uuml;bertragen und dort gek&uuml;rzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports &uuml;ber die Websiteaktivit&auml;ten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegen&uuml;ber dem Websitebetreiber zu erbringenDie im Rahmen von Google Analytics von Ihrem Browser &uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengef&uuml;hrt. Sie k&ouml;nnen die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website vollumf&auml;nglich werden nutzen k&ouml;nnen. Sie k&ouml;nnen dar&uuml;ber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verf&uuml;gbare Browser-Plugin <a href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">herunterladen</a> und installieren.<br />
<br />
<strong>Bonit&auml;tspr&uuml;fung und Scoring</strong><br />
Sofern wir in Vorleistung treten, z. B. bei einem Kauf auf Rechnung, holen wir zur Wahrung unserer berechtigten Interessen ggf. eine Bonit&auml;tsauskunft auf der Basis mathematisch-statistischer Verfahren bei der [B&uuml;rgel Wirtschaftsinformationen GmbH &amp; Co. KG, Gasstra&szlig;e 18, 22761 Hamburg] ein. Hierzu &uuml;bermitteln wir die zu einer Bonit&auml;tspr&uuml;fung ben&ouml;tigten personenbezogenen Daten an die [B&uuml;rgel Wirtschaftsinformationen GmbH &amp; Co. KG] und verwenden die erhaltenen Informationen &uuml;ber die statistische Wahrscheinlichkeit eines Zahlungsausfalls f&uuml;r eine abgewogene Entscheidung &uuml;ber die Begr&uuml;ndung, Durchf&uuml;hrung oder Beendigung des Vertragsverh&auml;ltnisses. Die Bonit&auml;tsauskunft kann Wahrscheinlichkeitswerte (Score-Werte) beinhalten, die auf Basis wissenschaftlich anerkannter mathematisch- statistischer Verfahren berechnet werden und in deren Berechnung unter anderem Anschriftendaten einflie&szlig;en. Ihre schutzw&uuml;rdigen Belange werden gem&auml;&szlig; den gesetzlichen Bestimmungen ber&uuml;cksichtigt.
<br />
<br />
<strong>E-Mail-Werbung (mit Newsletteranmeldung)</strong><br />
Wenn Sie sich gesondert zum Newsletter angemeldet haben, wird Ihre E-Mail-Adresse f&uuml;r eigene Werbezwecke genutzt, bis Sie sich vom Newsletter abmelden. Die Abmeldung ist jederzeit <a href="../newsletter.php" target="_parent">hier</a> m&ouml;glich, ohne dass Ihnen hierf&uuml;r andere als die &Uuml;bermittlungskosten nach den Basistarifen entstehen. <br />
<br />
<strong>Weitergabe personenbezogener Daten</strong><br />
Eine Weitergabe Ihrer Daten erfolgt an das mit der Lieferung beauftragte Versandunternehmen,
soweit dies zur Lieferung der Waren notwendig ist. Zur Abwicklung von Zahlungen
geben wir Ihre Zahlungsdaten an das mit der Zahlung beauftragte Kreditinstitut weiter.
<br />
<br />

<strong>Auskunftsrecht</strong><br />
Nach dem Bundesdatenschutzgesetz haben Sie ein Recht auf unentgeltliche Auskunft &uuml;ber
Ihre gespeicherten Daten sowie ggf. ein Recht auf Berichtigung, Sperrung oder L&ouml;schung
dieser Daten.
<br />
<br />
<strong>Datensicherheit</strong><br />
Ihre personenbezogenen Daten werden im Bestellprozess verschl&uuml;sselt mittels einer hochgradigen Verschl&uuml;sselung (RC4, 128-bit Schl&uuml;ssel), validiert von GeoTrust Inc. &uuml;ber das Internet &uuml;bertragen.  Wir
sichern unsere Website und sonstigen Systeme durch technische und organisatorische
Ma&szlig;nahmen gegen Verlust, Zerst&ouml;rung, Zugriff, Ver&auml;nderung oder Verbreitung Ihrer Daten
durch unbefugte Personen. Der Zugang zu Ihrem Kundenkonto ist nur nach Eingabe Ihres
pers&ouml;nlichen Passwortes m&ouml;glich. Sie sollten Ihre Zugangsinformationen stets vertraulich
behandeln und das Browserfenster schlie&szlig;en, wenn Sie die Kommunikation mit uns beendet
haben, insbesondere wenn Sie den Computer gemeinsam mit anderen nutzen.<br />
<br />
<strong>Ansprechpartner f&uuml;r Datenschutz</strong><br />
Bei Fragen zur Erhebung, Verarbeitung oder Nutzung Ihrer personenbezogenen Daten, bei
Ausk&uuml;nften, Berichtigung, Sperrung oder L&ouml;schung von Daten sowie Widerruf erteilter Einwilligungen
wenden Sie sich bitte an:<br />
Sven Safr, Am Bach 9, 93349 Mindelstetten, betrieblicher Datenschutzbeauftragter.
