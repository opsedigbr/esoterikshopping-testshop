<div id="customer-informations">
    <strong><h1>NEUHEIT !!!<br />
      Wundersch&ouml;ne Edelsteinblumen &ndash; ein wundervolles  Weihnachtsgeschenk!</h1>
    </strong><br />
    Bald  ist es schon wieder soweit&hellip;die stille Zeit r&uuml;ckt immer n&auml;her und damit auch die  Frage, was man seinen Liebsten zum Weihnachtsfest schenken soll. Eine echte  Neuheit sind Edelsteinblumen, die mittels Wechselb&uuml;gel und Schlangenkette  (beide Teile aus echtem 925er Sterling Silber) zu einer wundersch&ouml;nen und ganz  besonderen Edelstein-Kette werden. Der Clou: Jede Kombination ist durch den  Edelstein ein Unikat, da jeder Stein (Naturprodukt) anders ist. Die  Wechselb&uuml;gel wurden speziell f&uuml;r die Edelsteinblumen angefertigt und passen  daher nicht in andere Schmuckst&uuml;cke wie Edelsteindonuts. Sie erhalten die  Edelsteinkette in einer sch&ouml;nen Schmuckschachtel.
    
  <br /><br />
  
    <strong><h2>Der Mookaait</h2></strong><br />
  <div class="images left">
   	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Mookait-Edelstein-Collier.html" target="_parent"><img src="../media/content/mookait-edelsteinblume-edelsteinkette.jpg" alt="Mookait - Edelstein Collier" title="Mookait - Edelstein Collier" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
  Der  Mookait ist ein stark besch&uuml;tzender Stein, der vor allem in der Lage ist, die  Seele zu reinigen und zu beruhigen. Der <a href="/products/Edelsteinschmuck/Edelstein-Blume/Mookait-Edelstein-Collier.html" target="_parent">Mookait</a> wird haupts&auml;chlich in  Australien gefunden und dort als heiliger Wunderstein verehrt. Seine Farben  weichen vom hellen gelblich-braun &uuml;ber rotbraun ins dunkle braun. Auf dem Bild  sehen Sie den Mookait als wundersch&ouml;ne Edelsteinblume mit Wechselb&uuml;gel und  Schlangenkette in 925er Sterling Silber.
<div class="clear"></div>
    
    <br />
    
    <strong><h2>Der Rhyolith</h2></strong><br />
  <div class="images right">
    	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Rhyolith-Edelstein-Collier.html" target="_parent"><img src="../media/content/rhyolith-edelsteinblume-edelsteinkette.jpg" alt="Rhyolith - Edelstein Collier" title="Rhyolith - Edelstein Collier" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
  Dem Rhyolith  sagt man nach, er habe eine beruhigende Wirkung. Au&szlig;erdem trage der <a href="/products/Edelsteinschmuck/Edelstein-Blume/Rhyolith-Edelstein-Collier.html" title="Rhyolith" target="_parent">Rhyolith</a> dazu bei, sich selbst anzunehmen. Sich selbst anzunehmen ist die  Grundvoraussetzung, dass man auch von anderen Menschen akzeptiert und  angenommen wird. Der Rhyolith ist mit vielen, verschiedenen Farben gesprenkelt,  von gr&auml;ulich gr&uuml;n bis ins leicht br&auml;unliche. Auf dem Foto sehen Sie den Rhyolith&nbsp; als wundersch&ouml;ne Edelsteinblume mit Wechselb&uuml;gel  und Schlangenkette in 925er Sterling Silber.
<div class="clear"></div>
    
    <br />
    
    <strong><h2>Der Rosenquarz Edelstein</h2></strong><br />
  <div class="images left">
    	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Rosenquarz-Edelstein-Collier.html" target="_parent"><img src="../media/content/rosenquarz-edelsteinblume-edelsteinkette.jpg" alt="Rosenquarz - Edelstein Collier" title="Rosenquarz - Edelstein Collier" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
  Der  <a href="/products/Edelsteinschmuck/Edelstein-Blume/Rosenquarz-Edelstein-Collier.html" title="Rosenquarz" target="_parent">Rosenquarz</a> wurde zu allen Zeiten als Stein der Liebe und der Herzensw&auml;rme  verehrt. Griechen und R&ouml;mer glaubten, dass dieser Stein pers&ouml;nlich von den  G&ouml;ttern Eros und Amor auf die Erde gebracht wurde, um den Menschen die Kraft  der Liebe zu schenken. Der Rosenquarz wird neben der Eigenschaft Liebe zu  vermitteln auch als Fruchtbarkeits-Stein verehrt. Der Rosenquarz besticht durch  seine wundersch&ouml;ne, rosa Farbe. Auf dem Foto sehen Sie den Rosenquarz in seiner  wundersch&ouml;nen Farbe als Edelsteinblume mit Wechselb&uuml;gel und Schlangenkette in  925er Sterling Silber.
<div class="clear"></div>    
  
    <br />
    
    <strong><h2>Der Carneol</h2>
    </strong><br />
  <div class="images right">
    	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Carneol-Edelstein-Collier.html" target="_parent"><img src="../media/content/carneol-edelsteinblume-edelsteinkette.jpg" alt="Carneol - Edelstein Collier" title="Carneol - Edelstein Collier" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
	</div>
  Der  Carneol ist ein Stein der Liebe, der Herz und Seele w&auml;rmen soll. Aber auch Mut  und Lebensfreude soll der <a href="/products/Edelsteinschmuck/Edelstein-Blume/Carneol-Edelstein-Collier.html" title="Carneol" target="_parent">Carneol</a> massiv st&auml;rken. Er wurde sehr h&auml;ufig als  Schmuckstein in der Antike, speziell in &Auml;gypten verwendet. Wegen seiner roten  Farbe wird der Carneol auch der Liebe zugeschrieben. Der Carneol hat hell bis  dunkelbrauen Nuancen und ist sehr vielschichtig. Farblich bestehen  &Auml;hnlichkeiten zum Bernstein. Auf dem Foto sehen Sie den Carneol als  Edelsteinblume mit Wechselb&uuml;gel und Schlangenkette (Wechselb&uuml;gel und  Schlangenkette in 925er Sterling Silber).
<div class="clear"></div>      
  
    <br />
    
    <strong><h2>Versteinertes Holz</h2></strong><br />
  <div class="images left">
    	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Versteinertes-Holz-Edelstein-Collier.html" target="_parent"><img src="../media/content/versteinertes-holz-edelsteinblume-edelsteinkette.jpg" alt="Versteinertes Holz - Edelstein Blume" title="Versteinertes Holz - Edelstein Blume" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
  </div>
  <a href="/products/Edelsteinschmuck/Edelstein-Blume/Versteinertes-Holz-Edelstein-Collier.html" title="Versteinertes Holz" target="_parent">Versteinertes  Holz</a> entstand vor Millionen von Jahren, indem B&auml;ume von Erdmassen luftdicht  verschlossen wurden. Im Laufe von Millionen von Jahren drangen nach und nach  Minerals&auml;uren, die auch Kristalle wachsen lassen, in die Holzfasern ein und  bildeten die Versteinerungen. Das versteinerte Holz soll beruhigend,  harmonisierend wirken. Versteinertes Holz wird auch gerne f&uuml;r Meditationen  verwendet, um Einsicht in fr&uuml;here Leben zu erhalten. Versteinertes Holz macht  farblich seinem Namen alle Ehre. Es sieht aus wie Holz, ist aber tats&auml;chlich  ein Stein. Auf dem Foto sehen Sie das versteinerte Holz als Edelsteinblume mit  Wechselb&uuml;gel und Schlangenkette (Wechselb&uuml;gel und Schlangenkette in 925er  Sterling Silber).
<div class="clear"></div>    
  
    <br />
    
    <strong><h2>Last, but not least - der Bergkristall als Edelsteinblume</h2></strong><br />
  <div class="images right">
    	<a href="/products/Edelsteinschmuck/Edelstein-Blume/Bergkristall-Edelstein-Collier.html" target="_parent"><img src="../media/content/bergkristall-edelsteinblume-edelsteinkette.jpg" alt="Bergkristall - Edelstein Blume" title="Bergkristall - Edelstein Blume" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
  </div>
  Bereits  vor ungef&auml;hr 1000 Jahren fand die Nonne Hildegard von Bingen &uuml;ber den  <a href="/products/Edelsteinschmuck/Edelstein-Blume/Bergkristall-Edelstein-Collier.html" title="Bergkristall" target="_parent">Bergkristall</a> heraus, dass er - ihrer Meinung nach - heilende Eigenschaften hat.  Die Griechen glaubten vor einigen hundert Jahren, dass es sich beim Bergkristall  um gefrorenes Wasser handelt. Der Bergkristall soll Geist und Seele Licht  bringen, sie st&auml;rken und dazu verhelfen, ein gesundes Selbstwertgef&uuml;hl  aufzubauen. Seine Energie und seine Schwingungen sollen einzigartig hoch sein  und feinf&uuml;hlige Menschen sp&uuml;ren diese Energie, wenn Sie Bergkristall in den  H&auml;nden halten. Auf dem Foto sehen Sie den Bergkristall  als Edelsteinblume mit Wechselb&uuml;gel und Schlangenkette (Wechselb&uuml;gel und  Schlangenkette in 925er Sterling Silber). Edelsteine wie der Bergkristall sind  Naturprodukte und daher Unikate, so dass schlie&szlig;lich kein Schmuckst&uuml;ck dem  anderen gleicht. Die Wechselb&uuml;gel wurden speziell f&uuml;r die Edelsteinblumen  angefertigt und passen daher nicht in andere Schmuckst&uuml;cke wie Edelsteindonuts.
<div class="clear"></div>      
</div>
