<strong>Widerrufsbelehrung</strong><br /><br />
<strong>Widerrufsrecht</strong><br />
  Sie k&ouml;nnen Ihre Vertragserkl&auml;rung innerhalb von 14 Tagen ohne Angabe von Gr&uuml;nden in Textform
  (z.B. Brief, Fax, E-Mail) oder - wenn Ihnen die Sache vor Fristablauf &uuml;berlassen wird - auch durch
  R&uuml;cksendung der Sache widerrufen. Die Frist beginnt nach Erhalt dieser Belehrung in Textform, jedoch
  nicht vor Eingang der Ware beim Empf&auml;nger (bei der wiederkehrenden Lieferung gleichartiger. Waren nicht vor Eingang der ersten Teillieferung) und auch nicht vor Erf&uuml;llung unserer Informationspflichten
  gem&auml;&szlig; Artikel 246 &sect; 2 in Verbindung mit &sect; 1 Absatz 1 und 2 EGBGB sowie unserer Pflichten
  gem&auml;&szlig; &sect; 312g Absatz 1 Satz 1 BGB in Verbindung mit Artikel 246 &sect; 3 EGBGB. Zur Wahrung der Widerrufsfrist
  gen&uuml;gt die rechtzeitige Absendung des Widerrufs oder der Sache. Der Widerruf ist zu richten
  an. 
  <br />
  <br />
  OPSEDI GbR<br />
  Auslieferzentrum</span><br />
  Am Bach 9<br />
    93349 Mindelstetten<br />
    email: info@Esoterikshopping.de<br />
    Fax: 08404 939 265<br />
    <br />
    <strong>Widerrufsfolgen</strong><br />
  Im Falle eines wirksamen Widerrufs sind die beiderseits empfangenen Leistungen zur&uuml;ckzugew&auml;hren
  und ggf. gezogene Nutzungen (z.B. Zinsen) herauszugeben. K&ouml;nnen Sie uns die
  empfangene Leistung sowie Nutzungen (z.B. Gebrauchsvorteile) nicht oder teilweise nicht oder
  nur in verschlechtertem Zustand zur&uuml;ckgew&auml;hren beziehungsweise herausgeben, m&uuml;ssen Sie
  uns insoweit Wertersatz leisten. F&uuml;r die Verschlechterung der Sache und f&uuml;r gezogene Nutzungen
  m&uuml;ssen Sie Wertersatz nur leisten, soweit die Nutzungen oder die Verschlechterung
  auf einen Umgang mit der Sache zur&uuml;ckzuf&uuml;hren ist, der &uuml;ber die Pr&uuml;fung der Eigenschaften
  und der Funktionsweise hinausgeht. Unter &quot;Pr&uuml;fung der Eigenschaften und der Funktionsweise&quot; versteht man das Testen und Ausprobieren der jeweiligen Ware, wie es etwa im Ladengesch&auml;ft
  m&ouml;glich und &uuml;blich ist. Paketversandf&auml;hige Sachen sind auf unsere Gefahr zur&uuml;ckzusenden.
  Sie haben die regelm&auml;&szlig;igen Kosten der R&uuml;cksendung zu tragen, wenn die gelieferte
  Ware der bestellten entspricht und wenn der Preis der zur&uuml;ckzusendenden Sache einen Betrag
  von 40 Euro nicht &uuml;bersteigt oder wenn Sie bei einem h&ouml;heren Preis der Sache zum Zeitpunkt
  des Widerrufs noch nicht die Gegenleistung oder eine vertraglich vereinbarte Teilzahlung
  erbracht haben. Anderenfalls ist die R&uuml;cksendung f&uuml;r Sie kostenfrei. Nicht paketversandf&auml;hige
  Sachen werden bei Ihnen abgeholt. Verpflichtungen zur Erstattung von Zahlungen m&uuml;ssen innerhalb
  von 30 Tagen erf&uuml;llt werden. Die Frist beginnt f&uuml;r Sie mit der Absendung Ihrer Widerrufserkl&auml;rung
  oder der Sache, f&uuml;r uns mit deren Empfang.<br />
  <br />
  <strong>  Ende der Widerrufsbelehrung</strong></strong><br />
  <br />