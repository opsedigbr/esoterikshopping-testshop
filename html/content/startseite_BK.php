<div id="startseiten-text">
    Wir haben unser Sortiment stark erweitert. Sie finden in  unserem Shop nun neben Esoterik Produkten nun viele weitere interessante  Artikel zum Schwelgen. Unter dem 
    Punkt &bdquo;Neue Produkte&ldquo; finden Sie die in den  letzten 30 Tagen hinzugef&uuml;gten Artikel aufgelistet. Auch haben wir neue  Kategorien wie 
    <strong><a href="Chakra-Produkte" title="Chakra Produkte" target="_parent">Chakra</a></strong>, neue <strong><a href="Doreen-Virtue-Produkte" title="Doreen Virtue Produkte" target="_parent">Doreen Virtue</a></strong> Produkte, wundersch&ouml;ne <strong><a href="Duftlampen-Aromalampen-Duft-Drops" title="Duftlampen, Aromalampen, Duft-Drops und Zubeh&ouml;r" target="_parent">Duftlampen</a></strong>, eleganten <strong><a href="Edelsteinschmuck" title="Edelsteinschmuck" target="_parent">Edelsteinschmuck</a></strong>, 
    kleine und gro&szlig;e <strong><a href="Engel-Engelfiguren-mystische-Figuren-Gargoyles" title="Engel, Engelfiguren" target="_parent">Engelfiguren</a></strong>, neue <strong><a href="Chakra-Produkte/Chakra-Duftkerzen" title="Chakra Kerzen aus Stearin" target="_parent">Kerzen aus Stearin</a></strong> (besonders hochwertig), eine eigene Kategorie <strong><a href="Musik" title="Musik - Meditation und Entspannung" target="_parent">Musik mit 
    Meditation und Entspannung</a></strong>, <strong><a href="Aetherische-Oele-Ritual-Oele/Magic-Spray" title="&Ouml;le - Magic Spray" target="_parent">neue &Ouml;le</a></strong><a href="Aetherische-Oele-Ritual-Oele/Magic-Spray"> (Magic Sprays)</a>, wundervolle  neue <strong><a href="Radiaesthesie-Pendel-Tensoren" title="viele neue Pendel" target="_parent">Pendel (z.B. mit Kammer)</a>,</strong> neue 
    wohlriechende <strong><a href="Raeucherstaebchen-Halter-Raeucherkegel-Koros" title="R&auml;ucherst&auml;bchen und Halter" target="_parent">R&auml;ucherst&auml;bchen und Halter</a></strong>,  erleuchtenden <strong><a href="Raeucherwerk-Weihrauch-Harze-Kraeuter/Farbiger-Weihrauch-Gold-Silber-Rot-Gruen" title="farbiger Weihrauch" target="_parent">farbigen Weihrauch</a></strong>,  viel neues <strong><a href="Raeucherungen-Raeucherzubehoer" title="R&auml;ucherzubeh&ouml;r" target="_parent">Zubeh&ouml;r zum 
    Thema &bdquo;R&auml;uchern&ldquo;</a></strong> und einen eigenen neuen Bereich zum Thema <strong><a href="Wellness" title="Wellness" target="_parent">Wellness  mit interessanten B&uuml;chern, Hot Stone Massagesteinen 
    und u.a. Entspannungsmusik</a></strong>.&nbsp; Vieles und Neues also, was es zu entdecken  gilt: Hier in unserem neuen <strong><a href="index.php" title="Esoterik Shop" target="_parent">Esoterik-Shop</a></strong>.<br>
    <br>
    Lassen Sie sich von unserer Vielfalt der einigen tausend Produkten aus allen bekannten und unbekannten Bereichen der Esoterik und vieles weitere inspirieren. 
    Speziell zum Thema <a href="Kartenlegen-Tarot-Lenormand-Wahrsagekarten-Engelkarten" target="_self" title="Kartenlegen f&Atilde;&OElig;r Kartenleger und Kartenlegerinnen">Kartenlegen</a> haben wir hunderte von seltenen Kartendecks und Sets mit dazugeh&ouml;rigen <a href="Samtbeutel-Edelholzschatullen-Glasflaeschchen-und-Schraenke" target="_self" title="Samtbeutel g&Atilde;&OElig;nstig kaufen">Samtbeuteln</a> und edlen  handgeschnitzten <a href="Samtbeutel-Edelholzschatullen-Glasflaeschchen-und-Schraenke/Schatullen-aus-edlen-Hoelzern" target="_self" title="Holzschatullen handgeschnitzt">Holzschatullen</a>. Mit  &uuml;ber 30 verschiedenen <a href="Kartenlegen-Tarot-Lenormand-Wahrsagekarten-Engelkarten/Lenormandkarten-Lenormand-Wahrsagekarten" target="_self" title="Lenormandkarten online kaufen">Lenormandkarten</a> Versionen, einigen <a href="Kartenlegen-Tarot-Lenormand-Wahrsagekarten-Engelkarten/Kipperkarten-Leidingkarten" target="_self" title="Kipperkarten kaufen">Kipperkarten</a> und hunderten von  teilweise seltenen und antiken Tarot haben wir alles f&uuml;r die Kartenlegerin und  den Kartenleger parat. Edle <a href="Raeucherwerk-Weihrauch-Harze-Kraeuter" target="_self" title="R&Atilde;&euro;uchern mit R&Atilde;&euro;ucherharzen">R&auml;ucherharze</a> und  Kr&auml;uter, R&auml;ucherkegel, naturreine <a href="Aetherische-Oele-Ritual-Oele" target="_self" title="&Atilde;&bdquo;therische &Atilde;&ndash;le kaufen">&auml;therische &Ouml;le</a> und <a href="Aetherische-Oele-Ritual-Oele/Ritual-Oele" target="_self" title="Ritual&Atilde;&para;le">Ritual&ouml;le</a>, immer frisch durch schnellen  Warenumschlag...und nun w&uuml;nschen wir Ihnen viel Spa&szlig; beim St&ouml;bern und Einkaufen!
</div>

<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
        <a href="Raeucherungen-Raeucherzubehoer/Raeucher-Buecher"><img src="slider/images/bild_4.jpg" alt="Schutzengeltag" border="0" title= "Wir empfehlen: Psychologie des R&auml;ucherns - Die starke Wirkung subtiler D&uuml;fte!" /></a>
        <a href="Duftlampen-Aromalampen-Duft-Drops/Duftlampen"><img src="slider/images/bild_15.jpg" alt="Duftlampen" border="0" title="Die Herbstzeit ist eine wunderbare Zeit f&uuml;r Duftlampen!" /></a>
        <a href="Wahrsagen-Orakel/Kristallkugeln-Seherkugeln-Kristallkugelhalter"><img src="slider/images/bild_13.jpg" alt="" title="Jahrhundertelange Tradition - Das Hellsehen in einer Kristallkugel!" /></a>
        <a href="Edelsteinwasser-Wasserbelebung/Edelsteinwasser-Wasserbelebung-Edelstein-Mischungen"><img src="slider/images/bild_14.jpg" alt="" border="0" title="G&ouml;nnen Sie sich was! Erleben Sie eine wunderbare Edelsteinwasser - Erfrischung" /></a>
    </div>
</div>    
    


<!--                
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tbody>
                    <tr> 
                      <td width="185" valign="top"><a href="Doreen-Virtue-Produkte" target="_parent"><img src="templates/opsedi_1/img/doreen_virtue_1.jpg" alt="Doreen Virtue Produkte" width="151" height="220" border="0"></a></td>
                      <td valign="top"><font color="#000000"><strong>Unsere Empfehlung 
                        f&uuml;r Sie!</strong> <strong>Produkte von Doreen Virtue. 
                        </strong><br>
                        <br>
                        Doreen Virtue ist Psychologin und Familientherapeutin.<br>
                        Sie stammt aus einer hellseherisch begabten Familie und 
                        nutzte schon als Kind ihren <em>sechsten Sinn</em> zur 
                        Kommunikation mit ihren <em>unsichtbaren Freunden</em>. 
                        In der von ihr entwickelten Engeltherapie verbindet sie 
                        ihre Kompetenz als Psychologin mit ihren spirituellen 
                        F&auml;higkeiten.<br>
                        Doreen Virtue lebt in Kalifornien und gibt weltweit regelm&auml;&szlig;ig 
                        Workshops, in denen sie ihre Engeltherapie unterrichtet. 
                        </font></td>
                    </tr>
                    <tr> 
                      <td colspan="2" valign="top"><font color="#000000"><strong><br>
                        Ihre B&uuml;cher, Kartendecks und CDs haben sie zur bekanntesten 
                        Engel-Autorin in Deutschland gemacht mit einer Gesamtauflage 
                        von &uuml;ber einer Million.<br>
                        <br>
                        <br>
                        </strong></font></td>
                    </tr>
                  </tbody>
                </table>
                </span> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="33%" valign="bottom">
<div align="center"><a href="Doreen-Virtue-Produkte/Doreen-Virtue-CDs" target="_parent"><img src="templates/opsedi_1/img/doreen_virtue_cds_1.jpg" alt="CD´s" width="136" height="150" border="0"></a></div></td>
                    <td width="33%" valign="bottom">
<div align="center"><a href="Doreen-Virtue-Produkte/Doreen-Virtue-Buecher" target="_parent"><img src="templates/opsedi_1/img/doreen_virtue_bucher_1.jpg" alt="Buecher" width="138" height="172" border="0"></a></div></td>
                    <td width="33%" valign="bottom">
<div align="center"><a href="Doreen-Virtue-Produkte/Doreen-Virtue-Kartensets" target="_parent"><img src="templates/opsedi_1/img/doreen_virtue_karten_1.jpg" alt="Kartenset´s" width="139" height="171" border="0"></a></div></td>
                  </tr>
                </table>
                

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="middle"><strong><br>
                      &nbsp; &nbsp;Werden Sie jetzt unser FAN bei Facebook!</strong><br>
                      <br>
                      &nbsp; &nbsp;&gt; es werden regelm&auml;&szlig;ig neue Produkte 
                      vorgestellt<br>
                      &nbsp; &nbsp;&gt; Rabattaktionen nur f&uuml;r &quot;FANS&quot;<br>
                      &nbsp; &nbsp;&gt; entscheiden Sie mit, welche Produkte neu 
                      in den Shop aufgenommen werden <br>
                    </td>
                    <td valign="bottom"><div align="center"><a href="https://www.facebook.com/Esoterikshopping" target="_blank"><img src="images/facebook.jpg" width="226" height="150" border="0"></a></div></td>
                  </tr>
                </table>
                <span style="color: rgb(255, 0, 0);">
                <hr />
                </span></td>
                    </tr>
                </tbody>
            </table>

            
            <div align="center">
             Unsere Empfehlungen: <a href="https://www.foren4all.de/" target="_blank">Esoterik Forum</a> &infin; <a href="https://www.esopia.de/" target="_blank">Esopia Versand</a> &infin; <a href="https://www.sound-spirit.de/" target="_blank" title="Klangschalen">Klangschalen</a> </font> 	  </div>
-->                             

