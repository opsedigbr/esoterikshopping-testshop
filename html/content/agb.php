<div id="conditions">
    <h1>Vertragsbestimmungen</h1><br />
    <h2>&sect; 1 Anwendungsbereich</h2>
    Unsere nachstehenden Vertragsbestimmungen gelten f&uuml;r die Lieferung und den Verkauf unserer Waren gegen&uuml;ber privaten Verbrauchern und sonstigen Bestellern.<br /> 
    <br /> 
    Gegen&uuml;ber Bestellern, die keine privaten Verbraucher sind, gelten die Vertragsbestimmungen allerdings nur mit der Ma&szlig;gabe, dass das in &sect; 8 n&auml;her bezeichnete Widerrufsrecht keine Anwendung findet. Bestellungen erfolgen verbindlich per Email, Fax oder per Post. <br /><br /> 
   	<h2>&sect; 2 Zustandekommen des Vertrages</h2> 
    Sie w&auml;hlen zun&auml;chst aus unserem Online-Shop die von Ihnen gew&uuml;nschten Waren durch Einlegen in den virtuellen Warenkorb. Vor Abgabe der Bestellung haben Sie die M&ouml;glichkeit, &uuml;ber die Schaltfl&auml;che &quot;Warenkorb&quot; die von Ihnen zuvor gemachten Bestelleingaben zum Zwecke der Kontrolle und Pr&uuml;fung einzusehen. An dieser Stelle k&ouml;nnen Sie eventuelle Eingabefehler erkennen und berichtigen.<br /> 
     <br />
      Der Kauf erfolgt nach Eingabe Ihrer pers&ouml;nlichen Daten, der Wahl der Zahlungsart und Versandart durch Klicken des Buttons &quot;Kaufen&quot;. Der Eingang Ihrer Bestellung wird von uns per E-Mail best&auml;tigt. Hierdurch kommt ein rechtsg&uuml;ltiger Vertrag zwischen Ihnen und uns zustande.<br /><br /> 
     <h2>&sect; 3 Verf&uuml;gbarkeit des Vertragstextes</h2>
      Der Vertragstext steht ausschlie&szlig;lich in deutscher Sprache zur Verf&uuml;gung. <br /> 
      <br />
      Wir speichern den Vertragstext und senden Ihnen die Bestelldaten und unsere AGB per EMail zu. Die AGB k&ouml;nnen Sie jederzeit auch <a href="../content/Unsere-AGBs.html" target="_parent">hier</a> einsehen. Ihre vergangenen Bestellungen k&ouml;nnen Sie in unserem <a href="../account.php" target="_parent">Kunden LogIn-Bereich</a> einsehen.<br />
      <br />
<h2>&sect; 4 Beschaffenheit und wesentliche Merkmale der Ware</h2> 
      Die Beschaffenheit, die Merkmale und die Qualit&auml;t der gelieferten Ware ergeben sich aus den Beschaffenheitsangaben in den jeweiligen Produktbeschreibungen. Diese sind auch unter unserer Internetadresse www.Esoterikshopping.de abrufbar.<br /><br /> 
       <h2>&sect; 5 Nichtverf&uuml;gbarkeit der bestellten Ware</h2> 
       Der Vertragsschluss erfolgt unter dem Vorbehalt der Selbstbelieferung; der Vorbehalt gilt zu Lasten von Verbrauchern nur, sofern wir ein konkretes Deckungsgesch&auml;ft abgeschlossen haben und von dem Lieferanten ohne eigenes Verschulden nicht beliefert werden. Wir werden den Kunden unverz&uuml;glich &uuml;ber die Nichtverf&uuml;gbarkeit der Ware informieren und etwaige erbrachte Gegenleistungen des Kunden unverz&uuml;glich erstatten.<br /><br /> 
        <h2>&sect; 6 Preise, Liefer- und Zahlungsmodalit&auml;ten</h2> 
        Es besteht f&uuml;r den Endkunden kein Mindestbestellwert.<br /> 
        S&auml;mtliche angegebenen Preise verstehen sich inklusive der zum Zeitpunkt der Leistungsausf&uuml;hrung g&uuml;ltigen gesetzlichen Umsatzsteuer.<br /> <br /> Die Lieferung erfolgt zuz&uuml;glich Verpackungs- und Versandkosten an die von Ihnen in Ihrer Bestellung angegebene Lieferadresse.<br /> <br /> 
    <em>Endkunde: (EUR entspricht Euro)<br />
  </em>    <strong>Die Versandkosten betragen innerhalb von <span style="color:red;">Deutschland pauschal 3,90 EUR</span>, ab einem Bestellwert von 49,00 EUR erfolgt die Lieferung innerhalb Deutschlands versandkostenfrei.</strong><br />
    Die Versandkosten betragen f&uuml;r Bulgarien 	12,90 EUR <br />
  Die Versandkosten betragen f&uuml;r Belgien 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r D&auml;nemark 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Estland 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Finnland 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Frankreich 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Griechenland 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Grossbritanien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Irland 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Italien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Kroatien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Lettland 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Litauen 	12,90 EUR <br />
    <!--Die Versandkosten betragen f&uuml;r Italien 	12,90 EUR <br />-->
    Die Versandkosten betragen f&uuml;r Luxemburg 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Malta 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Niederlande 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r &Ouml;sterreich 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Polen 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Portugal 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Rum&auml;nien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Russland 	29,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Schweden 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Schweiz 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Slowakei 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Slowenien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Spanien 	12,90 EUR <br />
    <!--Die Versandkosten betragen f&uuml;r Spanien 	12,90 EUR <br />-->
    Die Versandkosten betragen f&uuml;r Thailand 	29,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Tschechien 	11,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Ungarn 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r USA 	29,90 EUR<br />
    Die Versandkosten betragen f&uuml;r Zypern 	12,90 EUR <br />
<br />
    <em>F&uuml;r H&auml;ndler: (EUR entspricht Euro)</em><br />
    Die Versandkosten betragen innerhalb von Deutschland f&uuml;r alle Gr&ouml;&szlig;en und Gewichtsklassen: 6.90 EUR.<br />
    Wir versenden in folgende L&auml;nder: &Ouml;sterreich, Belgien, Holland, Luxemburg, Tschechien, D&auml;nemark: Alle Gewichtsklassen (versichert): 8.90 EUR<br />
    Versandkosten England, Frankreich, <!--Italien,--> Polen: 
    Alle Gewichtsklassen (versichert): 12.90 EUR<br />
    Wir versenden in folgende L&auml;nder: Schweiz, Schweden, slowakische Republik, Slowenien, Ungarn, Ru&szlig;land: 
    Alle Gewichtsklassen (versichert): 24.00 EUR<br />
    Au&szlig;erhalb Europa: up to 500 grams: 10,90 Euro, 
    500 - 1000 grams: 14,90 Euro,
    1000 - 2000 grams 26,90 Euro.
    <br /><br />
    Wie k&ouml;nnen Sie bezahlen?<br />
    - per Vorkasse/&Uuml;berweisung (die Konto-Nr. wird Ihnen nach Ihrer Bestellung per Mail mitgeteilt). <br />
    - per Rechnung (ab einem Bestellwert von 15,00 Euro nur innerhalb Deutschland und nicht zur Verf&uuml;gung bei Gutscheinen)<br />
    - per Paypal<br />
	  - per Kreditkarte<br />
    - per Sofort&uuml;berweisung<br />
  <br /> 
<h2>&sect; 7 Gew&auml;hrleistung</h2> 
Unsere Gew&auml;hrleistungspflicht, das hei&szlig;t Pflichten bei der Lieferung fehlerhafter Waren, richtet sich nach den gesetzlichen Bestimmungen.<br /> <br /> Etwaige Beanstandungen sind zu richten an:<br />
     Esoterikshopping
    <br />
OPSEDI GbR<br />
Auslieferzentrum<br />
Am Bach 9<br />
93349 Mindelstetten<br />
email: info@Esoterikshopping.de<br />
Fax: 08404 939 265 <br /><br /> 
<h2>&sect; 8 Widerrufsrecht</h2>
<strong>Widerrufsbelehrung</strong><br /><br />
<strong>Widerrufsrecht</strong><br />
  Sie k&ouml;nnen Ihre Vertragserkl&auml;rung innerhalb von 14 Tagen ohne Angabe von Gr&uuml;nden in Textform
  (z.B. Brief, Fax, E-Mail) oder - wenn Ihnen die Sache vor Fristablauf &uuml;berlassen wird - auch durch
  R&uuml;cksendung der Sache widerrufen. Die Frist beginnt nach Erhalt dieser Belehrung in Textform, jedoch
  nicht vor Eingang der Ware beim Empf&auml;nger (bei der wiederkehrenden Lieferung gleichartiger. Waren nicht vor Eingang der ersten Teillieferung) und auch nicht vor Erf&uuml;llung unserer Informationspflichten
  gem&auml;&szlig; Artikel 246 &sect; 2 in Verbindung mit &sect; 1 Absatz 1 und 2 EGBGB sowie unserer Pflichten
  gem&auml;&szlig; &sect; 312g Absatz 1 Satz 1 BGB in Verbindung mit Artikel 246 &sect; 3 EGBGB. Zur Wahrung der Widerrufsfrist
  gen&uuml;gt die rechtzeitige Absendung des Widerrufs oder der Sache. Der Widerruf ist zu richten
  an. 
  <br />
  <br />
  OPSEDI GbR<br />
  Auslieferzentrum</span><br />
  Am Bach 9<br />
    93349 Mindelstetten<br />
    email: info@Esoterikshopping.de<br />
    Fax: 08404 939 265<br />
    <br />
    <strong>Widerrufsfolgen</strong><br />
  Im Falle eines wirksamen Widerrufs sind die beiderseits empfangenen Leistungen zur&uuml;ckzugew&auml;hren
  und ggf. gezogene Nutzungen (z.B. Zinsen) herauszugeben. K&ouml;nnen Sie uns die
  empfangene Leistung sowie Nutzungen (z.B. Gebrauchsvorteile) nicht oder teilweise nicht oder
  nur in verschlechtertem Zustand zur&uuml;ckgew&auml;hren beziehungsweise herausgeben, m&uuml;ssen Sie
  uns insoweit Wertersatz leisten. F&uuml;r die Verschlechterung der Sache und f&uuml;r gezogene Nutzungen
  m&uuml;ssen Sie Wertersatz nur leisten, soweit die Nutzungen oder die Verschlechterung
  auf einen Umgang mit der Sache zur&uuml;ckzuf&uuml;hren ist, der &uuml;ber die Pr&uuml;fung der Eigenschaften
  und der Funktionsweise hinausgeht. Unter &quot;Pr&uuml;fung der Eigenschaften und der Funktionsweise&quot; versteht man das Testen und Ausprobieren der jeweiligen Ware, wie es etwa im Ladengesch&auml;ft
  m&ouml;glich und &uuml;blich ist. Paketversandf&auml;hige Sachen sind auf unsere Gefahr zur&uuml;ckzusenden.
  Sie haben die regelm&auml;&szlig;igen Kosten der R&uuml;cksendung zu tragen, wenn die gelieferte
  Ware der bestellten entspricht und wenn der Preis der zur&uuml;ckzusendenden Sache einen Betrag
  von 40 Euro nicht &uuml;bersteigt oder wenn Sie bei einem h&ouml;heren Preis der Sache zum Zeitpunkt
  des Widerrufs noch nicht die Gegenleistung oder eine vertraglich vereinbarte Teilzahlung
  erbracht haben. Anderenfalls ist die R&uuml;cksendung f&uuml;r Sie kostenfrei. Nicht paketversandf&auml;hige
  Sachen werden bei Ihnen abgeholt. Verpflichtungen zur Erstattung von Zahlungen m&uuml;ssen innerhalb
  von 30 Tagen erf&uuml;llt werden. Die Frist beginnt f&uuml;r Sie mit der Absendung Ihrer Widerrufserkl&auml;rung
  oder der Sache, f&uuml;r uns mit deren Empfang.<br />
  <br />
  <strong>  Ende der Widerrufsbelehrung</strong></strong><br />
  <br />
  <strong>8a. R&uuml;cksendekosten bei Aus&uuml;bung des Widerrufsrechtes<br />
  </strong>  Machen Sie von Ihrem gesetzlichen Widerrufsrecht Gebrauch (siehe Widerrufsbelehrung),
  haben Sie die regelm&auml;&szlig;igen Kosten der R&uuml;cksendung zu tragen, wenn die gelieferte Ware
  der bestellten entspricht und wenn der Preis der zur&uuml;ckzusendenden Sache einen Betrag
  von 40,- Euro nicht &uuml;bersteigt oder wenn Sie bei einem h&ouml;heren Preis der Sache zum Zeitpunkt
  des Widerrufs noch nicht die Gegenleistung oder eine vertraglich vereinbarte Teilzahlung
  erbracht haben. Anderenfalls ist die R&uuml;cksendung f&uuml;r Sie kostenfrei.<br />
  <br />
<h2>&sect; 9 Eigentumsvorbehalt</h2>
 Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.<br /><br /> 
 <h2>&sect; 10 Haftung</h2>
Bei einer nicht vors&auml;tzlichen und nicht grob fahrl&auml;ssigen Verletzung einer Vertragspflicht durch uns ist die Haftung auf typische, voraussehbare Sch&auml;den begrenzt.<br /> 
<br /> Die vorstehende Haftungsbeschr&auml;nkung gilt nicht, soweit eine Verletzung von Leben, K&ouml;rper oder Gesundheit vorliegt. Die Haftungsbeschr&auml;nkung gilt ferner dann nicht, wenn der Besteller Anspr&uuml;che aus Produkthaftung geltend macht.<br /><br /> 
<h2>&sect; 11 Anwendbares Recht</h2>
F&uuml;r s&auml;mtliche Lieferungen gilt ausschlie&szlig;lich das Recht der Bundesrepublik Deutschland.<br /><br /> 
<h2>&sect; 12 Gerichtsstand und sonstige Bestimmungen</h2>
Als Gerichtsstand wird unserer Sitz vereinbart, wenn Sie Kaufmann sind oder keinen allgemeinen Gerichtsstand in der Bundesrepublik Deutschland haben.<br /> 
<br /> Sollten einzelne Bestimmungen unwirksam sein, so f&uuml;hrt dies nicht zur Unwirksamkeit des gesamten Vertrags sowie der &uuml;brigen vertraglichen Bestimmungen. Die unwirksame Bestimmung wird durch die entsprechenden gesetzlichen Vorgaben ersetzt.<br /><br />
</div>    
