<?php
/*------------------------------------------------------------
$Id: ot_payment.php,v 1.5 2007/09/04 16:58:36 Estelco Exp $

Estelco - Ebusiness & more
http://www.estelco.de

Copyright (C) 2007 Estelco

based on:
Andreas Zimmermann / IT eSolutions http://www.it-esolutions.de

Copyright (C) 2004 IT eSolutions
--------------------------------------------------------------
Released under the GNU General Public License
------------------------------------------------------------*/

$num = 3;

define('MODULE_ORDER_TOTAL_PAYMENT_TITLE', 'Vorkasse Rabatt');
define('MODULE_ORDER_TOTAL_PAYMENT_DESCRIPTION', 'Rabatt f&uuml;r Zahlungsarten');

define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_TITLE', 'Rabatt anzeigen');
define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_DESC', 'Wollen Sie den Zahlungsartenrabatt einschalten?');

define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_TITLE', 'Sortierreihenfolge');
define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_DESC', 'Anzeigereihenfolge');

for ($j=1; $j<=$num; $j++) {
    define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_TITLE', $j . '. Rabattstaffel');
    define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_DESC', 'Rabattierung (Mindestwert:Prozent)');
    define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_TITLE', $j . '. Zahlungsart');
    define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_DESC', 'Zahlungsarten, auf die Rabatt gegeben werden soll');
}

define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_TITLE', 'Inklusive Versandkosten');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_DESC', 'Versandkosten werden mit Rabattiert');

define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_TITLE', 'Inklusive Ust');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_DESC', 'Ust wird mit Rabattiert');

define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_TITLE', 'Ust Berechnung');
define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_DESC', 'erneutes berechnen der Ust Summe');

define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_TITLE', 'Erlaubte Zonen');
define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_DESC' , 'Geben Sie <b>einzeln</b> die Zonen an, welche f&uuml;r dieses Modul erlaubt sein sollen. (z.B. AT,DE (wenn leer, werden alle Zonen erlaubt))');

define('MODULE_ORDER_TOTAL_PAYMENT_DISCOUNT', 'Zahlart-Rabatt');
define('MODULE_ORDER_TOTAL_PAYMENT_FEE', 'Zahlart-Aufschlag');

define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_TITLE','Steuerklasse');
define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_DESC','Die Steuerklasse spielt keine Rolle und dient nur der Vermeidung einer Fehlermeldung.');

define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_TITLE','Mehrfachberechnung');
define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_DESC','Sollten Mehrfachberechnungen m&ouml;glich sein? Wenn nein, wird nach dem ersten passenden Rabatt abgebrochen.');
?>