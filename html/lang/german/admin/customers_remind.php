<?php
/* --------------------------------------------------------------
   $Id: price_alarm.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(whos_online.php,v 1.7 2002/03/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (whos_online.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'Kundenerinnerungen bei erneutem Eintreffen von Artikeln');

define('TABLE_HEADING_CUSTOMER', 'Kunde');
define('TABLE_HEADING_CUSTOMER_MAIL', 'eMail-Adresse');
define('TABLE_HEADING_PRODUCT', 'Produkt');
define('TABLE_HEADING_DATE_ADDED', 'Hinzugef&uuml;gt am');

define('CUSTOMERS_ADVERTISING_DELETE_CONFIRM', 'Sind Sie sicher, dass Sie diese Kundenwerbung unwiderruflich l&ouml;schen wollen?');

?>