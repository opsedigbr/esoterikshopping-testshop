<?php
/*
 *  Modul: Products Gift v1.0
 *  created by Sergej Stroh.
 *	www.southbridge.de
 *	28.11.2005
 *	Copyright 2005 by Sergej Stroh.
 */

  define('HEADING_TITLE','Produktgeschenk');
  define('CONTENT_NOTE','<p>Legen Sie fest ab welcher Summe der jeweilige Artikel als Gratis/Geschenk im Warenkorb als Zugabe angezeigt werden soll.</p>');

  define('PRODUCTS_GIFT_ID','ID');
	define('PRODUCTS_NAME','Produkt:');
	define('PRODUCTS_GIFT_SUM','Gratis ab:');
	define('PRODUCTS_GIFT_SUM_UPDATE','Update');
	define('PRODUCTS_GIFT_ACTION','Aktion');
	define('PRODUCTS_GIFT_DELETE','Entfernen');
	
	define('PRODUCTS_GIFT_GROUP','Sichtbar f�r Kundengruppen: ');

 ?>