<?php
/* --------------------------------------------------------------
   $Id: cvs_backend.php

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce

   Released under the GNU General Public License
   --------------------------------------------------------------*/


   define('TITLE','CSV Backend');

   define('IMPORT','Import');
   define('EXPORT','Export');
   define('UPLOAD','Datei auf Server laden');
   define('SELECT','Zu importierende Datei auswaehlen und Import durchfuehren (/import Verzeichnis)');
   define('SAVE','Auf Server Speichern (/export Verzeichnis)');
   define('LOAD','Datei an Browser senden');
   define('CSV_TEXTSIGN_TITLE','Texterkennungszeichen');
   define('CSV_TEXTSIGN_DESC','zb. "');
   define('CSV_SEPERATOR_TITLE','Trennzeichen');
   define('CSV_SEPERATOR_DESC','zb. ;');
   define('COMPRESS_EXPORT_TITLE','Kompression');
   define('COMPRESS_EXPORT_DESC','Kompression der exportierten Daten');
   define('CSV_SETUP','Einstellungen');
   define('TEXT_IMPORT','');
   define('TEXT_PRODUCTS','Produkte');
   define('TEXT_EXPORT','Exportierte Datei wird im /export Verzeichnis gespeichert');
   
    // ImExport_2 Modul
	define('TEXT_CATEGORIES', 'Kategorien');
	define('TEXT_PROD2CAT', 'Produktzuordnungen');
	define('TEXT_EXPORT','Exportierte Datei wird im /export Verzeichnis gespeichert');
	define('SELECT_EXPORT', 'Bitte w&auml;hlen sie die Art des Exports');
	define('CSV_TIME_LIMIT_TITLE', 'Zeitlimit');
	define('CSV_TIME_LIMIT_DESC', 'Zeit in Sekunden, nach der sich das Skript selbst neu l�dt, um Timeouts zu verhindern (z.B. Serverlimit 30 sek empfohlener Wert: 28)');
	define('CSV_DEFAULT_ACTION_TITLE', 'Standardaktion');
	define('CSV_DEFAULT_ACTION_DESC', 'CSV-Feld "action" mittels dieser Einstellung vorbelegen.');
	
	//Viacarta Export
	define('EXPORT_VIACARTA', 'Export Viacarta');

?>