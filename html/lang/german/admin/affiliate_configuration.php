<?php
/*------------------------------------------------------------------------------
   $Id: affiliate_configuration.php,v 1.1 2003/12/21 20:13:07 hubi74 Exp $

   XTC-Affiliate - Contribution for XT-Commerce http://www.xt-commerce.com
   modified by http://www.netz-designer.de

   Copyright (c) 2003 netz-designer
   -----------------------------------------------------------------------------
   based on:
   (c) 2003 OSC-Affiliate
   http://oscaffiliate.sourceforge.net/

   Contribution based on:

   osCommerce, Open Source E-Commerce Solutions
   http://www.oscommerce.com

   Copyright (c) 2002 - 2003 osCommerce

   Released under the GNU General Public License
   ---------------------------------------------------------------------------*/

define('AFFILIATE_EMAIL_ADDRESS_TITLE', 'E-Mail Adresse');
define('AFFILIATE_EMAIL_ADDRESS_DESC', 'Die e-Mail Adresse f�r das Affiliate Programm &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/email_adresse.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PERCENT_TITLE', 'Affiliate Pay Per Sale %-Rate');
define('AFFILIATE_PERCENT_DESC', 'Prozentuale Rate f�r das Pay Per Sale Affiliate Programm. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/pps_rate.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_THRESHOLD_TITLE', 'Auszahlungsgrenze');
define('AFFILIATE_THRESHOLD_DESC', 'Untere Grenze f�r die Auszahlung an Affiliates &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/min_provision.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_COOKIE_LIFETIME_TITLE', 'Cookie Lifetime');
define('AFFILIATE_COOKIE_LIFETIME_DESC', 'Wie lange (in Sekunden) der Lead eines Affiliates g�ltig bleibt. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/cookielifetime.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_BILLING_TIME_TITLE', 'Abrechnungszeit');
define('AFFILIATE_BILLING_TIME_DESC', 'Die Zeit, die zwischen einer erfolgreichen Bestellung und der Gutschrift beim Affiliate vergehen soll. Ben�tigt falls Bestellungen R�ckbelastet werden. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/time_til_provision.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_TITLE', 'Minimum Order Status');
define('AFFILIATE_PAYMENT_ORDER_MIN_STATUS_DESC', 'Der Status einer Berstellung, ab wann sie als beglichen gilt. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/min_order_status.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_CHECK_TITLE', 'Affiliate Scheck');
define('AFFILIATE_USE_CHECK_DESC', 'Affilaites per Scheck auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/affiliate_scheck.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_PAYPAL_TITLE', 'Affiliate Paypal');
define('AFFILIATE_USE_PAYPAL_DESC', 'Affiliates per Paypal auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/affiliate_paypal.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_USE_BANK_TITLE', 'Affiliate Bank');
define('AFFILIATE_USE_BANK_DESC', 'Affiliates per �berweisung auszahlen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/affiliate_bank.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILATE_INDIVIDUAL_PERCENTAGE_TITLE', 'Individueller Prozentsatz');
define('AFFILATE_INDIVIDUAL_PERCENTAGE_DESC', 'Prozentsatz individuell per Affiliate festlegen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/individual_percent.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILATE_USE_TIER_TITLE', 'Klassensystem');
define('AFFILATE_USE_TIER_DESC', 'Multiklassen Affiliate Programm verwenden &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/klassensystem.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_ALLOWED_TITLE', 'Standard f�r Subaffiliates');
define('AFFILIATE_TIER_ALLOWED_DESC', 'D�rfen neue Affiliates Standardm�ssig Subaffiliates haben. &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/subs_standard.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_LEVELS_TITLE', 'Anzahl Klassen');
define('AFFILIATE_TIER_LEVELS_DESC', 'Anzahl der beim Klassensystem zu verwendenden Klassen &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/tier_levels.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
define('AFFILIATE_TIER_PERCENTAGE_TITLE', 'Prozents�tze f�r Klassensystem');
define('AFFILIATE_TIER_PERCENTAGE_DESC', 'Individuelle Prozents�tze f�r die Unterklassen<br>Beispiel 8.00;5.00;1.00 &nbsp;&nbsp;&nbsp; <a href="http://www.netz-designer.de/tier_percentages.html" target="_blank"><img src="images/ask.gif" align="center" border="0"></a>');
?>
