<?php
/* --------------------------------------------------------------
   $Id: products_musik_hersteller.php 899 2005-04-29 02:40:57Z hhgag $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   --------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(orders_status.php,v 1.7 2002/01/30); www.oscommerce.com 
   (c) 2003	 nextcommerce (orders_status.php,v 1.4 2003/08/14); www.nextcommerce.org

   Released under the GNU General Public License 
   --------------------------------------------------------------*/

define('HEADING_TITLE', 'MUSIK: Hersteller');

define('TABLE_HEADING_PRODUCTS_MUSIK_HERSTELLER', 'MUSIK: Hersteller');
define('TABLE_HEADING_ACTION', 'Aktion');

define('TEXT_INFO_EDIT_INTRO', 'Bitte f&uuml;hren Sie alle notwendigen &Auml;nderungen durch');
define('TEXT_INFO_PRODUCTS_MUSIK_HERSTELLER_NAME', 'MUSIK: Hersteller-Text:');
define('TEXT_INFO_INSERT_INTRO', 'Bitte geben Sie die Bezeichnung der MUSIK: Hersteller ein');
define('TEXT_INFO_DELETE_INTRO', 'Sind Sie sicher, dass Sie diese MUSIK: Hersteller l&ouml;schen m&ouml;chten?');
define('TEXT_INFO_HEADING_NEW_PRODUCTS_MUSIK_HERSTELLER', 'Neue MUSIK: Hersteller');
define('TEXT_INFO_HEADING_EDIT_PRODUCTS_MUSIK_HERSTELLER', 'MUSIK: Hersteller bearbeiten');
define('TEXT_INFO_HEADING_DELETE_PRODUCTS_MUSIK_HERSTELLER', 'MUSIK: Hersteller l&ouml;schen');

define('ERROR_REMOVE_DEFAULT_PRODUCTS_MUSIK_HERSTELLER', 'Fehler: Standard MUSIK: Hersteller kann nicht gel&ouml;scht werden. Bitte definieren Sie eine neue MUSIK: Hersteller und wiederholen Sie den Vorgang.');
?>