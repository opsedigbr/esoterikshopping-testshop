<?php
/*
lang/german/admin/news.php	
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/
define('HEADING_TITLE', 'News Manager');
define('HEADING_CONTENT', 'News');
define('TABLE_HEADING_CHECK_ID', '[!]');
define('TABLE_HEADING_ID', 'Database ID: ');
define('TABLE_HEADING_TITLE', 'Titel');
define('TABLE_HEADING_SUBTITLE', 'Untertitel');
define('TABLE_HEADING_DATE', 'Datum');
define('TABLE_HEADING_EDIT', 'Aktionen');
define('TABLE_HEADING_OPTIONS', 'Optionen');
define('TABLE_HEADING_OPTIONS_OFFLINE', 'News Offline');
define('TABLE_HEADING_OPTIONS_DATE', 'Datum verbergen');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_NO_NEWS', 'Noch keine News vorhanden ...');
define('TABLE_HEADING_DELETE', 'Wollen Sie News wirklich l&ouml;schen?');
define('NEWS_IMAGE_SUBMIT_NEW', 'Neu');
define('NEWS_IMAGE_SUBMIT_CONFIG', 'Einstellungen');
define('TABLE_HEADING_IMAGE', 'Bild');
define('TABLE_HEADING_TEXT_SHORT', 'Einleitung f&uuml;r die Startseite');
define('TABLE_TEXT_SHORT_MAX', '(maximal 250 Zeichen)');
define('TABLE_HEADING_TEXT', 'Newstext');
define('SELECTION_EDIT', 'Bearbeiten');
define('SELECTION_OFFLINE', 'Offline stellen');
define('SELECTION_ONLINE', 'Online stellen');
define('SELECTION_DEL','L&ouml;schen');
define('TABLE_HEADING_CONTENT_ACTION', 'Aktion');
define('CONTENT_NOTE', 'Halten Sie Ihre Kunden stets auf dem Laufenden.');
?>