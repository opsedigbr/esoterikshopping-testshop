<?php
/*
lang/german/admin/news.php	
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/ 
define('HEADING_TITLE', 'News Manager');
define('HEADING_CONTENT', 'News');
define('TABLE_HEADING_CHECK_ID', '[!]');
define('TABLE_HEADING_ID', 'Database ID: ');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_SUBTITLE', 'Subtitle');
define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_EDIT', 'Actions');
define('TABLE_HEADING_OPTIONS', 'Options');
define('TABLE_HEADING_OPTIONS_OFFLINE', 'News Offline');
define('TABLE_HEADING_OPTIONS_DATE', 'Hide Date?');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_NO_NEWS', 'Still no News available ...');
define('TABLE_HEADING_DELETE', 'Want you News to really delete?');
define('NEWS_IMAGE_SUBMIT_NEW', 'New');
define('NEWS_IMAGE_SUBMIT_CONFIG', 'Configurations');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_TEXT_SHORT', 'Short Text for Startsite');
define('TABLE_TEXT_SHORT_MAX', '(maximal 250 Indication)');
define('TABLE_HEADING_TEXT', 'Newstext');
define('SELECTION_EDIT', 'Edit News');
define('SELECTION_OFFLINE', 'News Offline');
define('SELECTION_ONLINE', 'News Online');
define('SELECTION_DEL', 'Delete');
define('TABLE_HEADING_CONTENT_ACTION', 'Action');
define('CONTENT_NOTE', 'Always hold your customers up to date.');  
?>