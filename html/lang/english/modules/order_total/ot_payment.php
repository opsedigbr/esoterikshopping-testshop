<?php
/*------------------------------------------------------------
$Id: ot_payment.php,v 1.5 2007/09/04 16:58:36 Estelco Exp $

Estelco - Ebusiness & more
http://www.estelco.de

Copyright (C) 2007 Estelco

based on:
Andreas Zimmermann / IT eSolutions http://www.it-esolutions.de

Copyright (C) 2004 IT eSolutions
--------------------------------------------------------------
Released under the GNU General Public License
------------------------------------------------------------*/

$num = 3;

define('MODULE_ORDER_TOTAL_PAYMENT_TITLE', 'Payment Type Discount');
define('MODULE_ORDER_TOTAL_PAYMENT_DESCRIPTION', 'Payment Type Discount');

define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_TITLE', 'Display Discount');
define('MODULE_ORDER_TOTAL_PAYMENT_STATUS_DESC', 'Do you want to enable the Order Discount');

define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_TITLE', 'Sort Order');
define('MODULE_ORDER_TOTAL_PAYMENT_SORT_ORDER_DESC', 'Sort order of display');

for ($j=1; $j<=$num; $j++) {
    define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_TITLE', $j . '. Discount Percentage');
    define('MODULE_ORDER_TOTAL_PAYMENT_PERCENTAGE' . $j . '_DESC', 'Amount of Discount(countries|value:percentage&fee)');
    define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_TITLE', $j . '. Payment Type');
    define('MODULE_ORDER_TOTAL_PAYMENT_TYPE' . $j . '_DESC', 'Payment Type to get discount');
}

define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_TITLE', 'Include Shipping');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_SHIPPING_DESC', 'Include Shipping in calculation');

define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_TITLE', 'Include Tax');
define('MODULE_ORDER_TOTAL_PAYMENT_INC_TAX_DESC', 'Include Tax in calculation');

define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_TITLE', 'Calculate Tax');
define('MODULE_ORDER_TOTAL_PAYMENT_CALC_TAX_DESC', 'Re-calculate Tax on discounted amount');

define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_TITLE', 'Allowed Zones');
define('MODULE_ORDER_TOTAL_PAYMENT_ALLOWED_DESC' , 'Please enter the zones <b>separately</b> which should be allowed to use this modul (e. g. AT,DE (leave empty if you want to allow all zones))');

define('MODULE_ORDER_TOTAL_PAYMENT_DISCOUNT', 'Discount');
define('MODULE_ORDER_TOTAL_PAYMENT_FEE', 'Fee');

define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_TITLE','Tax Class');
define('MODULE_ORDER_TOTAL_PAYMENT_TAX_CLASS_DESC','Use the following tax class on the low order fee.');

define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_TITLE','Multiple calculation');
define('MODULE_ORDER_TOTAL_PAYMENT_BREAK_DESC','Should multiple calculation be possible? If false calculation will be stopped after the first fitting setting.');
?>