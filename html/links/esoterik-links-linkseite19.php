<hr>
          <div align="center">...und hier k&ouml;nnte Ihr Link stehen... <br>
            <br>
            Die Moqui Marble sind geborene Steine. Seit Jahrhunderten Heiligtum der
Indianer von Nordamerika. Man nennt sie deshalb lebende Steine, weil
sie nicht wie herk&ouml;mmliche Steine gefunden werden, sondern sie werden an einer bestimmten Stelle auf unserer Erde geboren und treten an die Oberfl&auml;che. Wie bei allen Lebewesen gibt es auch bei den Moquis m&auml;nnliche und weibliche Steine. Der Name kommt aus dem indianischen und bedeutet soviel wie treuer Liebling. Jede indianische Familie besitzt auch heutzutage ein solches Paar.<br>
Die energiereichen Schwingungen und Kr&auml;fte der Moqui Marbles sind mit den Kr&auml;ften au&szlig;erirdischer Steine zu vergleichen. Nur Meteoriten, Tektite und Moldavite weisen &auml;hnlich hohe Frequenzen auf!<br>
<br>
N&auml;here Informationen &uuml;ber die Herkunft, Geologie und Wirkungsweise der Moquis finden Sie auf unserer Partnerseite: <a href="http://www.moqui-marbles.com" target="_blank">www.Moqui-Marbles.com</a>.<br>
Der Ursprung der Moqui-Marble ist Utha, USA, dem heiligen Land der Moqui-Indianer.<br>
Moqui-Marbles f&ouml;rdern angeblich die Eisenaufnahme und Blutbildung sowie Kreativit&auml;t, Willenkraft und seelische Stabilit&auml;t.<br>
Unsere Moqui-Mable erhalten Sie immer als Paar. Ein eher runder Stein und ein flacher, Ufo-f&ouml;rmiger Stein.<br>
Zu allen Moqui-Marbles bekommen Sie eine deutsche Beschreibung der Steine mit farbiger Abbildung und eine Best&auml;tigung &uuml;ber die Herkunft von den Originalfundst&auml;tten sowie Hinweise &uuml;ber die Zusammensetzung und Verwendung.<br>
<br>
Moqui-Marbles - Die indianischen Heilsteine. Sie sind ein Mysterium und Wunderwerk, was den Indianern seit hunderten von Jahren schon bekannt ist.<br>
<br>
Die Moqui-Marbles werden weltweit nur in einem Land, einer Region gefunden: <br>
In einem Indianer Reservat im s&uuml;dlichen Utah, Nordamerika. Besser gesagt, sie werden nicht gefunden, sie werden geboren. Sie entstehen nicht wie normale Steine, die von zersplitterten Felsen stammen und &uuml;ber Millionen von Jahren durch Witterrungseinfl&uuml;sse oder Ozeane glattgeschliffen wurden, sondern sie werden regelrecht aus dem Scho&szlig; der Erde geboren. <br>
<br>
Der Preis bezieht sich auf ein Paar; einen m&auml;nnlichen (Ufo-f&ouml;rmig) und einen weiblichen (runden).<br>
<br>
Gr&ouml;&szlig;e: ca. 5 cm<br>
Gewicht: ca. 100g / St&uuml;ck<br>
<br>
Zu allen Moqui-Marbles bekommen Sie eine deutsche Beschreibung der Steine mit farbiger Abbildung und eine Best&auml;tigung &uuml;ber die Herkunft von den Originalfundst&auml;tten sowie Hinweise &uuml;ber die Zusammensetzung und Verwendung.<br>
Da die Moquis Unikate aus der Natur sind, k&ouml;nnen sie von Form und Gr&ouml;&szlig;e abweichen.
<p><b>Preis:</b> &euro;16.90 inkl. MwSt.</p>
<a href="http://www.esoterikshopping.de/view_product.php?product=moqui04" target="_blank"><img src="http://www.esoterikshopping.de/images/moqui_marbles04.jpg" alt="Moqui-Marbles XXL preiswert im Esoterik Online Shop zu kaufen - Esotherik Versand" border="0"></a><br>
            <hr></div>
