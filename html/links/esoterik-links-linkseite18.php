<hr>
          <div align="center">...und hier k&ouml;nnten Ihre Links stehen... <br>
            <br>
            Seifensteine sind viel bew&auml;hrte Therapiesteine und Massagesteine f&uuml;r herrlich entspannende Wohlf&uuml;hl-Massagen. Aber auch zur Linderung von Schmerzen und Verspannungen sollen die Seifensteine hervorragende Wirkungen entfalten.<br>
<br>
Sie sind auch zum Auflegen f&uuml;r die Chakren gedacht, oder einfach nur als sch&ouml;ne Dekoration.<br>
Die Seifensteine lassen sich durch ihre Form, die Gr&ouml;&szlig;e von ca. 50 x 70 mm und ihr Eigengewicht von ca. 120g hervorragend zum Auflegen verwenden.<br>
Auch f&uuml;r die sogenannte Hot-Stone-Massage, wo man die Steine vorher im Ofen leicht erw&auml;rmt, sind unsere Seifensteine hervorragend geeignet, oder einfach nur als Handschmeichler in der Hosentasche.<br>
<br>
<b>Seifenstein Jaspis rot, Hot Stone Massagestein</b><br>
<br>
Der rote Jaspis ist der Stein der inneren Harmonie<br>
<br>
Schon in der Antike geh&ouml;rte der rote Jaspis zu den kostbarsten Edelsteinen und verhalf zu innerer Hamonie und sollte eine beschwerdefreie Schwangerschaft beg&uuml;nstigen.<br>
<br>
Als Heilstein sollte er in keiner Sammlung fehlen, da er sehr blutstillende Eigenschaften besitzten soll und angeblich schnell Wunden schlie&szlig;en kann.<br>
<br>
Der rote Jaspis wirkt wie ein Blitzableiter und l&auml;&szlig;t negative Einfl&uuml;sse anderer Menschen und Strahlungen von uns abprallen. Seine intensivste Kraft entfaltet er auf dem Wurzelchakra oder Sexualchakra. Dort dringen seine Schwingungen direkt in den gesamten Blutkreislauf ein und l&ouml;sen Verkrampfungen und falsche Vorstellungen. Er f&uuml;hrt zur Ausgeglichenheit der Seele.<br>
<br>
Gr&ouml;&szlig;e: ca. 50 x 70 mm<br>
Gewicht: ca. 120g<br>
1A Qualit&auml;t, handverlesene Steine<br>
<br>
Chakra: Wurzel Chakra / Sexual Chakra<br>
Sternzeichen: Widder, Skorpion
<p><b>Preis:</b> &euro;8.90 inkl. MwSt.</p>
<a href="http://www.esoterikshopping.de/view_product.php?product=seifensteine01-jaspis-rot" target="_blank"><img src="http://www.esoterikshopping.de/images/seifensteine01-jaspis-rot.jpg" alt="Seifenstein Jaspis rot, Hot Stone Massagestein preiswert im Esoterik Online Shop zu kaufen - Esotherik Versand" border="0"></a><br>
            <hr></div>