<hr>
          <div align="center">...und hier k&ouml;nnten Ihre Links stehen... <br>
            <br>
            in der Naturmedizin werden Krankheiten als eine St&ouml;rung in der Harmonie zwischen Geist und K&ouml;rper betrachtet. Heilsteine k&ouml;nnen helfen, einen unbewussten, aber abgestumpften Selbstheilmechanismus zu aktivieren. Doch die eigentliche Wirkung geht vom Menschen aus. Der Stein ist dabei nur Mittel zum Zweck ohne direkte Heilwirkung. Er dient vielmehr als Schalter, der unseren k&ouml;rpereigenen Heilmechanismus einschaltet.<br>
            <br>
            <strong>Kraftquelle der Inspiration und Intuition</strong><br>
            <br>
Der Amethyst wurde schon zu allen Zeiten von allen Kulturen sehr gesch&auml;tz und verehrt. Man sprach ihm die Kraft zu, die Benebelung des Geistes - sei es durch Hilfsmittel wie Alkohol, der Glauben an die falsche Sache oder oder das Eindringen b&ouml;ser Kr&auml;fte in die Seele, zu verhindern. Der Amethyst sollte immer sichtbar getragen werden und f&ouml;rdert die Standfestigkeit im Glauben wie im Handeln.<br>
<br>
Er regt die Phantasie an und bewahrt vor Lernschwierigkeiten und Pr&uuml;fungsangst. <br>
<br>
Am intensivsten dringt die Wirkung des Amtheyst w&auml;hrend einer Meditation in das Scheitelchakra ein und schenkt Hingabe und Vertrauen zum Leben. Er verhilft seinem Tr&auml;ger die F&auml;higkeit zu entwickeln, Probleme weitsichtiger zu erkennen und diese auch l&ouml;sen zu k&ouml;nnen. Er l&ouml;st im K&ouml;rper Ruhe und Frieden aus und best&auml;rkt dadurch die Entwicklung der Intuition.<br>
<br>
Gr&ouml;&szlig;e: ca. 3 - 5 cm<br>
Gewicht: ca. 10 - 20g<br>
1A Qualit&auml;t<br>
<br>
Mit Bohrung f&uuml;r Lederband oder Halskette<br>
<br>
<b>Herkunftsland:</b> Brasilien<br>
<b>Zuordnung:</b> <br>
Chakra: Kronen Chakra (7. Chakra)<br>
Sternzeichen: Fische, Widder, Jungfrau
<p><b>Preis:</b> &euro;3.90 inkl. MwSt.</p>
<a href="http://www.esoterikshopping.de/view_product.php?product=heilstein04" target="_blank"><img src="http://www.esoterikshopping.de/images/heilstein04.jpg" alt="Amethyst preiswert im Esoterik Online Shop zu kaufen - Esotherik Versand" border="0"></a><br>
            <hr></div>