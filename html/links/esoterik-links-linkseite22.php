<hr>
          <div align="center">...und hier k&ouml;nnten Ihre Links stehen... <br>
            <br>
            <strong>Das Messen feiner Energien</strong><br>
            <br>
            Die Kunst oder Wissenschaft des Messens feiner Energien. Eine gro&szlig;e Auswahl an Pendeln, W&uuml;nschelruten und Tensoren gibt uns die M&ouml;glichkeit durch Kontakt mit den tieferen und subtileren Schichten von Wissen und Wahrnehmung oft erstaunlich pr&auml;zise und n&uuml;tzliche Informationen aus verschiedensten Lebensbereichen zu erhalten. <br>
So k&ouml;nnen wir erkennen, dass wir nicht immer auf Wissen aus &auml;u&szlig;eren Quellen angewiesen sind.<br>
<br>
<b>Bergkristall Pendel - Rohstein klein</b><br>
<br>
Dieser Bergkristall wurde im rohen, unbehandelten Zustand gelassen.
Viele Menschen behaupten, dass Bergkristalle durch den Prozess des
Schleifens ihre Kraft verlieren.<br>
Der Kristall ist in einer galvanischen Aufh&auml;ngung angebracht, mit &Ouml;se zum Durchziehen eines Lederbandes oder einer Kette.<br>
<br>
Mit seiner Eigenschaft Klarheit und Erkenntnis zu bringen, eignet sich der Bergkristall besonders gut zum Auspendeln von Problemen der Chakren und Organe. <br>
<br>
Gewicht: ca. 5g <br>
L&auml;nge: ca. 2 - 2,5 cm<br>
Durchmesser: ca. 0,8 cm<br>
<br>
Bei der Lieferung ist keine Kette oder Lederband vorhanden!
<p><b>Preis:</b> &euro;6.90 inkl. MwSt.</p>
<a href="http://www.esoterikshopping.de/view_product.php?product=edelsteinpendel28" target="_blank"><img src="http://www.esoterikshopping.de/images/edelsteinpendel28.jpg" alt="Bergkristall Pendel - Rohstein klein preiswert im Esoterik Online Shop zu kaufen - Esotherik Versand" border="0"></a><br>
            <hr></div>