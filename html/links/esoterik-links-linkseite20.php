<hr>
          <div align="center">...und hier k&ouml;nnten Ihre Links stehen... <br>
            <br>
            Shiva hei&szlig;t Licht oder Feuer und Lingam bedeutet S&auml;ule oder Turm. Shivalingam hei&szlig;t demnach S&auml;ule von Licht (oder Feuer). Ein Shivalingam ist ein m&auml;chtiges Symbol des Lichts, von einer Girlande von Flammen umgeben, die sich von der Erde zum Himmel erheben und Licht und W&auml;rme auf all die aussenden, die in ihre Reichweite kommen. Die l&auml;ngliche, ovale Form des Lingams symbolisiert die unsichtbare, unmanifestierte Sch&ouml;pfungskraft, die m&auml;nnliche Energie. Die roten Markierungen symbolisieren die weibliche Energie. M&auml;nnliche und weibliche Energie sind also hier vereint.<br>
<br>
<strong>Nutzen und Verwendung des Shivalingams</strong><br>
Der Shivalingam repr&auml;sentiert in seiner Form das Wesen des Menschen, seine Seele. Jeder Narmada Shivalingam strahlt ohne Unterla&szlig; sowohl m&auml;nnliche als auch weibliche Energie aus und erzeugt eine ausgleichende und Wirkung auf K&ouml;rper und Geist. Man sollte den Shiva- lingam an einer geeigneten Stelle im Haus aufbewahren, von wo die Energie ins gesamte Haus ausstr&ouml;men kann. Wenn man regelm&auml;&szlig;ig mit seinem Lingam meditiert, nimmt er die Energie auf und strahlt sie wieder ab. So wird er zu einem wertvollen Begleiter auf dem Weg zum inneren Selbst. Wann immer man der beruhigenden und gleichzeitig st&auml;rkenden Wirkung des Lingams bedarf, h&auml;lt man ihn mit beiden H&auml;nden, legt ihn auf die Herzgegend oder auf die Gegend des Dritten Auges. Einen noch gr&ouml;&szlig;eren Effekt erzielt man, wenn der Lingam vor eine Kupferpyramide gestellt wird, in der Agnihotra* zu Sonnenaufgang und Sonnenuntergang durchgef&uuml;hrt wird. Das Agnihotra-Feuer ist eine Verbindung von Shiva und Shakti, von m&auml;nnlicher und weiblicher Energie, was eine ganzheitliche Wirkung auf den Aus&uuml;benden, auf die Umgebung sowie auf die Atmosph&auml;re hat. Agnihotra l&auml;dt den Lingam jeden Tag zu Sonnenaufgang und Sonnenuntergang auf. Hierdurch verst&auml;rkt sich die Wirkung von Lingam und Agnihotra.<br>
<br>
<font size="1">*Agnihotra ist ein Vorgang zur Reinigung der Atmosph&auml;re mit Hilfe von Feuer, welches in einer Kupferpyramide im Einklang mit dem Biorhythmus von Sonnenaufgang und Sonnenuntergang durchgef&uuml;hrt wird.</font><br>
<br>
<b>Gr&ouml;&szlig;e: </b>ca. 7,5 cm<br>
<br>
Unsere Lingams sind etwas Besonderes. Sie werden nach dem Polieren in einem Kraftzentrum der Erde aufbewahrt und aufgeladen. Ein Teil des Erl&ouml;ses wird zum Aufbau und zur Aufrechterhaltung dieses Zentrums verwendet. <br>
<br>
<strong>DER NARMADA-FLUSS</strong><br>
Die Shiva-Lingams mit roten Markierungen werden nur im Narmada-Flu&szlig; gefunden. Der Narmada entspringt in &uuml;ber 3000 Meter H&ouml;he im Vindhya-Gebirge in Zentral-Indien. Er flie&szlig;t nach Westen und m&uuml;ndet nach ca. 1300 km im Bundesland Gujarat in das Arabische Meer. <br>
Gem&auml;&szlig; alter Guru-Tradition ist der Narmada der heiligste der Fl&uuml;sse. Ein Bad in ihm bringt Reinigung und Heilung. Traditionell werden Fl&uuml;sse als Ausdruck weiblicher Energie betrachtet. <br>
Wenn man die roten Markierungen des Shivalingams ber&uuml;hrt, ist dies so, als wenn man den Narmada selbst ber&uuml;hren w&uuml;rde. <br>
Es hei&szlig;t, da&szlig; dieser Flu&szlig; ununterbrochen Prana-Energie aussendet. Daher haben Yogis, Heilige und spirituelle Sucher seit urdenklichen Zeiten den Narmada aufgesucht, um durch Aus&uuml;bung von Askese an seinen Ufern Selbstverwirklichung zu erlangen, zum Beispiel Bhrigu, Jamadagni, Parashurama und viele mehr. In einem bestimmten Abschnitt des Narmada-Flusses in Zentralindien werden Steine gefunden, die rote Markierungen haben. Die Form wird gegebenenfalls korrigiert und gem&auml;&szlig; traditionellen Methoden gereinigt und poliert.
<p><b>Preis:</b> &euro;15.90 inkl. MwSt.</p>
<a href="http://www.esoterikshopping.de/view_product.php?product=shiva05" target="_blank"><img src="http://www.esoterikshopping.de/images/shiva01.jpg" alt="Shivalingam mittel preiswert im Esoterik Online Shop zu kaufen - Esotherik Versand" border="0"></a><br>
            <hr></div>
