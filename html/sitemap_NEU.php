<?php
class Sitemap {

	function __construct() {
		require_once('/kunden/159493_90461/xt/includes/configure.php');
		$this->mysqli = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
		if(mysqli_connect_errno()) {
			echo("construct: Keine Verbindung zur Datenbank m&ouml;glich!<br />Fehlercode: ".mysqli_connect_errno());
			$this->mysqli=false;
			exit();
		}	
		$this->ROOT_DOC = '/kunden/159493_90461/xt/';
		$this->file = 'sitemap_neu.xml';
		unlink($this->ROOT_DOC.$this->file);
	}

	public function close() {
		$this->mysqli->close();
		$this->mysqli=false;
	}	

	public function getActiveCategories() {
		$categoriesArray = array();

		$sql = ("SELECT categories_id FROM categories WHERE categories_status = 1");
		$query = $this->mysqli->query($sql);

		while($data = $query->fetch_array()) {
			$categoriesId = $data['categories_id'];

			$sqlBluegateCategories = ("SELECT url_text FROM bluegate_seo_url WHERE categories_id = '".$categoriesId."'");
			$queryBluegateCategories = $this->mysqli->query($sqlBluegateCategories);

			while($dataBluegateCategories = $queryBluegateCategories->fetch_array()) {
				$urlText = $dataBluegateCategories['url_text'];
				$lastUrlChar = substr($urlText, -1);

				if($lastUrlChar != '1') {
					$categoriesArray[] = '
					<url>
						<loc>http://www.esoterikshopping.de/'.$urlText.'</loc>
						<changefreq>daily</changefreq>
						<priority>1</priority>
					</url>';
				}
			}
		}
		return $categoriesArray;
	}

	public function getActiveProducts() {
		$productsArray = array();

		$sql = ("SELECT products_id FROM products WHERE products_status = 1");
		$query = $this->mysqli->query($sql);

		while($data = $query->fetch_array()) {
			$productsId = $data['products_id'];

			$sqlBluegateProducts = ("SELECT url_text FROM bluegate_seo_url WHERE products_id = '".$productsId."'");
			$queryBluegateProducts = $this->mysqli->query($sqlBluegateProducts);

			while($dataBluegateProducts = $queryBluegateProducts->fetch_array()) {
				$urlText = $dataBluegateProducts['url_text'];
				$lastUrlChar = substr($urlText, -1);

				if($lastUrlChar != '1') {
					$productsArray[] = '
					<url>
						<loc>http://www.esoterikshopping.de/products/'.$urlText.'.html</loc>
						<changefreq>daily</changefreq>
						<priority>1</priority>
					</url>';
				}
			}
		}
		return $productsArray;
	}	

	public function getXmlHeader() {
		$inhalt = "";
		$exc = '?'; 
		$EXC='>';
		$inhalt = '<?xml version="1.0" encoding="UTF-8"'.$exc.$EXC.'
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		return $inhalt;
		//<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">';
	}

	public function getXmlFooter() {
		$inhalt = '
		</urlset>';
		return $inhalt;
	}

	public function buildFile($c) {
		$fh = fopen($this->ROOT_DOC.$this->file, "w+");
		fputs($fh, $c);
		fclose($fh);
	}
}


$s = new Sitemap();

// Header
$header = $s->getXmlHeader();

// categories
$categories = $s->getActiveCategories();
foreach($categories as $categorie) {
	$c .= $categorie;
}

// products
$products = $s->getActiveProducts();
foreach($products as $product) {
	$p .= $product;
}

// Footer
$footer = $s->getXmlFooter();

// glue together
$xmlContent = $header.$c.$p.$footer;

// create file
$s->buildFile($xmlContent);

// schließen
$s->close();

?>