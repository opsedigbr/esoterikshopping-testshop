<?php
define("MOBILEREDIRECT", 'true'); //<- Auf 'true' stellen, wenn direkt weitergeleitete werden soll

function rm_url_param($param_rm, $query='') {
  empty($query) ? $query=$_SERVER['QUERY_STRING'] : '';
  parse_str($query, $params);
  unset($params[$param_rm]);
  $newquery = '';
  foreach($params as $k => $v)
      { $newquery .= '&'.$k.'='.$v; }
  return substr($newquery,1);
}
?>  

<style type="text/css">
#mobileBox {
  text-align: center; 
  font-size: 30px; 
  color: #fff; 
  background: #000; 
  padding: 20px; 
  line-height: 46px;
}

#mobileBox a {
  text-decoration: underline;
  color: #fff;
}
</style>

<script type="text/javascript">
  var redirectOnStart = true; //<- Auf 'true' stellen, wenn direkt weitergeleitete werden soll
  
  var redirectURL = "<?php echo $_SERVER["PHP_SELF"]; ?>?tpl=mobile&<?php echo rm_url_param('tpl'); ?>";
  
  function createMobileDiv() { 
    var first = document.body.children[0];
    var beforeEle = document.createElement("div");
    var attribute = document.createAttribute("id");
    beforeEle.innerHTML = "<?php echo MOBILETEXT; ?><br /><a href=\""+redirectURL+"\"><?php echo MOBILELINKTEXT; ?></a>";
    attribute.nodeValue = "mobileBox";
    beforeEle.setAttributeNode(attribute);
    document.body.insertBefore(beforeEle, first);
  }
  
  function checkMobile() { 
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent||navigator.vendor||window.opera)){
      return true;
    }
    else {
      return false;
    }
  }
  
  if (checkMobile()) {
    
    <?php 
      if (!isset ($_SESSION['mobile_redirect']) && MOBILEREDIRECT) { 
        $_SESSION['mobile_redirect'] = TRUE;
        echo "if (redirectOnStart) { window.location=redirectURL; }";
      } 
    ?>
    
    window.onload = createMobileDiv;
  }
    
</script>