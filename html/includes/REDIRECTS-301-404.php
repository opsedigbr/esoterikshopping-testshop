<?php
$redirect = $_SERVER['REQUEST_URI'];

function redirect404() {
	$html = file_get_contents('http://www.esoterikshopping.de/content/404.html', 'r');
	header('HTTP/1.1 404 Not Found', true, 404);
	echo $html;
	exit;
}

function redirect301New($redirect, $array301) {
	if(array_key_exists($redirect, $array301)) {
		foreach($array301 as $key => $value) {
			if($key == $redirect) {
				header('HTTP/1.1 301 Moved Permanently');
				header("Location: http://www.esoterikshopping.de".$key); 
				header("Connection: close");      
				xtc_redirect(xtc_href_link($value));
			}	
		}
	}
}

function redirect301ToHomepage() {
	header('HTTP/1.1 301 Moved Permanently');
	header("Location: http://www.esoterikshopping.de/"); 
	header("Connection: close");
	xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
}


/* ******************************************* */
/* Includieren der Liste mit den Redirects 301 */
/* ******************************************* */
$fileRedirects = ('/kunden/159493_90461/xt/includes/redirects.csv');

if(file_exists($fileRedirects) && is_readable($fileRedirects)) {
	$data = array();
	if( ($handle = fopen($fileRedirects, "r")) !== false ) {
		while (($data = fgets($handle, 1000)) !== false) {
			$d = array();
	    	$d = explode(';', $data);
	    	$d[0] = trim($d[0]);
	    	$d[1] = trim($d[1]);	    	
	    	$array301[$d[0]] = $d[1];
    	}
	}
	fclose($handle);
}
else {
	$message = "Fehler bei redirects.php unter includes/redirects.csv";
	mail('info@opsedi.de', 'Fehler: Redirects.csv', $message);
}


// Redirect List 
redirect301New($redirect, $array301);

// Sonstiges
switch($redirect) {
	case (preg_match('|Die-Welt-der-Links-Seite|', $redirect) > 0):
		redirect301ToHomepage();
	break;

	case (preg_match('|c256_Angebote|', $redirect) > 0):
		redirect301ToHomepage();
	break;		

	case (preg_match('|Fantasy-Puzzle|', $redirect) != 0):
		redirect301ToHomepage();
	break;	

	case (preg_match('|affiliate_banner_id|', $redirect) != 0):
		redirect301ToHomepage();
	break;

	case (preg_match('|Gratisproben|', $redirect) != 0):
		redirect301ToHomepage();
	break;		

	case (preg_match('|cat_id|', $redirect) != 0):
		redirect301ToHomepage();
	break;
}

?>