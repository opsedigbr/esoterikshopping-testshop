<?php
/*
 *  Modul: Products Gift v1.0
 *  created by Sergej Stroh.
 *	www.southbridge.de
 *	28.11.2005
 *	Copyright 2005 by Sergej Stroh.
 *
 * NEU: Kundengruppenabh�ngig
 */

define('MAX_GIFTS_IN_CART', 2);
$show_btn = '';
$class_of_sample = '';
$diff_groups_of_sums = array();
$diff_rows = 0;
$difference_of_cart_sum_for_free_sample = 0;
$cart_sum = $_SESSION['cart']->show_total();
$p_in_cart_array = array();

$products_gift_smarty = new Smarty;
$products_gift_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

//echo 'Cart: ';
//echo($_SESSION['cart']->show_total());

//print_r($_SESSION['customers_status']['customers_status_id']);

// include needed functions
require_once(DIR_FS_INC.'xtc_check_stock.inc.php');
require_once(DIR_FS_INC.'xtc_get_products_stock.inc.php');
require_once(DIR_FS_INC.'xtc_remove_non_numeric.inc.php');
require_once(DIR_FS_INC.'xtc_get_short_description.inc.php');
require_once(DIR_FS_INC.'xtc_format_price.inc.php');
require_once(DIR_WS_CLASSES.'xtcPrice.php');

$gift_content = array();

// products_gift
$products_gift = "	SELECT 
					pg.products_gift_id, 
					pg.products_gift_sum,
					pg.products_id,
					pg.customers_groups,
					p.products_tax_class_id,
					p.products_image,
					pd.products_name,
					pd.products_description 
					FROM ".TABLE_PRODUCTS." p,
					".TABLE_PRODUCTS_DESCRIPTION." pd,
					".TABLE_PRODUCTS_GIFT." pg
					WHERE pg.products_id = p.products_id
					AND p.products_id = pd.products_id
					AND pd.language_id = '".$_SESSION['languages_id']."'
					ORDER BY pg.products_gift_sum ASC";
		
$products_gift_query = xtc_db_query($products_gift);
$products_gift_exist = xtc_db_num_rows($products_gift_query);

// Pr�fen, wieviele Gratisproben im Warenkorb liegen
$gift_counter_already_in_cart=0;
$checkFreeProductsInCart = $_SESSION['cart']->get_products();
for($ii=0;$ii<count($checkFreeProductsInCart);$ii++)
{
	// schreibe die ID's der Gratisproben in ein Array
	$p_in_cart_array[] = $checkFreeProductsInCart[$ii]['id'];
	if($checkFreeProductsInCart[$ii]['products_gift'] == 1)
	{
		$gift_counter_already_in_cart += 1;
	}
}



// um wieviel verschieden-"preisige" Gratisproben handelt es sich
$different_sums_query = xtc_db_query("SELECT products_gift_sum FROM ".TABLE_PRODUCTS_GIFT." GROUP BY products_gift_sum ORDER BY products_gift_sum ASC");
while($different_sums = xtc_db_fetch_array($different_sums_query))
{
	$diff_groups_of_sums[$diff_rows] = $different_sums['products_gift_sum'];
	$difference_of_cart_sum_for_free_sample = $diff_groups_of_sums[$diff_rows]-$cart_sum;
	($difference_of_cart_sum_for_free_sample>0) ? $message = $difference_of_cart_sum_for_free_sample : $message = '';
	$groups_content[] = array('GROUPS_ID' => $diff_rows, 'GROUPS_PRICE' => number_format($diff_groups_of_sums[$diff_rows], 2, ',', '.'), 'MESSAGE' => number_format($message, 2, ',', '.'));
	$diff_rows+=1;
}
//echo var_dump($diff_groups_of_sums);


if($products_gift_exist > 0)
{
	// Warenkorb Check EOF
	while($products_gift = xtc_db_fetch_array($products_gift_query))
	{

		// Darf der Kunde Artikel sehen?
		$group_list = split('[,]', $products_gift['customers_groups']);
		$group_list_count = sizeof($group_list);
		
		// Gruppen durchgehen
		for($a = 0; $a < $group_list_count; $a++)
		{
			if($_SESSION['customers_status']['customers_status_id']	== $group_list[$a])
			{
				// Produkte durchgehen
				for($i = 0; $i < $products_gift_exist; $i++)
				{
					$show_btn = true;
					$class_of_sample = 'aktiv';
					
					$gift_sum = $products_gift['products_gift_sum'];
					
					$image		= '';
					$order_now	= '';
					$checked	= false;
					$mobileWrap	= 'false';
					
					if($products_gift['products_image'] != '') 
					{
						$image = DIR_WS_THUMBNAIL_IMAGES . $products_gift['products_image'];
						$image_big = DIR_WS_POPUP_IMAGES . $products_gift['products_image'];
					}
					
		
					// Ist die Gratisprobe bereits im Warenkorb, setze Attribute f�r inaktiv
					if(in_array($products_gift['products_id'], $p_in_cart_array) )
					{
						$show_btn			= true;
						$class_of_sample	= 'aktiv checked';
						$order_now			= '<img src="templates/'.CURRENT_TEMPLATE.'/img/free_samples_green_check.png" />';
						$checked			= true;
						$mobileWrap			= 'false';
					}
										
					
					// Button Handler
					if($cart_sum >= $gift_sum)
					{
						// Aktiv
						if($show_btn == true && $gift_counter_already_in_cart < MAX_GIFTS_IN_CART && !$checked )
						{
							$order_now = '<a href="'.xtc_href_link(basename($PHP_SELF), 'action=products_gift&FREEproducts_id='.$products_gift['products_id'].'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">'.xtc_image_button('btn_free_samples.png').'</a>';
						}
						
						// Inaktiv
						if($show_btn == false && $gift_counter_already_in_cart < MAX_GIFTS_IN_CART && !$checked)
						{
							$order_now = xtc_image_button('btn_free_samples_checked.png');
						}
						
						if($_SESSION['tpl'] == 'mobile')
						{
							$mobileWrap			= 'true';
							$mobileWrapStart	= '<a href="'.xtc_href_link(basename($PHP_SELF), 'action=products_gift&FREEproducts_id='.$products_gift['products_id'].'&'.xtc_get_all_get_params(array ('action')), 'NONSSL').'">';
							$mobileWrapEnd		= '</a>';
						}
					}
					
					// Bestellsumme ist f�r diese Gratisprobe noch nicht erreichts
					else
					{
						$order_now			= "&nbsp;";
						$class_of_sample	= 'aktiv';
						$mobileWrap			= 'false';
					}	
					
					// Max. Anzahl erreicht --> alle auf inaktiv setzen
					if($gift_counter_already_in_cart == MAX_GIFTS_IN_CART || $gift_counter_already_in_cart > MAX_GIFTS_IN_CART)
					{
						if($checked)
						{
							$order_now			= '<img src="templates/'.CURRENT_TEMPLATE.'/img/free_samples_green_check.png" />';
							$class_of_sample	= 'inaktiv';
							$mobileWrap			= 'false';
						}
						else
						{
							$order_now			= xtc_image_button('btn_free_samples_checked.png');
							$class_of_sample	= 'inaktiv';
							$mobileWrap			= 'false';
						}
					}
					
					// Wichtig: Holen der Gruppenverteilung
					$searchGroupOfSums = searchGroupOfSums($diff_groups_of_sums, $gift_sum);

					
					$gift_content[$searchGroupOfSums][] = array(
							'PRODUCTS_ID' 		=> $products_gift['products_id'],
							'PRODUCTS_NAME' 	=> $products_gift['products_name'],
							'PRODUCTS_DESC' 	=> $products_gift['products_description'], 
							'PRODUCTS_IMAGE' 	=> $image, 
							'PRODUCTS_IMAGE_POPUP' 	=> $image_big, 
							'PRODUCTS_PRICE' 	=> $xtPrice->xtcFormat('0.00', true), 
							'PRODUCTS_GIFT_SUM' => $xtPrice->xtcFormat($gift_sum, true),
							'BUTTON_BUY_NOW' => $order_now, 
							'CLASS_OF_SAMPLE' => $class_of_sample,
							'IS_GROUP_OF' => $searchGroupOfSums,
							'MOBILE' => $mobileWrap, 
							'MOBILE_START' => $mobileWrapStart, 
							'MOBILE_END' => $mobileWrapEnd, 
							'PRODUCTS_SHORT_DESCRIPTION' => xtc_get_short_description($products_gift['products_id']));	
							

					$products_gift_smarty->assign(PRODUCTS_GIFT_TEXT, PRODUCTS_GIFT_INFOTEXT);
					break;						
					

				}// FOR
			}// IF
		}// FOR Gruppen
	} // while
	
	$products_gift_smarty->assign('gift_content', $gift_content);
}// IF products exist


// Finde heraus, welcher Gruppe die Gratisprobe angeh�rt
function searchGroupOfSums($diff_groups, $gift_sum)
{
	for($i=0;$i<count($diff_groups);$i++)
	{
		if($diff_groups[$i] == $gift_sum)
		{
			return $i;
		}
	}
}

$products_gift_smarty->assign('language', $_SESSION['language']);
$products_gift_smarty->assign('GROUPS', count($diff_groups_of_sums));
$products_gift_smarty->assign('IMG_FREE_SAMPLES', 'templates/'.CURRENT_TEMPLATE.'/img/free_samples_caption_img.png');
$products_gift_smarty->assign('groups_content', $groups_content);
$products_gift_smarty->caching = 0;
$products_gift_module = $products_gift_smarty->fetch(CURRENT_TEMPLATE.'/module/products_gift.html');

$smarty->assign('MODULE_products_gift', $products_gift_module);

?>