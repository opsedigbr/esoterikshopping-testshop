<?php 

// ---------------------------------------------------------------------------------------
//	$Id: metatags.php 1140 2005-08-10 10:16:00Z mz $
//
//	XT-Commerce - community made shopping
//	http://www.xt-commerce.com
//
//	Copyright (c) 2003 XT-Commerce
// ---------------------------------------------------------------------------------------
//	based on:
//	(c) 2003	 nextcommerce (metatags.php,v 1.7 2003/08/14); www.nextcommerce.org
//
//	Released under the GNU General Public License
// ---------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------
//	AUTOMATISCHE METATAGS f�r xt:Commerce 3.04
// ---------------------------------------------------------------------------------------
//	by Gunnar Tillmann
//	http://www.gunnart.de
// ---------------------------------------------------------------------------------------
//	Version 0.9b / 15. Dezember 2007
// ---------------------------------------------------------------------------------------
//	Inspired by "Dynamic Meta" - Ein WordPress-PlugIn von Michael Schwarz
//	http://www.php-vision.de/plugins-scripte/dynamicmeta-wpplugin.php
// ---------------------------------------------------------------------------------------
//	Bislang nur getestet f�r xt:C 3.04 SP2.1, ShopStat-Erweiterung aktiv
//	Eventuell sollte die "includes/header.php" ein bisschen angepasst werden, um valides
//	HTML zu gew�hrleisten
// ---------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------
//	Konfiguration ... 
// ---------------------------------------------------------------------------------------
	global $metaStopWords, $metaGoWords, $metaMinLength, $metaMaxLength;
		$metaStopWords 	=	('aber,alle,alles,als,auch,auf,aus,bei,beim,beinahe,bin,bis,ist,dabei,dadurch,daher,dank,darum,danach,das,da�,dass,dein,deine,dem,den,der,des,dessen,dadurch,deshalb,die,dies,diese,dieser,diesen,diesem,dieses,doch,dort,durch,eher,ein,eine,einem,einen,einer,eines,einige,einigen,einiges,eigene,eigenes,eigener,endlich,euer,eure,etwas,fast,findet,f�r,gab,gibt,geben,hatte,hatten,hattest,hattet,heute,hier,hinter,ich,ihr,ihre,ihn,ihm,im,immer,in,ist,ja,jede,jedem,jeden,jeder,jedes,jener,jenes,jetzt,kann,kannst,kein,k�nnen,k�nnt,machen,man,mein,meine,mehr,mit,mu�,mu�t,musst,m�ssen,m��t,nach,nachdem,neben,nein,nicht,nichts,noch,nun,nur,oder,statt,anstatt,seid,sein,seine,seiner,sich,sicher,sie,sind,soll,sollen,sollst,sollt,sonst,soweit,sowie,und,uns,unser,unsere,unserem,unseren,unter,vom,von,vor,wann,warum,was,war,weiter,weitere,wenn,wer,werde,widmen,widmet,viel,viele,vieles,weil,werden,werdet,weshalb,wie,wieder,wieso,wir,wird,wirst,wohl,woher,wohin,wurdezum,zur,�ber');
		$metaGoWords 	=	('gola,adidas'); // Hier rein, was nicht gefiltert werden soll
		$metaMinLength 	=	9;		// Mindestl�nge eines Keywords
		$metaMaxLength 	=	18;		// Maximall�nge eines Keywords
		$metaDesLength 	=	364;	// maximale L�nge der "description" (in Buchstaben)
		$is_ssl			=	$_SERVER['SERVER_PORT'];
		$noIndexSSL		=	true;	// SSL Seiten nicht indexieren
// ---------------------------------------------------------------------------------------



// ---------------------------------------------------------------------------------------
//	Aufr�umen: Umlaute und Sonderzeichen wandeln. 
// ---------------------------------------------------------------------------------------
	function metaNoEntities($Text){
	    $translation_table = get_html_translation_table(HTML_ENTITIES,ENT_QUOTES);
	    $translation_table = array_flip($translation_table);
	    $Return= strtr($Text,$translation_table);
	    return preg_replace( '/&#(\d+);/me',"chr('\\1')",$Return);
	}
	function metaHtmlEntities($Text) {
		$translation_table=get_html_translation_table(HTML_ENTITIES,ENT_QUOTES);
		$translation_table[chr(38)] = '&';
		return preg_replace("/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/","&amp;",strtr($Text,$translation_table));
	}
// ---------------------------------------------------------------------------------------
//	Array basteln: Text aufbereiten -> Array erzeugen -> Array unique ...  
// ---------------------------------------------------------------------------------------
	function prepareWordArray($Text) {
		$Text = str_replace(array('&nbsp;','\t','\r','\n','\b'),' ',strip_tags($Text));
		$Text = metaHtmlEntities(metaNoEntities(strtolower($Text)),ENT_QUOTES);
		$Text = preg_replace("/(&([aou])[^;]*;)/",'$2e',$Text);
		$Text = preg_replace("/(&(s)[^;]*;)/",'$2$2',$Text);
		$Text = preg_replace("/(&([cizen])[^;]*;)/",'$2',$Text);
		$Text = preg_replace("/(&[^;]*;)/",' ',$Text);
		$Text = preg_replace("/([^0-9a-z|\-])/",' ',$Text);
		$Text = trim(preg_replace("/\s\s+/",' ',$Text));
		return($Text);
	}
	function makeWordArray($Text) {
		$Text = func_get_args();
		$Words = array();
		foreach($Text as $Word) {
			if((!empty($Word))&&(is_string($Word))) {
				$Words = array_merge($Words,explode(' ',$Word));
			}
		}
		return array_unique($Words);
	}
	function WordArray($Text) {
		return makeWordArray(prepareWordArray($Text));
	}
// ---------------------------------------------------------------------------------------
//	KeyWords aufr�umen:
// 	Stop- und KeyWords-Liste in Array umwandeln, StopWords l�schen, 
//	GoWords- und L�ngen-Filter anwenden
// ---------------------------------------------------------------------------------------
	function cleanKeyWords($KeyWords) {
		global $metaStopWords;
		$KeyWords 	= 	WordArray($KeyWords);
		$StopWords 	=	WordArray($metaStopWords);
		$KeyWords 	= 	array_diff($KeyWords,$StopWords);
		$KeyWords 	= 	array_filter($KeyWords,filterKeyWordArray);
		natsort($KeyWords);
		return $KeyWords;
	}
// ---------------------------------------------------------------------------------------
//	GoWords- und L�ngen-Filter: 
//	Alles, was zu kurz ist, fliegt raus, sofern nicht in der GoWords-Liste
// ---------------------------------------------------------------------------------------
	function filterKeyWordArray($KeyWord) {
		global $metaMinLength, $metaMaxLength;
		$GoWords = WordArray(getGoWords());
		if(!in_array($KeyWord,$GoWords)) {
			$Length = strlen($KeyWord);
			if($Length < $metaMinLength) // Mindest-L�nge
				return false;
			if($Length > $metaMaxLength) // Maximal-L�nge
				return false;
		}
		return true;
	}	
// ---------------------------------------------------------------------------------------
//	GoWords: Werden grunds�tzlich nicht gefiltert
//	Sofern angelegt, werden (zus�tzlich zu den Einstellungen oben) die "normalen"
//	Meta-Angaben genommen. 
// ---------------------------------------------------------------------------------------
	function getGoWords(){
		global $metaGoWords;
		$GoWords = $metaGoWords.' '.META_KEYWORDS;
		if(!empty($categories_meta['categories_meta_keywords']))
			$GoWords .= ' '.$categories_meta['categories_meta_keywords'];
		if(!empty($product->data['products_meta_keywords']))
			$GoWords .= ' '.$product->data['products_meta_keywords'];
		return $GoWords;
	}
// ---------------------------------------------------------------------------------------
//	Aufr�umen: Leerzeichen und HTML-Code raus, k�rzen, Umlaute und Sonderzeichen wandeln
// ---------------------------------------------------------------------------------------
	function metaClean($Text,$Length=false,$Abk=' ...') {
		$Text = metaNoEntities($Text);
		$Text = strip_tags($Text);
		$Text = str_replace(array('&nbsp;','\t','\r','\n','\b'),' ',$Text);
		$Text = trim(preg_replace("/\s\s+/",' ',$Text));
		if(($Length)&&($Length > 0)) {
			if(strlen($Text) > $Length) {
	        	$Length -= strlen($Abk);
	            $Text = preg_replace('/\s+?(\S+)?$/', '', substr($Text, 0, $Length+1));
	            $Text = substr($Text, 0, $Length).$Abk;
			}
		}
		return metaHtmlEntities($Text,ENT_QUOTES);
	}
// ---------------------------------------------------------------------------------------
//	metaTitle und metaKeyWords, R�ckgabe bzw. Formatierung
// ---------------------------------------------------------------------------------------
	function metaTitle($Title=array()) {
		$Title = func_get_args();
		$Title = array_filter($Title,metaClean);
		return implode(' - ',$Title);
	}
// ---------------------------------------------------------------------------------------
	function metaKeyWords($Text) {
		$KeyWords = cleanKeyWords($Text);
		return implode(', ',$KeyWords);
	}
// ---------------------------------------------------------------------------------------




// ---------------------------------------------------------------------------------------
//	Daten holen: Produktdetails
// ---------------------------------------------------------------------------------------
	if (strstr($_SERVER['SCRIPT_NAME'], FILENAME_PRODUCT_INFO)) { 
		if ($product->isProduct()) { 
			if(!empty($product->data['products_meta_keywords'])) { 
				$meta_keyw = metaKeyWords($product->data['products_meta_keywords']); 
			} else{ 
				$meta_keyw = metaKeyWords($product->data['products_name'].' '.$product->data['products_description']);
			} 
			if(!empty($product->data['products_meta_description'])) { 
				$meta_descr = $product->data['products_meta_description']; 
			} else { 
				$meta_descr = $product->data['products_name'].': '. 
				$product->data['products_description']; 
			} 
			$meta_title = metaTitle($product->data['products_name'],$product->data['manufacturers_name'],str_replace("Esoterikshopping.de", "", TITLE)); 
			
			$categories_meta_query = xtDBquery(
			"SELECT a.categories_meta_keywords, 
			a.categories_meta_description, 
			a.categories_meta_title, 
			a.categories_name, 
			a.categories_description 
			FROM ".TABLE_CATEGORIES_DESCRIPTION." a
			INNER JOIN ".TABLE_PRODUCTS_TO_CATEGORIES." b
			ON a.categories_id = b.categories_id
			WHERE b.products_id ='".$product->data['products_id']."' and 
			language_id='".$_SESSION['languages_id']."'"); 
			$categories_meta = xtc_db_fetch_array($categories_meta_query,true);
					/*echo "<pre>";
			var_dump($categories_meta);
			echo "</pre>";
			die();*/

		} 
	} 
// ---------------------------------------------------------------------------------------
//	Daten holen: Kategorie
// ---------------------------------------------------------------------------------------
	/*elseif(!empty($_REQUEST['cPath'])) { 
		if (strpos($_REQUEST['cPath'],'_') > 0) { 
			$arr    = explode('_',xtc_input_validation($_REQUEST['cPath'],'cPath','')); 
			$_cPath = array_pop($arr); 
		} else{ 
			$_cPath=(int)$_REQUEST['cPath']; 
		} */
		elseif(!empty($_GET['cPath'])) { 
		if (strpos($_GET['cPath'],'_') > 0) { 
			$arr    = explode('_',xtc_input_validation($_GET['cPath'],'cPath','')); 
			$_cPath = array_pop($arr); 
		} else{ 
			$_cPath=(int)$_GET['cPath']; 
		} 
		$categories_meta_query = xtDBquery(
			"SELECT categories_meta_keywords, 
			categories_meta_description, 
			categories_meta_title, 
			categories_name, 
			categories_description 
			FROM ".TABLE_CATEGORIES_DESCRIPTION." 
			WHERE categories_id='".$_cPath."' and 
			language_id='".$_SESSION['languages_id']."'"); 
			$categories_meta = xtc_db_fetch_array($categories_meta_query,true);
		

		$manu_id = false;
		$manu_name = false;

		if(!empty($_GET['manu']))
			$manu_id = intval($_GET['manu']);
		if(!empty($_GET['manufacturers_id']))
			$manu_id = $_GET['manufacturers_id'];
		if(!empty($_GET['filter_id']))
			$manu_id = $_GET['filter_id'];

		if($manu_id) {
			$manu_name_query = xtDBquery(
				"select manufacturers_name 
				from ".TABLE_MANUFACTURERS." 
				where manufacturers_id ='".$manu_id."'");
			$manu_name = implode('',xtc_db_fetch_array($manu_name_query,true));
		}
		
		if(!empty($categories_meta['categories_meta_keywords'])) { 
			$meta_keyw = metaKeyWords($categories_meta['categories_meta_keywords']); 
		} else{ 
			$meta_keyw = metaKeyWords($categories_meta['categories_name'].' '.$manu_name.' '.$categories_meta['categories_description']);
		} 
		
		if(!empty($categories_meta['categories_meta_description'])) { 
			$meta_descr = $categories_meta['categories_meta_description']; 
		} else{ 
			$meta_descr = TITLE.' - '.$categories_meta['categories_name'];
			if(!empty($manu_name)) 
				$meta_descr .= ' von: '.$manu_name;
			if(!empty($categories_meta['categories_description'])) 
				$meta_descr .= ' - '.$categories_meta['categories_description'];
		} 
		
		if(!empty($categories_meta['categories_meta_title'])) { 
			$meta_title = metaTitle($categories_meta['categories_meta_title'],TITLE); 
		} else{ 
			$meta_title = metaTitle($categories_meta['categories_name'],$manu_name,TITLE); 
		} 
	} 
// ---------------------------------------------------------------------------------------
//	Daten holen: Inhalts-Seite (ContentManager)
// ---------------------------------------------------------------------------------------
	elseif($_GET['coID']) { 
		$contents_meta_query=xtDBquery(
			"SELECT 
			content_title, 
			content_heading, 
			content_text 
			FROM ".TABLE_CONTENT_MANAGER." 
			WHERE content_group='".$_GET['coID']."' and 
			languages_id='".$_SESSION['languages_id']."'"); 
		$contents_meta = xtc_db_fetch_array($contents_meta_query,true); 
		
		if(count($contents_meta) > 0) { 
			$meta_title = metaTitle($contents_meta['content_title'],$contents_meta['content_heading'],TITLE); 
			$meta_descr = $contents_meta['content_heading'].': '.$contents_meta['content_text']; 
			$meta_keyw = metaKeyWords($contents_meta['content_title'].' '.$contents_meta['content_heading'].' '.$contents_meta['content_text']); 
		}
	}
// ---------------------------------------------------------------------------------------
//	... und wenn nix drin, dann Standard-Werte nehmen
// ---------------------------------------------------------------------------------------
	if(empty($meta_keyw))
		$meta_keyw    = metaKeyWords(META_KEYWORDS); 
	if(empty($meta_descr)) 
		$meta_descr   = META_DESCRIPTION; 
	
	### HIER KOMMT REIN, WAS IM TITLE ZUERST STEHEN SOLL. STANDARD: TITLE -HOL-###
	#if(empty($meta_title)) 
		#$meta_title   = TITLE;
		if(empty($meta_title))
		$meta_title   = "";
		
		
	//(empty($_SESSION['language_code'])) ? $_SESSION['language_code'] = 'de':false; ?
// ---------------------------------------------------------------------------------------

$meta_robots = META_ROBOTS;
if( ($is_ssl == '443') && ($noIndexSSL == true) )
{
	$meta_robots = 'noindex, follow';
}
?>
<!--<title><?php //echo $categories_meta_description,$meta_title,$categories_name,$category_name;?> Esoterik | Esoterik Shop | Tarot Engel Kartenlegen | Versand</title> -->
<title><?php echo $categories_meta_description,$meta_title,$categories_name,$category_name;?> bei Esoterikshopping.de</title> 

<meta name="DC.Title" content="<?php echo $meta_title,$categories_name;?> Esoterik | Esoterik Shop | Tarot Engel Kartenlegen | Versand" />
<meta name="DC.Subject" content="<?php echo $meta_title;?> Esoterik | Esoterik Shop | Tarot Engel Kartenlegen | Versand" />
<meta http-equiv="content-language" content="<?php echo $_SESSION['language_code']; ?>" /> 
<meta http-equiv="cache-control" content="no-cache" /> 
<meta name="keywords" content="<?php //include("variableKeyword.php"); ?>Esoterik, Tarot" /> 
<meta name="description" content="<?php echo $categories_meta['categories_name']." ", metaClean($meta_descr,$metaDesLength); ?> <?php include("description2.php"); ?>" /> 
<meta name="DC.Description" content="<?php echo $categories_meta['categories_name']." ", metaClean($meta_descr,$metaDesLength); ?> <?php include("description2.php"); ?> /">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="robots" content="<?php echo $meta_robots; ?>" />
<meta name="robots" content="all" />
<meta name="language" content="<?php echo $_SESSION['language_code']; ?>" />
<meta name="author" content="Holger Klinzmann, Uta Dittrich" />
<meta name="publisher" content="Holger Klinzmann, Uta Dittrich" />
<meta name="company" content="Esoterikshopping GbR" />
<meta name="page-topic" content="<?php echo metaClean(META_TOPIC); ?>" />
<meta name="distribution" content="global" />
<meta name="expires" content="0" />
<meta name="revisit-after" content="1 day" />