<?php

/* -----------------------------------------------------------------------------------------
   $Id: productinformation_news.php 1243 2005-09-25 09:33:02Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(productinformation_news.php,v 1.23 2003/02/12); www.oscommerce.com 
   (c) 2003	 nextcommerce (productinformation_news.php,v 1.7 2003/08/22); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

define('MAX_PRODUCTINFORMATION_NEWS', '5');
define('MAX_TEXT_LENGTH', '400');

$module_smarty = new Smarty;
$module_smarty->assign('tpl_path', 'templates/'.CURRENT_TEMPLATE.'/');

// include needed functions
require_once (DIR_FS_INC.'xtc_cleanName.inc.php');

$module_content = array ();

$pinQery = xtDBquery("select * from ".TABLE_CONTENT_MANAGER." where file_flag = '10' and content_status = '1' order by content_id DESC limit ".MAX_PRODUCTINFORMATION_NEWS);
if (xtc_db_num_rows($pinQery,true) > 0) 
{
	$row = 0;
	while ($pin = xtc_db_fetch_array($pinQery,true)) 
	{
		$row ++;
		
		// Text auslesen //
		$pin_content = '';
		$pin['content_title'] = xtc_cleanName($pin['content_title']);
		$pin_content = @file_get_contents(HTTP_SERVER.DIR_WS_CATALOG . 'media/content/' . $pin['content_file']);
		
		// Überschrift auslesen //
		if(empty($pin['content_heading']))
		{
			if(preg_match("/<h1>(.*)<\/h1>/", $pin_content, $match))
			{
				$pin_headline = $match[1];
			}
			else
			{
				$pin_headline = $pin['content_heading'];
			}
		}
		else
		{
			$pin_headline = $pin['content_heading'];
		}
		
		// 1. Bild auslesen //
		//$s_pattern = '/(img|src)=("|\')[^"\'>]+/i';
		$s_pattern = '/<img src=("|\')(.*)["\|\']/';
		
		if(preg_match($s_pattern, $pin_content, $img_match))
		{
			$pin_images = $img_match[0].' />';
		}
		else
		{
			$pin_images = 'leer';
		}
		
		// Daten aufbereiten
		$pin_content = trim(strip_tags(str_replace('<br />', "\n", $pin_content)));
		$pin_content = substr($pin_content, 0, MAX_TEXT_LENGTH);
		$pin['content_cpaths'] = str_replace(',','_',$pin['content_cpaths']);
		
		$pin_headline_1 = $pin['content_title'];
		$pin_headline_2 = $pin_headline;
		
		/*
		$module_content[] = array (
		'PIN_LINK' => FILENAME_PRODUCT_INFORMATIONS.'?coID='.$pin['content_group'].'&cPath='.$pin['content_cpaths'].'&product='.$pin['content_title'],
		'PIN_LINK_MORE' => '...mehr...',
		'PIN_HEADLINE_1' => $pin_headline_1,
		'PIN_HEADLINE_2' => $pin_headline_2,
		'PIN_CONTENT' => $pin_content, 
		'PIN_IMG' => $pin_images
		);
		*/
		$module_content[] = array (
		'PIN_LINK' => 'l-pg/'.$pin['content_group'].'/'.$pin['content_cpaths'].'/'.$pin['content_title'].'-guenstig-hier-im-shop-kaufen.html',
		'PIN_LINK_MORE' => '...mehr...',
		'PIN_HEADLINE_1' => $pin_headline_1,
		'PIN_HEADLINE_2' => $pin_headline_2,
		'PIN_CONTENT' => $pin_content, 
		'PIN_IMG' => $pin_images
		);
	}

	$module_smarty->assign('language', $_SESSION['language']);
	$module_smarty->assign('module_content', $module_content);
	// set cache ID

	$module_smarty->caching = 0;
	$module = $module_smarty->fetch(CURRENT_TEMPLATE.'/module/productinformation_news.html');

	$default_smarty->assign('MODULE_productinformation_news', $module);
}
?>