<?php
/*
includes/modules/news.php	14.11.2005
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/
require_once ('includes/application_top.php');
if (NEWS_MODUL_ON == 'true') {
  $module_smarty = new Smarty;
  $module_smarty->assign('tpl_path', 'templates/' . CURRENT_TEMPLATE . '/');
  $news_query = "
		SELECT 
			n.news_id,
			n.status,
			n.show_date,
			DATE_FORMAT(n.date, '%d.%m.%Y, %H:%i') AS DATE,
			nd.title, 
			nd.subtitle,
			nd.shorttext,
			nd.text,
			nd.image
    FROM " . TABLE_NEWS . " n
		INNER JOIN " . TABLE_NEWS_DESCRIPTION . " nd ON n.news_id = nd.news_id
		WHERE nd.language_id = '" . (int)$_SESSION['languages_id'] . "'
		ORDER BY n.news_id DESC 
		LIMIT " . NEWS_LIMIT_STARTPAGE . "";
  $row = 0;
  $news_modul = array();
  $news_query = xtDBquery($news_query);
  while ($news_data = xtc_db_fetch_array(&$news_query, true)) {
    $image = '';
    if ($news_data['image']) {
      $image = DIR_WS_CATALOG_INFO_NEWS_IMAGES . $news_data['image'];
    }

    $search = array( chr(228), chr(246), chr(252), chr(196), chr(214), chr(220), chr(153), chr(155), chr(167), chr(169), chr(171), chr(174), chr(223), ',', '"', ' ', "'" );
    $replace = array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', '-', '-', '-', '-', '-', '-', 'ss', '-', '-', '-', '-');

    $titleForUri = str_replace($search, $replace, $news_data['title']);
    $titleForUri = strtolower($titleForUri);

    // Überflüssige Bindestriche entfernen
    $titleForUri = preg_replace('/[-]+/','-',$titleForUri); 

    $news_modul[] = array(
			//'NEWS_LINK' => xtc_href_link(FILENAME_SHOP_NEWS, 'news_id=' . $news_data['news_id']), 
      'NEWS_LINK' => xtc_href_link('shopnews.php/'.$titleForUri.'/'.$news_data['news_id']),
			'NEWS_TITLE' => $news_data['title'], 
			'NEWS_TEXT' => $news_data['shorttext'], 
			'NEWS_IMAGE' => $image, 
			'NEWS_DATE' => $news_data['DATE'], 
			'NEWS_STATUS' => $news_data['status'], 
			'NEWS_SHOW_DATE' => $news_data['show_date']
		);
    $row++;
  }
  $module_smarty->assign('language', $_SESSION['language']);
  $module_smarty->assign('news_modul', $news_modul);
  if (USE_CACHE == 'false') {
    $module_smarty->caching = 0;
    $module = $module_smarty->fetch(CURRENT_TEMPLATE . '/module/news.html');
  } else {
    $module_smarty->caching = 0;
    $module = $module_smarty->fetch(CURRENT_TEMPLATE . '/module/news.html');
  }
  $default_smarty->assign('MODULE_news', $module);
}
?>