<?php
/* -----------------------------------------------------------------------------------------
   $Id: remind_mails.php,v 1.1 2007/05/11 12:49:30 xt-module.de

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on:
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(compatibility.php,v 1.19 2003/04/09); www.oscommerce.com
   (c) 2003     nextcommerce (compatibility.php,v 1.5 2003/08/13); www.nextcommerce.org
  
   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

function sendremindmails() {

	$objSmarty= new Smarty();
	$objSmarty->assign('tpl_path','templates/'.CURRENT_TEMPLATE.'/');
	$objSmarty->assign('logo_path',HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');
	$objSmarty->assign('STORE_NAME',STORE_NAME);

	$objSmarty->caching = false;
	
	$strstockQuery = "
		SELECT
			cr.remind_id,
			cr.customers_id,
			cr.customers_email_address,
			cr.customers_firstname,
			cr.customers_lastname,
			cr.products_id,
			pd.products_name,
			p.products_quantity,
			p.products_model,
			cr.remind_date_added
		FROM
			customers_remind cr,
			".TABLE_PRODUCTS." p,
			".TABLE_PRODUCTS_DESCRIPTION." pd						
		WHERE
			p.products_id = cr.products_id
		AND
			pd.products_id = cr.products_id
		AND
			p.products_quantity > '0'
		";	
	
	$resstockQuery = xtc_db_query($strstockQuery);

	while($arrstock = xtc_db_fetch_array($resstockQuery)) {
		
		$objSmarty->assign('PRODUCTS_NAME', $arrstock['products_name']);
		$objSmarty->assign('PRODUCTS_MODEL', $arrstock['products_model']);
		$objSmarty->assign('CUSTOMERS_FIRSTNAME', $arrstock['customers_firstname']);
		$objSmarty->assign('CUSTOMERS_LASTNAME', $arrstock['customers_lastname']);
			
		$link = HTTP_SERVER.DIR_WS_CATALOG.FILENAME_PRODUCT_INFO."?products_id=".$arrstock['products_id'];
			
		$objSmarty->assign('LINK', $link);
			
		$objSmarty->assign('logo_path', HTTP_SERVER.DIR_WS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/img/');	
	
		$html_mail = $objSmarty->fetch(CURRENT_TEMPLATE."/mail/".$_SESSION['language']."/remind_mail.html");
		$txt_mail = $objSmarty->fetch(CURRENT_TEMPLATE."/mail/".$_SESSION['language']."/remind_mail.txt");
			
		$strValidateQuery = "
			SELECT
				remind_id
			FROM
				customers_remind
			WHERE
				remind_id = '".$arrstock['remind_id']."'
			AND
				customers_id = '".$arrstock['customers_id']."'
		";		
				
		$strMarkQuery = "
			DELETE FROM
				customers_remind
			WHERE
				remind_id = '".$arrstock['remind_id']."'
			AND
				customers_id = '".$arrstock['customers_id']."'
		";
	
		if(xtc_db_num_rows(xtc_db_query($strValidateQuery)) == 1) {
	
			xtc_db_query($strMarkQuery);
			
			xtc_php_mail(
				EMAIL_SUPPORT_ADDRESS, EMAIL_SUPPORT_NAME, // from
				$arrstock['customers_email_address'], '', // to
				'', // bcc
				EMAIL_SUPPORT_REPLY_ADDRESS, EMAIL_SUPPORT_REPLY_ADDRESS_NAME, // reply-to
				'', '', // attachments
				sprintf('Produkterinnerung: '.$arrstock['products_name'].' ist wieder auf Lager', STORE_NAME), // subject
				$html_mail, // HTML content
				$txt_mail // text-only content
			);
		}
	}	
	return;
}
?>