<?php
/*
shopnews.php
News Manager for Xtcommerce 3.04
Copyright 2005 by Sergej Stroh.
www.southbridge.de
*/
include ('includes/application_top.php');
$smarty = new Smarty;
require (DIR_FS_CATALOG . 'templates/' . CURRENT_TEMPLATE . '/source/boxes.php');
$breadcrumb->add(NAVBAR_TITLE_NEWS, xtc_href_link(FILENAME_SHOP_NEWS));
require (DIR_WS_INCLUDES . 'header.php');

$idUri = explode('/', $_SERVER['REQUEST_URI']);
$newsId = end($idUri);
$newsId = (int)$newsId;

if($newsId == '' || $newsId == 0 || !is_numeric($newsId)) {
	header('HTTP/1.1 301 Moved Permanently');
	header("Location: http://www.esoterikshopping.de/content/Newsarchiv.html"); 
	header("Connection: close"); 		
	xtc_redirect(xtc_href_link('content/Newsarchiv.html'));
}

else {
	$news_query = xtDBquery("-- shopnews.php
		SELECT 
			n.news_id,
			n.status,
			n.show_date,
			DATE_FORMAT(n.date, '%d.%m.%Y, %h:%i') AS DATE,
			nd.title, 
			nd.subtitle,
			nd.shorttext,
			nd.text,
			nd.products_link,
			nd.image
		FROM " . TABLE_NEWS . " n
		JOIN " . TABLE_NEWS_DESCRIPTION . " nd ON (n.news_id = nd.news_id AND nd.language_id = " . (int)$_SESSION['languages_id'] . ")
	  WHERE n.news_id = '" . (int)$newsId . "'
	");
	$news_all_data = array();
	$news_data = xtc_db_fetch_array(&$news_query, true);
	$image = '';

	// Eine nicht existenter Datensatz wurde abgefragt
	// Umleitung zur Überichtsseite
	if($news_data['title'] == '' || empty($news_data['title'])) {
		header('HTTP/1.1 301 Moved Permanently');
		header("Location: http://www.esoterikshopping.de/content/Newsarchiv.html"); 
		header("Connection: close"); 		
		xtc_redirect(xtc_href_link('content/Newsarchiv.html'));
	}

	if ($news_data['image']) {
	  $image = DIR_WS_CATALOG_INFO_NEWS_IMAGES . $news_data['image'];
	}

	if($news_data['products_link'] != '') {
		$productsLink = '<form action="'.$news_data['products_link'].'"><button class="products_link">Link zum Angebot</button></form>';
	}

	$news_all_data[] = array(
		'NEWS_TITLE' => $news_data['title'], 
		'NEWS_SUBTITLE' => $news_data['subtitle'], 
		'NEWS_SHORTTEXT' => $news_data['shorttext'], 
		'NEWS_TEXT' => $news_data['text'], 
		'NEWS_PRODUCTS_LINK' => $productsLink,
		'NEWS_DATE' => $news_data['DATE'], 
		'NEWS_IMAGE' => $image);

	$smarty->assign('news_content', $news_all_data);
	$smarty->assign('language', $_SESSION['language']);
	if (USE_CACHE == 'false') {
	  $smarty->caching = 0;
	  $main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/shopnews.html');
	} else {
	  $smarty->caching = 0;
	  $main_content = $smarty->fetch(CURRENT_TEMPLATE . '/module/shopnews.html');
	}
	$smarty->assign('main_content', $main_content);
	$smarty->assign('language', $_SESSION['language']);
	$smarty->caching = 0;
	if (!defined('RM')) {
	  $smarty->load_filter('output', 'note');
	}
	$smarty->display(CURRENT_TEMPLATE . '/index.html');			
}

?>