<html>
<head>
<title>PHPMailer - Mail() basic test</title>
</head>
<body>

<?php

require_once('../class.phpmailer.php');

$mail             = new PHPMailer(); // defaults to using php "mail()"

$body             = file_get_contents('contents.html');
$body             = eregi_replace("[\]",'',$body);

$mail->AddReplyTo("info@opsedi.de","Sven Safr");

$mail->SetFrom('info@opsedi.de', 'Sven Safr');

$mail->AddReplyTo("info@opsedi.de","Sven Safr");

$address = "info@opsedi.de";
$mail->AddAddress($address, "Sven Safr");

$mail->Subject    = "Dies ist der Betreff";

//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML($body);

$mail->AddAttachment("images/logo_email.jpg");      // attachment

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent!";
}

?>

</body>
</html>
