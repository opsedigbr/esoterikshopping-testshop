<?php
error_reporting(E_ALL ^ E_NOTICE);
$error = false;

if(isset($_GET['ens']) && ($_GET['ens'] == 'run')) {
	if(isset($_GET['file']) && ($_GET['file'] != '')) {
		$file = htmlentities($_GET['file']);
	}
	else {
		$file = '';
		$error = true;
	}

	if(!$error && $error === false) {
		// $file = 'kueche' oder 'auto', ...

		require_once('amazon_phpexcel.php');
		require_once('amazon_filebuilder.php');

		$phpexcel		= new amazonPHPExcel();
		$filebuilder 	= new amazonFilebuilder();

		// build sql query and fetch content
		$query = $filebuilder->contentQuery(strtoupper($file));
		$res = $filebuilder->queryObjectArray($query);

		if($res != '') {
			$schema		= "";
			$counter	= 0;	
			
			foreach($res as $row) {
				if($counter < 5) {

					// create the general data
					$productsId					= $row->products_id;
					$productsModel				= $row->products_model;
					$productsName				= $filebuilder->clearInputData($row->products_name);
					$productsEan				= $filebuilder->clearInputData($filebuilder->getEan($row->products_ean));
					$productsDescription		= $row->products_description;
					$productsDescription		= $filebuilder->cutStringToCertainLength($productsDescription, 1900);
					$productsDescription		= $filebuilder->clearInputData($productsDescription);
					$productsQuantity			= $row->products_quantity;
					$standard_price				= $filebuilder->getProductsPriceWithTaxAdded($row->products_id,$row->products_price);
					$specialsPrice				= $filebuilder->getProductsSpecialsPrice($row->products_id);
					$productsWeight				= trim($filebuilder->getProductsWeight($row->products_weight));
					($productsWeight > 0 && !empty($productsWeight)) ? $itemWeightUnitOfMeasure = "GR" : $itemWeightUnitOfMeasure = "";
					$productsShortDescription	= $filebuilder->clearInputData($row->products_short_description);
					$productsMetaKeywords		= $filebuilder->clearInputData($row->products_meta_keywords);
					$productsKeywords			= $filebuilder->clearInputData($row->products_keywords);
					$bullet_point1				= $filebuilder->clearTags($filebuilder->clearInputData($productsName));
					$bullet_point2				= $filebuilder->clearInputData($filebuilder->clearTagsShortDescription($productsShortDescription));
					$bullet_point3				= $filebuilder->clearTags($filebuilder->clearInputData($productsMetaKeywords));
					$bullet_point4				= $filebuilder->clearTags($filebuilder->clearInputData($productsKeywords));
					$bullet_point5				= "";
					
					// Amazon Kategien
					$amazonCatId				= $filebuilder->getAmazonCategory($row->amazon_category_id);
					if(is_array($amazonCatId)) {
						$recommended_browse_nodes1 = $amazonCatId[0];
						$recommended_browse_nodes2 = $amazonCatId[1];
					} else {
						$recommended_browse_nodes1 = $amazonCatId;
						$recommended_browse_nodes2 = "";
					}

					// get Images
					$imgArray					= array();
					$imgArray					= $filebuilder->getImages($productsId);
					for($images=0;$images<5;$images++) {
						if(trim($imgArray[$images]["big"]) != "") {
							$image[$images]		= $imgArray[$images]["big"];
						} else {
							$image[$images]		= "";
						}
					}
					$main_image_url				= $image[0];
					$swatch_image_url			= "";
					$other_image_url1			= $image[1];
					$other_image_url2			= $image[2];
					$other_image_url3			= $image[3];
					$other_image_url4			= $image[4];
					$other_image_url5			= "";
					$other_image_url6			= "";
					$other_image_url7			= "";
					$other_image_url8			= "";

					$schemaTmp = $phpexcel->getSchemaCaption($file);
					$schema = $schemaTmp[2];

					// Hole die 				

					// Erzeuge Export Feed(!)
					if($counter==0) {
						$filebuilder->buildFileCaption($phpexcel->getSchemaCaption($file), $file);
					} 
					else {
						$filebuilder->buildFile($schema, $file);
					}
				}

				$counter++;

			}
		}	


		// Hole das Schemata von Amazon
		// Schreibe dieses Schema in das Exportfeed
		

	}

}



?>