<?php
//require_once("amazon.exit.php");
//$exit = new amazonExit();

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
define('DEBUG', 0);

require_once("debug_mode.php");
$sys = new debugMode();

//require_once('../Classes/PHPExcel.php');
if($sys->getTestLiveSystem() != '' && $sys->getTestLiveSystem() == 'test') {
	define('DIR_FS_DOCUMENT_ROOT', '../../');
	//echo $sys->getTestLiveSystem();
}
else {
	require_once('../../includes/configure.php');
}
require_once(DIR_FS_DOCUMENT_ROOT.'___export-files2013/Classes/PHPExcel.php');

class amazonPHPExcel {
	
	public $fileName;
	public $filePath;
	public $amazonExcelFile;
	public $error;
	public $copyFromPath;
	public $copyToPath;
	public $filenameOldArray = array();
	public $filenameNewArray = array();
	public $amazonCategorieNameArray = array();
	
	public function __construct() {
		//$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		//$cacheSettings = array( 'memoryCacheSize' => '10MB');
		//PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		//ini_set('max_execution_time', 300);		

		$objPHPExcel = new PHPExcel();
		$this->originalFilesPath	= (DIR_FS_DOCUMENT_ROOT.'___export-files2013/amazon/files/');
		$this->copyToPath			= (DIR_FS_DOCUMENT_ROOT.'___export-files2013/amazon/exportfeeds_amazon/');
		$this->filenameOldArray = array('kueche');
		$this->amazonCategorieNameArray = array('KUECHE');
		return $objPHPExcel;
	}

	public function checkFileExists($originalFileName) {
		if(file_exists($this->originalFilesPath.$originalFileName.".xlsx")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function getSchemaCaption($originalFileName) {
		if($this->checkFileExists($originalFileName)) {
			$objPHPExcel = PHPExcel_IOFactory::load($this->originalFilesPath.$originalFileName.".xlsx");
			$sheetNames = $objPHPExcel->getSheetNames();

			// determine the index of the needed sheet
			$a = 0;
			foreach($sheetNames as $sheetKey=>$sheetValue) {
				if($sheetValue == "Vorlage") {
					$sheetIndex = $sheetKey;
					break;
				}
				$a++;
			} 
					
			$objWorksheet		= $objPHPExcel->getSheet($sheetIndex);
			$highestRow			= $objWorksheet->getHighestRow(); // e.g. 10
			$highestColumn		= $objWorksheet->getHighestColumn(); // e.g 'F'
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

			
			// fetch the content of .xlsx file
			for ($row = 1; $row <= $highestRow; ++$row) {
				$schema = '';
				for ($col = 0; $col <= $highestColumnIndex; ++$col) {
					$schema .= $objWorksheet->getCellByColumnAndRow($col, $row)->getValue() . "\t";
				}
				//$schema .= "\n";
				$schemaArray[] = $schema;
			}

			//$schema = mb_convert_encoding($schema, 'UCS-2LE', 'UTF-8');
			//$this->createExportFeed($this->filenameOldArray[$i].".xlsx", $schemas[$i]);
					
			return $schemaArray;
		}
		else {
			return false;
		}
	}
	
	public function oldFileNamesArray() {
		return $this->filenameOldArray;
	}	
	
	public function getAmazonCategorieNameArray() {
		return $this->amazonCategorieNameArray;
	}	

}
?>