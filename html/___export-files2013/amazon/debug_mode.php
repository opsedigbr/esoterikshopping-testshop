<?php
error_reporting(E_ALL ^ E_NOTICE);

class debugMode {
	
	public function getTestLiveSystem() {
		$this->env = $_SERVER['SERVER_NAME'];
		
		if(preg_match('/localhost/', $this->env) == true) {
			return 'test';
		} 
		else {
			return 'live';
		}
	}
}

?>