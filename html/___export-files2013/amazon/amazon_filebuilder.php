<?php
//require_once("amazon.exit.php");
//$exit = new amazonExit();

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
define('DEBUG', 0);

class amazonFilebuilder {

	public function __construct() {
		require_once('../../includes/configure.php');

		$this->mysqli = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
		if(mysqli_connect_errno())
		{
			echo("construct: Keine Verbindung zur Datenbank m&ouml;glich!<br />Fehlercode: ".mysqli_connect_errno());
			$this->mysqli=false;
			exit();
		}	
	}
	
	public function close() {
		$this->mysqli->close();
		$this->mysqli=false;
	}	
	
	public function buildFileCaption($schemaCaption, $amazonFileName) {
		$this->assignmentsPath		= (DIR_FS_DOCUMENT_ROOT.'___export-files2013/amazon/exportfeeds_amazon/');
		$this->fileName 			= 'amazon-'.$amazonFileName.".txt";
		$this->schemeCaption		= $schemaCaption;
	
		// now create the file and put it to the right place
		$handle = fopen($this->assignmentsPath.$this->fileName, "w+");

		// get the schemaCaption
		header('Content-Type: text/html; charset=utf-8');
		//fputs($handle, mb_convert_encoding($schemaCaptionArray[0], 'UCS-2LE', 'UTF-8')."\r\n");
		//fputs($handle, mb_convert_encoding($schemaCaptionArray[1], 'UCS-2LE', 'UTF-8')."\r\n");		
		fputs($handle, mb_convert_encoding($this->schemeCaption[0], 'Windows-1252', 'UTF-8')."\n");
		fputs($handle, mb_convert_encoding($this->schemeCaption[1], 'Windows-1252', 'UTF-8')."\n");					
		fputs($handle, $this->schemeCaption[2]."\n");
	} 

	public function buildFile($schema, $amazonFileName)	{
		$this->assignmentsPath		= (DIR_FS_DOCUMENT_ROOT.'___export-files2013/amazon/exportfeeds_amazon/');
		$this->fileName 			= 'amazon-'.$amazonFileName.".txt";
		$this->schema				= $schema;

		// now create the file and put it to the right place
		$handle = fopen($this->assignmentsPath.$this->fileName, "a+");

		// get the schema
		header('Content-Type: text/html; charset=utf-8');
		fputs($handle, $this->schema."\n");		
	}

	public function contentQuery($amazonCategoryName) {
		$sql = ("
		SELECT
			 p.products_id,
			 p.products_ean, 
			 p.products_model,
			 p.products_quantity,
			 p.products_image,
			 p.products_price,
			 p.products_status,
			 p.products_date_available,
			 p.products_shippingtime,
			 p.products_discount_allowed,
			 p.products_tax_class_id,
			 p.products_date_added,
			 p.products_weight, 
 			 p.products_date_added, 
			 pd.products_name,
			 pd.products_description,
			 pd.products_short_description,
			 pd.products_meta_keywords,
			 pd.products_keywords,
			 m.manufacturers_name  
		 FROM
			 products p LEFT JOIN
			 manufacturers m
		   ON p.manufacturers_id = m.manufacturers_id LEFT JOIN
			 products_description pd
		   ON p.products_id = pd.products_id AND
			pd.language_id = '1' LEFT JOIN
			 specials s
		   ON p.products_id = s.products_id
		 WHERE
		   p.products_status = 1 

		 ORDER BY p.products_id ASC");
			
		return $sql;
	}	

	public function getEan($ean) {
		$ean = str_replace(array('-', ' '), array('', ''), $ean);
		return $ean;	
	}
	
	public function getKeywords($str) {
		$keyword = array();
		$str = explode(' ', $str);
		foreach($str as $keywords) {
			if(strlen($keywords) > 3)
				$keyword[] = $keywords;
		}
		return $keyword;
	}
	
	public function getAmazonCategory($amazonCateroryDB) {
		if(strpos($amazonCateroryDB, ',') !== false) {
			$amazonCateroryDB = explode(',', $amazonCateroryDB);
			foreach($amazonCateroryDB as $amazonIds) {
				$ac[] = $amazonIds;
			}
			return $ac;
		} else {
			return $amazonCateroryDB;
		}		
	}
		
	public function getProductsPriceWithTaxAdded($productsId, $productsPrice)
	{
		$result = $this->queryObjectArray("SELECT products_tax_class_id FROM products WHERE products_id = '".$productsId."' LIMIT 1");
		foreach($result as $rows) {
			$products_tax_class_id = $rows->products_tax_class_id;
		}
		
		$result = $this->queryObjectArray("SELECT tax_rate FROM tax_rates WHERE tax_class_id = '".$products_tax_class_id."' LIMIT 1");
		foreach($result as $rows) {
			$tax_rate = $rows->tax_rate;
		}
		
		$productsPriceWithTaxAdded = $productsPrice*(1+($tax_rate/100));
		$productsPriceWithTaxAdded = number_format($productsPriceWithTaxAdded, 2, '.', '');
		$productsPriceWithTaxAdded = $this->clearInputData($productsPriceWithTaxAdded);
		return $productsPriceWithTaxAdded;
	}	
	
	public function getProductsSpecialsPrice($productsId)
	{
		$specialsQuantity = 0;
		$specialsNewProductsPrice = 0;
		
		$result = $this->queryObjectArray("SELECT products_tax_class_id FROM products WHERE products_id = '".$productsId."' LIMIT 1");
		foreach($result as $rows) {
			$products_tax_class_id = $rows->products_tax_class_id;
		}
		
		$result = $this->queryObjectArray("SELECT tax_rate FROM tax_rates WHERE tax_class_id = '".$products_tax_class_id."' LIMIT 1");
		foreach($result as $rows) {
			$tax_rate = $rows->tax_rate;
		}
		
		$res = $this->queryObjectArray("SELECT specials_quantity, specials_new_products_price FROM specials WHERE products_id = '".$productsId."' LIMIT 1");
		if($res!='') {
			foreach($res as $rows) {
				$specialsQuantity = $rows->specials_quantity;
				$specialsNewProductsPrice = $rows->specials_new_products_price;
			}
		}
		
		if($specialsQuantity > 0 && $specialsNewProductsPrice > 0) {
			$specialsNewProductsPrice = $specialsNewProductsPrice*(1+($tax_rate/100));
			$specialsNewProductsPrice = number_format($specialsNewProductsPrice, 2, '.', '');
			$specialsNewProductsPrice = $this->clearInputData($specialsNewProductsPrice);
			return $specialsNewProductsPrice;
		} else {
			return '';
		}
	}	
	
	public function getSalesEndDate($productsId) {
		$expiresDate = '';
		
		$res = $this->queryObjectArray("SELECT expires_date FROM specials WHERE products_id = '".$productsId."' LIMIT 1");
		if($res!='') {
			foreach($res as $rows) {
				$expires = $rows->expires_date;
			}
			$expires = explode(' ', $expires);
			$expiresDate = $expires[0];
		}
		return $expiresDate;
	}
	
	public function removeCertainTag($data, $tag) {
		preg_match("/<".$tag."(.*?)<\/".$tag.">/", $data, $found);
		$foundTmp = $found[0];
		if($tag == "iframe" && count($found) > 0) {
			$foundTmp = explode(">", $foundTmp);
			$foundToReplace = $foundTmp[0].">";
			$data = str_replace($foundToReplace, '', $data);
			$data = str_replace("</iframe>", '', $data);
		} else {
			$data = str_replace($foundToReplace, '', $data);
		}
		return $data;
	}
	
	public function cutStringToCertainLength($data, $length) {
		if(strlen($data) > $length) {
			$data = substr($data, 0, $length)."...";
		}
		return $data;
	}		
	
	public function getProductsWeight($pWeight) {
		if(strpos($pWeight, '.') !== false) {
			$pWeight = explode('.', $pWeight);
			$productsWeight = $pWeight[0];
			if($productsWeight === '0')
				$productsWeight = '';

			return $productsWeight;
		} else {
			return '';
		}
	}
	
	public function getImages($pId)
	{
		$images = array();
		$res = $this->queryObjectArray("SELECT products_image FROM products WHERE products_id = '".$pId."' LIMIT 0,1");
		if($res != '') {
			foreach($res as $row) {
				$small = HTTP_SERVER.'/'.DIR_WS_THUMBNAIL_IMAGES.$row->products_image;
				$big = HTTP_SERVER.'/'.DIR_WS_ORIGINAL_IMAGES_AMAZON.'/'.$row->products_image;
				$images[] = array("small" => $small, "big" => $big);
			}	
		}
		
		$res = $this->queryObjectArray("SELECT image_name FROM products_images WHERE products_id = '".$pId."' LIMIT 0,4");
		if($res != '') {
			foreach($res as $row) {
				$small = HTTP_SERVER.'/'.DIR_WS_THUMBNAIL_IMAGES.$row->image_name;
				$big = HTTP_SERVER.'/'.DIR_WS_ORIGINAL_IMAGES_AMAZON.'/'.$row->image_name;
				$images[] = array("small" => $small, "big" => $big);
			}
		}
		return $images;
	}	
		
	public function queryObjectArray($sql) {
		if($result = $this->mysqli->query($sql)) {
			if($result->num_rows>0)
			{
				while($row = $result->fetch_object()) {
					$result_array[] = $row;	
				}
				return $result_array;
			} 
			else {
				$this->error++;
				return false;
			}
		}
		else {
			echo("queryObjectArray: Keine Verbindung zur Datenbank m&ouml;glich!<br />Fehlercode: ".mysqli_connect_errno());		
		}
	}
	
	public function clearTags($data)
	{
		if(trim($data) == '') {
			return trim($data);	
		} else {
		$data = strip_tags($data, "<br><p>");
		return $data;		
		}
	}	
	
	public function clearTagsShortDescription($data)
	{
		if(trim($data) == '') {
			return '';	
		} else {
			$data	 = strip_tags($data, "<br>");
			$tmp	 = explode('<br>', $data);
			$tmp1	 = explode('<br />', $tmp[0]);
			$dataNew = strip_tags($tmp1[0]);
			$dataNew = str_replace('&nbsp;', '', $dataNew);
			
			if(trim($dataNew) == '') {
				return '';
			} else {
				return $dataNew;		
			}
		}
	}	
	
	public function getModelYear($pid) {
		$sql = "SELECT products_date_added FROM products WHERE products_id = '".$pid."'";
		$res = $this->queryObjectArray($sql);
		if($res != '') {
			foreach($res as $rows) {
				$productsDateAdded = $rows->products_date_added;
			}		
			$productsDateAdded = explode('-', $productsDateAdded);
			return $productsDateAdded[0];
		} else {
			return '2013';	
		}
	}

	public function clearInputData($data)
	{
		if(trim($data) == '') {
			return trim($data);	
		} else {
		$data = strip_tags($data, '<br><p><b><strong>');
		$data = str_replace("\r", '', $data);
		$data = str_replace("\n", '', $data);
		$data = str_replace("\t", '', $data);
		$data = preg_replace('/<\s*html[^>]*>/', '', $data);
		$data = preg_replace('/<\s*head[^>]*>/', '', $data);
		$data = preg_replace('/<\s*\/\s*html[^>]*>/', '', $data);
		$data = preg_replace('/<\s*\/\s*head[^>]*>/', '', $data);
		$data = '"' . $data . '"';	
		$data = str_replace('""', '"', $data);
		return $data;		
		}
	}	
	
	public function getCategoryId($pid, $l=1) {
		$categoryArray = array();
		$sql = "SELECT categories_id FROM products_to_categories WHERE products_id = '".$pid."' ORDER BY categories_id ASC LIMIT 0,".$l;
		$res = $this->queryObjectArray($sql);
		
		if($res != '') {
			foreach($res as $rows) {
				$categoryArray[] = $rows->categories_id;
			}		
		}
		return $categoryArray;
	}
	
	public function buildCategory($catID, $l=false) {
		$cat=array();
		$tmpID=$catID;

		while ($this->getParent($catID)!=0 || $catID!=0) {
			$result = $this->queryObjectArray("SELECT categories_name FROM categories_description WHERE categories_id='".$catID."' ".$l);
			if($res != '') {
				foreach($result as $rows) {
					$cat[] = $rows->categories_name;
				}			
			}
			$catID=$this->getParent($catID);
		}
		
		$categoryString='';
		for ($i=count($cat);$i>0;$i--) {
			$categoryString.=$cat[$i-1].' > ';
		}
		$categoryString = substr($categoryString, 0, -3);
		
		return $categoryString;
	}
	
	function getCatId($catID) {
		$op_id = array();
		$tmpID=$catID;
		$op_id[] = $catID;

		while ($this->getParent($catID)!=0 || $catID!=0) {
			$catID=$this->getParent($catID);
			if($catID!=0)
				$op_id[]=$catID;
		}
		return $op_id;
    }	
    
	public function getParent($catID) {
		$res = $this->queryObjectArray("SELECT parent_id FROM categories WHERE categories_id='".$catID."'");
		if($res != '') {
			foreach($res as $rows) {
				$this->parentId = $rows->parent_id;
			}
			return $this->parentId;
		}
		else {
			return false;
		}
	}	
	
	public function getFileName() {
		$f = explode('&', $_SERVER['QUERY_STRING']);
		$fn = explode('=', $f[1]);
		return $fn[1];
	}
	
	public function buildCAT($catID) {
    	if (isset($this->CAT[$catID])) {
	         return $this->CAT[$catID];
        } 
		else {
        	$op_id = array();
         	$tmpID=$catID;
			$op_id[] = $catID;

            while ($this->getParent($catID)!=0 || $catID!=0) {
				$catID=$this->getParent($catID);
				$op_id[]=$catID;
			}
	    	return $op_id;
        }
    }
}
?>