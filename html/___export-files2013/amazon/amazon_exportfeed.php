<?php
error_reporting(E_ALL ^ E_NOTICE);
set_time_limit(0);
ignore_user_abort(true);
define('DEBUG', 0);

class amazonExportFeed {

	function __construct() {
		require_once('../../includes/configure.php');
		$this->mysqli = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
		if(mysqli_connect_errno())
		{
			echo("construct: Keine Verbindung zur Datenbank m&ouml;glich!<br />Fehlercode: ".mysqli_connect_errno());
			$this->mysqli=false;
			exit();
		}	
		
		// Const
		//define('DIR_WS_ORIGINAL_IMAGES_AMAZON', 'images/product_images/original_images_amazon');
	}

	public function close() {
		$this->mysqli->close();
		$this->mysqli=false;
	}

	

}		

