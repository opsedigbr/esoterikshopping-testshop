<?php
/* -----------------------------------------------------------------------------------------
   $Id: specials.php 1292 2005-10-07 16:10:55Z mz $   

   XT-Commerce - community made shopping
   http://www.xt-commerce.com

   Copyright (c) 2003 XT-Commerce
   -----------------------------------------------------------------------------------------
   based on: 
   (c) 2000-2001 The Exchange Project  (earlier name of osCommerce)
   (c) 2002-2003 osCommerce(specials.php,v 1.47 2003/05/27); www.oscommerce.com 
   (c) 2003	 nextcommerce (specials.php,v 1.12 2003/08/17); www.nextcommerce.org

   Released under the GNU General Public License 
   ---------------------------------------------------------------------------------------*/

include ('includes/application_top.php');
$smarty = new Smarty;
// include boxes
require (DIR_FS_CATALOG.'templates/'.CURRENT_TEMPLATE.'/source/boxes.php');

require_once (DIR_FS_INC.'xtc_get_short_description.inc.php');

$breadcrumb->add(NAVBAR_TITLE_SPECIALS, xtc_href_link(FILENAME_SPECIALS));

//fsk18 lock
$fsk_lock = '';
if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
	$fsk_lock = ' and p.products_fsk18!=1';
}
if (GROUP_CHECK == 'true') {
	$group_check = " and p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
}

switch ($_GET['multisort']) {
  case 'specialprice':
    $sorting = 'ORDER BY s.specials_new_products_price DESC';
    break;  
  case 'new_asc':
    $sorting = ' ORDER BY p.products_date_added ASC';
    break;
  case 'new_desc':
    $sorting = ' ORDER BY p.products_date_added DESC';
    break;      
  case 'name_asc':
    $sorting = ' ORDER BY pd.products_name ASC';
    break;
  case 'name_desc':
    $sorting = ' ORDER BY pd.products_name DESC';
    break;
  case 'price_asc':
    $sorting = ' ORDER BY p.products_price ASC';
    break; 
  case 'price_desc':
    $sorting = ' ORDER BY p.products_price DESC';
    break;  
  default:
    $sorting = ' ORDER BY s.specials_date_added DESC';  
}

$specials_query_raw = "select p.products_id,
                                pd.products_name,
                                p.products_price,
								                p.products_quantity, 
                                p.products_tax_class_id,
                								p.products_shippingtime,
                                p.products_image,
                								p.products_vpe_status,
                								p.products_vpe_value,
                								p.products_vpe,
                								p.products_fsk18,
                                s.specials_new_products_price from ".TABLE_PRODUCTS." p, ".TABLE_PRODUCTS_DESCRIPTION." pd, ".TABLE_SPECIALS." s
                                where p.products_status = '1'
                                and s.products_id = p.products_id
								                and p.products_gift != 1 
                                and p.products_id = pd.products_id
                                ".$group_check."
                                ".$fsk_lock."
                                and pd.language_id = '".(int) $_SESSION['languages_id']."'
                                and s.status = '1' ".$sorting;			

  $multisort_dropdown = xtc_draw_form('multisort', $_REQUEST['linkurl'], 'get') . "\n";

 if (isset($_GET['manufacturers_id'])) {
   $multisort_dropdown .= xtc_draw_hidden_field('manufacturers_id', $_GET['manufacturers_id']);  
 } else {
  $multisort_dropdown .= xtc_draw_hidden_field('cPath', $cPath) . "\n";
  $multisort_dropdown .= xtc_draw_hidden_field('sort', $_GET['sort']) . "\n";
 } 
  if(isset($_GET['filter_id'])) {
    $multisort_dropdown .= xtc_draw_hidden_field('filter_id', (int)$_GET['filter_id']);
   } 

  $options = array(
      array('text' => MULTISORT_STANDARD));
  /*
  $options[] = 
      array('id' => 'specialprice', 
            'text' => MULTISORT_SPECIALS_DESC);   
  */      
  $options[] = 
      array('id' => 'new_desc', 
            'text' => MULTISORT_NEW_DESC);
  $options[] = 
      array('id' => 'new_asc', 
            'text' => MULTISORT_NEW_ASC);
  $options[] = 
      array('id' => 'price_asc', 
            'text' => MULTISORT_PRICE_ASC);   
  $options[] = 
      array('id' => 'price_desc', 
            'text' => MULTISORT_PRICE_DESC);
  $options[] = 
      array('id' => 'name_asc', 
            'text' => MULTISORT_ABC_AZ); 
  $options[] = 
      array('id' => 'name_desc', 
            'text' => MULTISORT_ABC_ZA);                 
  

  $multisort_dropdown .= xtc_draw_pull_down_menu('multisort', $options, $_GET['multisort'], 'onchange="this.form.submit()"') . "\n";
  $multisort_dropdown .= '</form>' . "\n";                                					
								
$specials_split = new splitPageResults($specials_query_raw, $_GET['page'], MAX_DISPLAY_SPECIAL_PRODUCTS);

$module_content = '';
$row = 0;
if ($specials_split->number_of_rows==0) xtc_redirect(xtc_href_link(FILENAME_DEFAULT));
require (DIR_WS_INCLUDES.'header.php');
$specials_query = xtc_db_query($specials_split->sql_query);



while ($specials = xtc_db_fetch_array($specials_query)) {
	$module_content[] = $product->buildDataArray($specials);
}

if (($specials_split->number_of_rows > 0)) {
	$smarty->assign('NAVBAR', '
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
	          <tr>
	            <td class="smallText">'.$specials_split->display_count(TEXT_DISPLAY_NUMBER_OF_SPECIALS).'</td>
	            <td align="right" class="smallText">'.TEXT_RESULT_PAGE.' '.$specials_split->display_links(MAX_DISPLAY_PAGE_LINKS, xtc_get_all_get_params(array ('page', 'info', 'x', 'y'))).'</td>
	          </tr>
	        </table>
	
	');
}

$smarty->assign('MULTISORT_DROPDOWN',$multisort_dropdown);
$smarty->assign('language', $_SESSION['language']);
$smarty->assign('module_content', $module_content);
$smarty->caching = 0;
$main_content = $smarty->fetch(CURRENT_TEMPLATE.'/module/specials.html');

$smarty->assign('language', $_SESSION['language']);
$smarty->assign('main_content', $main_content);
$smarty->caching = 0;
if (!defined(RM))
	$smarty->load_filter('output', 'note');
$smarty->display(CURRENT_TEMPLATE.'/index.html');
include ('includes/application_bottom.php');
?>