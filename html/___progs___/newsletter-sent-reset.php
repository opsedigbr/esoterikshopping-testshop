<?php

class NewsletterSentReset {

	function __construct() {
		include('../includes/configure.php');
		$this->mysqli = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);
	}

	function resetNewsletterSentFlag() {
		$start = microtime(true);
		$sql = ("UPDATE customers SET newsletters_sent = '0';");
		$update = $this->mysqli->query($sql);
		$this->mysqli->affected_rows;
		$this->duration = microtime(true)-$start;
		$this->duration = round($this->duration, 6);
	}

	function sendEmail() {
		$nachricht = '';

		// E-Mail versenden
		$empfaenger = 'info@opsedi.de';
		$betreff = "Newsletter: Esoterikshopping.de";
		$nachricht .= "Die Flag (Newsletters_Sent) wurde auf '0' zurueck gesetzt.\n";
		$nachricht .= "Betroffen waren ".$this->mysqli->affected_rows." Datensaetze!\n\n";
		$nachricht .= "Dauer: ".$this->duration." Sekunden.";
		$header = 'From: info@opsedi.de' . "\r\n" . 'Reply-To: info@opsedi.de' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
		mail($empfaenger, $betreff, $nachricht, $header);
	}
}

$nl = new NewsletterSentReset();
$nl->resetNewsletterSentFlag();
$nl->sendEmail();

?>