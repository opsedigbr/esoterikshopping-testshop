<table width="550" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial;font-size:12px;">
  <tr>
    <td width="130" align="center"><img src="../images/ssl_1.gif" width="117" height="117" border="0"></td>
    <td align="center" style="font-size:16px;"><strong>SSL-Verschl&uuml;sselung<br />(Secure Socket Layer)</strong></td>
    <td width="130" align="center"><img src="../images/ssl_zertifikat.gif" width="90" height="50"></td>
  </tr>
  <tr>
    <td colspan="3"><br />
      <strong>Wieso ist die SSL-Verschl&uuml;sselung so wichtig ?</strong><br />
      <br />
    Wir ben&ouml;tigen f&uuml;r verschiedene Gesch&auml;ftsvorg&auml;nge Ihre personenbezogenen Daten. So m&uuml;ssen beispielsweise w&auml;hrend des Login-Prozesses zu Ihrem Kundenkonto sensible Daten an unseren Server &uuml;bermittelt werden. Damit diese nicht in falsche H&auml;nde gelangen, verschl&uuml;sseln wir Sie mit dem SSL-Verfahren (Secure-Socket-Layer-Verfahren).<br />
    <br />
    <strong>Dies ist das derzeit sicherste Daten&uuml;bertragungsverfahren im Internet.</strong><br />
    <br />
    Durch die Anwendung des SSL-Verfahrens werden Ihre Daten, bevor sie zu unserem Server &uuml;bertragen werden, so verschl&uuml;sselt, dass ein Dritter diese nicht rekonstruieren kann. Im Rahmen dieses Verschl&uuml;sselungsverfahrens wird sichergestellt, dass Ihre Daten ausschlie&szlig;lich an unseren Server geschickt werden, von dem Sie angefordert wurden. Beim Eintreffen Ihrer Daten auf unserem Server werden diese auf Vollst&auml;ndigkeit und Unver&auml;ndertheit gepr&uuml;ft.<br />
    <br />
    Im Internet Explorer (IE) erkennen Sie eine verschl&uuml;sselte &Uuml;bertragung Ihrer Daten an dem gelben Schloss-Symbol in der Leiste Ihres Browsers.</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><br /><br />
    <img src="../images/ssl_2.gif" width="531" height="40"><br /></td>
  </tr>
</table>
