<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="2"><strong>Wir bieten Ihnen folgende Zahlm&ouml;glichkeiten an:</strong></td>
  </tr>
  <tr> 
    <td width="30%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><img src="../../images/vorkasse.jpg" width="150" height="105"></td>
    <td valign="top"><strong>Vorkasse</strong><br>
      Bei der Bezahlung per Vorauskasse wird der Kaufpreis vorab &uuml;berwiesen.<br>
      Nach dem Kauf in unserem Onlineshop erhalten Sie eine Auftragsbest&auml;tigung 
      per E-Mail. Dort finde Sie unsere Bankverbindung f&uuml;r die &Uuml;berweisung. 
      Die Transaktion dauert ca. 1-3 Werktage. Nach Zahlungseingang auf unserem 
      Konto wird die Ware umgehend an Sie versendet.</td>
  </tr>
  <tr> 
    <td valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top"><img src="../../images/paypal.jpg" width="150" height="105"></td>
    <td valign="top"><strong>PayPal</strong><br>
      Direkt nach dem Kauf werden Sie automatisch zu PayPal.de weitergeleitet. 
      Sie m&uuml;ssen sich dann direkt bei PayPal.de einloggen und den Kaufpreis 
      online bezahlen.<br>
      Vorteil f&uuml;r Sie bei dieser Zahlart - Die Ware (sofern lieferbar) wird 
      innerhalb von 24h versendet da der Betrag sofort auf unserem PayPal-Konto 
      gutgeschrieben wird. </td>
  </tr>
  <tr> 
    <td valign="top">&nbsp;</td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td valign="top"><img src="../../images/sofortueberweisung.jpg" width="150" height="105"></td>
    <td valign="top"><strong>Sofort&uuml;berweisung</strong><br>
      Sie verf&uuml;gen &uuml;ber Onlinebanking - dann ist Sofort&uuml;berweisung 
      unser Tipp f&uuml;r Sie!<br>
      Direkt nach dem Kauf werden Sie automatisch zu Sofort&uuml;berweisung.de 
      weiter geleitet. Sie &uuml;berweisen den Bestellwert via Onlinebanking (PIN 
      / TAN) direkt von Ihrem Bankkonto aus. Eine schnelle und sichere Art der 
      Zahlungsabwicklung.<br>
      Da das Geld sofort auf unser Konto &uuml;berwiesen wird, erfolgt der Versand 
      der Ware (sofern lieferbar) innerhalb von 24h.</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top"><img src="../../images/rechnung.jpg" width="150" height="105"></td>
    <td valign="top"><strong>Rechnung</strong> (ab einem Bestellwert von 15,00 Euro nur innerhalb Deutschland)<br>
    Kaufen Sie bei uns bequem auf Rechnung ein. Bei Zahlung auf Rechnung ist der Rechnungsbetrag nach Erhalt der Ware innerhalb 
      von 7 Tagen zu begleichen. Wenn Sie die Zahlart &bdquo;Rechnung&ldquo; w&auml;hlen wird nach dem Kauf eine Bonit&auml;tspr&uuml;fung vorgenommen (<a href="../../content/Datenschutzerklaerung.html" target="_parent">mehr Informationen</a>). F&uuml;r die Kosten, die uns daf&uuml;r  entstehen, erheben wir einen Aufschlag in H&ouml;he von 1,25 Euro. Falls die Bonit&auml;tspr&uuml;fung negativ bzw. nicht eindeutig ist (z.B. Person unbekannt), behalten wir uns vor die gew&auml;hlte Zahlart abzulehnen. In diesem Fall werden wir Sie per E-Mail dar&uuml;ber informieren.</td>
  </tr>
  <tr> 
    <td colspan="2"><br /></td>
  </tr>
  <tr> 
    <td><img src="../../images/kreditkarte.jpg" width="150" height="121"></td>
    <td valign="top"><strong>Kreditkarte</strong><br>
Einfach - Bequem - Schnell! 
Kaufen Sie bei uns bequem per Kreditkarte (Visa/Mastercard) ein. 
Nach Zahlungseingang auf unserem 
    Konto wird die Ware umgehend an Sie versendet.</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
