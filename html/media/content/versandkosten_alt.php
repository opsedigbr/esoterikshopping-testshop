<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unbenanntes Dokument</title>
</head>

<body>
<p>Um ein schnelles und reibungsloses Zustellen Ihrer bestellten Waren zu garantieren, versenden Ihre Ware generell nur mit Transportversicherung.<br />
  Eine Lieferung an Packstationen ist nicht möglich.</p>
<p>Versandkosten Innerdeutsch:<br />
  Alle Gewichtsklassen (versichert): 4.90 €<br />
</p>
<p>Versandkosten in der EU:</p>
<p>Wir versenden in folgende Länder:<br />
  Österreich,  Belgien, Holland, Luxemburg, Dänermark und Tschechien.</p>
<p>Alle Gewichtsklassen (versichert): 8.90 €<br />
</p>
<p>Versand in andere europäische Länder / Weltweit:</p>
<p>Wir versenden in folgende Länder:<br />
  Schweiz, Frankreich, England, Italien, Polen, Ungarn, Schweden, slowakische Republik, Slowenien, Rußland</p>
<p>Alle Gewichtsklassen (versichert): 12.90 €<br />
</p>
<p>Bestellungen in die Schweiz:<br />
  - Da die Einfuhr von Dolchen und Athamen gegen das schweizer Waffengesetz verstößt, dürfen wir dementsprechend diese Artikel nicht in die Schweiz versenden!<br />
  - Die anfallenden Verzollungskosten und Zölle müssen vom Kunden übernommen werden. Informationen dazu finden Sie beim Schweizer Zoll, unter www.Zoll.ch</p>
<p>&nbsp;</p>
<p>Versandkosten Händler innerdeutsch:<br />
  Alle Größen und Gewichtsklassen: 6.90 €<br />
</p>
<p>Versandkosten Händler in der EU:</p>
<p>Wir versenden in folgende Länder:<br />
  Österreich, Belgien, Holland, Luxemburg, Tschechien, Dänemark</p>
<p>Alle Gewichtsklassen (versichert): 8.90 €<br />
</p>
<p>Versandkosten England, Frankreich, Italien, Polen:<br />
  Alle Gewichtsklassen (versichert): 12.90 €<br />
</p>
<p>Versandkosten Händler in andere europäische Länder / Weltweit:</p>
<p>Wir versenden in folgende Länder:<br />
  Schweiz, Schweden, slowakische Republik, Slowenien, Ungarn, Rußland</p>
<p>Alle Gewichtsklassen (versichert): 24.00 €<br />
</p>
<p>Shipping outside the EU:</p>
<p>if you order from outside Europe, the shipping costs depends on the weight of the products.<br />
  Please wait after your order, until we mail you your actual shipping costs, or ask us by mail, how much the shipping costs of your products are.</p>
<p>The shipping prices:</p>
<p>up to 500 grams: 10,90 Euros<br />
  500 - 1000 grams: 14,90 Euros<br />
  1000 - 2000 grams 26,90 Euros</p>
</body>
</html>
