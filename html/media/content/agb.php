<?php
$vrrl_umstellung_zeit = mktime(00,00,00,6,13,2014);
$now = time();
$umstellung = $vrrl_umstellung_zeit - $now;
?>

<div id="conditions">
    <h1>Vertragsbestimmungen</h1><br />
    <h2>&sect; 1 Anwendungsbereich</h2>
    Unsere nachstehenden Vertragsbestimmungen gelten f&uuml;r die Lieferung und den Verkauf unserer Waren gegen&uuml;ber privaten Verbrauchern und sonstigen Bestellern.<br /> 
    <br /> 
    Gegen&uuml;ber Bestellern, die keine privaten Verbraucher sind, gelten die Vertragsbestimmungen allerdings nur mit der Ma&szlig;gabe, dass das in &sect; 8 n&auml;her bezeichnete Widerrufsrecht keine Anwendung findet. Bestellungen erfolgen verbindlich per Email, Fax oder per Post. <br /><br /> 
   	<h2>&sect; 2 Zustandekommen des Vertrages</h2> 
    Sie w&auml;hlen zun&auml;chst aus unserem Online-Shop die von Ihnen gew&uuml;nschten Waren durch Einlegen in den virtuellen Warenkorb. Vor Abgabe der Bestellung haben Sie die M&ouml;glichkeit, &uuml;ber die Schaltfl&auml;che &quot;Warenkorb&quot; die von Ihnen zuvor gemachten Bestelleingaben zum Zwecke der Kontrolle und Pr&uuml;fung einzusehen. An dieser Stelle k&ouml;nnen Sie eventuelle Eingabefehler erkennen und berichtigen.<br /> 
     <br />
      Der Kauf erfolgt nach Eingabe Ihrer pers&ouml;nlichen Daten, der Wahl der Zahlungsart und Versandart durch Klicken des Buttons &quot;Kaufen&quot;. Der Eingang Ihrer Bestellung wird von uns per E-Mail best&auml;tigt. Hierdurch kommt ein rechtsg&uuml;ltiger Vertrag zwischen Ihnen und uns zustande.<br /><br /> 
     <h2>&sect; 3 Verf&uuml;gbarkeit des Vertragstextes</h2>
      Der Vertragstext steht ausschlie&szlig;lich in deutscher Sprache zur Verf&uuml;gung. <br /> 
      <br />
      Wir speichern den Vertragstext und senden Ihnen die Bestelldaten und unsere AGB per EMail zu. Die AGB k&ouml;nnen Sie jederzeit auch <a href="../content/Unsere-AGBs.html" target="_parent">hier</a> einsehen. Ihre vergangenen Bestellungen k&ouml;nnen Sie in unserem <a href="../account.php" target="_parent">Kunden LogIn-Bereich</a> einsehen.<br />
      <br />
<h2>&sect; 4 Beschaffenheit und wesentliche Merkmale der Ware</h2> 
      Die Beschaffenheit, die Merkmale und die Qualit&auml;t der gelieferten Ware ergeben sich aus den Beschaffenheitsangaben in den jeweiligen Produktbeschreibungen. Diese sind auch unter unserer Internetadresse www.Esoterikshopping.de abrufbar.<br /><br /> 
       <h2>&sect; 5 Nichtverf&uuml;gbarkeit der bestellten Ware</h2> 
       Der Vertragsschluss erfolgt unter dem Vorbehalt der Selbstbelieferung; der Vorbehalt gilt zu Lasten von Verbrauchern nur, sofern wir ein konkretes Deckungsgesch&auml;ft abgeschlossen haben und von dem Lieferanten ohne eigenes Verschulden nicht beliefert werden. Wir werden den Kunden unverz&uuml;glich &uuml;ber die Nichtverf&uuml;gbarkeit der Ware informieren und etwaige erbrachte Gegenleistungen des Kunden unverz&uuml;glich erstatten.<br /><br /> 
        <h2>&sect; 6 Preise, Liefer- und Zahlungsmodalit&auml;ten</h2> 
        Es besteht f&uuml;r den Endkunden kein Mindestbestellwert.<br /> 
        S&auml;mtliche angegebenen Preise verstehen sich inklusive der zum Zeitpunkt der Leistungsausf&uuml;hrung g&uuml;ltigen gesetzlichen Umsatzsteuer.<br /> <br /> Die Lieferung erfolgt zuz&uuml;glich Verpackungs- und Versandkosten an die von Ihnen in Ihrer Bestellung angegebene Lieferadresse.<br /> <br /> 
    <em>Endkunde: (EUR entspricht Euro)<br />
  </em>    <strong>Die Versandkosten betragen innerhalb von <span style="color:red;">Deutschland pauschal 4,50 EUR</span>, ab einem Bestellwert von 49,00 EUR erfolgt die Lieferung innerhalb Deutschlands versandkostenfrei.</strong><br />
    Die Versandkosten betragen f&uuml;r Bulgarien 	13,90 EUR <br />
  Die Versandkosten betragen f&uuml;r Belgien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r D&auml;nemark 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Estland 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Finnland 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Frankreich 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Griechenland 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Grossbritanien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Irland 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Italien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Kroatien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Lettland 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Litauen 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Italien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Luxemburg 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Malta 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Niederlande 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r &Ouml;sterreich 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Polen 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Portugal 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Rum&auml;nien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Russland 	30,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Schweden 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Schweiz 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Slowakei 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Slowenien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Spanien 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Thailand 	30,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Tschechien 	12,90 EUR <br />
    Die Versandkosten betragen f&uuml;r Ungarn 	13,90 EUR <br />
    Die Versandkosten betragen f&uuml;r USA 	30,90 EUR<br />
    Die Versandkosten betragen f&uuml;r Zypern 	13,90 EUR <br />
<br />
    <em>F&uuml;r H&auml;ndler: (EUR entspricht Euro)</em><br />
    Die Versandkosten betragen innerhalb von Deutschland f&uuml;r alle Gr&ouml;&szlig;en und Gewichtsklassen: 6.90 EUR.<br />
    Wir versenden in folgende L&auml;nder: &Ouml;sterreich, Belgien, Holland, Luxemburg, Tschechien, D&auml;nemark: Alle Gewichtsklassen (versichert): 8.90 EUR<br />
    Versandkosten England, Frankreich, <!--Italien,--> Polen: 
    Alle Gewichtsklassen (versichert): 12.90 EUR<br />
    Wir versenden in folgende L&auml;nder: Schweiz, Schweden, slowakische Republik, Slowenien, Ungarn, Ru&szlig;land: 
    Alle Gewichtsklassen (versichert): 24.00 EUR<br />
    Au&szlig;erhalb Europa: up to 500 grams: 10,90 Euro, 
    500 - 1000 grams: 14,90 Euro,
    1000 - 2000 grams 26,90 Euro.
    <br /><br />
    Wie k&ouml;nnen Sie bezahlen?<br />
    - per Vorkasse/&Uuml;berweisung (die Konto-Nr. wird Ihnen nach Ihrer Bestellung per Mail mitgeteilt). <br />
    - per Rechnung (ab einem Bestellwert von 15,00 Euro nur innerhalb Deutschland und nicht zur Verf&uuml;gung bei Gutscheinen)<br />
    - per Paypal<br />
	  - per Kreditkarte<br />
    - per Sofort&uuml;berweisung<br />
  <br /> 

<h2>&sect; 7 Gew&auml;hrleistung</h2> 
Bei allen Waren aus unserem Shop bestehen gesetzliche Gew&auml;hrleistungsrechte.<br /><br />



<h2>&sect; 8 Widerrufsrecht</h2>
<strong>Widerrufsbelehrung</strong><br /><br />
Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gr&uuml;nden diesen Vertrag zu widerrufen.
<br />
Die Widerrufsfrist betr&auml;gt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Bef&ouml;rderer ist, 
die letzte Ware in Besitz genommen haben bzw. hat. 
Um Ihr Widerrufsrecht auszu&uuml;ben, m&uuml;ssen Sie uns Spart Bares GbR, Humboldtstrasse 134, 90459 N&uuml;rnberg, 
email: info@Esoterikshopping.de, Tel. 08404/939-264, Fax 08404/939-265 mittels einer eindeutigen Erkl&auml;rung 
(z. B. ein mit der Post versandter Brief, Telefax oder E-Mail) &uuml;ber Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. 
Sie k&ouml;nnen daf&uuml;r das beigef&uuml;gte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist.
Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung &uuml;ber die Aus&uuml;bung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.
<br /><br />
<strong>Folgen des Widerrufs</strong>
<br />
Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschlie&szlig;lich der Lieferkosten 
(mit Ausnahme der zus&auml;tzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, 
g&uuml;nstigste Standardlieferung gew&auml;hlt haben), unverz&uuml;glich und sp&auml;testens binnen vierzehn Tagen ab dem Tag zur&uuml;ckzuzahlen, 
an dem die Mitteilung &uuml;ber Ihren Widerruf dieses Vertrags bei uns eingegangen ist. F&uuml;r diese R&uuml;ckzahlung verwenden wir dasselbe Zahlungsmittel, 
das Sie bei der urspr&uuml;nglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdr&uuml;cklich etwas anderes vereinbart; 
in keinem Fall werden Ihnen wegen dieser R&uuml;ckzahlung Entgelte berechnet. Wir k&ouml;nnen die R&uuml;ckzahlung verweigern, 
bis wir die Waren wieder zur&uuml;ckerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zur&uuml;ckgesandt haben, 
je nachdem, welches der fr&uuml;here Zeitpunkt ist.
<br />
Sie haben die Waren unverz&uuml;glich und in jedem Fall sp&auml;testens binnen vierzehn Tagen ab dem Tag, an dem Sie uns &uuml;ber den Widerruf dieses Vertrags unterrichten, 
an OPSEDI GbR, Am Bach 9, 93349 Mindelstetten zur&uuml;ckzusenden oder zu &uuml;bergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden. 
Sie tragen die unmittelbaren Kosten der R&uuml;cksendung der Waren.
<br /><br />


<h2>&sect; 9 Eigentumsvorbehalt</h2>
 Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.<br /><br /> 
 <h2>&sect; 10 Haftung</h2>
Bei einer nicht vors&auml;tzlichen und nicht grob fahrl&auml;ssigen Verletzung einer Vertragspflicht durch uns ist die Haftung auf typische, voraussehbare Sch&auml;den begrenzt.<br /> 
<br /> Die vorstehende Haftungsbeschr&auml;nkung gilt nicht, soweit eine Verletzung von Leben, K&ouml;rper oder Gesundheit vorliegt. Die Haftungsbeschr&auml;nkung gilt ferner dann nicht, wenn der Besteller Anspr&uuml;che aus Produkthaftung geltend macht.<br /><br /> 
<h2>&sect; 11 Anwendbares Recht</h2>
F&uuml;r s&auml;mtliche Lieferungen gilt ausschlie&szlig;lich das Recht der Bundesrepublik Deutschland.<br /><br /> 
<h2>&sect; 12 Gerichtsstand und sonstige Bestimmungen</h2>
Als Gerichtsstand wird unserer Sitz vereinbart, wenn Sie Kaufmann sind oder keinen allgemeinen Gerichtsstand in der Bundesrepublik Deutschland haben.<br /> 
<br /> Sollten einzelne Bestimmungen unwirksam sein, so f&uuml;hrt dies nicht zur Unwirksamkeit des gesamten Vertrags sowie der &uuml;brigen vertraglichen Bestimmungen. Die unwirksame Bestimmung wird durch die entsprechenden gesetzlichen Vorgaben ersetzt.<br /><br />
</div>    
