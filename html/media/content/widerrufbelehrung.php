<strong>Widerrufsbelehrung</strong><br /><br />
<strong>Widerrufsrecht</strong><br />
Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gr&uuml;nden diesen Vertrag zu widerrufen.
<br />
Die Widerrufsfrist betr&auml;gt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Bef&ouml;rderer ist, 
die letzte Ware in Besitz genommen haben bzw. hat. 
Um Ihr Widerrufsrecht auszu&uuml;ben, m&uuml;ssen Sie uns Spart Bares GbR, Humboldtstrasse 134, 90459 N&uuml;rnberg, 
email: info@Esoterikshopping.de, Tel. 08404/939-264, Fax 08404/939-265 mittels einer eindeutigen Erkl&auml;rung 
(z. B. ein mit der Post versandter Brief, Telefax oder E-Mail) &uuml;ber Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. 
Sie k&ouml;nnen daf&uuml;r das beigef&uuml;gte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist.
Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung &uuml;ber die Aus&uuml;bung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.
<br /><br />
<strong>Folgen des Widerrufs</strong>
<br />
Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschlie&szlig;lich der Lieferkosten 
(mit Ausnahme der zus&auml;tzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, 
g&uuml;nstigste Standardlieferung gew&auml;hlt haben), unverz&uuml;glich und sp&auml;testens binnen vierzehn Tagen ab dem Tag zur&uuml;ckzuzahlen, 
an dem die Mitteilung &uuml;ber Ihren Widerruf dieses Vertrags bei uns eingegangen ist. F&uuml;r diese R&uuml;ckzahlung verwenden wir dasselbe Zahlungsmittel, 
das Sie bei der urspr&uuml;nglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdr&uuml;cklich etwas anderes vereinbart; 
in keinem Fall werden Ihnen wegen dieser R&uuml;ckzahlung Entgelte berechnet. Wir k&ouml;nnen die R&uuml;ckzahlung verweigern, 
bis wir die Waren wieder zur&uuml;ckerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zur&uuml;ckgesandt haben, 
je nachdem, welches der fr&uuml;here Zeitpunkt ist.
<br />
Sie haben die Waren unverz&uuml;glich und in jedem Fall sp&auml;testens binnen vierzehn Tagen ab dem Tag, an dem Sie uns &uuml;ber den Widerruf dieses Vertrags unterrichten, 
an OPSEDI GbR, Am Bach 9, 93349 Mindelstetten zur&uuml;ckzusenden oder zu &uuml;bergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden. 
Sie tragen die unmittelbaren Kosten der R&uuml;cksendung der Waren.
<br />
<br />
Das Muster-Widerrufsformular finden Sie <a href="../content/Widerrufsformular.html">hier</a>.