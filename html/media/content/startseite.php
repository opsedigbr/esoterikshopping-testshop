<div id="startseiten-text">
    Wir haben unser Sortiment stark erweitert. Sie finden in  unserem Shop nun neben Esoterik Produkten nun viele weitere interessante  Artikel zum Schwelgen. Unter dem 
    Punkt &bdquo;Neue Produkte&ldquo; finden Sie die in den  letzten 30 Tagen hinzugef&uuml;gten Artikel aufgelistet. Auch haben wir neue  Kategorien wie 
    <strong><a href="Chakra-Produkte" title="Chakra Produkte" target="_parent">Chakra</a></strong>, neue <strong><a href="Doreen-Virtue-Produkte" title="Doreen Virtue Produkte" target="_parent">Doreen Virtue</a></strong> Produkte, wundersch&ouml;ne <strong><a href="Duftlampen-Aromalampen-Duft-Drops" title="Duftlampen, Aromalampen, Duft-Drops und Zubeh&ouml;r" target="_parent">Duftlampen</a></strong>, eleganten <strong><a href="Edelsteinschmuck" title="Edelsteinschmuck" target="_parent">Edelsteinschmuck</a></strong>, 
    kleine und gro&szlig;e <strong><a href="Engel-Engelfiguren-mystische-Figuren-Gargoyles" title="Engel, Engelfiguren" target="_parent">Engelfiguren</a></strong>, neue <strong><a href="Chakra-Produkte/Chakra-Duftkerzen" title="Chakra Kerzen aus Stearin" target="_parent">Kerzen aus Stearin</a></strong> (besonders hochwertig), eine eigene Kategorie <strong><a href="Wellness/Entspannung" title="Musik - Meditation und Entspannung" target="_parent">Musik mit 
    Meditation und Entspannung</a></strong>, <strong><a href="Aetherische-Oele-Ritual-Oele/Magic-Spray" title="&Ouml;le - Magic Spray" target="_parent">neue &Ouml;le</a></strong><a href="Aetherische-Oele-Ritual-Oele/Magic-Spray"> (Magic Sprays)</a>.<br />
</div>

<!--
<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
		<a href="Kartenlegen-Tarot-Lenormand-Wahrsagekarten-Engelkarten/Inspirationskarten-Weisheitskarten"><img src="slider/images/bild_40.jpg" alt="Der Sommertrend 2014! Diese Wohlf&uuml;hlkarten m&uuml;ssen Sie ausprobieren!" border="0" title="Der Sommertrend 2014! Diese Wohlf&uuml;hlkarten m&uuml;ssen Sie ausprobieren!" /></a>
        <a href="Edelsteine"><img src="slider/images/bild_39.jpg" alt="Jeder Edelstein hat eine bestimmte Energie die wir nutzen k&ouml;nnen" border="0" title="Jeder Edelstein hat eine bestimmte Energie die wir nutzen k&ouml;nnen" /></a>
        <a href="Edelsteinwasser-Wasserbelebung"><img src="slider/images/bild_14.jpg" alt="" border="0" title="G&ouml;nnen Sie sich was! Genie&szlig;en Sie ein wunderbares Edelsteinwasser" /></a>
        <a href="Aetherische-Oele-Ritual-Oele/Aetherische-Oele-100-Naturrein"><img src="slider/images/bild_35.jpg" alt="" title="In dieser Jahreszeit sind &auml;therische &Ouml;le eine richtige Wohltat fur jede Seele." /></a>
        <a href="Wahrsagen-Orakel"><img src="slider/images/bild_13.jpg" alt="Kristallkugeln" title="Kristallkugeln - Inspiration aus Kristall & Klarheit und Energie an jedem Ort" /></a>
		<a href="Kartenlegen-Tarot-Lenormand-Wahrsagekarten-Engelkarten/Antike-und-seltene-Wahrsagekarten-Tarot-und-Buecher"><img src="slider/images/bild_36.jpg" alt="" title="Antike und seltene Tarots, Wahrsagekarten, Lenormandkarten und B&uuml;cher." /></a>  
    </div>
</div>
-->
