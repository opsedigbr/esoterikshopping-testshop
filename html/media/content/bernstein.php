<div id="customer-informations">
    <p><strong><h1>Der Bernstein - das Gold des Meeres</h1></strong><br />
    Alles begann mit Harz, das vor Millionen von Jahren aus  einem Baum ausgetreten ist. G&uuml;nstige Umwelteinfl&uuml;sse sorgten f&uuml;r eine schnelle Aush&auml;rtung des Harzes 
    und zur sp&auml;teren Versteinerung des fossilen Harzes.  H&auml;ufig blieben an der sehr klebrigen und dickfl&uuml;ssigen Substanz Insekten oder  Teile von Pflanzen h&auml;ngen 
    und wurden darin f&uuml;r immer eingeschlossen. Das  verleiht dem Bernstein heute sein unverwechselbares Aussehen.</p>
    
    <br />
  
    <strong><h2>Splitterketten aus Bernstein - Balsam f&uuml;r die Haut und die Seele</h2></strong><br />
  <div class="images left">
   	<a href="/Edelsteinschmuck/Edelstein-Bernstein" target="_parent"><img src="../media/content/4031_0.jpg" alt="Splitterkette aus Bernstein" title="Splitterkette aus Bernstein" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
    Die Astrologie wie auch verschiedene  Naturheilverfahren kennen und sch&auml;tzen die Wirkung, die der Bernstein auf den  menschlichen K&ouml;rper und die Psyche haben kann. 
    Der Stein ist den Sternzeichen  Zwilling und  L&ouml;wen zugeteilt. W&auml;hrend der Bernstein beim L&ouml;wen bei der Verwirklichung  von Pl&auml;nen unterst&uuml;tzend wirkt, 
    findet der Zwilling durch den Bernstein  Entspannung.  Allergische Reaktionen oder  Schmerzen im Bereich von Rachen und Hals sollen mit dem Bernstein gelindert werden k&ouml;nnen. 
    Grunds&auml;tzlich soll das Bernstein positiven Einfluss auf den  Gem&uuml;tszustand seines Tr&auml;gers bzw. seiner Tr&auml;gerin nehmen. Durch die W&auml;rme und das Licht soll 
    der Bernstein  die Lebensqualit&auml;t erh&ouml;hen und  die &Uuml;berwindung  von Depressionen und &Auml;ngsten erleichtern. Damit der Bernstein seine Wirkung entfalten kann, 
    tr&auml;gt man ihn am besten direkt auf der Haut. In Form von modischen und sehr  attraktiven Splitterketten schmeichelt der Bernstein der Haut und wird zugleich zu einem 
    &uuml;beraus modischen Accessoire, das von elegant bis legere jedem Outfit  passt. Die Bernstein Ketten aus dem Edelstein Bernstein sind in der hellen 
    (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterkette-Amber-hell.html" target="_parent">Helle Bernsteinkette</a>) und der dunklen 
    Variante (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterkette-Amber-dunkel.html" target="_parent">Dunkle Bernsteinkette</a>) erh&auml;ltlich. 
    Splitterketten bestehen aus zahlreichen geschliffenen und polierten  Bernstein-Elementen. Jeder Splitter besitzt ein individuelles Aussehen und  f&auml;ngt das Licht 
    auf eine ganz besondere Art und Weise ein. Auf der Haut getragen, erw&auml;rmt sich die Bernstein Halskette binnen weniger Augenblicke. Splitterketten  aus Bernstein 
    bestechen zudem durch ihre unregelm&auml;&szlig;ige Form und lassen den Schmuck noch lebendiger erscheinen.
    <div class="clear"></div>
    
    <br />
    
    <strong><h2>Das Splitterarmband aus Bernstein</h2></strong><br />
  <div class="images right">
    	<a href="/Edelsteinschmuck/Edelstein-Bernstein" target="_parent"><img src="../media/content/4032_0.jpg" alt="Splitterarmband aus Bernstein" title="Splitterarmband aus Bernstein" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
    Schmuck aus Bernstein fasziniert durch ein unvergleichliches Licht- und Farbenspiel. Durch die W&auml;rme des Bernsteins ist es &uuml;beraus angenehm, das Armband und die Bernsteine 
    direkt auf der Haut zu tragen. Ein Splitterarmband aus Bernstein, das sowohl in der hellen Farbvariante 
    (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterarmband-Amber-hell.html" target="_parent">Splitterarmband hell</a>) wie auch in der dunklen 
    Farbvariante (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterarmband-Amber-dunkel.html" target="_parent">Splitterarmband dunkel</a>) erh&auml;ltlich ist, 
    ist eine perfekte Erg&auml;nzung zu einer Bernstein Halskette. Durch ein weiches und transparentes Gummiband, das als Basis f&uuml;r die Bernstein-Splitter dient, kann das Armband 
    einfach &uuml;ber das Handgelenk gezogen werden. Kaum ein Edelstein wird schon seit Jahrhunderten so vermehrt als Edelstein eingesetzt, wie der Bernstein. Der Bernstein soll 
    Harmonie und Lebensfreude verbreiten, aber auch die k&ouml;rperliche St&auml;rke und Macht beg&uuml;nstigen. Daher trugen fr&uuml;her viele K&ouml;nige de Bernstein auch in der Krone.
    <div class="clear"></div>
    
    <br />
    
    <strong><h2>Eine Bernsteinkette f&uuml;r Babys</h2></strong><br />
  <div class="images left">
    	<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-bunt.html" target="_parent"><img src="../media/content/4035_0.jpg" alt="Bakykette aus Bernstein" title="Bakykette aus Bernstein" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
</div>
  Ketten aus Bernstein werden h&auml;ufig verwendet, um  verschiedenste Leiden zu lindern. Dazu z&auml;hlen auch Entz&uuml;ndungen im Mund- und  Rachenraum, Zahnschmerzen oder bei Kindern das Zahnen. Babyketten aus Bernstein  sollen helfen dazu beizutragen, das Durchsto&szlig;en der Z&auml;hne durch das Zahnfleisch zu  erleichtern. Die Babyketten sind sehr kurz und die einzelnen Steine sanft  abgerundet. Ein wiederverschlie&szlig;barer Sicherheitsverschluss erm&ouml;glicht ein  einfaches &Ouml;ffnen und Schlie&szlig;en der Babykette aus Bernstein. Bernstein ist in  vielen verschiedenen Farbt&ouml;nen erh&auml;ltlich. Eine Babykette aus Bernstein  begeistert durch genau diese Farbvielfalt und soll beim Lindern der  Zahnungsbeschwerden bei Babys beitragen. Des Weiteren sind die neuen Babyketten aus Bernstein  mit einem wieder verschlie&szlig;baren   Sicherheitsverschluss ausgestattet. Der T&Uuml;V Rheinland  hat   die Rei&szlig;festigkeit der Bernstein-Babyketten und die Funktion des   Sicherheitsverschlusses getestet und best&auml;tigt. 
  Jede Kette wird in einem kleinen Etui mit einem viersprachigem Beileger ausgeliefert.
  <div class="clear"></div>    
  
    <br />
    
    <strong><h2>Bernstein  Splitterkette &bdquo;Lena&ldquo; &ndash; harmonisch modisch</h2>
    </strong><br />
  <div class="images right">
    	<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-bunt.html" target="_parent"><img src="../media/content/4055_0-4056_0.jpg" alt="Bernstein Armband und Bernstein Splitterkette Lena" title="Bernstein Armband und Bernstein Splitterkette Lena" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
	</div>
  	Das konservierte Harz der B&auml;ume fasziniert in seinem  beinahe unendlichen Facettenreichtum, unz&auml;hligen Farbvarianten und seinem  individuellen Aussehen. Besonders lebendig erscheinen dabei Splitterketten aus Bernstein. Jedes einzelne Element der Bernstein Splitterkette verleiht dem filigranen Kunstwerk sein ganz  eigenes Gesicht. Weiche, abgerundete Formen und die verschiedenen Farben lassen  die Splitterkette aus Bernstein zu einem sehr modischen  Accessoire werden, das sich harmonisch in jedes Styling einf&uuml;gt. Ganz gleich,  ob einem strengen klassischen Business-Look leichte und harmonische  Weiblichkeit verliehen werden soll oder ob ein legeres Freizeit-Outfit zum  Tragen kommt, der Bernstein ist ein  vielseitiger Begleiter. Egal ob Kurz  oder lang; dies ist eine Frage des pers&ouml;nlichen Geschmacks. Die mehrfarbige  Bernstein Splitterkette &bdquo;Lena&ldquo; ist  sowohl in einer kurzen Ausf&uuml;hrung (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterkette-Lena.html">Splitterkette  &bdquo;Lena&ldquo; kurz</a>) mit einer L&auml;nge von 45 cm, wie auch in einer langen Variante (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Splitterkette-Lena2.html">Splitterkette  &bdquo;Lena&ldquo; lang</a>) mit 65 cm erh&auml;ltlich. Ein praktischer Drehverschluss sorgt f&uuml;r  ein einfaches und unkompliziertes &Ouml;ffnen und Schlie&szlig;en der Bernstein Edelsteinkette. Die hochwertige Verarbeitung und die  liebevolle Auff&auml;delung der einzelnen Bernstein  Elemente l&auml;sst die Kette zu einem Accessoire  werden, der sich perfekt jedem modischen Trend anpassen kann. Das zu den Edelsteinhalsketten  passende Edelsteinarmband der Serie &bdquo;<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Bernstein-Armband-Lena.html">Lena</a>&ldquo;  verst&auml;rkt die energetische Wirkung des Bernsteins und rundet gleichzeitig Ihr  Outfit perfekt ab.
  <div class="clear"></div>      
  
    <br />
    
    <strong><h2><span class="red">*NEU*</span> Weitere Babyketten aus Bernstein</h2></strong><br />
  <div class="images left">
    	<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-bunt.html" target="_parent"><img src="../media/content/4035_0.jpg" alt="Bakykette aus Bernstein" title="Bakykette aus Bernstein" border="0" /></a><br />
        <div class="image-subtext">Bildrechte bei Esoterikshopping.de</div>
  </div>
  Die attraktiven Schmucksteine sind auf einer L&auml;nge von  ca. 34 cm besonders rei&szlig;fest aufgef&auml;delt. Ein Sicherheitsverschluss sorgt f&uuml;r  ein einfaches &Ouml;ffnen und Schlie&szlig;en der Babyketten  aus Bernstein. Bereits im Vorfeld wurden die Ketten aus Bernstein f&uuml;r Babys  vom T&Uuml;V Rheinland (Hong Kong) auf deren Sicherheit getestet und best&auml;tigt. Ganz  dem pers&ouml;nlichen Geschmack entsprechend, ist die Baby Bernsteinkette Classic in den Varianten bunt (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-bunt.html">Babykette  Bernsteinkette Classic &ndash; bunt</a>), hell (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-hell.html">Babykette  Bernsteinkette Classic - hell</a>) oder dunkel (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Classic-dunkel.html">Babykette  Bernsteinkette Classic - dunkel</a>) erh&auml;ltlich. Besonders edel pr&auml;sentiert  sich die Bernsteinkette Barock (<a href="/products/Edelsteinschmuck/Edelstein-Bernstein/Babykette-Bernsteinkette-Barock-bunt.html">Babykette  Bernsteinkette Barock - bunt</a>). Zwischen den hellen, rundlichen Steinen  setzen dunkle, ebenfalls weich abgerundete Bernstein-Perlchen attraktive  Akzente. Auch diese Baby Bernsteinkette  wurde durch den T&Uuml;V Rheinland in puncto Rei&szlig;festigkeit und  Sicherheitsverschluss einer Pr&uuml;fung unterzogen.
  <div class="clear"></div>    
  
  
</div>
