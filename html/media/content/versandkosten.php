<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html lang="en">
<head>
<style type="text/css">
.rot {
	color: #F00;
}
td.unterstrich {
	border-bottom:1px solid #ccc;
	font-size:12px;
	font-family:arial;
}
</style>
</head>
<body>
<strong>Versandinformationen &amp; Versandkosten f&uuml;r <span class="rot">Endkunden:</span></strong><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="170"><img src="../../media/content/flaggen/versand_dhl_logo.jpg" width="165" height="165"></td>
    <td align="left" valign="top"> <ul>
        Um eine schnelle und reibungslose Zustellung Ihrer bestellten Waren zu 
        garantieren, versenden wir ausschlie&szlig;lich per DHL.<br>
        Folgende Vorteile ergeben sich dadurch f&uuml;r Sie:<br>
        <br>
        <strong>- Samstagszustellung<br>
        - Lieferung auch an Packstationen<br>
        - bei nicht antreffen Abholung in Ihrer Postfiliale </strong> </ul></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="unterstrich" bgcolor="#CCCCCC"><strong>&nbsp;Land</strong></td>
    <td class="unterstrich" bgcolor="#CCCCCC">&nbsp;</td>
    <td class="unterstrich" bgcolor="#CCCCCC"><strong>&nbsp;&nbsp;&nbsp;&nbsp;Kosten</strong></td>
    <td class="unterstrich" bgcolor="#CCCCCC"><strong>Versandkostenfrei ab</strong></td>
  </tr>
  <tr>
    <td class="unterstrich" width="35"><img src="../../media/content/flaggen/belgien.png" width="16" height="11" alt="Versand nach Belgien" title="Versand nach Belgien"></td>
    <td class="unterstrich">Belgien</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/bulgarien.png" width="16" height="11" alt="Versand nach Bulgarien" title="Versand nach Bulgarien"></td>
    <td class="unterstrich">Bulgarien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/daenemark.png" width="16" height="11" alt="Versand nach D&auml;nemark" title="Versand nach D&auml;nemark"></td>
    <td class="unterstrich">D&auml;nemark</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/deutschland.png" width="16" height="11" alt="Versand nach Deutschland" title="Versand nach Deutschland"></td>
    <td class="unterstrich"><strong style="font-size:14px;">Deutschland</strong></td>
    <td class="unterstrich"><strong style="font-size:14px;">&nbsp;4,50 EUR</strong></td>
    <td class="unterstrich"><strong style="font-size:14px;">49,00 EUR</strong></td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/estland.png" width="16" height="11" alt="Versand nach Estland" title="Versand nach Estland"></td>
    <td class="unterstrich">Estland</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/finnland.png" width="16" height="11" alt="Versand nach Finnland" title="Versand nach Finnland"></td>
    <td class="unterstrich">Finnland</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/frankreich.png" width="16" height="11" alt="Versand nach Frankreich" title="Versand nach Frankreich"></td>
    <td class="unterstrich">Frankreich</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/griechenland.png" width="16" height="11" alt="Versand nach Griechenland" title="Versand nach Griechenland"></td>
    <td class="unterstrich">Griechenland</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>    
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/grossbritanien.png" width="16" height="11" alt="Versand nach Grossbritanien" title="Versand nach Grossbritanien"></td>
    <td class="unterstrich">Grossbritanien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/irland.png" width="16" height="11" alt="Versand nach Irland" title="Versand nach Irland"></td>
    <td class="unterstrich">Irland</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>    
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/italien.png" width="16" height="11" alt="Versand nach Italien" title="Versand nach Italien"></td>
    <td class="unterstrich">Italien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/kroatien.png" width="16" height="11" alt="Versand nach Kroatien" title="Versand nach Kroatien"></td>
    <td class="unterstrich">Kroatien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>    
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/lettland.png" width="16" height="11" alt="Versand nach Lettland" title="Versand nach Lettland"></td>
    <td class="unterstrich">Lettland</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/litauen.png" width="16" height="11" alt="Versand nach Litauen" title="Versand nach Litauen"></td>
    <td class="unterstrich">Litauen</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>     
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/luxemburg.png" width="16" height="11" alt="Versand nach Luxemburg" title="Versand nach Luxemburg"></td>
    <td class="unterstrich">Luxemburg</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/malta.png" width="16" height="11" alt="Versand nach Malta" title="Versand nach Malta"></td>
    <td class="unterstrich">Malta</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>       
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/niederlande.png" width="16" height="11" alt="Versand in die Niederlande" title="Versand in die Niederlande"></td>
    <td class="unterstrich">Niederlande</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/oesterreich.png" width="16" height="11" alt="Versand nach &Ouml;sterreich" title="Versand nach &Ouml;sterreich"></td>
    <td class="unterstrich">&Ouml;sterreich</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/polen.png" width="16" height="11" alt="Versand nach Polen" title="Versand nach Polen"></td>
    <td class="unterstrich">Polen</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/portugal.png" width="16" height="11" alt="Versand nach Portugal" title="Versand nach Portugal"></td>
    <td class="unterstrich">Portugal</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/rumaenien.png" width="16" height="11" alt="Versand nach Rum&auml;nien" title="Versand nach Rum&auml;nien"></td>
    <td class="unterstrich">Rum&auml;nien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>    
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/russland.png" width="16" height="11" alt="Versand nach Russland" title="Versand nach Russland"></td>
    <td class="unterstrich">Russland</td>
    <td class="unterstrich">30,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/schweden.png" width="16" height="11" alt="Versand nach Schweden" title="Versand nach Schweden"></td>
    <td class="unterstrich">Schweden</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/schweiz.png" width="16" height="11" alt="Versand in die Schweiz" title="Versand in die Schweiz"></td>
    <td class="unterstrich">Schweiz</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/slowakei.png" width="16" height="11" alt="Versand nach Slowakei" title="Versand nach Slowakei"></td>
    <td class="unterstrich">Slowakei</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>   
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/slowenien.png" width="16" height="11" alt="Versand nach Slowenien" title="Versand nach Slowenien"></td>
    <td class="unterstrich">Slowenien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/spanien.png" width="16" height="11" alt="Versand nach Spanien" title="Versand nach Spanien"></td>
    <td class="unterstrich">Spanien</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
  	<td class="unterstrich"><img width="16" height="11" title="Versand nach Thailand" alt="Versand nach Thailand" src="../../media/content/flaggen/thailand.png" /></td>
    <td class="unterstrich">Thailand</td>
    <td class="unterstrich">30,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/tschechien.png" width="16" height="11" alt="Versand nach Tschechien" title="Versand nach Tschechien"></td>
    <td class="unterstrich">Tschechien</td>
    <td class="unterstrich">12,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/ungarn.png" width="16" height="11" alt="Versand nach Ungarn" title="Versand nach Ungarn"></td>
    <td class="unterstrich">Ungarn</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/usa.png" width="16" height="11" alt="Versand in die USA" title="Versand in die USA"></td>
    <td class="unterstrich">USA</td>
    <td class="unterstrich">30,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>
  <tr>
    <td class="unterstrich"><img src="../../media/content/flaggen/zypern.png" width="16" height="11" alt="Versand nach Zypern" title="Versand nach Zypern"></td>
    <td class="unterstrich">Zypern</td>
    <td class="unterstrich">13,90 EUR</td>
    <td class="unterstrich">---</td>
  </tr>  
</table>
<br>
<br>
<strong>Bestellungen in die Schweiz:</strong><br>
- Die anfallenden Verzollungskosten und Z&ouml;lle m&uuml;ssen vom Kunden &uuml;bernommen werden. Informationen dazu finden Sie beim Schweizer Zoll, unter <a href="http://www.Zoll.ch" target="_blank">www.Zoll.ch</a><br>
<br>
<strong>Ihr Land ist nicht gelistet? /  Your country is not listed?</strong><br>
DE: Falls Ihr Land nicht aufgelistet ist und Sie bestellen m&ouml;chten, wenden Sie sich bitte &uuml;ber das <strong><a href="http://www.esoterikshopping.de/content/Kontakt-zu-uns.html">Kontaktformular</a></strong> an uns. Wir werden versuchen Ihnen weiter zu helfen.<br>
EN: If your country is not listed, please feel 
free to contact us <strong><a href="http://www.esoterikshopping.de/content/Kontakt-zu-uns.html">here</a></strong>. We will try our best to make your order come true.<br>
<br>
<strong>Sie sind H&auml;ndler? Dann klicken Sie bitte <a href="../../content/Versandkosten-fuer-Haendler.html" target="_parent">hier</a>, um zu den <a href="../../content/Versandkosten-fuer-Haendler.html" target="_parent">Versandkosten f&uuml;r H&auml;ndler</a> zu gelangen.<br>
</strong>
</p>
<br>
</body>