<?php
/* -----------------------------------------------------------------------------------------
   jQuery Autocomplete

   Changelog:
   2011-04-18: Soundex/Koelner search in category-names by TechWay
   2011-04-19: NEW LINK DEFINITION by web28 www.rpa-com.de
   2011-04-19: products_short_description added by TechWay
   2011-04-20: NEW option SHOW_PRODUCT_IMAGE and function productImage, products/categoriesLink by web28 www.rpa-com.de
   2011-04-21: noimage.gif if no products_image, cleaned short_description (no html-tags), products_price added by TechWay
   2011-04-22: NEW functions handling, NEW option SHOW_PRODUCT_PRICE  by web28 www.rpa-com.de

   Released under the GNU General Public License
   ---------------------------------------------------------------------------------------*/

//The settings you can change:
$searchConfig = array(
	'USE_SUMA_LINKS' => true,         				// Would you like SUMA Links? Possible values: true / false. Standard: true
	'SEARCH_IN_PRODUCTS_NAME' => true,				// Would you like to search in the product names? Possible values: true / false. Standard: true
	'SEARCH_IN_PRODUCTS_KEYWORDS' => true,			// Would you like to search in the product keywords? Possible values: true / false. Standard: true
	'SEARCH_IN_PRODUCTS_DESCRIPTION' => false,		// Would you like to search in the product description? Possible values: true / false. Standard: true
	'SEARCH_IN_PRODUCTS_SHORT_DESCRIPTION' => false,// Would you like to search in the product short description? Possible values: true / false. Standard: true
	'SEARCH_IN_CATEGORY_NAMES' => true,				// Would you like to search in the category names? Possible values: true / false. Standard: true
	'SEARCH_IN_CATEGORY_DESCRIPTION' => true,		// Would you like to search in the category names? Possible values: true / false. Standard: true
	'SEARCH_IN_PRODUCTS_MODEL' => true,			// Would you like to search in the products model? Possible values: true / false. Standard: false
	'SEARCH_IN_PRODUCTS_EAN' => true,				// Would you like to search in the products ean? Possible values: true / false. Standard: false
	'SHOW_PRODUCT_IMAGE' => true,					// Would you like to show products_image? Possible values: true / false. Standard: false
	'SHOW_PRODUCT_PRICE' => true,					// Would you like to show products_price? Possible values: true / false. Standard: false
	'SHOW_PRODUCT_DESC' => true,					// Would you like to show products_short_description? Possible values: true / false. Standard: false
	'PRODUCT_DESC_MAX_LENGTH' => 120,				// Max length of products_short_description. Possible values: INTEGER, Standard: 100
	'SUBSTRING_COUNT' => 60,						// Length of results in the list (signs). Possible values: INT from 1 to 256. Standard: 40
	'SUBSTRING_FINALIZER' => ' ...',				// Signs after a word is cutted in the suggestlist. Possible values: Any String. Standard: ' ...'
	'PRODUCT_RESULT_COUNT' => 15,					// How many found products should be displayed? Possible values: INT from 1 to 256. Standard: 10
	'CATEGORY_RESULT_COUNT' => 10,					// How many found categories should be displayed? Possible values: INT from 1 to 256. Standard: 5
	'SHOW_CATEGORY_WITH_PRODUCT' => false,			// Would you like to show the category name in the product result like "category => productname" Possible values: true / false. Standard: false
	'GROUP_RESULTS' => true,						// Would you like to group the results by the category_name? Possible values: true / false. Standard: true
	'SEVERAL_KEYWORDS_AND' => true,					// Search for several keywords with AND-logic. Possible values: true / false. Standard: true
	'SOUNDEX_SEARCH_PRODUCT_NAMES' => false,		// Would you like to search for keyword with soundex-method (sounds like) Possible values: true / false. Standard: false
	'SOUNDEX_SEARCH_CATEGORY_NAMES' => false,		// Would you like to search for keyword with soundex-method (sounds like) Possible values: true / false. Standard: false
	'KOELNER_PHONETIK_PRODUCT_NAMES' => true,		// Would you like to search for keyword with koelner-method (sounds like) Possible values: true / false. Standard: false
	'KOELNER_PHONETIK_CATEGORY_NAMES' => false,		// Would you like to search for keyword with koelner-method (sounds like) Possible values: true / false. Standard: false
	'FILTER_LINKED_PRODUCTS' => false				// Would you like to show the linked products only one time? Possible values: true / false. Standard: true
	);

//Settings END

require('includes/application_top.php');
$languages_id = $_SESSION['languages_id'];

$ent_keyword = trim(htmlentities($_GET['term']));
if (!strlen($ent_keyword)) {
	unset($searchConfig);
	return json_encode(array());
}
$ent_keyword = ($ent_keyword != $_GET['term']) ? addslashes($ent_keyword) : false;

$keyword = mysql_real_escape_string(urldecode($_GET['term']));

$keyword = str_replace(array('%', '_'), array('\%', '\_'), $keyword);
$ent_keyword = str_replace(array('%', '_'), array('\%', '\_'), $ent_keyword);

//initialize array for results
$return_arr = array();
$num_rows_cat=0;

$str_group_by = "";
if ($searchConfig['FILTER_LINKED_PRODUCTS'] == 1) {
	$str_group_by = " GROUP BY p.products_id ";
}

//BOF - Search in category_name ################################################
if ($searchConfig['SEARCH_IN_CATEGORY_NAMES'] == 1 || $searchConfig['SEARCH_IN_CATEGORY_DESCRIPTION'] == 1){
	$where_str_cat = " AND ( ";
	$firstCatItem = true;

	if ($searchConfig['SEARCH_IN_CATEGORY_NAMES'] == 1 ) {
		!$firstCatItem ? $where_str_cat.=" OR " : false;
		if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
			$temp_ary = explode(' ', $keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " cd.categories_name LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str_cat.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}

			if ($ent_keyword)
			{
				$temp_ary = explode(' ', $ent_keyword);
				$temp_ary2 = array();
				for ( $i = 0; $i < count($temp_ary); ++$i )
				{
					$temp_ary2[] = " cd.categories_name LIKE '%" . $temp_ary[$i] . "%' ";
				}
				if ( count($temp_ary2) > 0 )
				{
					$where_str_cat.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
				}
			}

		} else {
			$where_str_cat .= "cd.categories_name LIKE '%" . $keyword . "%' ";
			$where_str_cat .= ($ent_keyword) ? "OR cd.categories_name LIKE ('%".$ent_keyword."%') " : '';
		}
		$firstCatItem=false;
	}

	if ($searchConfig['SEARCH_IN_CATEGORY_DESCRIPTION'] == 1 ) {
		!$firstCatItem ? $where_str_cat.=" OR " : false;
		if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
			$temp_ary = explode(' ', $keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " cd.categories_description LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str_cat.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}

			if ($ent_keyword)
			{
				$temp_ary = explode(' ', $ent_keyword);
				$temp_ary2 = array();
				for ( $i = 0; $i < count($temp_ary); ++$i )
				{
					$temp_ary2[] = " cd.categories_description LIKE '%" . $temp_ary[$i] . "%' ";
				}
				if ( count($temp_ary2) > 0 )
				{
					$where_str_cat.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
				}
			}

		} else {
			$where_str_cat .= "cd.categories_name LIKE '%" . $keyword . "%' ";
			$where_str_cat .= ($ent_keyword) ? "OR cd.categories_name LIKE ('%".$ent_keyword."%') " : '';
		}
		$firstCatItem=false;
	}

	$where_str_cat .= " ) ";

  //Build SQL-Query for category-search
  $group_check = ''; //DokuMan - 2010-02-28 - set undefined variable group_check
  if (GROUP_CHECK == 'true') {
    $group_check = " AND p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
    $group_check_c = " AND c.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
    } else {
      unset ($group_check);
      unset ($group_check_c);
  }
   
  //BOF - web28 - 2011-04-22 - NEW SQL QUERY and change $where_str to %s
  $sql_str_cat = "SELECT cd.categories_id,
                         cd.categories_name
                    FROM ".TABLE_CATEGORIES." c
              INNER JOIN ".TABLE_CATEGORIES_DESCRIPTION." cd
                      ON c.categories_id = cd.categories_id
                   WHERE cd.language_id = '".(int)$languages_id."'
                   ".$group_check."
                     AND c.categories_status = '1'
                         %s
                ORDER BY cd.categories_name
                   LIMIT 0,".$searchConfig['CATEGORY_RESULT_COUNT'];
  //EOF - web28 - 2011-04-22 - NEW SQL QUERY and change $where_str to %s

	$fetch_cat = xtc_db_query(sprintf($sql_str_cat,$where_str_cat)); //web28 - 2011-04-22 - using sprintf
	$num_rows_cat = mysql_num_rows($fetch_cat);

  while ($row_cat = xtc_db_fetch_array($fetch_cat)) {  
    getCatReturnArray($row_cat,TEXT_AUTOCOMPLETE_CATEGORIES); //web28 - 2011-04-22 - New function handling
  }

	//Soundex Category Names
	if ($num_rows_cat == 0 && $searchConfig['SOUNDEX_SEARCH_CATEGORY_NAMES'] == 1) {

		$where_str_cat = " AND ( soundex_match('".$keyword."', cd.categories_name, ' ') = 1 ) ";    

		$fetch_cat = xtc_db_query(sprintf($sql_str_cat,$where_str_cat)); //web28 - 2011-04-22 - using sprintf
		$num_rows_cat = mysql_num_rows($fetch_cat);

    while ($row_cat = xtc_db_fetch_array($fetch_cat)) {
      getCatReturnArray($row_cat,TEXT_AUTOCOMPLETE_SOUNDEX); //web28 - 2011-04-22 - New function handling
    }
	}

	//Koelner Phonetik category names
	if ($num_rows_cat == 0 && $searchConfig['KOELNER_PHONETIK_CATEGORY_NAMES'] == 1) {

		$where_str_cat = " AND ( koelner_match('".$keyword."', cd.categories_name, ' ') = 1 ) ";    

		$fetch_cat = xtc_db_query(sprintf($sql_str_cat,$where_str_cat)); //web28 - 2011-04-22 - using sprintf
		$num_rows_cat = mysql_num_rows($fetch_cat);

    while ($row_cat = xtc_db_fetch_array($fetch_cat)) {
      getCatReturnArray($row_cat,TEXT_AUTOCOMPLETE_SOUNDEX); //web28 - 2011-04-22 - New function handling
    }
	}
}
//EOF - Search in category_name ################################################


//BOF - Search for products ####################################################
$where_str = " AND ( ";
$firstItem = true;

//Search in products_name
if($searchConfig['SEARCH_IN_PRODUCTS_NAME'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary2[] = " pd.products_name LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}

		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " pd.products_name LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$where_str .= "pd.products_name LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR pd.products_name LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}

//Search in products_keywords
if($searchConfig['SEARCH_IN_PRODUCTS_KEYWORDS'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary2[] = " pd.products_keywords LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}

		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " pd.products_keywords LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$where_str .= "OR pd.products_keywords LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR pd.products_keywords LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}

//Search in products_description
if ($searchConfig['SEARCH_IN_PRODUCTS_DESCRIPTION'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary2[] = " pd.products_description LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}


		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " pd.products_description LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$where_str .= "OR pd.products_description LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR pd.products_description LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}

//Search in products_short_description
if ($searchConfig['SEARCH_IN_PRODUCTS_SHORT_DESCRIPTION'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary2[] = " pd.products_short_description LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}

		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " pd.products_short_description LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$where_str .= "OR pd.products_short_description LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR pd.products_short_description LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}

//Search in products_model
if ($searchConfig['SEARCH_IN_PRODUCTS_MODEL'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary2[] = " p.products_model LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}

		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary2[] = " p.products_model LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$where_str .= "OR p.products_model LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR p.products_model LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}

//Search in products_ean
if ($searchConfig['SEARCH_IN_PRODUCTS_EAN'] == 1) {
	!$firstItem ? $where_str.=" OR " : false;
	if($searchConfig['SEVERAL_KEYWORDS_AND'] == 1) {
		$temp_ary = explode(' ', $keyword);
		$temp_ary2 = array();
		for ( $i = 0; $i < count($temp_ary); ++$i )
		{
			$temp_ary[$i] = str_replace('-', '', $temp_ary[$i]);
			$temp_ary2[] = " REPLACE(p.products_ean, '-', '') LIKE '%" . $temp_ary[$i] . "%' ";
		}
		if ( count($temp_ary2) > 0 )
		{
			$where_str.= ' ( ' . implode(' AND ', $temp_ary2) . ' ) ';
		}

		if ($ent_keyword)
		{
			$temp_ary = explode(' ', $ent_keyword);
			$temp_ary2 = array();
			for ( $i = 0; $i < count($temp_ary); ++$i )
			{
				$temp_ary[$i] = str_replace('-', '', $temp_ary[$i]);
				$temp_ary2[] = " REPLACE(p.products_ean, '-', '') LIKE '%" . $temp_ary[$i] . "%' ";
			}
			if ( count($temp_ary2) > 0 )
			{
				$where_str.= ' OR ( ' . implode(' AND ', $temp_ary2) . ' ) ';
			}
		}

	} else {
		$keyword = str_replace('-', '', $keyword);
		$where_str .= "OR REPLACE(p.products_ean, '-', '') LIKE '%" . $keyword . "%' ";
		$where_str .= ($ent_keyword) ? "OR REPLACE(p.products_ean, '-', '') LIKE ('%".$ent_keyword."%') " : '';
	}
	$firstItem=false;
}


$where_str.=" ) ";

//fsk18 lock
  	if ($_SESSION['customers_status']['customers_fsk18_display'] == '0') {
	   	$fsk_lock = " AND p.products_fsk18 != '1' ";
	  } else {
		  unset ($fsk_lock);
	  }

//group check
  	if (GROUP_CHECK == 'true') {
	   	$group_check = " AND p.group_permission_".$_SESSION['customers_status']['customers_status_id']."=1 ";
	  } else {
		  unset ($group_check);
	  }

//Build SQL-Query for products-search
//BOF web28 - 2011-04-22 - change $where_str to %s
$sql_str = "
  SELECT
    p.products_id,
    p.products_tax_class_id,
    p.products_price,
    p.products_image,
    pd.products_name,
    pd.products_short_description,
    p2c.categories_id,
    c.categories_status,
    cd.categories_name
  FROM
     " . TABLE_PRODUCTS . " p
    LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.products_id = pd.products_id
    LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ON p2c.products_id = pd.products_id
    LEFT JOIN " . TABLE_CATEGORIES . " c ON c.categories_id = p2c.categories_id
    LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON c.categories_id = cd.categories_id
  WHERE
  cd.language_id = '" . (int)$languages_id . "'
  AND
  p.products_status = '1'
  AND
  c.categories_status = '1'
  AND
  pd.language_id = '" . (int)$languages_id . "'"
  .$fsk_lock.$group_check."%s".$str_group_by." 
  ORDER BY cd.categories_name ASC, pd.products_name ASC 
  LIMIT 0,".$searchConfig['PRODUCT_RESULT_COUNT'];
  //EOF web28 - 2011-04-22 - change $where_str to %s

$fetch = xtc_db_query(sprintf($sql_str,$where_str)); //web28 - 2011-04-22 - using sprintf
$num_rows = mysql_num_rows($fetch);
if ($num_rows > 0)
{
  while ($row = xtc_db_fetch_array($fetch)) {
    getProdReturnArray($row,false); //web28 - 2011-04-22 - New function handling -> true: use $text_soundex
  }
}

//nothing found? try soundex
if ($num_rows == 0 && $num_rows_cat == 0 && $searchConfig['SOUNDEX_SEARCH_PRODUCT_NAMES'] == 1){

	$where_str = " AND ( soundex_match('".$keyword."', products_name, ' ') = 1 ) ";	

	$fetch = xtc_db_query(sprintf($sql_str,$where_str)); //web28 - 2011-04-22 - using sprintf
	$num_rows = mysql_num_rows($fetch);
  
  while ($row = xtc_db_fetch_array($fetch)) {
    getProdReturnArray($row,true); //web28 - 2011-04-22 - New function handling -> true: use $text_soundex
  }
}

//nothing found? try koelner phonetik
if ($num_rows == 0 && $num_rows_cat == 0 && $searchConfig['KOELNER_PHONETIK_PRODUCT_NAMES'] == 1){

	$where_str = " AND ( koelner_match('".$keyword."', products_name, ' ') = 1 ) ";	

	$fetch = xtc_db_query(sprintf($sql_str,$where_str)); //web28 - 2011-04-22 - using sprintf
	$num_rows = mysql_num_rows($fetch);  
  
  while ($row = xtc_db_fetch_array($fetch)) {
    getProdReturnArray($row,true); //web28 - 2011-04-22 - New function handling -> true: use $text_soundex
  }  
}
//EOF - Search for products ####################################################

unset($searchConfig);
print json_encode($return_arr);


//BOF - functions ####################################################

/**
  * change the global $return_arr -> add categorie entries
  * @param Array $row_cat
  * @param String $text_autocomplete
  * @global $searchConfig
  * @global $text_autocomplete
  * @change global $return_arr 
*/
function getCatReturnArray($row_cat,$text_autocomplete) {
  global $searchConfig, $return_arr;
  
  $str_cat = utf8_encode($row_cat['categories_name']);
  //if string is too long then substr
  if (strlen($str_cat)>$searchConfig['SUBSTRING_COUNT']){
    $str_cat = substr($str_cat, 0, $searchConfig['SUBSTRING_COUNT']) . $searchConfig['SUBSTRING_FINALIZER'];
  }

  $category='';
  if ($searchConfig['GROUP_RESULTS'] == 1){
    $category=utf8_encode($text_autocomplete);
  }

  //append to results array
  //BOF - web28 - 2011-04-18 - NEW categories_link_link
  $return_arr[] = array(
                        'label'     => $str_cat,
                        'link'      => categoriesLink($row_cat['categories_id']),
                        'value'     => $row_cat['categories_id'],
                        'category'  => $category,
                        'desc'      => ''
                       );
  //EOF - web28 - 2011-04-18 - NEW categories_link_link
}

/**
  * change the global $return_arr -< add product entries
  * @param Array $row
  * @param Boolean $soundex
  * @global $searchConfig
  * @global $return_arr
  * @global $xtPrice
  * @change global $return_arr 
*/
function getProdReturnArray($row,$text_soundex) {
  global $searchConfig, $return_arr, $xtPrice;
  
  if($searchConfig['SHOW_CATEGORY_WITH_PRODUCT'] == 1) {
    $str = utf8_encode($row['categories_name'] .' => '. $row['products_name']);
  } else {
    $str = utf8_encode($row['products_name']);
  }

  //if string is too long then substr
  if (strlen($str)>$searchConfig['SUBSTRING_COUNT']){
    $str = substr($str, 0, $searchConfig['SUBSTRING_COUNT']) . $searchConfig['SUBSTRING_FINALIZER'];
  }

  $category='';
  if (!$text_soundex) {
    if ($searchConfig['GROUP_RESULTS'] == 1 && !is_null($row['categories_name']) && strlen($row['categories_name']) > 0){
      $category=utf8_encode($row['categories_name']);
    }
  } else {
    if ($searchConfig['GROUP_RESULTS'] == 1){
      $category=utf8_encode(TEXT_AUTOCOMPLETE_SOUNDEX);
    }
  }

  $desc = '';
  if ($searchConfig['SHOW_PRODUCT_DESC'] == 1) {
    $desc = truncateStr($row['products_short_description']);
  }

  $products_price = '';
  if ($searchConfig['SHOW_PRODUCT_PRICE'] == 1) {
    $products_price = $xtPrice->xtcGetPrice($row['products_id'], $format = true, 1, $row['products_tax_class_id'], $row['products_price'], 1); //functionxtcGetPrice in class xtcprice
  }

  //append to results array
  //BOF - web28 - 2011-04-18 - NEW products_link
  $return_arr[] = array(
                        'label'     => $str,
                        'link'      => productLink($row['products_id'], $row['products_name']),
                        'value'     => $row['products_id'],
                        'category'  => $category,
                        'image'     => productImage($row['products_image']),
                        'desc'      => $desc,
                        'price'     => $products_price['formated']
                       );
  //BOF - web28 - 2011-04-18 - NEW products_link  
}

/**
  * returns a truncate string by defined lenght $searchConfig['PRODUCT_DESC_MAX_LENGTH']
  * @param String $str
  * @global $searchConfig
  * @return String  
*/
function truncateStr($str) {
  global $searchConfig;
  $str = utf8_encode(strip_tags($str));
  if (strlen($str)>$searchConfig['PRODUCT_DESC_MAX_LENGTH']) {
    $str = substr($str, 0, $searchConfig['PRODUCT_DESC_MAX_LENGTH']) . $searchConfig['SUBSTRING_FINALIZER'];
  }
  return $str;
}

/**
  * returns the categories link created by xtc_href_link
  * @param Integer $categories_id
  * @global $searchConfig
  * @return String categories_link 
*/
function categoriesLink($categories_id){
  global $searchConfig;
  $categories_link = xtc_href_link(FILENAME_DEFAULT, 'cPath='.xtc_get_category_path($categories_id), 'NONSSL', false, $searchConfig['USE_SUMA_LINKS']);
  $categories_link = str_replace('&amp;', '&',$categories_link);
  return $categories_link;
}

/**
  * returns the product image using the function productImage in class product
  * @param String $image
  * @global $searchConfig
  * @global $product
  * @return String productImage 
*/
function productImage($image){
  global $searchConfig, $product;
  if (!$searchConfig['SHOW_PRODUCT_IMAGE']) {
    return '';
  }
  return $product->productImage($image, 'thumbnail'); //function productImage in class product 
}

/**
  * returns the product link created by xtc_href_link
  * @param Integer $products_id
  * @param String $products_name
  * @global $searchConfig
  * @return String productLink 
*/
function productLink($products_id, $products_name){
  global $searchConfig;
  $products_link = xtc_href_link(FILENAME_PRODUCT_INFO, xtc_product_link($products_id, $products_name), 'NONSSL', false, $searchConfig['USE_SUMA_LINKS']);
  $products_link = str_replace('&amp;', '&',$products_link);
  return $products_link;
}

//BOF - functions ####################################################
?>